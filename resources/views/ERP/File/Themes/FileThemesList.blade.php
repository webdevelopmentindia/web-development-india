@if(!(isset($OnlyData) && $OnlyData=="ERP Themes"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPFileThemes">
	<div id="FileThemesList" class="FileThemesSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>Themes</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
	<?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 5; //5 for ERP Themes
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?> 
	<div class="row row-col">
		<div class="col-xs-12">
			@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			<span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileThemesForm" SectionName="FileThemesSection"> Add New </a>
			</span>
			@endif
			<span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="FileThemesSearch" SectionName="FileThemesSection" FilterChangeDivEmpty="ERPFileThemesFilterChange" FormClassName="ERPFileThemesRemoveSearchSubmit"> Search </a></span>
		</div>
	</div>

		<?php 
$CheckParameter = 0;
?>
<div id="FileThemesListSearch" >
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPFileThemesRemoveSearchSubmit add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileThemesList" SectionName="FileThemesSection" TabName="ERPFileThemes">	
@if(isset($ParaMeter))
    		@if(
    (((isset($ParaMeter["Theme"]) && $ParaMeter["Theme"]!="") && (isset($ParaMeter["ThemeCondition"]) && $ParaMeter["ThemeCondition"]!="") || (isset($ParaMeter["ThemeCondition"]) && $ParaMeter["ThemeCondition"]==4)))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if(((isset($ParaMeter["Theme"]) && $ParaMeter["Theme"]!="") && (isset($ParaMeter["ThemeCondition"]) && $ParaMeter["ThemeCondition"]!="") || (isset($ParaMeter["ThemeCondition"]) && $ParaMeter["ThemeCondition"]==4)))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchTheme" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileThemes" FilterName="FileThemesTheme" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['ThemeCondition'] }}" ShowDivClass="ERPFileThemesFilterChange"
		        FieldNameValue='{{$ParaMeter["Theme"]}}' FormName="FileThemesList" AddFilterPopup="FileThemesAddFilterShow">
			        Theme: {{ $ParaMeter["ThemeConditionValue"] }} 
			            {{ $ParaMeter["Theme"] }} 
	            </div>
	            <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileThemesList" RemoveSearch="SearchTheme" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileThemesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i> </a>
	        </span>

	        <input type="hidden" id="theme" name="theme" value='{{ $ParaMeter["Theme"] }}'>
	        <input type="hidden" id="theme_condition" name="theme_condition" value='{{ $ParaMeter["ThemeCondition"] }}'>
	        <div style="display: none" class="ERPFileThemesFilterChange add-drop-down popclose">
	        </div>
	        
        </div>
   @else
        <?php 
			$AddFilterOptions.='<li value="FileThemesTheme" id="ERPFileThemesSearchList" name="ERPFileThemesSearchList" class="ERPAddFilterShow" ShowDivId="FileThemesAddFilterShow" FilterChangeModal="ERPFileThemesFilterChange" FormName="FileThemesList">Theme</li>';
		?>
   @endif
   @if((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchEnabledFlag" class="SearchSection addfield-col">
		    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileThemes" FilterName="FileThemesEnabledFlag" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EnabledFlagCondition'] }}" ShowDivClass="ERPFileThemesFilterChange"
		        FieldNameValue='{{$ParaMeter["EnabledFlag"]}}' FormName="FileThemesList" AddFilterPopup="FileThemesAddFilterShow">
		            Active : {{ $ParaMeter["EnabledFlagConditionValue"] }} 
		                @if($ParaMeter["EnabledFlag"]==1) {{ 'Yes' }} @else {{ 'No' }} @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileThemesList" RemoveSearch="SearchEnabledFlag" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileThemesFilterChange"><i class="fa fa-times" aria-hidden="true"></i> </a>
		    </span>
	        <input type="hidden" id="enabled_flag" name="enabled_flag" value='{{ $ParaMeter["EnabledFlag"] }}'>
	        <input type="hidden" id="enabled_flag_condition" name="enabled_flag_condition" value='{{ $ParaMeter["EnabledFlagCondition"] }}'>
	        <div style="display: none" class="ERPFileThemesFilterChange add-drop-down popclose">
	        </div>
	        
	    </div>
	@else
	    <?php 
	        $AddFilterOptions.='<li value="FileThemesEnabledFlag" id="ERPFileThemesSearchList" name="ERPFileCountriesSearchList" class="ERPAddFilterShow" ShowDivId="FileThemesAddFilterShow" FilterChangeModal="ERPFileThemesFilterChange" FormName="FileThemesList">Active</li>';
	    ?>
	@endif
   @if($CheckParameter)
	   <div class="addfield-col">
		   <!-- <select id="ERPFileThemesSearchList" name="ERPFileThemesSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="FileThemesAddFilterShow">
		   	    <option value=''>+ Add Filter</option>
		   	    <?php echo $AddFilterOptions; ?>
		   </select> -->
	   @if($AddFilterOptions!="")
				    <div class="dropdown">
					  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
					  <span class="caret"></span></button>
					  <ul class="dropdown-menu">
					    <?php echo $AddFilterOptions; ?>
					  </ul>
					</div>
				@endif
			   <div id="FileThemesAddFilterShow" class="add-drop-down popclose" style="display: none">
			   	
		   		</div>
		</div>
		<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
			   @if(!$GetAllErpSaveFilters->isEmpty())
			       <select id="ERPGetOldFilterData" name="ERPGetOldFilterData" class="form-control ERPSearchBySaveFilter" PageName="ERPFileThemes" SectionName="FileThemesList" ManuName="ERPFileThemes" FormName="">
			       	  <option value=""></option>
			          <option value="">Public</option>
				      @foreach($GetAllErpSaveFilters as $Filter)
				   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
				   	  @endforeach
			      </select>
			   @endif
		</div>
   <div id="FileThemesAddFilterShow" style="display: none">
   </div>
   	<div class="addfield-col">
	   		<button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="FileThemesList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPFileThemesFilterChange" HideAddFilterPopup="FileThemesAddFilterShow">Save Filter</button>
	   	</div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileThemesList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="FileThemesList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
	   			<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="FileThemesList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
	   		</div>
   @endif
   @if(
    (((isset($ParaMeter["Theme"]) && $ParaMeter["Theme"]!="") && (isset($ParaMeter["ThemeCondition"]) && $ParaMeter["ThemeCondition"]!="") || (isset($ParaMeter["ThemeCondition"]) && $ParaMeter["ThemeCondition"]==4)))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
    )
    </div>
</div>
    @endif
@endif
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPFileThemestable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>Theme ID</th>
			        <th>Theme</th>
			        <th>Active</th>
			        <th>Mod On</th>
			        <th>Created On</th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$ERPThemes->isEmpty())
				    @foreach($ERPThemes as $Theme)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Theme->erp_table_id }}</td>
			                <td>{{ $Theme->theme_name }}</td>
			                <td>
			                	@if($Theme->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Theme->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td>{{ $Theme->mod_on }}</td>
			                <td>{{ $Theme->created_at }}</td>
			                <td>{{ $Theme->created_by }}</td>
			                <td class="not-export-col">
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="FileThemesForm" SectionName="FileThemesSection" EditId="{{ $Theme->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn text-danger ERPRecordDelete" FormName="FileThemesAdd" DeleteId="{{ $Theme->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>Theme ID</th>
			        <th>Theme</th>
			        <th>Active</th>
			        <th>Mod On</th>
			        <th>Created On</th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="ERP Themes"))
	</div>
	<div id="FileThemesForm" style="display: none" class="FileThemesSection card">
	</div>
	<div id="FileThemesSearch" style="display: none" class="FileThemesSection card">
	</div>
</div>
@endif