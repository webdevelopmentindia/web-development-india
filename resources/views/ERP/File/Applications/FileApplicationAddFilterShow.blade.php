<div class="close-btn HideAddFilterModal" ModalId="FileApplicationAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileApplicationAppName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('App name') }}</label>
	<div class="col-sm-3 p-0">
	    <select id="app_name_condition" name="app_name_condition" class="form-control ERPSearchEmptyContent" FieldId="app_name">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="app_name" type="text" class="form-control" name="app_name"  autofocus placeholder="App Name" value=""> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="app_name_condition" FieldName="app_name" FormName="FileApplicationList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileApplicationAppURL")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('App URL') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="app_url_condition" name="app_url_condition" class="form-control ERPSearchEmptyContent" FieldId="app_url">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="app_url" type="text" class="form-control" name="app_url"  autofocus placeholder="App URL" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileApplicationList" FieldNameCondition="app_url_condition" FieldName="app_url"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileApplicationDescription")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Description') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="description_condition" name="description_condition" class="form-control ERPSearchEmptyContent" FieldId="description">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="description" type="text" class="form-control" name="description"  autofocus placeholder="Description" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="description_condition" FieldName="description" FormName="FileApplicationList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileApplicationAppDeployed")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('App deployed?') }}</label>
    <div class="col-sm-6 p-0">
	    <select id="app_deployed_condition" name="app_deployed_condition" class="form-control">
	    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-3 check-f text-right">
        <span class="mr-1"><input id="app_deployed" type="radio" class="" name="app_deployed" value="1" checked>Yes</span>
        <span class="mr-1"><input id="app_deployed" type="radio" class="" name="app_deployed" value="0">No</span>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileApplicationList" FieldNameCondition="app_deployed_condition"> Apply </a></div>
</div>
@endif