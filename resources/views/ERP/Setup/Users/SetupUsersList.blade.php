@if(!(isset($OnlyData) && $OnlyData=="ERP Users"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPSetupUsers">
	<div id="SetupUsersList" class="SetupUsersSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
	<div class="page-title">
		<h3>Users</h3>
	</div>
	<div class="header-elements">
		<span><?php
		        echo " ".date("m/d/Y");
	        ?></span>
	</div>
</div>
<?php 
    $ParaMeter["GroupId"] = Auth::user()->GroupId;
    $ParaMeter["AppId"] = 7; //7 for ERP Users
    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
?> 
<div class="row row-col">
	<div class="col-xs-12">
		@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
		<span class="link-src">
			<a href="javascript:" class="btn save-btn ERPFormShow" FormName="SetupUsersForm" SectionName="SetupUsersSection"> Add New </a>
		</span>
		@endif
		<span class="link-src">
			<a href="javascript:" class="btn save-btn ERPSearchShow" FormName="SetupUsersSearch" SectionName="SetupUsersSection" FilterChangeDivEmpty="ERPSetupUsersFilterChange" FormClassName="ERPSetupUsersRemoveSearchSubmit"> Search </a>
		</span>
	</div>
</div>
<?php 
$CheckParameter = 0;
?>
<div id="SetupUsersListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SetupUsersList" SectionName="SetupUsersSection" TabName="ERPSetupUsers">	
@if(isset($ParaMeter))
    @if(
    ((isset($ParaMeter["UserId"]) && $ParaMeter["UserId"]!="") && (isset($ParaMeter["UserIdCondition"]) && $ParaMeter["UserIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="")) || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4))
    ||
    (((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
    ||
    (((isset($ParaMeter["EMail"]) && $ParaMeter["EMail"]!="") && (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!="")) || (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]==4))
    ||
    (((isset($ParaMeter["AdminPriv"]) && $ParaMeter["AdminPriv"]!="") && (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!="")) || (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]==4))
    ||
    ((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]!="") && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    <?php 
    // echo "<pre>";
    // print_r($ParaMeter);
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if((isset($ParaMeter["UserId"]) && $ParaMeter["UserId"]!="") && (isset($ParaMeter["UserIdCondition"]) && $ParaMeter["UserIdCondition"]!=""))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchUserId" class="SearchSection addfield-col">

	        <input type="hidden" id="user_id" name="user_id" value='{{ $ParaMeter["UserId"] }}'>
	        <input type="hidden" id="user_id_condition" name="user_id_condition" value='{{ $ParaMeter["UserIdCondition"] }}'>
	        <div style="display: none" class="ERPSetupUsersFilterChange add-drop-down">
	        </div>

	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        	<div PageName="ERPSetupUsers" FilterName="SetupUsersUserId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['UserIdCondition'] }}" ShowDivClass="ERPSetupUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["UserIdCondition"]!=4){{$ParaMeter["UserId"]}} @endif' FormName="SetupUsersList" AddFilterPopup="SetupUsersAddFilterShow">
		        User ID : {{ $ParaMeter["UserIdConditionValue"] }} 
		        {{ $ParaMeter["UserId"] }} 
            	</div>
	        	<a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SetupUsersList" RemoveSearch="SearchUserId" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSetupUsersFilterChange">
	        		<i class="fa fa-times" aria-hidden="true"></i> 
	        	</a>
	        </span>
        </div>   
   @else
        <?php 
			$AddFilterOptions.='<li value="SetupUsersUserId" id="ERPSetupUsersSearchList" name="ERPSetupUsersSearchList" class="ERPAddFilterShow" ShowDivId="SetupUsersAddFilterShow" FilterChangeModal="ERPSetupUsersFilterChange" FormName="SetupUsersList">User ID</li>';
		?>
   @endif <!-- for UserId -->
   @if(((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="")) || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchLogin" class="SearchSection addfield-col"> 
        	
	        <input type="hidden" name="login" value='{{ $ParaMeter["Login"] }}'>
	        <input type="hidden" name="login_condition" value='{{ $ParaMeter["LoginCondition"] }}'>
	        <div style="display: none" class="ERPSetupUsersFilterChange add-drop-down">
	        </div>
	         <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPSetupUsers" FilterName="SetupUsersLogin" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['LoginCondition'] }}" ShowDivClass="ERPSetupUsersFilterChange"
		        FieldNameValue='@if($ParaMeter["LoginCondition"]!=4){{$ParaMeter["Login"]}} @endif' FormName="SetupUsersList" AddFilterPopup="SetupUsersAddFilterShow">
			        Login: {{ $ParaMeter["LoginConditionValue"] }} 
			        @if(isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!=4)
			            {{ $ParaMeter["Login"] }} 
			        @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SetupUsersList" RemoveSearch="SearchLogin" RemoveSearchType="ColumnSearchRemove">
		        	<i class="fa fa-times" aria-hidden="true"></i> 
		        </a>
		    </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="SetupUsersLogin" id="ERPSetupUsersSearchList" name="ERPSetupUsersSearchList" class="ERPAddFilterShow" ShowDivId="SetupUsersAddFilterShow" FilterChangeModal="ERPSetupUsersFilterChange" FormName="SetupUsersList">Login</li>';
		?>
   @endif <!-- for Login -->
   @if(((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchFullName" class="SearchSection addfield-col">
            
            <input type="hidden" name="full_name" value='{{ $ParaMeter["FullName"] }}'>
            <input type="hidden" name="full_name_condition" value='{{ $ParaMeter["FullNameCondition"] }}'>
            <div style="display: none" class="ERPSetupUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPSetupUsers" FilterName="SetupUsersFullName" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['FullNameCondition'] }}" ShowDivClass="ERPSetupUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["FullNameCondition"]!=4){{$ParaMeter["FullName"]}} @endif' FormName="SetupUsersList" AddFilterPopup="SetupUsersAddFilterShow">
	        	FullName: {{ $ParaMeter["FullNameConditionValue"] }} 
	        	@if(isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!=4)
		            {{ $ParaMeter["FullName"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SetupUsersList" RemoveSearch="SearchFullName" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="SetupUsersFullName" id="ERPSetupUsersSearchList" name="ERPSetupUsersSearchList" class="ERPAddFilterShow" ShowDivId="SetupUsersAddFilterShow" FilterChangeModal="ERPSetupUsersFilterChange" FormName="SetupUsersList">FullName</li>';
		?>
   @endif <!-- for FullName -->
   @if(((isset($ParaMeter["EMail"]) && $ParaMeter["EMail"]!="") && (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!="")) || (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchEMail" class="SearchSection addfield-col">
            
            <input type="hidden" name="e_mail" value='{{ $ParaMeter["EMail"] }}'>
            <input type="hidden" name="e_mail_condition" value='{{ $ParaMeter["EMailCondition"] }}'>
            <div style="display: none" class="ERPSetupUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPSetupUsers" FilterName="SetupUsersEMail" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EMailCondition'] }}" ShowDivClass="ERPSetupUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["EMailCondition"]!=4){{$ParaMeter["EMail"]}} @endif' FormName="SetupUsersList" AddFilterPopup="SetupUsersAddFilterShow">
	        	E-Mail: {{ $ParaMeter["EMailConditionValue"] }} 
	        	@if(isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!=4)
		            {{ $ParaMeter["EMail"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SetupUsersList" RemoveSearch="SearchEMail" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="SetupUsersEMail" id="ERPSetupUsersSearchList" name="ERPSetupUsersSearchList" class="ERPAddFilterShow" ShowDivId="SetupUsersAddFilterShow" FilterChangeModal="ERPSetupUsersFilterChange" FormName="SetupUsersList">E-Mail</li>';
		?>
   @endif <!-- for E-Mail -->
   @if(((isset($ParaMeter["AdminPriv"]) && $ParaMeter["AdminPriv"]!="") && (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!="")) || (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchAdminPriv" class="SearchSection addfield-col">
            
            <input type="hidden" name="admin_priv" value='{{ $ParaMeter["AdminPriv"] }}'>
            <input type="hidden" name="admin_priv_condition" value='{{ $ParaMeter["AdminPrivCondition"] }}'>
            <div style="display: none" class="ERPSetupUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPSetupUsers" FilterName="SetupUsersAdminPriv" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AdminPrivCondition'] }}" ShowDivClass="ERPSetupUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["AdminPrivCondition"]!=4){{$ParaMeter["AdminPriv"]}} @endif' FormName="SetupUsersList" AddFilterPopup="SetupUsersAddFilterShow">
	        	Admin Priv.: {{ $ParaMeter["AdminPrivConditionValue"] }} 
	        	@if(isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!=4)
		            {{ $ParaMeter["AdminPriv"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SetupUsersList" RemoveSearch="SearchAdminPriv" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="SetupUsersAdminPriv" id="ERPSetupUsersSearchList" name="ERPSetupUsersSearchList" class="ERPAddFilterShow" ShowDivId="SetupUsersAddFilterShow" FilterChangeModal="ERPSetupUsersFilterChange" FormName="SetupUsersList">Admin Priv</li>';
		?>
   @endif <!-- for AdminPriv -->
   @if((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]!="") && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCustomerId" class="SearchSection addfield-col">
            
            <input type="hidden" name="customer_id" value='{{ $ParaMeter["CustomerId"] }}'>
            <input type="hidden" name="customer_id_condition" value='{{ $ParaMeter["CustomerIdCondition"] }}'>
            <div style="display: none" class="ERPSetupUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPSetupUsers" FilterName="SetupUsersCustomerId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CustomerIdCondition'] }}" ShowDivClass="ERPSetupUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["CustomerIdCondition"]!=4){{$ParaMeter["CustomerId"]}} @endif' FormName="SetupUsersList" AddFilterPopup="SetupUsersAddFilterShow">
	        	Customer ID: {{ $ParaMeter["CustomerIdConditionValue"] }} 
	        	{{ $ParaMeter["CustomerIdValue"] }} 
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SetupUsersList" RemoveSearch="SearchCustomerId" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>
        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="SetupUsersCustomerId" id="ERPSetupUsersSearchList" name="ERPSetupUsersSearchList" class="ERPAddFilterShow" ShowDivId="SetupUsersAddFilterShow" FilterChangeModal="ERPSetupUsersFilterChange" FormName="SetupUsersList">Customer ID</li>';
		?>
   @endif <!-- for CustomerId -->
   @if($CheckParameter)
	   	<div class="addfield-col">
		    @if($AddFilterOptions!="")
			    <div class="dropdown">
				  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				    <?php echo $AddFilterOptions; ?>
				  </ul>
				</div>
			@endif
		   <div id="SetupUsersAddFilterShow" class="add-drop-down popclose" style="display: none">
		   	
	   		</div>
		</div>
		<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
	        <select id="erp_filter_list" name="erp_filter_list" class="form-control ERPSearchBySaveFilter" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" SectionName="SetupUsersList" ManuName="ERPSetupUsers" FormName="">
	   @if(!$GetAllErpSaveFilters->isEmpty())
	       	  <option value=""></option>
	          <option value="">Public</option>
		      @foreach($GetAllErpSaveFilters as $Filter)
		   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
		   	  @endforeach
	   @endif 
	        </select>
	   </div>
	   <!-- GetAllErpSaveFilters -->
	   	<div class="addfield-col"><button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="SetupUsersList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPSetupUsersFilterChange" HideAddFilterPopup="SetupUsersAddFilterShow">Save Filter</button></div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="SetupUsersList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="SetupUsersList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
	   		<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="SetupUsersList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
	   	</div>
    @endif <!-- CheckParameter -->
    
@if(
    ((isset($ParaMeter["UserId"]) && $ParaMeter["UserId"]!="") && (isset($ParaMeter["UserIdCondition"]) && $ParaMeter["UserIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="")) || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4))
    ||
    (((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
    ||
    (((isset($ParaMeter["EMail"]) && $ParaMeter["EMail"]!="") && (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!="")) || (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]==4))
    ||
    (((isset($ParaMeter["AdminPriv"]) && $ParaMeter["AdminPriv"]!="") && (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!="")) || (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]==4))
    ||
    ((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]!="") && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
    )
    </div>
</div>
    @endif
@endif <!-- ParaMeter -->
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPSetupUserstable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>User ID</th>
			        <th>Customer ID</th>
			        <th>Login</th>
			        <th>Full Name</th>
			        <th>Last Login</th>
			        <th>E-mail</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$GetAllUsers->isEmpty())
				    @foreach($GetAllUsers as $Language)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Language->erp_table_id }}</td>
			                <td>{{ $Language->full_name }}</td>
			                <td>{{ $Language->login }}</td>
			                <td>{{ $Language->name }}</td>
			                <td>{{ $Language->last_login }}</td>
			                <td>{{ $Language->email }}</td>
			                <td>
			                	@if($Language->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Language->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td class="not-export-col">
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn ERPFormShow text-primary" FormName="SetupUsersForm" SectionName="SetupUsersSection" EditId="{{ $Language->erp_table_id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn ERPRecordDelete text-danger" FormName="SetupUsersAdd" DeleteId="{{ $Language->erp_table_id }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>User ID</th>
			        <th>Customer ID</th>
			        <th>Login</th>
			        <th>Full Name</th>
			        <th>Last Login</th>
			        <th>E-mail</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="ERP Users"))
	</div>
	<div id="SetupUsersForm" style="display: none" class="SetupUsersSection card">
	</div>
	<div id="SetupUsersSearch" style="display: none" class="SetupUsersSection card">
	</div>
</div>
@endif