<div class="row">
        <div class="card-header header-elements-inline">
            <div class="page-title">
                <h3>
                    <?php 
                    echo strtoupper("Summary"); 
                    ?>
                </h3>
            </div>
            <div class="header-elements">
                <span><?php 
                    echo " ".date("m/d/Y");
                ?></span>
            <span class="pull-right"></span>
            </div>
        </div> <!-- emloy-hd p-0 -->
        <div class="row row-col">
        <div class="col-xs-12">
            <span class="link-src">
    <button type="button" class="btn save-btn" ShowSection="ReportsReportOfCustomerUsersList" SectionName="ReportsReportOfCustomerUsersSection" id="ERPBackCancelButton">
        Back
    </button>
</span>
</div>
     </div>
<table id="ERPReportsReportOfCustomerUsersSummarytable" class="display table table-bordered table-striped" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>Customer ID</th>
                    <th>Active(Count)</th>
                </tr>
            </thead>
            <tbody>
@if(!$GetAllUsers->isEmpty())
    <?php 
    $i = 1;
    $CustomerUserCount = 0;
    ?>
    @foreach($GetAllUsers as $User)
        <?php 
            $CustomerUserCount+=$User->UsersCount;
        ?>
        <tr>
        <td>{{ $i }}</td>
        <td><a href="javascript:" class="ERPManuData" ManuName="ERPReportsReportOfCustomerUsers" SectionName="ReportsReportOfCustomerUsersSection" FormName="ReportsReportOfCustomerUsersList" ReportsSummaryCustomerId="{{ $User->erp_table_id }}">{{ $User->full_name }}</a></td>
        <td>{{ $User->UsersCount }}</td>
        </tr>
        <?php 
    $i++;
    ?>
    @endforeach
    <tr>
        <th></th>
        <td align="left">Grand Summary</td>
        <td>{{ $CustomerUserCount }}</td>
    </tr>  
@endif
            </tbody>
            <!-- <tfoot>
                <tr>
                    <th></th>
                    <th>Customer ID</th>
                    <th>Active(Count)</th>
                </tr>
            </tfoot> -->
</table>
</div>