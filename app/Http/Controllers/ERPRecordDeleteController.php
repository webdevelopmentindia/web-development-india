<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\ErpCountrie;
use App\ErpSource;
use App\ErpTheme;
use App\ErpLanguage;
use App\User;
use App\Application;
use App\Group;
use App\Services\PayUService\Exception;
class ERPRecordDeleteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPRecordDeleteAjax(Request $request)
    {
        $FormName = "";
        try {
            $Status = "success";
            $ParaMeter = array();
            $DeleteId = 0;
            if($request->DeleteId>0)
            {
              $DeleteId = $request->DeleteId;    
            }
            $ParaMeter["DeleteId"] = $DeleteId;
            if($request->FormName=="FileApplicationAdd")
            {
              $FormName = "File Applications";
              $DeleteERPApplications = ErpApplication::DeleteERPApplications($ParaMeter);
            }
            else if($request->FormName=="FileCustomerAdd")
            {
              $FormName = "File Customers";
              $DeleteERPApplications = ErpCustomer::DeleteErpCustomers($ParaMeter);
            }
            else if($request->FormName=="FileCountriesAdd")
            {
              $FormName = "File Countries";
              $DeleteERPApplications = ErpCountrie::DeleteErpCountries($ParaMeter);
            }
            else if($request->FormName=="FileSourcesAdd")
            {
              $FormName = "File Sources";
              $DeleteERPSources = ErpSource::DeleteErpSources($ParaMeter);
            }
            else if($request->FormName=="FileThemesAdd")
            {
              $FormName = "File Themes";
              $DeleteERPThemes = ErpTheme::DeleteErpThemes($ParaMeter);
            }
            else if($request->FormName=="FileLanguagesAdd")
            {
              $FormName = "File Languages";
              $DeleteERPLanguages = ErpLanguage::DeleteErpLanguages($ParaMeter);
            }
            else if($request->FormName=="SetupUsersAdd")
            {
              $FormName = "Setup Users";
              $DeleteERPUsers = User::DeleteUsers($ParaMeter);
            }
            else if($request->FormName=="SetupUsersGroupsAdd")
            {
              $FormName = "Setup Users";
              $DeleteERPUsers = User::DeleteUsers($ParaMeter);
            }
            else if($request->FormName=="SecurityApplicationsAdd")
            {
              $FormName = "Security Applications";
              $DeleteERPApplications = Application::DeleteApplications($ParaMeter);
            }
            else if($request->FormName=="SecurityGroupsAdd")
            {
              $FormName = "Security Groups";
              $DeleteERPGroups = Group::DeleteGroups($ParaMeter);
            }
            $Message = "Error in ERP $FormName Deletion!";
        }
        catch (\Exception $e) {
          $Message = "Error in ERP $FormName Deletion " . $e->getMessage();
          $Status = "error";
        }
        return json_encode(["Status"=>$Status,"Message"=>$Message]);
    }
}