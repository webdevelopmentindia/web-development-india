@if(!isset($ParaMeter["ERPTableId"]))
<div class="card-header header-elements-inline">
    <div class="page-title">
        <h3>
        	<?php 
			    echo strtoupper("Search of erp_users");
			?>
        </h3>
    </div>
    <div class="header-elements">
        <span>
        	<?php 
			    echo " ".date("m/d/Y");
			?>
        </span>
    </div>
</div> <!-- emloy-hd p-0 -->
	<div class="card-body">
	<div class="row">
	    <div class="col-md-12 employee-form">
	    	<form method="POST" aria-label="{{ __('ERPSearchSubmit') }}" enctype="multipart/form-data" id="ERPSearchSubmit" name="ERPSearchSubmit" FormParentDivId="ReportsReportOfCustomerUsersSearch">
		    @csrf
			<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="ReportsReportOfCustomerUsersList" SectionName="ReportsReportOfCustomerUsersSection" TabName="ERPReportsReportOfCustomerUsers">
            <div id="ERPChangeFilter">
@endif	
		
			<?php 

			    $EnabledFlagCondition = $ParaMeter["EnabledFlagCondition"];
			    $EnabledFlag = $ParaMeter["EnabledFlag"];
			    $LoginCondition = $ParaMeter["LoginCondition"];
			    $Login = $ParaMeter["Login"];
			    $FullNameCondition = $ParaMeter["FullNameCondition"];
			    $FullName = $ParaMeter["FullName"];
                $EMailCondition = $ParaMeter["EMailCondition"];
			    $EMail = $ParaMeter["EMail"];
                $AdminPrivCondition = $ParaMeter["AdminPrivCondition"];
			    $AdminPriv = $ParaMeter["AdminPriv"];
			    $CustomerIdCondition = $ParaMeter["CustomerIdCondition"];
			    $CustomerId = $ParaMeter["CustomerId"];

			    $ERPTableId = "";
			    if(isset($ParaMeter["ERPTableId"]))
			    {
			       $ERPTableId = $ParaMeter["ERPTableId"];
			    }
			?>
			<div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
	            <div class="col-sm-3">
				    <select id="customer_id_condition" name="customer_id_condition" class="form-control">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
		                            <option value="{{ $RadioOption->id }}" @if($CustomerIdCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <select id="customer_id" name="customer_id" class="form-control">
				    	@if(!$GetAllERPCustomers->isEmpty())
		               	   @foreach($GetAllERPCustomers as $Customer)
		                            <option value="{{ $Customer->id }}" @if($CustomerId==$Customer->id) {{ 'selected' }} @endif>{{ $Customer->full_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
	            </div>
	        </div>
	        <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('Login') }}</label>
	            <div class="col-sm-3">
				    <select id="login_condition" name="login_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LoginCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }} SearchFieldShow" name="login"  autofocus placeholder="Login" value="<?php echo $Login; ?>" style='@if($LoginCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
	            <div class="col-sm-3">
				    <select id="full_name_condition" name="full_name_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($FullNameCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="full_name" type="text" class="form-control{{ $errors->has('full_name') ? ' is-invalid' : '' }} SearchFieldShow" name="full_name"  autofocus placeholder="Full Name" value="<?php echo $FullName; ?>" style='@if($FullNameCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('E-mail') }}</label>
	            <div class="col-sm-3">
				    <select id="e_mail_condition" name="e_mail_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($EMailCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="e_mail" type="text" class="form-control{{ $errors->has('e_mail') ? ' is-invalid' : '' }} SearchFieldShow" name="e_mail"  autofocus placeholder="E-mail" value="<?php echo $EMail; ?>" style='@if($EMailCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('Admin Role') }}</label>
	            <div class="col-sm-3">
				    <select id="admin_priv_condition" name="admin_priv_condition" class="form-control">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
		                            <option value="{{ $RadioOption->id }}" @if($AdminPrivCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6 check-f">
	                <span class="mr-1"><input id="admin_priv" type="radio" class="" name="admin_priv" value="1" @if($AdminPriv==NULL || $AdminPriv==1 ) {{ 'checked' }} @endif>Yes</span>
	                <span class="mr-1"><input id="admin_priv" type="radio" class="" name="admin_priv" value="0" @if($AdminPriv==0 && $AdminPriv!=NULL) {{ 'checked' }} @endif>No</span>
	            </div>
	        </div>
	        <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('Active') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
	                            <option value="{{ $RadioOption->id }}" @if($EnabledFlagCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6 check-f">
	                <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($EnabledFlag==NULL || $EnabledFlag==1 ) {{ 'checked' }} @endif>Yes</span>
	                <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($EnabledFlag==0 && $EnabledFlag!=NULL) {{ 'checked' }} @endif>No</span>
	            </div>
	        </div>
	        <div class="clearfix down-filtr">
			    <div class="erp-src-app form-flex justifi-content-center">
		            <div class="col-flex link-src">
		               <button type="submit" class="btn save-btn">Search</button>
		               <a type="button" onclick="ERPFormReset(this); return false;" class="btn cancel-btn save-btn" >Clear</a>
		            </div>
		            <div class="col-flex flex-2 link-src" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
		               <select id="erp_filter_list" name="erp_filter_list" class="form-control" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" FormName="ReportsReportOfCustomerUsersSearch">
		               	@if(!$GetAllErpSaveFilters->isEmpty())
		               	   <option value=""></option>
		               	   <option value="">Public</option>
		               	   @foreach($GetAllErpSaveFilters as $Filter)
	                            <option value="{{ $Filter->id }}" @if($ERPTableId==$Filter->id) {{ 'selected' }} @endif >{{ $Filter->filter_name }}</option>
		               	   @endforeach
		               	@endif
			           </select>
			       	</div>
			        <div class="col-flex link-src">
		               <a href="javascript:" type="button" class="btn save-btn SaveFilterModalShow" FormName="ReportsReportOfCustomerUsersSearch" ModalId="SearchSaveFilter">Save Filter</a>
		               <button type="button" class="btn btn-danger" ShowSection="ReportsReportOfCustomerUsersList" SectionName="ReportsReportOfCustomerUsersSection" id="ERPBackCancelButton">Back</button>
		           	</div>
		        </div>
	    	</div>
@if(!isset($ParaMeter["ERPTableId"]))
            </div>
	        </form>
	    </div>
	    <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter </h3>
			            </div>
			            <div class="header-elements">
			                <button id="modelhide" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9">
				               	<input id="filter_name" type="text" class="form-control{{ $errors->has('filter_name') ? ' is-invalid' : '' }}" name="filter_name"  autofocus placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3">
				               	<button type="button" class="btn save-btn ERPFilterSave" FormName="ReportsReportOfCustomerUsersSearch" FormSubmitId="ERPSearchSubmit">Save</button></div>
				        </div>
				        <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				                    @if(!$GetAllErpSaveFilters->isEmpty())
					               	   <option value=""></option>
					               	   <option value="">Public</option>
					               	   @foreach($GetAllErpSaveFilters as $Filter)
				                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
					               	   @endforeach
					               	@endif
				               </select>
				            </div>
				            <div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" FormName="ReportsReportOfCustomerUsersSearch">Delete</button></div>
				        </div>
			       </div>
			    </div>
			</div>
	</div>
</div>
@endif