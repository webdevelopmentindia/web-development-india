@if(!(isset($OnlyData) && $OnlyData=="Users/Groups"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPSetupUsersGroups">
	<div id="SetupUsersGroupsList" class="SetupUsersGroupsSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>List</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
    <?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 8; //8 for ERP Users Groups
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?> 
	<div class="row row-col">
		<div class="col-xs-12">
			    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			    <span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="SetupUsersGroupsForm" SectionName="SetupUsersGroupsSection"> Add New </a></span>
				@endif
				<!-- <a href="javascript:" class="btn save-btn ERPSearchShow" FormName="SetupUsersGroupsSearch" SectionName="SetupUsersGroupsSection" FilterChangeDivEmpty="ERPSetupUsersGroupsFilterChange" FormClassName="ERPSetupUsersGroupsRemoveSearchSubmit"> Search </a> -->
		</div>
	</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPSetupUsersGroupstable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>Login</th>
			        <th>Full Name</th>
			        <th>E-mail</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				<?php
                 //echo "<pre>";
                 //print_r($GetAllUsers);
				?>
				@if(!$GetAllUsers->isEmpty())
				    @foreach($GetAllUsers as $User)
				        @if($User->login!="")
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $User->login }}</td>
			                <td>{{ $User->name }}</td>
			                <td>{{ $User->email }}</td>
			                <td>
			                	@if($User->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($User->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                 <td class="not-export-col">
			                 	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="SetupUsersGroupsForm" SectionName="SetupUsersGroupsSection" EditId="{{ $User->erp_table_id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn ERPRecordDelete text-danger" FormName="SetupUsersGroupsAdd" DeleteId="{{ $User->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
			                    @endif
			                </td>
		                </tr>
		                @endif
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>Login</th>
			        <th>Full Name</th>
			        <th>E-mail</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Users/Groups"))
	</div>
	<div id="SetupUsersGroupsForm" style="display: none" class="SetupUsersGroupsSection card">
	</div>
	<div id="SetupUsersGroupsSearch" style="display: none" class="SetupUsersGroupsSection card">
	</div>
</div>
@endif