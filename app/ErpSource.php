<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpSource extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpSource($request)
    {
        if($request->erp_table_id>0)
        {
            $ErpSource = ErpSource::find($request->erp_table_id);
        }
        else
        {
            $ErpSource = new ErpSource();  
        } 

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }

        $ErpSource->source_code  = $request->code;
        $ErpSource->description = $request->description;
        $ErpSource->auto_post = $request->auto_post;
        $ErpSource->enabled_flag = $request->enabled_flag;
        $ErpSource->ip_address = getUserIpAddr();
        $ErpSource->save();
        return $ErpSource->id;
    }
    Static Public function GetAllErpSources($ParaMeter)
    {
        $GetAllErpSources = ErpSource::select("erp_sources.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpSources =$GetAllErpSources->addselect("erp_sources.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpSources =$GetAllErpSources->where('erp_sources.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_sources.*"); 
        }
        else if((isset($ParaMeter["FileSourcesSearch"]) && $ParaMeter["FileSourcesSearch"]!="") && (isset($ParaMeter["FileSourcesSearchArray"]) && count($ParaMeter["FileSourcesSearchArray"])>0))
        {
            $FileSourcesSearch = $ParaMeter["FileSourcesSearchArray"];
            if(count($FileSourcesSearch)>0)
            {
              foreach ($FileSourcesSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];

                if($Search["ColumnName"]=="id" && $ColumnCondition==1) // 1 for Greater Than
                {
                  $GetAllErpSources = $GetAllErpSources->where("$TableName.$ColumnName",">",$ColumnValue);
                }
                else if($Search["ColumnName"]=="id" && $ColumnCondition==2) // 2 for Less Than
                {
                  $GetAllErpSources = $GetAllErpSources->where("$TableName.$ColumnName","<",$ColumnValue);
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpSources = $GetAllErpSources->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllErpSources = $GetAllErpSources->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllErpSources = $GetAllErpSources->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllErpSources = $GetAllErpSources->whereNull("$TableName.$ColumnName");
                }
              }
            }
            $GetAllErpSources = $GetAllErpSources->addSelect("erp_sources.*");
        }
        $GetAllErpSources = $GetAllErpSources->orderBy("erp_sources.id","DESC")
                                            ->get();
        return $GetAllErpSources;
    }
    Static Public function DeleteErpSources($ParaMeter)
    {
        $ErpSource = ErpSource::find($ParaMeter["DeleteId"]);
        $ErpSource->delete();
        return $ErpSource->id;
    }
}