@if($ParaMeter["FilterName"]=="FileThemesTheme")
<div class="form-group SearchSection">
    <label class="control-label col-sm-3">{{ __('Theme') }}</label>
    <div class="col-sm-3">
	    <select id="theme_condition_change" name="theme_condition_change" class="form-control ERPSearchEmptyContent" FieldId="theme_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="theme_change" type="text" class="form-control" name="theme_change"  autofocus placeholder="Theme" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileThemesTheme" FieldNameCondition="theme_condition_change" FieldName="theme_change"> Apply </a>
</div>
@elseif($ParaMeter["FilterName"]=="FileThemesEnabledFlag")
<div class="form-group">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
    <div class="col-sm-3">
      <select id="enabled_flag_condition_change" name="enabled_flag_condition_change" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="enabled_flag_change" type="radio" class="" name="enabled_flag_change" value="1" @if($ParaMeter["FieldNameValue"]==NULL || $ParaMeter["FieldNameValue"]==1 ) {{ 'checked' }} @endif>Yes
        <input id="enabled_flag_change" type="radio" class="" name="enabled_flag_change" value="0" @if($ParaMeter["FieldNameValue"]==0 && $ParaMeter["FieldNameValue"]!=NULL) {{ 'checked' }} @endif>No
    </div>
    <a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileThemesEnabledFlag" FieldNameCondition="enabled_flag_condition_change" FieldName="enabled_flag_change"> Apply </a>
</div>
@endif