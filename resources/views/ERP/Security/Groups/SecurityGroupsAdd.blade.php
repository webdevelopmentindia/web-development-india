<?php
$erp_table_id = $app_name = $description = "";
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpGroupDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpGroupDetails[0]->erp_table_id;
    $description = $ErpGroupDetails[0]->description;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="SecurityGroupsForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="SecurityGroupsList" SectionName="SecurityGroupsSection" TabName="ERPSecurityGroups">
<div class="row">

<div class="card-header header-elements-inline">
    <div class="page-title">
        <h3>
           <?php 
            if($erp_table_id!="")
            {
                echo strtoupper("Editing - sec_groups");
            }
            else
            {
                echo strtoupper("New Record - sec_groups"); 
            } 
            ?>
        </h3>
    </div>
    <div class="header-elements">
        <span><?php 
            echo " ".date("m/d/Y");
        ?></span>
    </div>
</div> <!-- emloy-hd p-0 -->
    <div class="row row-col">
        <div class="col-xs-12 emloy-hd">
             @if($erp_table_id!="")
             <a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityGroupsForm" SectionName="SecurityGroupsSection"> Add New </a>
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <span class="link-src"><button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button></span>
             <span class="link-src"><button type="button" class="btn cancel-btn save-btn" ShowSection="SecurityGroupsList" SectionName="SecurityGroupsSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button></span>
        </div>
    </div>
    <div class="card-body employee-form">
        <div class="row">
            <div class="col-xs-12">
                <span class="req-field">* Required field(s)</span>
            </div>
        </div>
        <div class="col-md-12 employee-form">
            <div class="form-group form-flex">
                <label class="control-label col-sm-3">{{ __('Description') }}</label>
                <div class="col-sm-9">
                    <input id="description" type="text" class="form-control" name="description" value="{{ $description }}" placeholder="Description">                
                </div>
            </div>
        </div>
    </div>
</div>
</form>