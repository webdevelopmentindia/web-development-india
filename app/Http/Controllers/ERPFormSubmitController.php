<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\ErpCountrie;
use App\ErpSource;
use App\ErpTheme;
use App\ErpLanguage;
use App\User;
use App\UserGroup;
use App\Application;
use App\Group;
use App\GroupsApp;
use Auth;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
class ERPFormSubmitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFormSubmitAjax(Request $request)
    {
        //die();
        $AddUpdateMessage = "Added";
        $ERPTableId = "";
        $ERPFormName = "";
        $ERPManuDataRecord = "";
        $GetExportPermission = "";
        if($request->erp_table_id>0)
        {
            $ERPTableId = $request->erp_table_id;
            $AddUpdateMessage = "Updated";
        }
        try {
            if($request->ERPFormName=="FileApplicationList")
            {
                $ValidatorArray = [
                    'app_name' => 'required|unique:erp_applications,app_name,'.$ERPTableId,
                    'description' => 'required|max:100|unique:erp_applications,description,'.$ERPTableId,
                    'app_url' => 'required|unique:erp_applications,app_url,'.$ERPTableId,
                ];
                $ValidatorMessageArray = [
                ];
            }
            else if($request->ERPFormName=="FileCustomerList")
            {
                $ValidatorArray = [
                    'country_id' => 'required',
                    'full_name' => 'required|unique:erp_customers,full_name,'.$ERPTableId,
                    'tax_id' => 'required|unique:erp_customers,tax_id,'.$ERPTableId,
                    'total_licenses' => 'required|numeric',
                    'address1' => 'required',
                ];
                $ValidatorMessageArray = [
                ];
            }
            else if($request->ERPFormName=="FileCountriesList")
            {
                $ValidatorArray = [
                    'code' => 'required|max:5|unique:erp_countries,country_code,'.$ERPTableId,
                    'description' => 'required|unique:erp_countries,description,'.$ERPTableId,
                    'enabled_flag' => 'required',
                ];
                 $ValidatorMessageArray = [
                    'enabled_flag.required' => 'The active field is required',
                ];
            }
            else if($request->ERPFormName=="FileSourcesList")
            {
                $ValidatorArray = [
                    'code' => 'required|max:5|unique:erp_sources,source_code,'.$ERPTableId,
                    'description' => 'required|unique:erp_sources,description,'.$ERPTableId,
                    'auto_post' => 'required',
                    'enabled_flag' => 'required',
                ];
                 $ValidatorMessageArray = [
                    'enabled_flag.required' => 'The active field is required',
                ];
            }
            else if($request->ERPFormName=="FileThemesList")
            {
                $ValidatorArray = [
                    'theme_id' => 'required|unique:erp_themes,theme_id,'.$ERPTableId,
                    'enabled_flag' => 'required',
                ];
                $ValidatorMessageArray = [
                    'theme_id.required' => 'The theme field is required',
                    'theme_id.unique' => 'The theme has already been taken',
                    'enabled_flag.required' => 'The active field is required',
                ];
            }
            else if($request->ERPFormName=="FileLanguagesList")
            {
                $ValidatorArray = [
                    'language_id' => 'required|unique:erp_languages,language_id,'.$ERPTableId,
                    'enabled_flag' => 'required',
                ];
                $ValidatorMessageArray = [
                    'language_id.required' => 'The language field is required',
                    'language_id.unique' => 'The language has already been taken',
                    'enabled_flag.required' => 'The active field is required',
                ];
            }
            else if($request->ERPFormName=="SetupUsersList")
            {
                $ValidatorArray = [
                    'login' => 'required|unique:users,login,'.$ERPTableId,
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required',
                    'name' => 'required',
                    'admin_priv' => 'required',
                    'email' => 'required|unique:users,email,'.$ERPTableId,
                    'customer_id' => 'required',
                    'enabled_flag' => 'required',
                ];
                $ValidatorMessageArray = [
                    'login.required' => 'Login: Required field',
                    'login.unique' => 'Error inserting - Unique key violation: Login',
                    'password.required' => 'Password: Required field',
                    'password.confirmed' => 'The password and confirmation do not match.',
                    'password_confirmation.required' => 'Confirm Password: Required field',
                    'name.required' => 'Full Name : Required field',
                    'admin_priv.required' => 'Admin Priv: Required field',
                    'email.required' => 'E-mail: Required field',
                    'email.unique' => 'Error inserting - Unique key violation: E-mail',
                    'customer_id.required' => 'Customer ID: Required field',
                    'enabled_flag.required' => 'Active: Required field',
                ];
            }
            else if($request->ERPFormName=="SetupUsersGroupsList")
            {
                $ValidatorArray = [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$ERPTableId,
                    'enabled_flag' => 'required',
                ];
                $ValidatorMessageArray = [
                    'name.required' => 'Full Name : Required field',
                    'email.required' => 'E-mail: Required field',
                    'email.unique' => 'Error inserting - Unique key violation: E-mail',
                    'enabled_flag.required' => 'Active: Required field',
                ];
                if($ERPTableId=="")
                {
                   $ValidatorArray['login'] = 'required|unique:users,login,'.$ERPTableId;
                   $ValidatorArray['password'] = 'required|confirmed';
                   $ValidatorArray['password_confirmation'] = 'required';
                   
                   $ValidatorMessageArray['login.required'] = 'Login: Required field';
                   $ValidatorMessageArray['login.unique'] = 'Error inserting - Unique key violation: Login';

                   $ValidatorMessageArray['password.required'] = 'Password: Required field';
                   $ValidatorMessageArray['password.confirmed'] = 'The password and confirmation do not match.';
                   $ValidatorMessageArray['password_confirmation.required'] = 'Confirm Password: Required field';
                }
            }
            else if($request->ERPFormName=="SecurityApplicationsList")
            {     
                $ValidatorArray = [
                    'app_name' => "required|unique:applications,app_name,{$ERPTableId}",
                ];
                $ValidatorMessageArray = [
                    'app_name.required' => 'Application name: Required field',
                    'app_name.unique' => 'Error inserting - Unique key violation: Application name',
                ];
            }
            else if($request->ERPFormName=="SecurityGroupsList")
            {     
                $ValidatorArray = [
                    'description' => "required|unique:groups,description,".$ERPTableId,
                ];
                $ValidatorMessageArray = [
                    'description.required' => 'Group name: Required field',
                    'description.unique' => 'Error inserting - Unique key violation: Group name',
                ];
            }
            else if($request->ERPFormName=="SecurityGroupUsersList")
            {
                $ValidatorArray = [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,'.$ERPTableId,
                    'enabled_flag' => 'required',
                ];
                $ValidatorMessageArray = [
                    'name.required' => 'Full Name : Required field',
                    'email.required' => 'E-mail: Required field',
                    'email.unique' => 'Error inserting - Unique key violation: E-mail',
                    'enabled_flag.required' => 'Active: Required field',
                ];
            }
            else if($request->ERPFormName=="SecurityChangePassword")
            {

                $ValidatorArray = [
                    'old_password' => 'required',
                    'password' => 'required|confirmed',
                    'password_confirmation' => 'required',
                ];
                
                $ValidatorMessageArray = [
                    'old_password.required' => 'Old Password: Required field',
                    'password.required' => 'Password: Required field',
                    'password.confirmed' => 'The password and confirmation do not match.',
                    'password_confirmation.required' => 'Confirm Password: Required field',
                ];
            }
            else if($request->ERPFormName=="SecurityGroupsApplicationsList")
            {
                $ValidatorArray = [
                ];
                
                $ValidatorMessageArray = [
                ];
            }
            $validator = \Validator::make($request->all(), $ValidatorArray,$ValidatorMessageArray);
            if($validator->fails())
            {
                $Message = $validator->errors()->all();
                $Status = "errors";
            }
            else
            {   
                $ParaMeter = array();
                $ParaMeter["All"] = "All";
                if($request->ERPFormName=="FileApplicationList")
                {
                    $ErpApplication = ErpApplication::AddErpApplication($request);
                    $ERPFormName = $OnlyData = "ERP Applications";
                    $ERPApplications = ErpApplication::GetAllERPApplications($ParaMeter);
                    $ERPManuDataRecord = view('ERP.File.Applications.FileApplicationList',compact("ERPApplications","OnlyData"))->render();

                    $Permission["AppId"] = 1;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="FileCustomerList")
                {
                    $ErpCustomer = ErpCustomer::AddErpCustomer($request);
                    $ERPFormName = $OnlyData = "ERP Customers";
                    $ERPCustomers = ErpCustomer::GetAllErpCustomers($ParaMeter);
                    $ERPManuDataRecord = view('ERP.File.Customers.FileCustomerList',compact("ERPCustomers","OnlyData"))->render();
                    $Permission["AppId"] = 2;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="FileCountriesList")
                {
                    $ErpCountrie = ErpCountrie::AddErpCountrie($request);
                    $ERPFormName = $OnlyData = "Countries";
                    $ERPCountries = ErpCountrie::GetAllErpCountries($ParaMeter);
                    $ERPManuDataRecord = view('ERP.File.Countries.FileCountriesList',compact("ERPCountries","OnlyData"))->render();
                    $Permission["AppId"] = 3;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="FileSourcesList")
                {
                    $ErpSource = ErpSource::AddErpSource($request);
                    $ERPFormName = $OnlyData = "Sources";
                    $ERPSources = ErpSource::GetAllErpSources($ParaMeter);
                    $ERPManuDataRecord = view('ERP.File.Sources.FileSourcesList',compact("ERPSources","OnlyData"))->render();
                    $Permission["AppId"] = 4;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="FileThemesList")
                {
                    $ErpTheme = ErpTheme::AddErpTheme($request);
                    $ERPFormName = $OnlyData = "ERP Themes";
                    $ERPThemes = ErpTheme::GetAllErpThemes($ParaMeter);
                    $ERPManuDataRecord = view('ERP.File.Themes.FileThemesList',compact("ERPThemes","OnlyData"))->render();
                    $Permission["AppId"] = 5;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="FileLanguagesList")
                {
                    $ErpLanguage = ErpLanguage::AddErpLanguage($request);
                    $ERPFormName = $OnlyData = "ERP Languages";
                    $ERPLanguages = ErpLanguage::GetAllErpLanguages($ParaMeter);
                    $ERPManuDataRecord = view('ERP.File.Languages.FileLanguagesList',compact("ERPLanguages","OnlyData"))->render();
                    $Permission["AppId"] = 6;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="SetupUsersList")
                {
                    $ErpUser = User::AddUser($request);
                    $ERPFormName = $OnlyData = "ERP Users";
                    $GetAllUsers = User::GetAllUsers($ParaMeter);
                    $ERPManuDataRecord = view('ERP.Setup.Users.SetupUsersList',compact("GetAllUsers","OnlyData"))->render();
                    $Permission["AppId"] = 7;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="SetupUsersGroupsList")
                {
                    $ErpUser = User::AddUser($request);
                    if($ErpUser)
                    {
                       if(!$request->erp_table_id>0)
                       {
                            $request["erp_table_id"] = $ErpUser;
                       }
                       $ErpUserGroup = UserGroup::AddUserGroup($request);
                    }
                    $ERPFormName = $OnlyData = "Users/Groups";
                    $GetAllUsers = User::GetAllUsers($ParaMeter);
                    $ERPManuDataRecord = view('ERP.Setup.UsersGroups.SetupUsersGroupsList',compact("GetAllUsers","OnlyData"))->render();
                    $Permission["AppId"] = 8;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="SecurityApplicationsList")
                {
                    $ErpApplication = Application::AddApplication($request);
                    $ERPFormName = "Security Applications";
                    $OnlyData = "Applications";
                    $GetAllApplications = Application::GetAllApplications($ParaMeter);
                    $ERPManuDataRecord = view('ERP.Security.Applications.SecurityApplicationsList',compact("GetAllApplications","OnlyData"))->render();
                    $Permission["AppId"] = 9;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="SecurityGroupsList")
                {
                    $ErpGroup = Group::AddGroup($request);
                    $ERPFormName = "Security Groups";
                    $OnlyData = "Groups";
                    $GetAllGroups = Group::GetAllGroups($ParaMeter);
                    $ERPManuDataRecord = view('ERP.Security.Groups.SecurityGroupsList',compact("GetAllGroups","OnlyData"))->render();
                    $Permission["AppId"] = 10;
                    $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                }
                else if($request->ERPFormName=="SecurityGroupUsersList")
                {
                    $ErpUser = User::AddUser($request);
                    if($ErpUser)
                    {
                       $ErpUserGroup = UserGroup::AddUserGroup($request);
                    }
                    $SearchShow = "Hide";
                    $ERPFormName = $OnlyData = "Group users";

                    $ParaMeter1 = array();
                    $ParaMeter1["SecurityGroupId"] = $SecuritySearchGroupId =$request->group_id;
                    $GetAllGroupUsers = User::GetAllUsers($ParaMeter1);
                    
                    $ERPManuDataRecord = view('ERP.Security.GroupUsers.SecurityGroupUsersList',compact("GetAllGroupUsers","OnlyData","SearchShow","SecuritySearchGroupId"))->render();
                }
                else if($request->ERPFormName=="SecurityChangePassword")
                {
                    $user = User::find(auth()->user()->id);
                    if(!Hash::check($request->old_password, $user->password)) 
                    {
                        return response()->json(["Status"=>"error","Message"=>'Old password incorrect!',"ERPManuDataRecord"=>$ERPManuDataRecord]);
                    }
                    $ErpUser = User::AddUser($request);
                }
                else if($request->ERPFormName=="SecurityGroupsApplicationsList")
                {
                    if($request->permissions=="")
                    {
                       return response()->json(["Status"=>"error","Message"=>'Please select permissions!',"ERPManuDataRecord"=>$ERPManuDataRecord]);
                    }
                    $ParaMeter2["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter2["AppId"] = 12; //12 for ERP Groups / Applications
                    $AccessPermissionCheck = GroupsApp::GetAllGroupsApps($ParaMeter2);
                    if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
                    {
                        $ErpGroup = GroupsApp::AddGroupsApp($request);
                    }
                    else if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="N")
                    {
                       return response()->json(["Status"=>"error","Message"=>'Permission Denied!',"ERPManuDataRecord"=>$ERPManuDataRecord]); 
                    }
                }
                $Message = "$ERPFormName $AddUpdateMessage Successfully!";
                $Status = "success";
            }
        } 
        catch (\Exception $e) {
            $Message = "Error in $ERPFormName $AddUpdateMessage " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"ERPManuDataRecord"=>$ERPManuDataRecord,"GetExportPermission"=>$GetExportPermission]);
    }
}