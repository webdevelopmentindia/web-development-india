@if(!(isset($OnlyData) && $OnlyData=="Applications"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPSecurityApplications">
	<div id="SecurityApplicationsList" class="SecurityApplicationsSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
	<div class="page-title">
		<h3>Applications</h3>
	</div>
	<div class="header-elements">
		<span><?php
		        echo " ".date("m/d/Y");
	        ?></span>
	</div>
</div>
<?php 
    $ParaMeter["GroupId"] = Auth::user()->GroupId;
    $ParaMeter["AppId"] = 9; //9 for ERP Security Applications
    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
?> 
<div class="row row-col">
	<div class="col-xs-12">
		    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			<span class="link-src"><a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityApplicationsForm" SectionName="SecurityApplicationsSection"> Add New </a>
			</span>
			@endif
			<!-- <span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="SecurityApplicationsSearch" SectionName="SecurityApplicationsSection" FilterChangeDivEmpty="ERPSecurityApplicationsFilterChange" FormClassName="ERPSecurityApplicationsRemoveSearchSubmit"> Search </a></span> -->
	</div>
</div>

		<?php 
$CheckParameter = 0;
?>
<div id="SecurityApplicationsListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPSecurityApplicationsRemoveSearchSubmit">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecurityApplicationsList" SectionName="SecurityApplicationsSection" TabName="ERPSecurityApplications">	
@if(isset($ParaMeter))
    @if(
    (((isset($ParaMeter["Application"]) && $ParaMeter["Application"]!="") && (isset($ParaMeter["ApplicationCondition"]) && $ParaMeter["ApplicationCondition"]!="") || (isset($ParaMeter["ApplicationCondition"]) && $ParaMeter["ApplicationCondition"]==4)))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif		
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if(((isset($ParaMeter["Application"]) && $ParaMeter["Application"]!="") && (isset($ParaMeter["ApplicationCondition"]) && $ParaMeter["ApplicationCondition"]!="") || (isset($ParaMeter["ApplicationCondition"]) && $ParaMeter["ApplicationCondition"]==4)))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchApplication" class="SearchSection">
	        <div PageName="ERPSecurityApplications" FilterName="SecurityApplicationsApplication" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['ApplicationCondition'] }}" ShowDivClass="ERPSecurityApplicationsFilterChange"
	        FieldNameValue='{{$ParaMeter["Application"]}}'>
		        Application: {{ $ParaMeter["ApplicationConditionValue"] }} 
		            {{ $ParaMeter["Application"] }} 
            </div>

	        <input type="hidden" id="Application" name="Application" value='{{ $ParaMeter["Application"] }}'>
	        <input type="hidden" id="Application_condition" name="Application_condition" value='{{ $ParaMeter["ApplicationCondition"] }}'>
	        <div style="display: none" class="ERPSecurityApplicationsFilterChange">
	        </div>
	        <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SecurityApplicationsList" RemoveSearch="SearchApplication" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSecurityApplicationsFilterChange"> Remove </a>
        </div>
   @else
        <?php 
			$AddFilterOptions.='<option value="SecurityApplicationsApplication">Application</option>';
		?>
   @endif
   @if((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchEnabledFlag" class="SearchSection">
	        <div PageName="ERPSecurityApplications" FilterName="SecurityApplicationsEnabledFlag" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EnabledFlagCondition'] }}" ShowDivClass="ERPSecurityApplicationsFilterChange"
	        FieldNameValue='{{$ParaMeter["EnabledFlag"]}}'>
	            Active : {{ $ParaMeter["EnabledFlagConditionValue"] }} 
	                @if($ParaMeter["EnabledFlag"]==1) {{ 'Yes' }} @else {{ 'No' }} @endif
	        </div>

	        <input type="hidden" id="enabled_flag" name="enabled_flag" value='{{ $ParaMeter["EnabledFlag"] }}'>
	        <input type="hidden" id="enabled_flag_condition" name="enabled_flag_condition" value='{{ $ParaMeter["EnabledFlagCondition"] }}'>
	        <div style="display: none" class="ERPSecurityApplicationsFilterChange">
	        </div>
	        <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SecurityApplicationsList" RemoveSearch="SearchEnabledFlag" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSecurityApplicationsFilterChange"> Remove </a>
	    </div>
	@else
	    <?php 
	        $AddFilterOptions.='<option value="SecurityApplicationsEnabledFlag">Active</option>';
	    ?>
	@endif
   @if($CheckParameter)
   <select id="ERPSecurityApplicationsSearchList" name="ERPSecurityApplicationsSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="SecurityApplicationsAddFilterShow">
   	    <option value=''>+ Add Filter</option>
   	    <?php echo $AddFilterOptions; ?>
   </select>
   @if(!$GetAllErpSaveFilters->isEmpty())
       <select id="ERPGetOldFilterData" name="ERPGetOldFilterData" class="form-control ERPGetOldFilterData" PageName="ERPSecurityApplications">
       	  <option value=""></option>
          <option value="">Public</option>
	      @foreach($GetAllErpSaveFilters as $Filter)
	   	    <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
	   	  @endforeach
      </select>
   @endif
   <div id="SecurityApplicationsAddFilterShow" style="display: none">
   </div>
   <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit" FormName="SecurityApplicationsList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > Remove </a>
   @endif
   @if(
    (((isset($ParaMeter["Application"]) && $ParaMeter["Application"]!="") && (isset($ParaMeter["ApplicationCondition"]) && $ParaMeter["ApplicationCondition"]!="") || (isset($ParaMeter["ApplicationCondition"]) && $ParaMeter["ApplicationCondition"]==4)))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
    )
    </div>
</div>
    @endif
@endif
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPSecurityApplicationstable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr>
			    <tr>
			    	<th></th>
			        <th>Application name</th>
			        <th>Description</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$GetAllApplications->isEmpty())
				    @foreach($GetAllApplications as $Application)
		                <tr>
		                	<td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td>
			                <td>{{ $Application->app_name }}</td>
			                <td>{{ $Application->description }}</td>
			                <td class="not-export-col">
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn ERPFormShow text-primary" FormName="SecurityApplicationsForm" SectionName="SecurityApplicationsSection" EditId="{{ $Application->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn ERPRecordDelete text-danger" FormName="SecurityApplicationsAdd" DeleteId="{{ $Application->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<th></th>
		            <th>Application name</th>
			        <th>Description</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Applications"))
	</div>
	<div id="SecurityApplicationsForm" style="display: none" class="SecurityApplicationsSection card">
	</div>
	<div id="SecurityApplicationsSearch" style="display: none" class="SecurityApplicationsSection card">
	</div>
</div>
@endif