<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\ErpCountrie;
use App\ErpSource;
use App\ErpTheme;
use App\ErpThemesList;
use App\ErpLanguagesList;
use App\ErpLanguage;
use App\User;
use App\Group;
use App\Application;
use App\Services\PayUService\Exception;
class ERPFormShowController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFormShowAjax(Request $request)
    {
        $ParaMeter = array();
        $ERPTableId = 0;
        $AddUpdateMessage = "Add";
        if($request->EditId>0)
        {
            $AddUpdateMessage = "Update";
            $ERPTableId = $request->EditId;
        }
        $ParaMeter["ERPTableId"] = $ERPTableId;

        $Status = "success";
        $Html = "";
        $Message = "";
        try {
            if($request->FormName=="FileApplicationForm")
            {
                $ERPApplicationDetails = ErpApplication::GetAllERPApplications($ParaMeter);
                $Html = view('ERP.File.Applications.FileApplicationAdd',compact("ERPApplicationDetails"))->render();
            }
            else if($request->FormName=="FileCustomerForm")
            {
                $ParaMeter1 = array();
                $ParaMeter1["All"] = "All";
                $ERPCountries = ErpCountrie::GetAllErpCountries($ParaMeter1);
                $ERPCustomerDetails = ErpCustomer::GetAllERPCustomers($ParaMeter);
                $Html = view('ERP.File.Customers.FileCustomerAdd',compact("ERPCustomerDetails","ERPCountries"))->render();
            }
            else if($request->FormName=="FileCountriesForm")
            {
                $ErpCountryDetails = ErpCountrie::GetAllErpCountries($ParaMeter);
                $Html = view('ERP.File.Countries.FileCountriesAdd',compact("ErpCountryDetails"))->render();
            }
            else if($request->FormName=="FileSourcesForm")
            {
                $ErpSourceDetails = ErpSource::GetAllErpSources($ParaMeter);
                $Html = view('ERP.File.Sources.FileSourcesAdd',compact("ErpSourceDetails"))->render();
            }
            else if($request->FormName=="FileThemesForm")
            {
                $ErpThemeDetails = ErpTheme::GetAllErpThemes($ParaMeter);
                $ParaMeter1 = array();
                $ParaMeter1["All"] = "All";
                $ErpThemesList = ErpThemesList::GetAllErpThemesLists($ParaMeter1);
                $Html = view('ERP.File.Themes.FileThemesAdd',compact("ErpThemeDetails","ErpThemesList"))->render();
            }
            else if($request->FormName=="FileLanguagesForm")
            {
                $ErpLanguageDetails = ErpLanguage::GetAllErpLanguages($ParaMeter);
                $ParaMeter1 = array();
                $ParaMeter1["All"] = "All";
                $ErpLanguagesList = ErpLanguagesList::GetAllErpLanguagesLists($ParaMeter1);
                $Html = view('ERP.File.Languages.FileLanguagesAdd',compact("ErpLanguageDetails","ErpLanguagesList"))->render();
            }
            else if($request->FormName=="SetupUsersForm")
            {
                $ErpUserDetails = User::GetAllUsers($ParaMeter);
                $ParaMeter1 = array();
                $ParaMeter1["All"] = "All";
                $ErpCustomersList = ErpCustomer::GetAllErpCustomers($ParaMeter1);
                $ErpLanguagesList = ErpLanguagesList::GetAllErpLanguagesLists($ParaMeter1);
                $ErpThemesList = ErpThemesList::GetAllErpThemesLists($ParaMeter1);
                $Html = view('ERP.Setup.Users.SetupUsersAdd',compact("ErpUserDetails","ErpCustomersList","ErpLanguagesList","ErpThemesList"))->render();
            }
            else if($request->FormName=="SetupUsersGroupsForm")
            {
                $ErpUserDetails = User::GetAllUsers($ParaMeter);
                $ParaMeter1 = array();
                $ParaMeter1["All"] = "All";
                if(!$ErpUserDetails->isEmpty())
                {
                   $ParaMeter1["Login"] = $ErpUserDetails[0]->id; 
                }
                $GetAllGroups = Group::GetAllGroups($ParaMeter1);
                $Html = view('ERP.Setup.UsersGroups.SetupUsersGroupsAdd',compact("ErpUserDetails","GetAllGroups"))->render();
            }
            else if($request->FormName=="SecurityGroupsForm")
            {
                $ErpGroupDetails = Group::GetAllGroups($ParaMeter);
                $Html = view('ERP.Security.Groups.SecurityGroupsAdd',compact("ErpGroupDetails","GetAllGroups"))->render();
            }
            else if($request->FormName=="SecurityApplicationsForm")
            {
                if($ParaMeter["ERPTableId"]==0)
                {
                   $ParaMeter["ERPTableId"]=""; 
                }
                if($ParaMeter["ERPTableId"]==0 && $request->EditId!="")
                {
                    $AddUpdateMessage = "Update";
                    $ParaMeter["ERPTableId"] = $request->EditId;
                }
                $ErpApplicationDetails = Application::GetAllApplications($ParaMeter);
                $Html = view('ERP.Security.Applications.SecurityApplicationsAdd',compact("ErpApplicationDetails","GetAllGroups"))->render();
            }
            else if($request->FormName=="SecurityGroupUsersForm")
            {
                $SecuritySearchGroupId = $request->GroupId;
                $ErpUserDetails = User::GetAllUsers($ParaMeter);
                $ParaMeter1 = array();
                $ParaMeter1["All"] = "All";
                if(!$ErpUserDetails->isEmpty())
                {
                   $ParaMeter1["Login"] = $ErpUserDetails[0]->id; 
                }
                $GetAllGroups = Group::GetAllGroups($ParaMeter1);
                $Html = view('ERP.Security.GroupUsers.SecurityGroupUsersAdd',compact("ErpUserDetails","GetAllGroups","SecuritySearchGroupId"))->render();
            }
            else if($request->FormName=="SecurityGroupsApplicationsForm")
            {
                $GetAllGroups = Group::GetAllGroups($ParaMeter);
                unset($ParaMeter["ERPTableId"]);

                $ParaMeter["GroupsApplications"] = $request->EditId;
                $ApplicationDetails = Application::GetAllApplications($ParaMeter);

                $SecuritySearchGroupId = $request->EditId;
                $Html = view('ERP.Security.GroupsApplications.SecurityGroupsApplicationsAdd',compact("GetAllGroups","ApplicationDetails"))->render();
            }
            else if($request->FormName=="ReportsReportOfCustomerUsersForm")
            {
                unset($ParaMeter["ERPTableId"]);
                $ParaMeter["CustomerUsersCount"] = "CustomerUsersCount";
                $GetAllUsers = ErpCustomer::GetAllERPCustomers($ParaMeter);
                $Html = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersAdd',compact("GetAllUsers"))->render();
            }
        }
        catch (\Exception $e) {
          $Message = "Error in ERP $AddUpdateMessage Form Show " . $e->getMessage();
          $Status = "error";
        }
        return json_encode(["Status"=>$Status,"Html"=>$Html,"Message"=>$Message]);
    }
}