<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpSearchTextOption extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    Static Public function GetAllErpSearchTextOptions($ParaMeter)
    {
        $GetAllErpSearchTextOptions = ErpSearchTextOption::select("erp_search_text_options.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpSearchTextOptions =$GetAllErpSearchTextOptions->addselect("erp_search_text_options.*"); 
        }
        else if(isset($ParaMeter["TableId"]) && $ParaMeter["TableId"]>0)
        {
           $GetAllErpSearchTextOptions =$GetAllErpSearchTextOptions->where("erp_search_text_options.id",$ParaMeter["TableId"])->addselect("erp_search_text_options.*"); 
        }
        $GetAllErpSearchTextOptions = $GetAllErpSearchTextOptions->orderBy("erp_search_text_options.id","ASC")
            ->get();
        return $GetAllErpSearchTextOptions;
    }
}