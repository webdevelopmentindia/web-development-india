<div class="close-btn HideAddFilterModal" ModalId="SecuritySystemLogAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="SecuritySystemLogLogin")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Login') }}</label>
    <div class="col-sm-3">
	    <select id="login_condition" name="login_condition" class="form-control ERPSearchEmptyContent" FieldId="login">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="login" type="text" class="form-control" name="login"  autofocus placeholder="Login" value="">
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SecuritySystemLogList" FieldNameCondition="login_condition" FieldName="login"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SecuritySystemLogDate")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Date') }}</label>
    <div class="col-sm-3">
      <select id="date_condition" name="date_condition" class="form-control ERPSearchEmptyContent" FieldId="date">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
           @foreach($GetAllErpSearchTextOptions as $TextOption)
                <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           @endforeach
        @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="date" type="text" class="form-control" name="date"  autofocus placeholder="Date" value="">
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SecuritySystemLogList" FieldNameCondition="date_condition" FieldName="date"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SecuritySystemLogSession")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Session') }}</label>
    <div class="col-sm-3">
      <select id="session_condition" name="session_condition" class="form-control ERPSearchEmptyContent" FieldId="session">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
           @foreach($GetAllErpSearchTextOptions as $TextOption)
                <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           @endforeach
        @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="session" type="text" class="form-control" name="session"  autofocus placeholder="Session" value="">
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SecuritySystemLogList" FieldNameCondition="session_condition" FieldName="session"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SecuritySystemLogIp")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('IP') }}</label>
    <div class="col-sm-3">
      <select id="ip_condition" name="ip_condition" class="form-control ERPSearchEmptyContent" FieldId="ip">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
           @foreach($GetAllErpSearchTextOptions as $TextOption)
                <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           @endforeach
        @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="ip" type="text" class="form-control" name="ip"  autofocus placeholder="IP" value="">
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SecuritySystemLogList" FieldNameCondition="ip_condition" FieldName="ip"> Apply </a></div>
</div>
@endif