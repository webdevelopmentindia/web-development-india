<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpSearchRadioOption;
use App\ErpSearchTextOption;
use App\ErpCustomer;
use App\Services\PayUService\Exception;
class ERPFilterChangeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFilterChangeAjax(Request $request)
    {
        $FilterChangeHtml = "";
        $ERPFilterName = "";
        try {
            $ParaMeter = array();
            $ParaMeter["All"] = "All";
            $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter);
            $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter);
            if($request->PageName=="ERPSetupUsers" || $request->PageName=="ERPReportsReportOfCustomerUsers")
            {
                $GetAllERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);  
            }
            unset($ParaMeter["All"]);
            $ParaMeter["FilterName"] = $request->FilterName;
            $ParaMeter["FieldNameConditionValue"] = $request->FieldNameConditionValue;
            $ParaMeter["FieldNameValue"] = $request->FieldNameValue;
            if($request->PageName=="ERPFileApplication")
            {
               $ERPFilterName = "ERP File Applications";
               $FilterChangeHtml = view('ERP.File.Applications.FileApplicationFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPFileCustomer")
            {
               $ERPFilterName = "ERP File Customers";
               $FilterChangeHtml = view('ERP.File.Customers.FileCustomerFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPFileCountries")
            {
               $ERPFilterName = "ERP File Countries";
               $FilterChangeHtml = view('ERP.File.Countries.FileCountriesFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPFileSources")
            {
               $ERPFilterName = "ERP File Sources";
               $FilterChangeHtml = view('ERP.File.Sources.FileSourcesFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPFileThemes")
            {
               $ERPFilterName = "ERP File Themes";
               $FilterChangeHtml = view('ERP.File.Themes.FileThemesFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPFileLanguages")
            {
               $ERPFilterName = "ERP File Languages";
               $FilterChangeHtml = view('ERP.File.Languages.FileLanguagesFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPSetupUsers")
            {
               $ERPFilterName = "ERP Setup Users";
               $FilterChangeHtml = view('ERP.Setup.Users.SetupUsersFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter","GetAllERPCustomers"))->render(); 
            }
            else if($request->PageName=="ERPSecuritySystemLog")
            {
               $ERPFilterName = "ERP Security System Log";
               $FilterChangeHtml = view('ERP.Security.SystemLog.SecuritySystemLogFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter"))->render(); 
            }
            else if($request->PageName=="ERPReportsReportOfCustomerUsers")
            {
               $ERPFilterName = "ERP Report Of Customer Users";
               $FilterChangeHtml = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersFilterChange',compact("GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","ParaMeter","GetAllERPCustomers"))->render(); 
            }
            $Status = "success";
            $Message = "$ERPFilterName Filter Change Successfully!";
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPFilterName Filter Change " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"FilterChangeHtml"=>$FilterChangeHtml]);
    }
}