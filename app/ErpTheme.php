<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class ErpTheme extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpTheme($request)
    {
        if($request->erp_table_id>0)
        {
            $ErpTheme = ErpTheme::find($request->erp_table_id);
        }
        else
        {
            $ErpTheme = new ErpTheme();  
        } 

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }

        $ErpTheme->theme_id  = $request->theme_id;
        $ErpTheme->created_by = Auth::user()->id;
        $ErpTheme->enabled_flag = $request->enabled_flag;
        $ErpTheme->ip_address = getUserIpAddr();
        $ErpTheme->save();
        return $ErpTheme->id;
    }
    Static Public function GetAllErpThemes($ParaMeter)
    {
        $GetAllErpThemes = ErpTheme::select("erp_themes.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpThemes =$GetAllErpThemes->join("erp_themes_lists","erp_themes_lists.id","erp_themes.theme_id")
               ->addselect("erp_themes.*","erp_themes_lists.theme_name"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpThemes =$GetAllErpThemes->join("erp_themes_lists","erp_themes_lists.id","erp_themes.theme_id")
                            ->where('erp_themes.id',$ParaMeter["ERPTableId"])
                            ->addselect("erp_themes.*","erp_themes_lists.theme_name"); 
        }
        else if((isset($ParaMeter["FileThemesSearch"]) && $ParaMeter["FileThemesSearch"]!="") && (isset($ParaMeter["FileThemesSearchArray"]) && count($ParaMeter["FileThemesSearchArray"])>0))
        {
            $GetAllErpThemes = $GetAllErpThemes->join("erp_themes_lists","erp_themes_lists.id","erp_themes.theme_id")->addselect("erp_themes.*","erp_themes_lists.theme_name");
            $FileThemesSearch = $ParaMeter["FileThemesSearchArray"];
            if(count($FileThemesSearch)>0)
            {
              foreach ($FileThemesSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];

                if($ColumnCondition==3 && $Search["ColumnName"]=="theme_name") //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpThemes = $GetAllErpThemes->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpThemes = $GetAllErpThemes->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllErpThemes = $GetAllErpThemes->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllErpThemes = $GetAllErpThemes->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllErpThemes = $GetAllErpThemes->whereNull("$TableName.$ColumnName");
                }
              }
            }
        }
        $GetAllErpThemes = $GetAllErpThemes->orderBy("erp_themes.id","DESC")
                                            ->get();
        return $GetAllErpThemes;
    }
    Static Public function DeleteErpThemes($ParaMeter)
    {
        $ErpTheme = ErpTheme::find($ParaMeter["DeleteId"]);
        $ErpTheme->delete();
        return $ErpTheme->id;
    }
}