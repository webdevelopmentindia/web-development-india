@if(!isset($ParaMeter["ERPTableId"]))
<?php 
    echo strtoupper("Search of Logged users");
    echo " ".date("m/d/Y");
?>
	<div class="row">
	    <div class="col-md-12 emloy-hd">
	    </div>
	    <div class="col-md-12 employee-form">
	    	<form method="POST" aria-label="{{ __('ERPSearchSubmit') }}" enctype="multipart/form-data" id="ERPSearchSubmit" name="ERPSearchSubmit" FormParentDivId="SecuritySystemLogSearch">
		    @csrf
			<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecuritySystemLogList" SectionName="SecuritySystemLogSection" TabName="ERPSecuritySystemLog">
            <div id="ERPChangeFilter">
@endif	
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>		
			<?php 
			    $LoginCondition = $ParaMeter["LoginCondition"];
			    $Login = $ParaMeter["Login"];
			    $DateCondition = $ParaMeter["DateCondition"];
			    $Date = $ParaMeter["Date"];
			    $SessionCondition = $ParaMeter["SessionCondition"];
			    $Session = $ParaMeter["Session"];
			    $IpCondition = $ParaMeter["IpCondition"];
			    $Ip = $ParaMeter["Ip"];
			    $ERPTableId = "";
			    if(isset($ParaMeter["ERPTableId"]))
			    {
			       $ERPTableId = $ParaMeter["ERPTableId"];
			    }
			?>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('User Name') }}</label>
	            <div class="col-sm-3">
				    <select id="login_condition" name="login_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LoginCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }} SearchFieldShow" name="login"  autofocus placeholder="User Name" value="<?php echo $Login; ?>" style='@if($LoginCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('Date') }}</label>
	            <div class="col-sm-3">
				    <select id="date_condition" name="date_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($DateCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="date" type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} SearchFieldShow" name="date"  autofocus placeholder="Date" value="<?php echo $Date; ?>" style='@if($DateCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('Session') }}</label>
	            <div class="col-sm-3">
				    <select id="session_condition" name="session_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($SessionCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="session" type="text" class="form-control{{ $errors->has('session') ? ' is-invalid' : '' }} SearchFieldShow" name="session"  autofocus placeholder="Session" value="<?php echo $Session; ?>" style='@if($SessionCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('IP') }}</label>
	            <div class="col-sm-3">
				    <select id="ip_condition" name="ip_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($IpCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="ip" type="text" class="form-control{{ $errors->has('ip') ? ' is-invalid' : '' }} SearchFieldShow" name="ip"  autofocus placeholder="IP" value="<?php echo $Ip; ?>" style='@if($IpCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3"></label>
	            <div class="col-sm-3">
		        </div>
	            <div class="col-sm-6">
	               <button type="submit" class="btn cancel-btn">Search</button>
	               <a type="button" onclick="ERPFormReset(this); return false;" class="btn cancel-btn" >Clear</a>
	               <select id="erp_filter_list" name="erp_filter_list" class="form-control" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" FormName="SecuritySystemLogSearch">
	               	@if(!$GetAllErpSaveFilters->isEmpty())
	               	   <option value=""></option>
	               	   <option value="">Public</option>
	               	   @foreach($GetAllErpSaveFilters as $Filter)
                            <option value="{{ $Filter->id }}" @if($ERPTableId==$Filter->id) {{ 'selected' }} @endif >{{ $Filter->filter_name }}</option>
	               	   @endforeach
	               	@endif
		           </select>
	               <a href="javascript:" type="button" class="btn cancel-btn">Save Filter</a>
	               <button type="button" class="btn cancel-btn" ShowSection="SecuritySystemLogList" SectionName="SecuritySystemLogSection" id="ERPBackCancelButton">Back</button>
	            </div>
	        </div>
@if(!isset($ParaMeter["ERPTableId"]))
            </div>
	        </form>
	        <div class="form-group">
	            <label class="control-label col-sm-3"></label>
	            <div class="col-sm-3">
		        </div>
		        <div class="col-sm-6">
		               Save Filter 
		               <input id="filter_name" type="text" class="form-control{{ $errors->has('filter_name') ? ' is-invalid' : '' }}" name="filter_name"  autofocus placeholder="Filter Name" value="">
		               <button type="button" class="btn cancel-btn ERPFilterSave" FormName="SecuritySystemLogSearch">Save</button>
		        </div>
		        <div class="col-sm-6" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
		               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
		                    @if(!$GetAllErpSaveFilters->isEmpty())
			               	   <option value=""></option>
			               	   <option value="">Public</option>
			               	   @foreach($GetAllErpSaveFilters as $Filter)
		                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
			               	   @endforeach
			               	@endif
		               </select>
		               <button type="button" class="btn cancel-btn ERPFilterDelete" FormName="SecuritySystemLogSearch">Delete</button>
		        </div>
	       </div>
	    </div>
	</div>
@endif