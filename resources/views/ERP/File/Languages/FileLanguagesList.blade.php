@if(!(isset($OnlyData) && $OnlyData=="ERP Languages"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPFileLanguages">
	<div id="FileLanguagesList" class="FileLanguagesSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>Languages</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
    <?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 6; //6 for ERP Languages
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?> 
	<div class="row row-col">
		<div class="col-xs-12">
			    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			    <span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileLanguagesForm" SectionName="FileLanguagesSection"> Add New </a></span>
				@endif
				<span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="FileLanguagesSearch" SectionName="FileLanguagesSection" FilterChangeDivEmpty="ERPFileLanguagesFilterChange" FormClassName="ERPFileLanguagesRemoveSearchSubmit"> Search </a></span>
		</div>
	</div>

		<?php 
$CheckParameter = 0;
?>
<div id="FileLanguagesListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPFileLanguagesRemoveSearchSubmit add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileLanguagesList" SectionName="FileLanguagesSection" TabName="ERPFileLanguages">	
@if(isset($ParaMeter))
    
    		@if(
    (((isset($ParaMeter["Language"]) && $ParaMeter["Language"]!="") && (isset($ParaMeter["LanguageCondition"]) && $ParaMeter["LanguageCondition"]!="") || (isset($ParaMeter["LanguageCondition"]) && $ParaMeter["LanguageCondition"]==4)))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if(((isset($ParaMeter["Language"]) && $ParaMeter["Language"]!="") && (isset($ParaMeter["LanguageCondition"]) && $ParaMeter["LanguageCondition"]!="") || (isset($ParaMeter["LanguageCondition"]) && $ParaMeter["LanguageCondition"]==4)))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchLanguage" class="SearchSection addfield-col">
        	<span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileLanguages" FilterName="FileLanguagesLanguage" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['LanguageCondition'] }}" ShowDivClass="ERPFileLanguagesFilterChange"
		        FieldNameValue='{{$ParaMeter["Language"]}}' FormName="FileLanguagesList" AddFilterPopup="FileLanguagesAddFilterShow">
			        Language: {{ $ParaMeter["LanguageConditionValue"] }} 
			            {{ $ParaMeter["Language"] }} 
	            </div>
	            <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileLanguagesList" RemoveSearch="SearchLanguage" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileLanguagesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i> </a>
	        </span>

	        <input type="hidden" id="language" name="language" value='{{ $ParaMeter["Language"] }}'>
	        <input type="hidden" id="language_condition" name="language_condition" value='{{ $ParaMeter["LanguageCondition"] }}'>
	        <div style="display: none" class="ERPFileLanguagesFilterChange add-drop-down">
	        </div>
	        
        </div>
   @else
        <?php 
			$AddFilterOptions.='<li value="FileLanguagesLanguage" id="ERPFileLanguagesSearchList" name="ERPFileLanguagesSearchList" class="ERPAddFilterShow" ShowDivId="FileLanguagesAddFilterShow" FilterChangeModal="ERPFileLanguagesFilterChange" FormName="FileLanguagesList">Language</li>';
		?>
   @endif
   @if((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchEnabledFlag" class="SearchSection addfield-col">
	    	<span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileLanguages" FilterName="FileLanguagesEnabledFlag" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EnabledFlagCondition'] }}" ShowDivClass="ERPFileLanguagesFilterChange"
		        FieldNameValue='{{$ParaMeter["EnabledFlag"]}}' FormName="FileLanguagesList" AddFilterPopup="FileLanguagesAddFilterShow">
		            Active : {{ $ParaMeter["EnabledFlagConditionValue"] }} 
		                @if($ParaMeter["EnabledFlag"]==1) {{ 'Yes' }} @else {{ 'No' }} @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileLanguagesList" RemoveSearch="SearchEnabledFlag" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileLanguagesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i> </a>
		    </span>

	        <input type="hidden" id="enabled_flag" name="enabled_flag" value='{{ $ParaMeter["EnabledFlag"] }}'>
	        <input type="hidden" id="enabled_flag_condition" name="enabled_flag_condition" value='{{ $ParaMeter["EnabledFlagCondition"] }}'>
	        <div style="display: none" class="ERPFileLanguagesFilterChange add-drop-down">
	        </div>
	        
	    </div>
	@else
	    <?php 
	        $AddFilterOptions.='<li value="FileLanguagesEnabledFlag" id="ERPFileLanguagesSearchList" name="ERPFileLanguagesSearchList" class="ERPAddFilterShow" ShowDivId="FileLanguagesAddFilterShow" FilterChangeModal="ERPFileLanguagesFilterChange" FormName="FileLanguagesList">Active</li>';
	    ?>
	@endif
   @if($CheckParameter)

   			<div class="addfield-col">
		   <!-- <select id="ERPFileLanguagesSearchList" name="ERPFileLanguagesSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="FileLanguagesAddFilterShow">
		   	    <option value=''>+ Add Filter</option>
		   	    <?php echo $AddFilterOptions; ?>
		   </select> -->
		   @if($AddFilterOptions!="")
			    <div class="dropdown">
				  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				    <?php echo $AddFilterOptions; ?>
				  </ul>
				</div>
			@endif
		   <div id="FileLanguagesAddFilterShow" class="add-drop-down" style="display: none">
		   	
	   		</div>
	   	</div>






<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">  
   @if(!$GetAllErpSaveFilters->isEmpty())
       <select id="ERPGetOldFilterData" name="ERPGetOldFilterData" class="form-control ERPSearchBySaveFilter" PageName="ERPFileLanguages" SectionName="FileLanguagesList" ManuName="ERPFileLanguages" FormName="">
       	  <option value=""></option>
          <option value="">Public</option>
	      @foreach($GetAllErpSaveFilters as $Filter)
	   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
	   	  @endforeach
      </select>
   @endif
   <div id="FileLanguagesAddFilterShow"  style="display: none">
   </div>
</div>


<!-- GetAllErpSaveFilters -->
	   	<div class="addfield-col">
	   		<button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="FileLanguagesList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPFileLanguagesFilterChange" HideAddFilterPopup="FileLanguagesAddFilterShow">Save Filter</button>
	   	</div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileLanguagesList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="FileLanguagesList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
	   			<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="FileLanguagesList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
	   		</div>
   @endif
   @if(
    (((isset($ParaMeter["Language"]) && $ParaMeter["Language"]!="") && (isset($ParaMeter["LanguageCondition"]) && $ParaMeter["LanguageCondition"]!="") || (isset($ParaMeter["LanguageCondition"]) && $ParaMeter["LanguageCondition"]==4)))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]>=1))
    )
    </div>
</div>
    @endif
@endif
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPFileLanguagestable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>Lang ID</th>
			        <th>Language</th>
			        <th>Active</th>
			        <th>Mod On</th>
			        <th>Created On</th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$ERPLanguages->isEmpty())
				    @foreach($ERPLanguages as $Language)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Language->erp_table_id }}</td>
			                <td>{{ $Language->language_name }}</td>
			                <td>
			                	@if($Language->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Language->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td>{{ $Language->mod_on }}</td>
			                <td>{{ $Language->created_at }}</td>
			                <td>{{ $Language->created_by }}</td>
			                 <td class="not-export-col">
			                 	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="FileLanguagesForm" SectionName="FileLanguagesSection" EditId="{{ $Language->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn text-danger ERPRecordDelete" FormName="FileLanguagesAdd" DeleteId="{{ $Language->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>Lang ID</th>
			        <th>Language</th>
			        <th>Active</th>
			        <th>Mod On</th>
			        <th>Created On</th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="ERP Languages"))
	</div>
	<div id="FileLanguagesForm" style="display: none" class="FileLanguagesSection card">
	</div>
	<div id="FileLanguagesSearch" style="display: none" class="FileLanguagesSection card">
	</div>
</div>
@endif