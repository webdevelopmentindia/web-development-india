<?php
$erp_table_id = $customer_id = $login = $customer_id = $language_id = $theme_id = $name = $email = "";
$admin_priv = $enabled_flag = -1;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpUserDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpUserDetails[0]->erp_table_id;
    $customer_id = $ErpUserDetails[0]->customer_id;
    $language_id = $ErpUserDetails[0]->user_language;
    $theme_id = $ErpUserDetails[0]->user_theme;
    $customer_id = $ErpUserDetails[0]->customer_id;
    $login = $ErpUserDetails[0]->login;
    $name = $ErpUserDetails[0]->name;
    $email = $ErpUserDetails[0]->email;
    $admin_priv = $ErpUserDetails[0]->admin_priv;
    //dd($admin_priv);
    $enabled_flag = $ErpUserDetails[0]->enabled_flag;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="SetupUsersForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="SetupUsersList" SectionName="SetupUsersSection" TabName="ERPSetupUsers">
<div class="row">
     <div class="card-header header-elements-inline">
            <div class="page-title">
                <h3>
                    <?php 
                    if($erp_table_id>0)
                    {
                        echo strtoupper("Update of erp_Users");
                    }
                    else
                    {
                        echo strtoupper("New record of erp_Users"); 
                    } 
                    
                    ?>
                </h3>
            </div>
            <div class="header-elements">
                <span><?php 
                    echo " ".date("m/d/Y");
                ?></span>
            </div>
        </div> <!-- emloy-hd p-0 -->



     <div class="row row-col">
        <div class="col-xs-12 emloy-hd">
             @if($erp_table_id>0)
             <a href="javascript:" class="btn save-btn ERPFormShow" FormName="SetupUsersForm" SectionName="SetupUsersSection"> Add New </a>
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button>
             <button type="button" class="btn cancel-btn save-btn" ShowSection="SetupUsersList" SectionName="SetupUsersSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button>
        </div>
    </div>
    <div class="card-body employee-form">
        <div class="row">
            <div class="col-xs-12">
                <span class="req-field">* Required field(s)</span>
            </div>
        </div>
        <div class="row">
            <div class="card col-md-12">
                <div class="card-body">
                    <div class="col-md-6">                
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('User ID') }}</label>
                            <div class="col-sm-8">
                            	@if($erp_table_id>=1)
                                 {{ $erp_table_id }}
                              @endif
                            </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Password *') }}</label>
                            <div class="col-sm-8">
                                <input id="password" type="password" class="form-control" name="password" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Full Name *') }}</label>
                            <div class="col-sm-8">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $name }}">
                            </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('E-mail *') }}</label>
                            <div class="col-sm-8">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email }}">
                            </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Customer ID *') }}</label>
                            <div class="col-sm-8">
                                <select id="customer_id" class="form-control{{ $errors->has('customer_id') ? ' is-invalid' : '' }}" name="customer_id">
                                      @if(!$ErpCustomersList->isEmpty())
                                         <option value="">Select Customer</option>
                                         @foreach($ErpCustomersList as $Customer)
                                             <option value="{{ $Customer->erp_table_id }}" @if($Customer->erp_table_id==$customer_id) {{ 'selected' }} @endif>{{ $Customer->full_name }}</option>
                                         @endforeach
                                      @endif
                                </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Login *') }}</label>
                            <div class="col-sm-8">
                                <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login"  autofocus placeholder="Login" value="<?php if(old('login')) { echo old('login'); } else { echo $login; }?>" maxlength1="5">
                          </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Confirm Password *') }}</label>
                            <div class="col-sm-8">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" value="" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Admin Priv. *') }}</label>
                            <div class="col-sm-8 check-f">
                                <span class="mr-1"><input id="admin_priv" type="radio" class="" name="admin_priv" value="1" 
                                @if($admin_priv==1 && $admin_priv!=-1){{ "checked" }} @endif>Yes</span>
                                <span class="mr-1"><input id="admin_priv" type="radio" class="" name="admin_priv" value="0" 
                                @if($admin_priv==0 && $admin_priv!=-1){{ "checked" }} @endif>No</span>
                          </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('
                    Default Language') }}</label>
                            <div class="col-sm-8">
                                <select id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" name="language_id">
                                      @if(!$ErpLanguagesList->isEmpty())
                                         @foreach($ErpLanguagesList as $Language)
                                             <option value="{{ $Language->erp_table_id }}" @if($Language->erp_table_id==$language_id) {{ 'selected' }} @endif>{{ $Language->language_name }}</option>
                                         @endforeach
                                      @endif
                                </select>
                          </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Default theme') }}</label>
                            <div class="col-sm-8">
                                <select id="theme_id" class="form-control{{ $errors->has('theme_id') ? ' is-invalid' : '' }}" name="theme_id">
                                    @if(!$ErpThemesList->isEmpty())
                                        @foreach($ErpThemesList as $Theme)
                                            <option value="{{ $Theme->erp_table_id }}" @if($Theme->erp_table_id==$theme_id) {{ 'selected' }} @endif>
                                                {{ $Theme->theme_name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                          </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('Active *') }}</label>
                            <div class="col-sm-8 check-f">
                                <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==1){{ "checked" }} @endif>Yes</span>
                                <span class="mr-1"> <input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0){{ "checked" }} @endif>No</span>
                          </div>
                        </div>
                        <div class="form-group form-flex">
                            <label class="control-label col-sm-4">{{ __('User *') }}</label>
                            <div class="col-sm-8">
                                <select id="User_id" class="form-control{{ $errors->has('User_id') ? ' is-invalid' : '' }}" name="User_id">
                                      
                                </select>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>
</form>