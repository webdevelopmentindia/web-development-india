<?php
$erp_table_id = $app_name = $description = "";
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpApplicationDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpApplicationDetails[0]->id;
    $app_name = $ErpApplicationDetails[0]->app_name;
    $description = $ErpApplicationDetails[0]->description;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="SecurityApplicationsForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="SecurityApplicationsList" SectionName="SecurityApplicationsSection" TabName="ERPSecurityApplications">
<div class="row">

<div class="card-header header-elements-inline">
    <div class="page-title">
        <h3>
            <?php 
            if($erp_table_id>0)
            {
                echo strtoupper("Update of erp_applications");
            }
            else
            {
                echo strtoupper("New record of erp_applications"); 
            } 
            ?>
        </h3>
    </div>
    <div class="header-elements">
        <span><?php 
            echo " ".date("m/d/Y");
        ?></span>
    </div>
</div> <!-- emloy-hd p-0 -->






    <div class="row row-col">
        <div class="col-xs-12 emloy-hd">
             @if($erp_table_id>0)
             <!-- <a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityApplicationsForm" SectionName="SecurityApplicationsSection"> Add New </a> -->
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <span class="link-src"><button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button></span>
             <span class="link-src"><button type="button" class="btn cancel-btn save-btn" ShowSection="SecurityApplicationsList" SectionName="SecurityApplicationsSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button></span>
        </div>
    </div>
<div class="card-body employee-form">
    <div class="row">
        <div class="col-xs-12">
            <span class="req-field">* Required field(s)</span>
        </div>
    </div>
    <div class="col-md-12 employee-form form-flex">
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Application name *') }}</label>
            <div class="col-sm-9">
                <input id="app_name" type="text" class="form-control" name="app_name" value="{{ $app_name }}" placeholder="Application name">
            </div>
        </div>
        <div class="form-group form-flex">
            <label class="control-label col-sm-3">{{ __('Description') }}</label>
            <div class="col-sm-9">
                <input id="description" type="text" class="form-control" name="description" value="{{ $description }}" placeholder="Description">                
            </div>
        </div>
    </div>
</div>
</div>
</form>