<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\Services\PayUService\Exception;
class ERPFormSubmitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFormAddAjax(Request $request)
    {
        $AddUpdateMessage = "Added";
        $ErpTableId = "";
        if($request->erp_table_id>0)
        {
            $ErpTableId = $request->erp_table_id;
            $AddUpdateMessage = "Updated";
        }
        if($request->ERPFormName=="FileApplicationsAdd")
        {
            $validator = \Validator::make($request->all(), [
                'app_name' => 'required|unique:erp_applications,app_name,'.$ErpTableId,
                'description' => 'required|unique:erp_applications,description,'.$ErpTableId,
                'app_url' => 'required|unique:erp_applications,app_url,'.$ErpTableId,
            ]);
            if ($validator->fails())
            {
                $Message = $validator->errors()->all();
                $Status = "errors";
            }
            else
            {
                try {
                    $ErpApplication = ErpApplication::AddErpApplication($request);
                    $Message = "File Application $AddUpdateMessage Successfully!";
                    $Status = "success";
                } 
                catch (\Exception $e) {
                  $Message = "Error in File Application $AddUpdateMessage " . $e->getMessage();
                  $Status = "error";
                }
            }
        }
        else if($request->ERPFormName=="FileCustomersAdd")
        {
            $validator = \Validator::make($request->all(), [
                'country_id' => 'required',
                'full_name' => 'required|unique:erp_customers,full_name,'.$ErpTableId,
                'tax_id' => 'required|unique:erp_customers,tax_id,'.$ErpTableId,
                'total_licenses' => 'required',
                'address1' => 'required',
            ]);
            if ($validator->fails())
            {
                $Message = $validator->errors()->all();
                $Status = "errors";
            }
            else
            {
                try {
                    $ErpCustomer = ErpCustomer::AddErpCustomer($request);
                    $Message = "File Customer $AddUpdateMessage Successfully!";
                    $Status = "success";
                } 
                catch (\Exception $e) {
                  $Message = "Error in File Customer $AddUpdateMessage " . $e->getMessage();
                  $Status = "error";
                }
            }
        }
        return response()->json(["Status"=>$Status, "Message"=>$Message]);
    }
}