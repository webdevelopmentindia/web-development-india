@if(!(isset($OnlyData) && $OnlyData=="Groups / Applications"))
<div class="ERPTabManuData card tab-pane fade in active" id="ERPSecurityGroupsApplications">
	<div id="SecurityGroupsApplicationsList" class="SecurityGroupsApplicationsSection">
@endif
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>
				   {{ 'Search - sec_groups	' }}
			</h3>
		</div>
		<div class="header-elements">
			<span>
				<?php
			        echo " ".date("m/d/Y");
		        ?>
		    </span>
		</div>
	</div>
<div id="SecurityGroupsListSearch">
<div class="card-body clearfix">
	<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPSecurityGroupsApplicationsRemoveSearchSubmit add-field-listp">
	    @csrf
		<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecurityGroupsApplicationsList" SectionName="SecurityGroupsApplicationsSection" TabName="ERPSecurityGroupsApplications">
		<div class="form-group form-flex">
            <label class="control-label col-sm-3">Group</label>
            <div class="col-sm-9">
               <select id="group_id" name="group_id" class="form-control SecurityGroupsApplicationsPermissionSet">
				    	@if(!$GetAllGroups->isEmpty())
		               	   @foreach($GetAllGroups as $Group)
	                            <option value="{{ $Group->id }}">{{ $Group->description }}</option>
		               	   @endforeach
		               	@endif
		        </select>
        	</div>
        </div>
        <div class="clearfix down-filtr">
            <div class="erp-src-app form-flex justifi-content-center">
            	<div class="col-flex link-src">
            		<a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityGroupsApplicationsForm" SectionName="SecurityGroupsApplicationsSection" editid="@if(!$GetAllGroups->isEmpty()){{ $GetAllGroups[0]->id }}@endif"> OK </a>
	            </div>
        	</div>
        </div>	        
	</form>	
</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Groups / Applications"))
	</div>
	<div id="SecurityGroupsApplicationsForm" style="display: none" class="SecurityGroupsApplicationsSection">
		  
	</div>
	<div id="SecurityGroupsApplicationsSearch" style="display: none" class="SecurityGroupsApplicationsSection">
	</div>
</div>
@endif