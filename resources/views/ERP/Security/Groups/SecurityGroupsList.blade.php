@if(!(isset($OnlyData) && $OnlyData=="Groups"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPSecurityGroups">
	<div id="SecurityGroupsList" class="SecurityGroupsSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
	<div class="page-title">
		<h3>Groups</h3>
	</div>
	<div class="header-elements">
		<span><?php
		        echo " ".date("m/d/Y");
	        ?></span>
	</div>
</div>
<?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 10; //10 for ERP Groups
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
?> 
<div class="row row-col">
	<div class="col-xs-12">
		    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			<span class="link-src"><a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityGroupsForm" SectionName="SecurityGroupsSection"> Add New </a></span>
            @endif
			<!-- <span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="SecurityGroupsSearch" SectionName="SecurityGroupsSection" FilterChangeDivEmpty="ERPSecurityGroupsFilterChange" FormClassName="ERPSecurityGroupsRemoveSearchSubmit"> Search </a></span> -->
	</div>
</div>
<div id="SecurityGroupsListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPSecurityGroupsRemoveSearchSubmit">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecurityGroupsList" SectionName="SecurityGroupsSection" TabName="ERPSecurityGroups">
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPSecurityGroupstable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr>
			    <tr>
			    	<th></th>
			        <th>Group ID</th>
			        <th>Description</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$GetAllGroups->isEmpty())
				    @foreach($GetAllGroups as $Group)
		                <tr>
		                	<td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td>
			                <td>{{ $Group->id }}</td>
			                <td>{{ $Group->description }}</td>
			                <td class="not-export-col">
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="SecurityGroupsForm" SectionName="SecurityGroupsSection" EditId="{{ $Group->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn text-danger ERPRecordDelete" FormName="SecurityGroupsAdd" DeleteId="{{ $Group->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<th></th>
		            <th>Group ID</th>
			        <th>Description</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Groups"))
	</div>
	<div id="SecurityGroupsForm" style="display: none" class="SecurityGroupsSection card">
	</div>
	<div id="SecurityGroupsSearch" style="display: none" class="SecurityGroupsSection card">
	</div>
</div>
@endif