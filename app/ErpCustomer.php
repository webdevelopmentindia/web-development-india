<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class ErpCustomer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpCustomer($request)
    {
        if($request->erp_table_id>0)
        {
            $ErpCustomer = ErpCustomer::find($request->erp_table_id);
        }
        else
        {
            $ErpCustomer = new ErpCustomer();  
        } 

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }

        $ErpCustomer->country_id = $request->country_id;
        $ErpCustomer->full_name = $request->full_name;
        $ErpCustomer->tax_id = $request->tax_id;
        $ErpCustomer->total_licenses = $request->total_licenses;
        $ErpCustomer->address1 = $request->address1;
        $ErpCustomer->address2 = $request->address2;
        $ErpCustomer->office_phone = $request->office_phone;
        $ErpCustomer->mobile_phone = $request->mobile_phone;
        $ErpCustomer->service_status = $request->service_status;
        $ErpCustomer->enabled_flag = $request->enabled_flag;
        $ErpCustomer->ip_address = getUserIpAddr();
        $ErpCustomer->save();
        return $ErpCustomer->id;
    }
    Static Public function GetAllERPCustomers($ParaMeter)
    {
        $GetAllErpCustomers = ErpCustomer::select("erp_customers.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpCustomers =$GetAllErpCustomers->addselect("erp_customers.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpCustomers =$GetAllErpCustomers->where('erp_customers.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_customers.*"); 
        }
        else if(isset($ParaMeter["CustomerUsersCount"]) && $ParaMeter["CustomerUsersCount"]=="CustomerUsersCount")
        {
           $GetAllErpCustomers =$GetAllErpCustomers->join("users","erp_customers.id","users.customer_id")
                    ->addselect("erp_customers.full_name",DB::raw('COUNT(users.id) AS UsersCount'))
                    ->groupBy("erp_customers.id"); 
        }
        else if(isset($ParaMeter["FileCustomerSearch"]) && $ParaMeter["FileCustomerSearch"]!="")
        {
            $FileCustomerSearch = $ParaMeter["FileCustomerSearchArray"];
            if(count($FileCustomerSearch)>0)
            {
              foreach ($FileCustomerSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];
                if(($Search["ColumnName"]=="id" || $Search["ColumnName"]=="country_id") && $ColumnCondition==1) // 1 for Greater Than
                {

                  $GetAllErpCustomers = $GetAllErpCustomers->where("$TableName.$ColumnName",">",$ColumnValue);
                }
                else if(($Search["ColumnName"]=="id" || $Search["ColumnName"]=="country_id") && $ColumnCondition==2) // 2 for Less Than
                {
                  $GetAllErpCustomers = $GetAllErpCustomers->where("$TableName.$ColumnName","<",$ColumnValue);
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpCustomers = $GetAllErpCustomers->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllErpCustomers = $GetAllErpCustomers->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllErpCustomers = $GetAllErpCustomers->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllErpCustomers = $GetAllErpCustomers->whereNull("$TableName.$ColumnName");
                }
              }
            }

            $GetAllErpCustomers = $GetAllErpCustomers->addSelect("erp_customers.*");
        }
        $GetAllErpCustomers = $GetAllErpCustomers->orderBy("erp_customers.id","DESC")
                                            ->get();
        return $GetAllErpCustomers;
    }
    Static Public function DeleteErpCustomers($ParaMeter)
    {
        $ErpCustomer = ErpCustomer::find($ParaMeter["DeleteId"]);
        $ErpCustomer->delete();
        return $ErpCustomer->id;
    }
}