<?php
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Back";
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="SecurityGroupsApplicationsForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="SecurityGroupsApplicationsList" SectionName="SecurityGroupsApplicationsSection" TabName="ERPSecurityGroupsApplications">
<div class="row">
    <div class="col-md-12 emloy-hd">
        <h4>
            <?php 
            if($GetAllGroups[0]->id>0)
            {
                echo strtoupper("Editing - Groups / Applications");
            }
            echo " Group: ".$GetAllGroups[0]->description;
            ?>
            <span class="pull-right">*Required</span></h4>
         @if($GetAllGroups[0]->id>0)
         <!-- <a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityGroupsApplicationsForm" SectionName="SecurityGroupsApplicationsSection"> Add New </a> -->
         <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php echo $GetAllGroups[0]->id; ?>">
         @endif
         <?php 
            $ParaMeter["GroupId"] = Auth::user()->GroupId;
            $ParaMeter["AppId"] = 12; //12 for ERP Groups / Applications
            $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
         ?> 
         @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
         <button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button>
         @endif
         <button type="button" class="btn cancel-btn" ShowSection="SecurityGroupsApplicationsList" SectionName="SecurityGroupsApplicationsSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button>
    </div>
    <div class="col-md-12 employee-form">
        <?php
        //echo "<pre>";
        //print_r($ApplicationDetails);
        ?>
        <table id="ERPSecurityGroupsApplicationstable" class="display table table-bordered" style="width:100%;">
            <thead>
                <tr>
                    <th>Application name</th>
                    <th>Access</th>
                    <th>Insert</th>
                    <th>Delete</th>
                    <th>Update</th>
                    <th>Export</th>
                    <!-- <th>Print</th> -->
                </tr>
            </thead>
            <tbody>
                @if(!$ApplicationDetails->isEmpty())
                    <?php 
                    // echo "<pre>";
                    // print_r($ApplicationDetails);
                    $Index = 1;
                    ?>
                    @foreach($ApplicationDetails as $Application)
                        <tr>
                            <td>{{ $Application->app_name }}</td>
                            <td><!-- <input type="hidden" name="permissions[]" value="{{ $Application->app_name }}"> -->
                            <input type="checkbox" name="permissions[{{ $Application->erp_table_id }}][]" value="access" id="permissions_access_{{ $Application->erp_table_id }}" @if($Application->priv_access=="Y") {{ 'checked' }} @endif></td>
                            <td><input type="checkbox" name="permissions[{{ $Application->erp_table_id }}][]" value="insert" class="CheckPermissionAccess" AccessId="{{ $Application->erp_table_id }}" @if($Application->priv_insert=="Y") {{ 'checked' }} @endif></td>
                            <td><input type="checkbox" name="permissions[{{ $Application->erp_table_id }}][]" value="delete" class="CheckPermissionAccess" AccessId="{{ $Application->erp_table_id }}" @if($Application->priv_delete=="Y") {{ 'checked' }} @endif></td>
                            <td><input type="checkbox" name="permissions[{{ $Application->erp_table_id }}][]" value="update" class="CheckPermissionAccess" AccessId="{{ $Application->erp_table_id }}" @if($Application->priv_update=="Y") {{ 'checked' }} @endif></td>
                            <td><input type="checkbox" name="permissions[{{ $Application->erp_table_id }}][]" value="export" class="CheckPermissionAccess" AccessId="{{ $Application->erp_table_id }}"@if($Application->priv_export=="Y") {{ 'checked' }} @endif></td>
                            <!-- <td><input type="checkbox" name="permissions[{{ $Application->erp_table_id }}][]" value="print" class="CheckPermissionAccess" AccessId="{{ $Application->erp_table_id }}" @if($Application->priv_print=="Y") {{ 'checked' }} @endif></td> -->
                        </tr>
                        <?php 
                        $Index++;
                        ?>
                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <th>Application name</th>
                    <th>Access</th>
                    <th>Insert</th>
                    <th>Delete</th>
                    <th>Update</th>
                    <th>Expor</th>
                    <!-- <th>Print</th> -->
                </tr>
            </tfoot>
        </table>
    </div>
</div>
* Required field(s)
</form>