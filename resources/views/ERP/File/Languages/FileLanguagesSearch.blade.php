@if(!isset($ParaMeter["ERPTableId"]))
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="card-header header-elements-inline">
    <div class="page-title">
        <h3>
        	<?php 
			    echo strtoupper("Search of erp_languages");
			?>
        </h3>
    </div>
    <div class="header-elements">
        <span>
        	<?php 
			    echo " ".date("m/d/Y");
			?>
        </span>
    </div>
</div>
<div class="card-body">
	<div class="row">
	    <div class="col-md-12 emloy-hd">
	    </div>
	    <div class="col-md-12 employee-form">
	    	<form method="POST" aria-label="{{ __('ERPSearchSubmit') }}" enctype="multipart/form-data" id="ERPSearchSubmit" name="ERPSearchSubmit" FormParentDivId="FileLanguagesSearch">
		    @csrf
			<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileLanguagesList" SectionName="FileLanguagesSection" TabName="ERPFileLanguages">
            <div id="ERPChangeFilter">
@endif	
		
			<?php 
			    $LanguageCondition = $ParaMeter["LanguageCondition"];
			    $Language = $ParaMeter["Language"];
			    $EnabledFlagCondition = $ParaMeter["EnabledFlagCondition"];
			    $EnabledFlag = $ParaMeter["EnabledFlag"];
			    $ERPTableId = "";
			    if(isset($ParaMeter["ERPTableId"]))
			    {
			       $ERPTableId = $ParaMeter["ERPTableId"];
			    }
			?>
	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('Language') }}</label>
				<div class="col-sm-3">
				    <select id="language_condition" name="language_condition" class="form-control ERPSearchEmptyContent" FieldId="language">
			        	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LanguageCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="language" type="text" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }} SearchFieldShow" name="language"  autofocus placeholder="Language" value="<?php echo $Language; ?>" style='@if($LanguageCondition==4) {{ "display: none" }} @endif'>
	                <!-- 4 for Empty Content --> 
	            </div>
	        </div>
            <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('Active') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
			               	   @if($RadioOption->option_name=="Equal")
		                            <option value="{{ $RadioOption->id }}" @if($EnabledFlagCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		                       @endif
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6 check-f">
	            	<!-- <label class="switch">
	                  <input type="checkbox" value="1" checked name="enabled_flag1[]">
	                  <span class="slider round"></span>
	                </label>Yes
	                <label class="switch">
	                  <input type="checkbox" value="0" name="enabled_flag1[]">
	                  <span class="slider round"></span>
	                </label>No -->
	                 <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($EnabledFlag==NULL || $EnabledFlag==1 ) {{ 'checked' }} @endif>Yes</span>
	                 <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($EnabledFlag==0 && $EnabledFlag!=NULL) {{ 'checked' }} @endif>No</span>
	            </div>
	        </div>
	        <div class="clearfix down-filtr">
			    <div class="erp-src-app form-flex justifi-content-center">
			    	<div class="col-flex link-src">
		               <button type="submit" class="btn save-btn">Search</button>
		               <a type="button" onclick="ERPFormReset(this); return false;" class="btn cancel-btn save-btn" >Clear</a>
		           	</div>
		           	<div class="col-flex flex-2 link-src" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
		               <select id="erp_filter_list" name="erp_filter_list" class="form-control" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" FormName="FileLanguagesSearch">
		               	@if(!$GetAllErpSaveFilters->isEmpty())
		               	   <option value=""></option>
		               	   <option value="">Public</option>
		               	   @foreach($GetAllErpSaveFilters as $Filter)
	                            <option value="{{ $Filter->id }}" @if($ERPTableId==$Filter->id) {{ 'selected' }} @endif >{{ $Filter->filter_name }}</option>
		               	   @endforeach
		               	@endif
			           </select>
			       	</div>
			       	<div class="col-flex link-src">
		               <a href="javascript:" type="button" class="btn save-btn SaveFilterModalShow" FormName="FileLanguagesSearch" ModalId="SearchSaveFilter">Save Filter</a>
		               <button type="button" class="btn btn-danger" ShowSection="FileLanguagesList" SectionName="FileLanguagesSection" id="ERPBackCancelButton">Back</button>
		           	</div>
	            </div>
	        </div>
@if(!isset($ParaMeter["ERPTableId"]))
            </div>
	        </form>

	        <div class="col-xs-12 model-d" id="SearchSaveFilter">
		        <div class="src-modal employee-form card">
					<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter </h3>
			            </div>
			            <div class="header-elements">
			                <button id="modelhide" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-group">
					        <div class="form-flex form-group">
					               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control{{ $errors->has('filter_name') ? ' is-invalid' : '' }}" name="filter_name"  autofocus placeholder="Filter Name" value=""></div>
					               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileLanguagesSearch" FormSubmitId="ERPSearchSubmit">Save</button></div>
					        </div>
					        <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
					        	 <div class="col-sm-9">
					               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
					                    @if(!$GetAllErpSaveFilters->isEmpty())
						               	   <option value=""></option>
						               	   <option value="">Public</option>
						               	   @foreach($GetAllErpSaveFilters as $Filter)
					                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
						               	   @endforeach
						               	@endif
					               </select>
					           </div>
				                <div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" FormName="FileLanguagesSearch">Delete</button></div>
					        </div>
				       </div>
				   	</div>
				</div>
			</div>
	    </div>
	</div>
</div>
@endif