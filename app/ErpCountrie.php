<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpCountrie extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpCountrie($request)
    {
        if($request->erp_table_id>0)
        {
            $ErpCountrie = ErpCountrie::find($request->erp_table_id);
        }
        else
        {
            $ErpCountrie = new ErpCountrie();  
        } 

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }

        $ErpCountrie->country_code = $request->code;
        $ErpCountrie->description = $request->description;
        $ErpCountrie->enabled_flag = $request->enabled_flag;
        $ErpCountrie->ip_address = getUserIpAddr();
        $ErpCountrie->save();
        return $ErpCountrie->id;
    }
    Static Public function GetAllErpCountries($ParaMeter)
    {
        $GetAllErpCountries = ErpCountrie::select("erp_countries.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpCountries =$GetAllErpCountries->addselect("erp_countries.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpCountries =$GetAllErpCountries->where('erp_countries.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_countries.*"); 
        }
        else if((isset($ParaMeter["FileCountriesSearch"]) && $ParaMeter["FileCountriesSearch"]!="") && (isset($ParaMeter["FileCountriesSearchArray"]) && count($ParaMeter["FileCountriesSearchArray"])>0))
        {
            $FileCountriesSearch = $ParaMeter["FileCountriesSearchArray"];
            if(count($FileCountriesSearch)>0)
            {
              foreach ($FileCountriesSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];

                if($Search["ColumnName"]=="id" && $ColumnCondition==1) // 1 for Greater Than
                {
                  $GetAllErpCountries = $GetAllErpCountries->where("$TableName.$ColumnName",">",$ColumnValue);
                }
                else if($Search["ColumnName"]=="id" && $ColumnCondition==2) // 2 for Less Than
                {
                  $GetAllErpCountries = $GetAllErpCountries->where("$TableName.$ColumnName","<",$ColumnValue);
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpCountries = $GetAllErpCountries->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllErpCountries = $GetAllErpCountries->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllErpCountries = $GetAllErpCountries->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllErpCountries = $GetAllErpCountries->whereNull("$TableName.$ColumnName");
                }
              }
            }
            $GetAllErpCountries = $GetAllErpCountries->addSelect("erp_countries.*");
        }
        $GetAllErpCountries = $GetAllErpCountries->orderBy("erp_countries.id","DESC")
                                            ->get();
        return $GetAllErpCountries;
    }
    Static Public function DeleteErpCountries($ParaMeter)
    {
        $ErpCountrie = ErpCountrie::find($ParaMeter["DeleteId"]);
        $ErpCountrie->delete();
        return $ErpCountrie->id;
    }
}