<?php
$erp_table_id = $app_name = $description = $app_url = $app_icon = $app_picture = "";
$app_deployed = $enabled_flag = -1;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ERPApplicationDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ERPApplicationDetails[0]->erp_table_id;
    $app_name = $ERPApplicationDetails[0]->app_name;
    $description = $ERPApplicationDetails[0]->description;
    $app_url = $ERPApplicationDetails[0]->app_url;
    $app_icon = $ERPApplicationDetails[0]->app_icon;
    $app_picture = $ERPApplicationDetails[0]->app_picture;
    $app_deployed = $ERPApplicationDetails[0]->app_deployed;
    $enabled_flag = $ERPApplicationDetails[0]->enabled_flag;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="FileApplicationForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="FileApplicationList" SectionName="FileApplicationsSection" TabName="ERPFileApplications">
<div class="row">
        <div class="card-header header-elements-inline">
            <div class="page-title">
                <h3>
                    <?php 
                    if($erp_table_id>0)
                    {
                        echo strtoupper("Update of erp_applications");
                    }
                    else
                    {
                        echo strtoupper("New record of erp_applications"); 
                    } 
                    
                    ?>
                </h3>
            </div>
            <div class="header-elements">
                <span><?php 
                    echo " ".date("m/d/Y");
                ?></span>
            <span class="pull-right"></span>
            </div>
        </div> <!-- emloy-hd p-0 -->


    <div class="row row-col">
        <div class="col-xs-12">
            @if($erp_table_id>0)
             <span class="link-src"><a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileApplicationForm" SectionName="FileApplicationsSection" > Add New </a></span>
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <span class="link-src"><button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button></span>
             <span class="link-src"><button type="button" class="btn cancel-btn save-btn" ShowSection="FileApplicationList" SectionName="FileApplicationsSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button></span>
         </div>
     </div>
    <div class="card-body employee-form">
        <div class="row">
            <div class="col-xs-12">
                <span class="req-field">* Required field(s)</span>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 p-0">
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('App name *') }}</label>
                    <div class="col-sm-9">
                        <input id="app_name" type="text" class="form-control{{ $errors->has('app_name') ? ' is-invalid' : '' }}" name="app_name"  autofocus placeholder="App Name" value="<?php if(old('app_name')) { echo old('app_name'); } else { echo $app_name; }?>">
                        @if($errors->has('app_name'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('app_name') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Description  *') }}</label>
                    <div class="col-sm-9">
                        <input id="description " type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description"  autofocus placeholder="Description" value="<?php if(old('description')) { echo old('description'); } else { echo $description; }?>">
                        @if($errors->has('description'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('description') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('App URL *') }}</label>
                    <div class="col-sm-9">
                        <input id="app_url" type="text" class="form-control{{ $errors->has('app_url') ? ' is-invalid' : '' }}" name="app_url"  autofocus placeholder="App URL" value="<?php if(old('app_url')) { echo old('app_url'); } else { echo $app_url; }?>">
                        @if($errors->has('app_url'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('app_url') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('App icon') }}</label>
                    <div class="col-sm-9">
                        <input id="app_icon" type="file" class="form-control{{ $errors->has('app_icon') ? ' is-invalid' : '' }}" name="app_icon"  autofocus placeholder="" value="<?php if(old('app_icon')) { echo old('app_icon'); } else { echo $app_icon; }?>">
                        @if($errors->has('app_icon'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('app_icon') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('App image') }}</label>
                    <div class="col-sm-9">
                        <input id="app_picture" type="file" class="form-control{{ $errors->has('app_picture') ? ' is-invalid' : '' }}" name="app_picture"  autofocus placeholder="" value="<?php if(old('app_picture')) { echo old('app_picture'); } else { echo $app_picture; }?>">
                        @if($errors->has('app_picture'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('app_picture') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('App deployed? *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="app_deployed" type="radio" class="" name="app_deployed" value="1" @if($app_deployed==-1 || $app_deployed==1) {{ 'checked' }} @endif>Yes</span>
                        <span class="mr-1"><input id="app_deployed" type="radio" class="" name="app_deployed" value="0" @if($app_deployed==0) {{ 'checked' }} @endif>No</span>
                        @if($errors->has('app_deployed'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('app_deployed') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Active *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==-1 || $enabled_flag==1) {{ 'checked' }} @endif>Yes</span>
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0) {{ 'checked' }} @endif>No</span>
                        @if($errors->has('enabled_flag'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('enabled_flag') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>