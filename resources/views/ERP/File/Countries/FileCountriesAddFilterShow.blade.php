<div class="close-btn HideAddFilterModal" ModalId="FileCountriesAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileCountriesCountryId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Country ID') }}</label>
	<div class="col-sm-3 p-0">
	    <select id="country_id_condition" name="country_id_condition" class="form-control" FieldId="app_name">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="country_id" type="number" class="form-control" name="country_id"  autofocus placeholder="Country ID" value=""> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="country_id_condition" FormName="FileCountriesList" FieldName="country_id"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCountriesCode")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Code') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="code_condition" name="code_condition" class="form-control ERPSearchEmptyContent" FieldId="code">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="code" type="text" class="form-control" name="code"  autofocus placeholder="Code" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileCountriesList" FieldNameCondition="code_condition" FieldName="code"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCountriesDescription")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Description') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="description_condition" name="description_condition" class="form-control ERPSearchEmptyContent" FieldId="description">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="description" type="text" class="form-control" name="description"  autofocus placeholder="Description" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileCountriesList" FieldNameCondition="description_condition" FieldName="description"> Apply </a></div>
</div>
@endif