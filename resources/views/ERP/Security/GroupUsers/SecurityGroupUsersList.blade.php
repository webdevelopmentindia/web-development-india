@if(!(isset($OnlyData) && $OnlyData=="Group users"))
<div class="ERPTabManuData card tab-pane fade in active" id="ERPSecurityGroupUsers">
	<div id="SecurityGroupUsersList" class="SecurityGroupUsersSection">
@endif


<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>
				@if($SearchShow=="Show")
				   {{ 'Group Users' }}
				@else	
				   {{ 'List - sec_users' }}
				@endif
			</h3>
		</div>
		<div class="header-elements">
			<span>
				<?php
			        echo " ".date("m/d/Y");
		        ?>
		    </span>
		</div>
	</div>
@if($SearchShow!="Show")
<div class="row row-col">
	<div class="col-xs-12">
		<span class="link-src">
			<a href="javascript:" class="ERPManuData btn save-btn" ManuName="ERPSecurityGroupUsers" SectionName="SecurityGroupUsersSection" FormName="SecurityGroupUsersList" GroupId="{{ $SecuritySearchGroupId }}"> Back </a>
		</span>
	</div>
</div>
	
@endif
<div id="SecurityGroupsListSearch">
@if($SearchShow=="Show")
<div class="card-body clearfix">
	<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPSecurityGroupsRemoveSearchSubmit add-field-listp">
	    @csrf
		<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecurityGroupUsersList" SectionName="SecurityGroupUsersSection" TabName="ERPSecurityGroupUsers">
		<div class="form-group form-flex">
            <label class="control-label col-sm-3">Group</label>
            <div class="col-sm-9">
               <select id="group_id" name="group_id" class="form-control">
				    	@if(!$GetAllGroups->isEmpty())
		               	   @foreach($GetAllGroups as $Group)
	                            <option value="{{ $Group->id }}">{{ $Group->description }}</option>
		               	   @endforeach
		               	@endif
		        </select>
        	</div>
        </div>
        <div class="clearfix down-filtr">
            <div class="erp-src-app form-flex justifi-content-center">
            	<div class="col-flex link-src">
            		<a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SecurityGroupUsersList" FieldNameCondition="group_id" FieldName="ERPSearchName"> Search </a>
	            	<a type="button" onclick="ERPSecurityGroupUsersFormReset('SecurityGroupUsersList'); return false;" class="btn cancel-btn save-btn" >Clear</a>
	            </div>
	            <div class="col-flex flex-2 link-src" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div"> 
	            	<select id="erp_filter_list" name="erp_filter_list" class="form-control"  FormName="SecurityGroupUsersList" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif">
	               	@if(!$GetAllErpSaveFilters->isEmpty())
	               	    <option value=""></option>
	               	    <option value="">Public</option>
	               	    @foreach($GetAllErpSaveFilters as $Filter)
                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
	               	    @endforeach
	               	@endif
		           </select>
		       	</div>
	           	<div class="col-flex link-src">
	           		<button type="button" class="btn save-btn SaveFilterModalShow" FormName="SecurityGroupUsersList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPSecurityGroupUsersFilterChange" HideAddFilterPopup="SecurityGroupUsersAddFilterShow">Save Filter</button>
	           	</div>
        	</div>
        </div>
		      
			        
	</form>	

<!-- Save Filter Start -->
<div class="col-xs-12 model-d" id="SearchSaveFilter">
	<div class="src-modal employee-form card">
		<div class="card-header header-elements-inline head-save">
            <div class="page-title">
                <h3>Save Filter</h3>
            </div>
            <div class="header-elements">
                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
            </div>
        </div>
        <div class="card-body">
	        <div class="form-flex form-group">
	               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
	               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="SecurityGroupUsersList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
	        </div>
	        
			  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
	            <div class="col-sm-9">
	               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
	               	@if(!$GetAllErpSaveFilters->isEmpty())
	                    <option value=""></option>
		               	<option value="">Public</option>
				        @foreach($GetAllErpSaveFilters as $Filter)
				   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
				   	    @endforeach
				   	@endif
			      </select>
			  	</div>
			  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="SecurityGroupUsersList">Delete</button></div>
			  </div>
       </div>
    </div>
</div>
<!-- Save Filter Start -->
</div>
@endif
</div>
@if($SearchShow!='Show')
<div class="card-body">
	<div class="table-responsive">
		<table id="ERPSecurityGroupUserstable" class="display table table-bordered" style="width:100%;">
			<thead>
			    <tr>
			        <th>Login</th>
			        <th>Full Name</th>
			        <th>E-mail</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$GetAllGroupUsers->isEmpty())
				    @foreach($GetAllGroupUsers as $User)
		                <tr>
			                <td>{{ $User->login }}</td>
			                <td>{{ $User->name }}</td>
			                <td>{{ $User->email }}</td>
			                <td>
			                	@if($User->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($User->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td class="not-export-col">
			                	 <a href="javascript:" class="btn text-primary ERPFormShow" FormName="SecurityGroupUsersForm" SectionName="SecurityGroupUsersSection" EditId="{{ $User->erp_table_id }}" GroupId="{{ $SecuritySearchGroupId }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
			                     <a href="javascript:" class="btn text-danger ERPRecordDelete" FormName="SecurityGroupUsersAdd" DeleteId="{{ $User->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i>  </a>
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		            <th>Login</th>
			        <th>Full Name</th>
			        <th>E-mail</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@endif
@if(!(isset($OnlyData) && $OnlyData=="Group users"))
	</div>
	<div id="SecurityGroupUsersForm" style="display: none" class="SecurityGroupUsersSection">
	</div>
	<div id="SecurityGroupUsersSearch" style="display: none" class="SecurityGroupUsersSection">
	</div>
</div>
@endif