<div class="close-btn HideFilterChangeModal" ModalClass="ERPSecuritySystemLogFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="SecuritySystemLogLogin")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Login') }}</label>
    <div class="col-sm-3">
	    <select id="login_condition_change" name="login_condition_change" class="form-control ERPSearchEmptyContent" FieldId="login_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="login_change" type="text" class="form-control" name="login_change"  autofocus placeholder="Login" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SecuritySystemLogLogin" FieldNameCondition="login_condition_change" FieldName="login_change" FormName="SecuritySystemLogList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SecuritySystemLogDate")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Date') }}</label>
    <div class="col-sm-3">
      <select id="date_condition_change" name="date_condition_change" class="form-control ERPSearchEmptyContent" FieldId="date_change">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="date_change" type="text" class="form-control" name="date_change"  autofocus placeholder="Date" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SecuritySystemLogDate" FieldNameCondition="date_condition_change" FieldName="date_change" FormName="SecuritySystemLogList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SecuritySystemLogSession")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Session') }}</label>
    <div class="col-sm-3">
      <select id="session_condition_change" name="session_condition_change" class="form-control ERPSearchEmptyContent" FieldId="session_change">
       @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="session_change" type="text" class="form-control" name="session_change"  autofocus placeholder="Session" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SecuritySystemLogSession" FieldNameCondition="session_condition_change" FieldName="session_change" FormName="SecuritySystemLogList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SecuritySystemLogIp")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('IP') }}</label>
    <div class="col-sm-3">
      <select id="ip_condition_change" name="ip_condition_change" class="form-control ERPSearchEmptyContent" FieldId="ip_change">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="ip_change" type="text" class="form-control" name="ip_change"  autofocus placeholder="IP" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SecuritySystemLogIp" FieldNameCondition="ip_condition_change" FieldName="ip_change" FormName="SecuritySystemLogList"> Apply </a></div>
</div>
@endif