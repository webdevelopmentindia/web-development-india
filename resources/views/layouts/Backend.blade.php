<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <title>{{ config('app.name', 'Delta ERP') }}</title> -->
    <title>Delta ERP</title>
    <!-- Scripts -->
    <!-- <script src="{{ asset('public/js/app.js') }}" defer></script> -->
    <!-- Fonts laravel default-->
    <!-- <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css"> -->

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
    

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('public/css/crm_style.css') }}">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css">
    <link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
    <style type="text/css">
        thead input {
            width: 100%;
        }
    </style>
    <!-- <link href="{{ asset('public/css/app.css') }}" rel="stylesheet"> -->
    <!-- Scripts -->
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var Csrf ="{{ csrf_token() }}"; 
    </script>
</head>
<body>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid p-0">
        <div class="navbar-header logo-admin">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand-logo" href="{{ url('/home') }}">DELTA ERP</a>
            <a class="navbar-brand" href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right right-menu">
                <?php
                if(Auth::check()) 
                {
                ?>
                <!-- <li class="dropdown menu-h">
                    <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">File</button>
                      <ul class="dropdown-menu">
                        <li class="ERPManuData" ManuName="ERPFileApplications" SectionName="FileApplicationsSection" FormName="FileApplicationList">{{ __('ERP Applications') }}</li>
                        <li class="ERPManuData" ManuName="ERPFileCustomers">{{ __('ERP Customers') }}</li>
                        <li class="ERPManuData" ManuName="ERPFileCountries">{{ __('Countries') }}</li>
                        <li class="ERPManuData" ManuName="ERPFileSources">{{ __('Sources') }}</li>
                        <li class="ERPManuData" ManuName="ERPFileThemes">{{ __('ERP Themes') }}</li>
                        <li class="ERPManuData" ManuName="ERPFileLanguages">{{ __('ERP Languages') }}</li>
                      </ul>
                </li> -->
                <li class="menu-h dropdown">
                    <a href="javascript:void(0)" class=" dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('public/images/ERP/flag-icon/english.png') }}" style="margin-right: 5px;">English<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#"><img src="{{ asset('public/images/ERP/flag-icon/english.png') }}" style="margin-right: 5px;">English</a></li>
                      <li><a href="#"><img src="{{ asset('public/images/ERP/flag-icon/spanish.png') }}" style="margin-right: 5px;">Spanish</a></li>
                    </ul>
                </li>
                <li class="menu-h dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Company<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Company-1</a></li>
                      <li><a href="#">Company-2</a></li>
                    </ul>
                </li>
                <!-- <li class="menu-h dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Admin<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">English</a></li>
                      <li><a href="#">Spanish</a></li>
                    </ul>
                </li> -->
                <li class="dropdown menu-h with-admin">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><img src="{{ asset('public/images/ERP/ApplicationIcon/admin.png') }}" style="width: 38px;margin-right: 8px;">{{ Auth::user()->email }}<span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" id="LogOutErp">
                                        {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form></li>
                      </ul>
                </li>
                <?php
                } 
                ?>
            </ul>
        </div>
    </div>
</nav>
        @yield('content')
        <footer class="navbar-fixed-bottom">
            <div class="container-fluid">
                <div class="footer">
                    <div class="col-md-6 pull-left">
                        <ul class="footer_page">
                            <!-- <li><a href="#">About Us</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Help Me!</a></li> -->
                        </ul>
                    </div>
                    <div class="col-md-6 pull-right">
                        <p class="text-right">Delta ERP by Delta Consulting, Copyright © 2018 - 2019. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script>

    $("#menu-toggle").click(function(e) {
        e.preventDefault();;
        $("#wrapper").toggleClass("toggled");
    });
        </script>
    <script type="text/javascript">
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
    </script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>
<script src="https://cdn.datatables.net/colreorder/1.5.1/js/dataTables.colReorder.min.js"></script>
<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
<script>
    function DataTableDraw(ManuName,ExportPermission)
    {
    //var table = $('#table').DataTable();
    // indivisiual column search in datatables start
    // $('#'+ManuName+'table thead tr').clone(true).appendTo( '#'+ManuName+'table thead' );
    // $('#'+ManuName+'table thead tr:eq(1) th').each( function (i) {
    //     var title = $(this).text();
    //     if(title!="")
    //     {
    //         $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
    //         $( 'input', this ).on( 'keyup change', function () {
    //             if ( table.column(i).search() !== this.value ) {
    //                 table
    //                     .column(i)
    //                     .search( this.value )
    //                     .draw();
    //             }
    //         }); 
    //     }
        
    // });
    // indivisiual column search in datatables end
    var not_show = [];
    var not_visible = [];
    var FixedColumnsLeft = 0;
    var FixedColumnsRight = 2;
    if(ManuName=="ERPFileApplications" || ManuName=="ERPFileCustomers")
    {
        not_show = [5];
        not_visible = [7,8];
    }
    else if(ManuName=="ERPFileCountries")
    {
        not_show = [4,5];
        not_visible = [3,6];
    }
    else if(ManuName=="ERPFileSources")
    {
        not_show = [5,6];
        not_visible = [3,7];
    }
    else if(ManuName=="ERPFileThemes")
    {
        not_show = [3,4,5];
        not_visible = [6];
    }
    else if(ManuName=="ERPFileLanguages")
    {
        not_show = [3,4,5];
        not_visible = [6];
    }
    else if(ManuName=="ERPSetupUsers")
    {
        not_show = [5];
        not_visible = [7];
    }
    else if(ManuName=="ERPSetupUsersGroups")
    {
        //not_show = [6,7];
        not_visible = [4];
    }
    else if(ManuName=="ERPSecuritySystemLog")
    {
        not_show = [3];
        not_visible = [0];
        FixedColumnsLeft = 0;
        FixedColumnsRight = 0;
    }
    else if(ManuName=="ERPSecurityGroupUsers")
    {
        //not_show = [];
        not_visible = [4];
    }
    else if(ManuName=="ERPReportsReportOfCustomerUsers")
    {
        not_show = [2];
        not_visible = [5,6,7];
    }

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

    var ExportButton = {
                extend: 'collection',
                text: 'Export',
                buttons: [
                    {
                        extend: 'copyHtml5',
                        exportOptions: {
                            //columns: [ 0, ':visible' ]
                            //columns: ':visible:not(:eq(0))'
                            columns: ':visible'
                        },
                        title: "test "+date,
                    },
                    {
                        extend: 'excelHtml5',
                        exportOptions: {
                            //columns: ':visible:not(:eq(0))'
                            columns: ':visible'
                        },
                        title: "test "+date,
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            //columns: [ 0, 1, 2, 5 ]
                            //columns: ':visible:not(:eq(0))'
                            columns: ':visible'
                        },
                        title: "test "+date,
                    },
                    {
                        extend: 'csvHtml5',
                        exportOptions: {
                            //columns: [ 0, 1, 2, 5 ]
                            //columns: ':visible:not(:eq(0))'
                            columns: ':visible'
                        },
                        title: "test "+date,//filename show not show title
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            //columns: [ 0, 1, 2, 5 ]
                            //columns: ':visible:not(:eq(0))'
                            columns: ':visible'
                        },
                        title: "test "+date,
                    }, 
                ]
            };
    var ButtonsArray = [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)',
                columnText: function ( dt, idx, title ) 
                {
                    //return (idx+1)+': '+title; 
                    return title;//show column visibility
                }
            },
            ExportButton
            //'colvis'
            //'copy', 'csv', 'excel', 'pdf', 'print',
        ];
    if(ExportPermission=="N")
    {
        ButtonsArray = [
            'pageLength',
            {
                extend: 'colvis',
                columns: ':not(.noVis)',
                columnText: function ( dt, idx, title ) 
                {
                    //return (idx+1)+': '+title; 
                    return title;//show column visibility
                }
            }
        ];
    }
    //ButtonsArray[] = ExportButton;
    if(ManuName=="ERPSetupUsersGroups" || ManuName=="ERPSecurityApplications" || ManuName=="ERPSecurityGroups" || ManuName=="ERPReportsReportOfCustomerUsersSummary")
    {
        ButtonsArray = [
            ExportButton
        ];
        if(ExportPermission=="N")
        {
           ButtonsArray = [
           ]; 
        }
    }

    if(ManuName=="ERPSecurityGroupsApplications")
    {
        ButtonsArray = [
        ];
    }
    
    var table = $('#'+ManuName+'table').DataTable({
        "aaSorting": [],// row auto sorting off
        fixedHeader: true,
        //colReorder: true,
        orderCellsTop: true,
        colReorder: {
            fixedColumnsLeft: FixedColumnsLeft,//0 column not colReorder
            fixedColumnsRight: FixedColumnsRight
        },
        dom: 'Bfrtip',
        columnDefs: [
            {
                visible: false, 
                targets: not_show,// defaul set not show 
            },
            { 
                targets: not_visible,
                className: 'noVis',//some column not show in column visibility
            }
        ],
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]//show lenth manu with buttons
        ],
        buttons: ButtonsArray
      });
    }
</script>
<script type="text/javascript">
    var ERPManuDataAjax = "{{ route('ERPManuDataAjax') }}";// get ERP Manu Data Ajax Url Path
    var ERPRecordDeleteAjax = "{{ route('ERPRecordDeleteAjax') }}";// ERP Record Delete Ajax Url Path
    var ERPFormShowAjax = "{{ route('ERPFormShowAjax') }}"; // get ERP Form Show Ajax Url Path
    var ERPFormSubmitAjax = "{{ route('ERPFormSubmitAjax') }}"; // ERP Form Submit Add Ajax Url Path
    var ERPSearchShowAjax = "{{ route('ERPSearchShowAjax') }}"; // ERP Search Show Ajax Url Path
    var ERPSearchSubmitAjax = "{{ route('ERPSearchSubmitAjax') }}"; // ERP Search Submit Ajax Url Path
    var ERPSearchSubmitAjax = "{{ route('ERPSearchSubmitAjax') }}"; // ERP Search Submit Ajax Url Path
    var ERPFilterSaveAjax = "{{ route('ERPFilterSaveAjax') }}"; // ERP Filter Save Ajax Url Path
    var ERPFilterGetAjax = "{{ route('ERPFilterGetAjax') }}"; // ERP Particular Old Filter Get Ajax Url Path
    var ERPFilterDeleteAjax = "{{ route('ERPFilterDeleteAjax') }}"; // ERP Particular Old Filter Delete Ajax Url Path 
    var ERPAddFilterShowAjax = "{{ route('ERPAddFilterShowAjax') }}"; // ERP Particular Add Filter Show Ajax Url Path  
    var ERPFilterChangeAjax = "{{ route('ERPFilterChangeAjax') }}"; // ERP Filter Change Ajax Url Path
    var ERPGetOldFilterDataAjax = "{{ route('ERPGetOldFilterDataAjax') }}"; // ERP Get Old Filter Data Ajax Url Path 
    var ERPFilterSaveGetListDataAjax  = "{{ route('ERPFilterSaveGetListDataAjax') }}"; // ERP Get Old Filter Save Get List Data Ajax Url Path  
    var Csrf = "{{ csrf_token() }}";
</script>
<script>
$(document).ready(function(){
  $("#menu-toggle").click(function(){
    class1 = $("#wrapper").attr("class");
    if (class1 == "wrapper") 
    {
    $(".navbar-brand-logo").html("DELTA ERP");
    $(".navbar-brand-logo").removeClass("logosmall");
  }

    else{
      $(".navbar-brand-logo").html("D");
      $(".navbar-brand-logo").addClass("logosmall");
    }
  });

});
</script>
<script type="text/javascript" src="{{ asset('public/js/BackendCommon.js') }}"></script>
</body>
</html>