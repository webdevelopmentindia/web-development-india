@if(!(isset($OnlyData) && $OnlyData=="ERP Customers"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPFileCustomers">
	<div id="FileCustomerList" class="FileCustomersSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>Customers</h3>
		</div>
		<div class="header-elements">
			<span>
				<?php 
			        echo " ".date("m/d/Y");
		        ?>
			</span>
		</div>
	</div>
	<?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 2; //2 for ERP Customers
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?>
	<div class="row row-col">
		<div class="col-xs-12">
			@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			<span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileCustomerForm" SectionName="FileCustomersSection"> Add New </a>
			</span>
			@endif
			<span class="link-src">
				<a href="javascript:" class="btn save-btn ERPSearchShow" FormName="FileCustomerSearch" SectionName="FileCustomersSection" FilterChangeDivEmpty="ERPFileCustomerFilterChange"> Search </a>
			</span>
		</div>
	</div>
		

        

        <?php 
$CheckParameter = 0;
?>
<div id="FileCustomerListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileCustomerList" SectionName="FileCustomersSection" TabName="ERPFileCustomers">	
@if(isset($ParaMeter))
    @if(
    (((isset($ParaMeter["TaxId"]) && $ParaMeter["TaxId"]!="") && (isset($ParaMeter["TaxIdCondition"]) && $ParaMeter["TaxIdCondition"]!="")) || (isset($ParaMeter["TaxIdCondition"]) && $ParaMeter["TaxIdCondition"]==4))
    ||
    ((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]>=0) && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
    ||
    ((isset($ParaMeter["CountryId"]) && $ParaMeter["CountryId"]>=0) && (isset($ParaMeter["CountryIdCondition"]) && $ParaMeter["CountryIdCondition"]!=""))
    ||
    (((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if(((isset($ParaMeter["TaxId"]) && $ParaMeter["TaxId"]!="") && (isset($ParaMeter["TaxIdCondition"]) && $ParaMeter["TaxIdCondition"]!="")) || (isset($ParaMeter["TaxIdCondition"]) && $ParaMeter["TaxIdCondition"]==4))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchTaxId" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileCustomer" FilterName="FileCustomerTaxId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['TaxIdCondition'] }}" ShowDivClass="ERPFileCustomerFilterChange"
		        FieldNameValue='@if($ParaMeter["TaxIdCondition"]!=4){{$ParaMeter["TaxId"]}} @endif' FormName="FileCustomerList" AddFilterPopup="FileCustomerAddFilterShow">
			        Tax ID: {{ $ParaMeter["TaxIdConditionValue"] }} 
			        @if($ParaMeter["TaxIdCondition"]!=4)
			            {{ $ParaMeter["TaxId"] }} 
			        @endif
	            </div>
	            <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCustomerList" RemoveSearch="SearchTaxId" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileCustomerFilterChange"> <i class="fa fa-times" aria-hidden="true"></i></a>
	        </span>

	        <input type="hidden" id="tax_id" name="tax_id" value='{{ $ParaMeter["TaxId"] }}'>
	        <input type="hidden" id="tax_id_condition" name="tax_id_condition" value='{{ $ParaMeter["TaxIdCondition"] }}'>
	        <div style="display: none" class="ERPFileCustomerFilterChange add-drop-down">
	        </div>
	        
        </div>
   @else
        <?php 
        	$AddFilterOptions.='<li value="FileCustomerTaxId" id="ERPFileCustomerSearchList" name="ERPFileCustomerSearchList" class="ERPAddFilterShow" ShowDivId="FileCustomerAddFilterShow" FilterChangeModal="ERPFileCustomerFilterChange" FormName="FileCustomerList">Tax ID</li>';
		?>
   @endif
   @if((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]>=0) && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCustomerId" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        	<div PageName="ERPFileCustomer" FilterName="FileCustomerCustomerId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CustomerIdCondition'] }}" ShowDivClass="ERPFileCustomerFilterChange"
		        FieldNameValue='{{$ParaMeter["CustomerId"]}}' FormName="FileCustomerList" AddFilterPopup="FileCustomerAddFilterShow">
		        	Customer ID: {{ $ParaMeter["CustomerIdConditionValue"] }} 
		            {{ $ParaMeter["CustomerId"] }}
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCustomerList" RemoveSearch="SearchCustomerId" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i> </a>
		    </span>
            <input type="hidden" name="customer_id" value='{{ $ParaMeter["CustomerId"] }}'>
            <input type="hidden" name="customer_id_condition" value='{{ $ParaMeter["CustomerIdCondition"] }}'>
            <div style="display: none" class="ERPFileCustomerFilterChange add-drop-down">
	        </div>
            
        </div>
    @else
	    <?php 

	    	$AddFilterOptions.='<li value="FileCustomerCustomerId" id="ERPFileCustomerSearchList" name="ERPFileCustomerSearchList" class="ERPAddFilterShow" ShowDivId="FileCustomerAddFilterShow" FilterChangeModal="ERPFileCustomerFilterChange" FormName="FileCustomerList">Customer ID</li>';
		?>
   @endif
   @if((isset($ParaMeter["CountryId"]) && $ParaMeter["CountryId"]>=0) && (isset($ParaMeter["CountryIdCondition"]) && $ParaMeter["CountryIdCondition"]!=""))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCountryId" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        	<div PageName="ERPFileCustomer" FilterName="FileCustomerCountryId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CountryIdCondition'] }}" ShowDivClass="ERPFileCustomerFilterChange"
		        FieldNameValue='{{$ParaMeter["CountryId"]}}' FormName="FileCustomerList" AddFilterPopup="FileCustomerAddFilterShow">
		        	Country ID: {{ $ParaMeter["CountryIdConditionValue"] }} 
		            {{ $ParaMeter["CountryId"] }}
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCustomerList" RemoveSearch="SearchCountryId" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>
		    </span>
            <input type="hidden" name="country_id" value='{{ $ParaMeter["CountryId"] }}'>
            <input type="hidden" name="country_id_condition" value='{{ $ParaMeter["CountryIdCondition"] }}'>
            <div style="display: none" class="ERPFileCustomerFilterChange add-drop-down">
	        </div>
            
        </div>
    @else
	    <?php 
	    $AddFilterOptions.='<li value="FileCustomerCountryId" id="ERPFileCustomerSearchList" name="ERPFileCustomerSearchList" class="ERPAddFilterShow" ShowDivId="FileCustomerAddFilterShow" FilterChangeModal="ERPFileCustomerFilterChange" FormName="FileCustomerList">Country ID</li>';
		?>
   @endif
   @if(((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchFullName" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	            <div PageName="ERPFileCustomer" FilterName="FileCustomerFullName" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['FullNameCondition'] }}" ShowDivClass="ERPFileCustomerFilterChange"
		        FieldNameValue='@if($ParaMeter["FullNameCondition"]!=4){{$ParaMeter["FullName"]}} @endif' FormName="FileCustomerList" AddFilterPopup="FileCustomerAddFilterShow">
		        	Full Name: {{ $ParaMeter["FullNameConditionValue"] }} 
		        	@if(isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!=4)
			            {{ $ParaMeter["FullName"] }} 
			        @endif
			    </div>
			    <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCustomerList" RemoveSearch="SearchFullName" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i> </a>
			</span>
            <input type="hidden" name="full_name" value='{{ $ParaMeter["FullName"] }}'>
            <input type="hidden" name="full_name_condition" value='{{ $ParaMeter["FullNameCondition"] }}'>
            <div style="display: none" class="ERPFileCustomerFilterChange add-drop-down">
	        </div>
            
        </div>
    @else
	    <?php 
	    	$AddFilterOptions.='<li value="FileCustomerFullName" id="ERPFileCustomerSearchList" name="ERPFileCustomerSearchList" class="ERPAddFilterShow" ShowDivId="FileCustomerAddFilterShow" FilterChangeModal="ERPFileCustomerFilterChange" FormName="FileCustomerList">Full Name</li>';
		?>
   @endif
   @if($CheckParameter)
   	<div class="addfield-col">
	   <!-- <select id="ERPFileCustomerSearchList" name="ERPFileCustomerSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="FileCustomerAddFilterShow">
	   	    <option value=''>+ Add Filter</option>
	   	    <?php echo $AddFilterOptions; ?>
	   </select> -->
	   @if($AddFilterOptions!="")
			    <div class="dropdown">
				  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				    <?php echo $AddFilterOptions; ?>
				  </ul>
				</div>
			@endif
		   <div id="FileCustomerAddFilterShow" class="add-drop-down popclose" style="display: none">
		   	
	   		</div>
	</div>
	<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
	   @if(!$GetAllErpSaveFilters->isEmpty())
	       <select id="ERPGetOldFilterData" name="ERPGetOldFilterData" class="form-control ERPSearchBySaveFilter" PageName="ERPFileCustomer" SectionName="FileCustomerList" ManuName="ERPFileCustomers" FormName="">
	       	  <option value=""></option>
	          <option value="">Public</option>
		      @foreach($GetAllErpSaveFilters as $Filter)
		   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
		   	  @endforeach
	      </select>
	   @endif
	   <div id="FileCustomerAddFilterShow" style="display: none">
	   </div>
	</div>






   <!-- GetAllErpSaveFilters -->
	   	<div class="addfield-col">
	   		<button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="FileCustomerList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPFileCustomerFilterChange" HideAddFilterPopup="FileCustomerAddFilterShow">Save Filter</button>
	   	</div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileCustomerList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="FileCustomerList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
	   			<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="FileCustomerList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>
	   		</div>
	   		@endif
	   		@if(
    (((isset($ParaMeter["TaxId"]) && $ParaMeter["TaxId"]!="") && (isset($ParaMeter["TaxIdCondition"]) && $ParaMeter["TaxIdCondition"]!="")) || (isset($ParaMeter["TaxIdCondition"]) && $ParaMeter["TaxIdCondition"]==4))
    ||
    ((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]>=0) && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
    ||
    ((isset($ParaMeter["CountryId"]) && $ParaMeter["CountryId"]>=0) && (isset($ParaMeter["CountryIdCondition"]) && $ParaMeter["CountryIdCondition"]!=""))
    ||
    (((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
    )
    </div>
</div>
   @endif
@endif
</form>
</div>
</div>

	<div class="card">
	<div class="table-responsive">
		<table id="ERPFileCustomerstable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>Customer Id</th>
			        <th>Country ID</th>
			        <th>Full Name</th>
			        <th>Tax ID</th>
			        <th>Total Licenses</th>
			        <th>Address1</th>
			        <th>Service status</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$ERPCustomers->isEmpty())
				    @foreach($ERPCustomers as $Customer)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Customer->erp_table_id }}</td>
			                <td>{{ $Customer->country_id }}</td>
			                <td>{{ $Customer->full_name }}</td>
			                <td>{{ $Customer->tax_id }}</td>
			                <td>{{ $Customer->total_licenses }}</td>
			                <td>{{ $Customer->address1 }}</td>
			                <td>
			                    @if($Customer->service_status==1)
			                	   {{ 'Active' }}
			                	@elseif($Customer->service_status==0)
			                	   {{ 'Suspended' }}
		                	   @elseif($Customer->service_status==-1)
		                	       {{ 'Terminated' }}
			                	@endif
			                </td>
			                <td>
			                    @if($Customer->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Customer->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                 <td class="not-export-col">
			                 	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="FileCustomerForm" SectionName="FileCustomersSection" EditId="{{ $Customer->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i></a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn text-danger ERPRecordDelete" FormName="FileCustomerAdd" DeleteId="{{ $Customer->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i>  </a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		        	<th>Customer Id</th>
			        <th>Country ID</th>
			        <th>Full Name</th>
			        <th>Tax ID</th>
			        <th>Total Licenses</th>
			        <th>Address1</th>
			        <th>Service status</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="ERP Customers"))
	</div>
	<div id="FileCustomerForm" style="display: none" class="FileCustomersSection card">
	</div>
	<div id="FileCustomerSearch" style="display: none" class="FileCustomersSection card">
	</div>
</div>
@endif