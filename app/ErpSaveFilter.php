<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpSaveFilter extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_name', 'filter_name', 'first_column_value','first_column_condition', 'second_column_value', ' second_column_condition','third_column_value', 'third_column_condition', 'fourth_column_value','fourth_column_condition','fifth_column_value','fifth_column_condition'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpSaveFilter($ParaMeter)
    {
        $ErpSaveFilter = ErpSaveFilter::firstOrNew(array('page_name' => $ParaMeter["page_name"],'filter_name' => $ParaMeter["filter_name"])); 
        $ErpSaveFilter->page_name = $ParaMeter["page_name"];
        $ErpSaveFilter->filter_name = $ParaMeter["filter_name"];
        $ErpSaveFilter->first_column_value = $ParaMeter["first_column_value"];
        $ErpSaveFilter->first_column_condition = $ParaMeter["first_column_condition"];
        if(isset($ParaMeter["second_column_value"]) && isset($ParaMeter["second_column_condition"]))
        {
            $ErpSaveFilter->second_column_value = $ParaMeter["second_column_value"];
            $ErpSaveFilter->second_column_condition = $ParaMeter["second_column_condition"];
        }
        if(isset($ParaMeter["third_column_value"]) && isset($ParaMeter["third_column_condition"]))
        {
            $ErpSaveFilter->third_column_value = $ParaMeter["third_column_value"];
            $ErpSaveFilter->third_column_condition = $ParaMeter["third_column_condition"];
        }
        if(isset($ParaMeter["fourth_column_value"]) && isset($ParaMeter["fourth_column_condition"]))
        {
            $ErpSaveFilter->fourth_column_value = $ParaMeter["fourth_column_value"];
            $ErpSaveFilter->fourth_column_condition = $ParaMeter["fourth_column_condition"];
        }
        if(isset($ParaMeter["fifth_column_value"]) && isset($ParaMeter["fifth_column_condition"]))
        {
            $ErpSaveFilter->fifth_column_value = $ParaMeter["fifth_column_value"];
            $ErpSaveFilter->fifth_column_condition = $ParaMeter["fifth_column_condition"];
        }
        if(isset($ParaMeter["six_column_value"]) && isset($ParaMeter["six_column_condition"]))
        {
            $ErpSaveFilter->six_column_value = $ParaMeter["six_column_value"];
            $ErpSaveFilter->six_column_condition = $ParaMeter["six_column_condition"];
        }
        $ErpSaveFilter->save();
        return $ErpSaveFilter->id;
    }
    Static Public function GetAllErpSaveFilters($ParaMeter)
    {
        $GetAllErpSaveFilters = ErpSaveFilter::select("erp_save_filters.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpSaveFilters =$GetAllErpSaveFilters->addselect("erp_save_filters.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpSaveFilters =$GetAllErpSaveFilters->where('erp_save_filters.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_save_filters.*"); 
        }
        else if(isset($ParaMeter["PageName"]) && $ParaMeter["PageName"]!="")
        {
            $GetAllErpSaveFilters =$GetAllErpSaveFilters->where('erp_save_filters.page_name',$ParaMeter["PageName"])
                ->addselect("erp_save_filters.*"); 
        }
        $GetAllErpSaveFilters = $GetAllErpSaveFilters->orderBy("erp_save_filters.id","DESC")
            ->get();
        return $GetAllErpSaveFilters;
    }
    Static Public function DeleteErpSaveFilters($ParaMeter)
    {
        $ErpSaveFilter = ErpSaveFilter::find($ParaMeter["ERPTableId"]);
        $ErpSaveFilter->delete();
        return $ErpSaveFilter->page_name;
    }
}