<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class Application extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];

    static public function AddApplication($request)
    {
        if($request->erp_table_id!="")
        {
            $Application = Application::find($request->erp_table_id);
        }
        else
        {
            $Application = new Application();  
        }

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }
        $Application->app_name  = $request->app_name;
        $Application->description  = $request->description;
        $Application->save();
        return $Application->id;
    }
    Static Public function GetAllApplications($ParaMeter)
    {
        $GetAllApplications = Application::select("applications.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllApplications =$GetAllApplications->addselect("applications.*"); 
        }
        else if(isset($ParaMeter["GroupsApplications"]) && $ParaMeter["GroupsApplications"]>0)
        {
           $GroupId = $ParaMeter["GroupsApplications"];
           $GetAllApplications =$GetAllApplications->leftjoin('groups_apps', function($join) use ($GroupId)
                                {
                                  $join->on('groups_apps.app_id', '=', 'applications.id')
                                  ->where('groups_apps.group_id', '=', $GroupId);
                                })
                                ->addselect("applications.*","groups_apps.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]))
        {
            $GetAllApplications =$GetAllApplications->where('applications.id',$ParaMeter["ERPTableId"])
                            ->addselect("applications.*"); 
        }
        $GetAllApplications = $GetAllApplications->orderBy("applications.id","ASC")
                                                 ->get();
        return $GetAllApplications;
    }
    Static Public function DeleteApplications($ParaMeter)
    {
        $Application = Application::find($ParaMeter["DeleteId"]);
        $Application->delete();
        return $Application;
    }
}