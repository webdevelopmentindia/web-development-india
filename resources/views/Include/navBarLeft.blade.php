<div id="wrapper" class="wrapper">
<div id="page-content-wrapper">
<div id="sidebar-wrapper">
<div class="sidenav">
    <ul class="left-side-navbar">
        <li class="ERPManuData" ManuName="ERPDashboard" SectionName="" FormName=""><a class="nav-link" href="javascript:"><i class="fa fa-home" aria-hidden="true"></i> ERP Dashboard </a> </li>
        <li><button class="dropdown-btn"><i class="fa fa-th-large" aria-hidden="true"></i> ERP Management </button>
    <div class="dropdown-container">
    <ul class="navbar-collapse">
        <li><button class="dropdown-btn"><i class="fa fa-file-o" aria-hidden="true"></i> File </button>
            <div class="dropdown-container">
                <ul>
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 1; //1 for ERP Applications
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPFileApplications" SectionName="FileApplicationsSection" FormName="FileApplicationList"><a class="nav-link" href="javascript:">{{ __('ERP Applications') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 2; //1 for ERP Applications
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPFileCustomers" SectionName="FileCustomersSection" FormName="FileCustomerList"><a class="nav-link" href="javascript:">{{ __('ERP Customers') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 3; //1 for ERP Applications
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPFileCountries" SectionName="FileCountriesSection" FormName="FileCountriesList"><a class="nav-link" href="javascript:">{{ __('Countries') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 4; //1 for ERP Applications
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPFileSources" SectionName="FileSourcesSection" FormName="FileSourcesList"><a class="nav-link" href="javascript:">{{ __('Sources') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 5; //1 for ERP Applications
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPFileThemes" SectionName="FileThemesSection" FormName="FileThemesList"><a class="nav-link" href="javascript:">{{ __('ERP Themes') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 6; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPFileLanguages" SectionName="FileLanguagesSection" FormName="FileLanguagesList"><a class="nav-link" href="javascript:">{{ __('ERP Languages') }}</a></li>
                    @endif
                </ul>
            </div>
        </li>
        <li><button class="dropdown-btn"><i class="fa fa-tasks" aria-hidden="true"></i> Setup </button>
            <div class="dropdown-container">
                <ul>
                   <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 7; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                   <li class="ERPManuData" ManuName="ERPSetupUsers" SectionName="SetupUsersSection" FormName="SetupUsersList"><a class="nav-link" href="javascript:">{{ __('ERP Users') }}</a></li>
                   @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 8; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                   <li class="ERPManuData" ManuName="ERPSetupUsersGroups" SectionName="SetupUsersGroupsSection" FormName="SetupUsersGroupsList"><a class="nav-link" href="javascript:">{{ __('Users / Groups') }}</a></li>
                   @endif
                </ul>
            </div>
        </li>
        <li><button class="dropdown-btn"><i class="fa fa-shield" aria-hidden="true"></i> Security </button>
            <div class="dropdown-container">
                <ul>
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 9; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPSecurityApplications" SectionName="SecurityApplicationsSection" FormName="SecurityApplicationsList"><a class="nav-link" href="javascript:">{{ __('Applications') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 10; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPSecurityGroups" SectionName="SecurityGroupsSection" FormName="SecurityGroupsList"><a class="nav-link" href="javascript:">{{ __('Groups') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 11; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                   <li class="ERPManuData" ManuName="ERPSecurityGroupUsers" SectionName="SecurityGroupUsersSection" FormName="SecurityGroupUsersList"><a class="nav-link" href="javascript:">{{ __('Group / Users') }}</a></li>
                   @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 12; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                   <li class="ERPManuData" ManuName="ERPSecurityGroupsApplications" SectionName="SecurityGroupsApplicationsSection" FormName="SecurityGroupsApplicationsList"><a class="nav-link" href="javascript:">{{ __('Groups / Applications') }}</a></li>
                   @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 13; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPSecuritySyncApplications" SectionName="SecuritySyncApplicationsSection" FormName="SecuritySyncApplicationsList"><a class="nav-link" href="javascript:">{{ __('Sync Applications') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 14; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPSecuritySystemLog" SectionName="SecuritySystemLogSection" FormName="SecuritySystemLogList"><a class="nav-link" href="javascript:">{{ __('System Log') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 15; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPSecurityChangePassword" SectionName="SecurityChangePasswordSection" FormName="SecurityChangePasswordList"><a class="nav-link" href="javascript:">{{ __('Change Password') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 16; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li><a class="nav-link" href="javascript:"  onclick="ERPLogOut(); return false;">{{ __('Logout') }}</a></li>
                    @endif
                </ul>
            </div>
        </li>
        <li><button class="dropdown-btn"><i class="fa fa-file-text-o" aria-hidden="true"></i> Reports </button>
            <div class="dropdown-container">
                <ul>
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 17; //1 for ERP Languages
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData" ManuName="ERPReportsReportOfCustomerUsers" SectionName="ReportsReportOfCustomerUsersSection" FormName="ReportsReportOfCustomerUsersList"><a class="nav-link" href="javascript:">{{ __('Report of Customer / Users') }}</a></li>
                    @endif
                    <?php 
                    $ParaMeter["GroupId"] = Auth::user()->GroupId;
                    $ParaMeter["AppId"] = 18; //18 for ERP Report Applications
                    $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
                    ?>
                    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_access=="Y")
                    <li class="ERPManuData1" ManuName="ERPFileLanguages"><a class="nav-link" href="javascript:">{{ __('ERP Applications') }}</a></li>
                    @endif
                </ul>
            </div>
        </li>
    </ul>
</div>
</li>
<!-- <li><button class="dropdown-btn">Financial Management </button>
    <div class="dropdown-container">
    <ul class="navbar-collapse">
        <li><button class="dropdown-btn">Accounting </button>
            </li>
        <li><button class="dropdown-btn">Expenses </button>
            </li>
    </ul>
    </div>
</li>
<li><button class="dropdown-btn">Supply Chain </button>
    <div class="dropdown-container">
    <ul class="navbar-collapse">
        <li><button class="dropdown-btn">Inventory Management </button>
            </li>
        <li><button class="dropdown-btn">Purchase and Imports </button>
            </li>
    </ul>
    </div>
</li>
<li><button class="dropdown-btn">Sales Management </button>
    <div class="dropdown-container">
    <ul class="navbar-collapse">
        <li><button class="dropdown-btn">Point of Sales </button>
            </li>
    </ul>
    </div>
</li>
<li><button class="dropdown-btn">Administration </button>
    <div class="dropdown-container">
    <ul class="navbar-collapse">
        <li><button class="dropdown-btn">Church Management </button>
            </li>
    </ul>
    </div>
</li> -->
</ul>
</div>
</div>