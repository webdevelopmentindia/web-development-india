<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\Services\PayUService\Exception;
class ERPFileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function FileApplicationAddAjax(Request $request)
    {
        // $Status = "error";
        // $Message = "";
        // $AddUpdateMessage = "Added";
        // if($request->app_id>0)
        // {
        //     $AddUpdateMessage = "Updated";
        // }
        // try {
        //   if($request->app_name=="")
        //   {
        //     $Message.="App name : Required Field"; 
        //   }
        //   if($request->description=="")
        //   {
        //     $Message.="Description : Required Field";
        //   }
        //   if($request->app_url=="")
        //   {
        //     $Message.="App URL : Required Field";
        //   }
        //   if($request->app_name!="" && $request->description && $request->app_url)
        //   {
        //     $ErpApplication = ErpApplication::AddErpApplication($request);
        //     $Message = "File Application $AddUpdateMessage Successfully!";
        //     $Status = "success";
        //   }
        // } 
        // catch (\Exception $e) {
        //   $Message = "Error in File Application $AddUpdateMessage " . $e->getMessage();
        // }
        // return json_encode(["Status"=>$Status,"Message"=>$Message]);
        $AddUpdateMessage = "Added";
        $AppId = "";
        if($request->app_id>0)
        {
            $AppId = $request->app_id;
            $AddUpdateMessage = "Updated";
        }

        $validator = \Validator::make($request->all(), [
            'app_name' => 'required|unique:erp_applications,app_name,'.$AppId,
            'description' => 'required|unique:erp_applications,description,'.$AppId,
            'app_url' => 'required|unique:erp_applications,app_url,'.$AppId,
        ]);
        
        if ($validator->fails())
        {
            $Message = $validator->errors()->all();
            $Status = "errors";
        }
        else
        {
            try {
                $ErpApplication = ErpApplication::AddErpApplication($request);
                $Message = "File Application $AddUpdateMessage Successfully!";
                $Status = "success";
            } 
            catch (\Exception $e) {
              $Message = "Error in File Application $AddUpdateMessage " . $e->getMessage();
              $Status = "error";
            }
        }
        return response()->json(["Status"=>$Status, "Message"=>$Message]);
    }
}
