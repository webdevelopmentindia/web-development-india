<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpSaveFilter;
use App\Services\PayUService\Exception;
class ERPFilterSaveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFilterSaveAjax(Request $request)
    {
        $ERPSearchName = "";
        $SaveFilterLists = "";
        try {
            $ParaMeter = array();
            if($request->ERPSearchName=="FileApplicationList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP File Applications";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->app_name;
                $ParaMeter["first_column_condition"] = $request->app_name_condition;
                $ParaMeter["second_column_value"] = $request->app_url;
                $ParaMeter["second_column_condition"] = $request->app_url_condition;
                $ParaMeter["third_column_value"] = $request->description;
                $ParaMeter["third_column_condition"] = $request->description_condition;
                $ParaMeter["fourth_column_value"] = $request->app_deployed;
                $ParaMeter["fourth_column_condition"] = $request->app_deployed_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                $ParaMeter1["PageName"] = "ERP File Applications";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="FileCustomerList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP File Customers";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->tax_id;
                $ParaMeter["first_column_condition"] = $request->tax_id_condition;
                $ParaMeter["second_column_value"] = $request->customer_id;
                $ParaMeter["second_column_condition"] = $request->customer_id_condition;
                $ParaMeter["third_column_value"] = $request->country_id;
                $ParaMeter["third_column_condition"] = $request->country_id_condition;
                $ParaMeter["fourth_column_value"] = $request->full_name;
                $ParaMeter["fourth_column_condition"] = $request->full_name_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP File Customers";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="FileCountriesList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP File Countries";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->country_id;
                $ParaMeter["first_column_condition"] = $request->country_id_condition;
                $ParaMeter["second_column_value"] = $request->code;
                $ParaMeter["second_column_condition"] = $request->code_condition;
                $ParaMeter["third_column_value"] = $request->description;
                $ParaMeter["third_column_condition"] = $request->description_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP File Countries";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="FileSourcesList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP File Sources";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->source_id;
                $ParaMeter["first_column_condition"] = $request->source_id_condition;
                $ParaMeter["second_column_value"] = $request->code;
                $ParaMeter["second_column_condition"] = $request->code_condition;
                $ParaMeter["third_column_value"] = $request->description;
                $ParaMeter["third_column_condition"] = $request->description_condition;
                $ParaMeter["fourth_column_value"] = $request->auto_post;
                $ParaMeter["fourth_column_condition"] = $request->auto_post_condition;
                $ParaMeter["fifth_column_value"] = $request->enabled_flag;
                $ParaMeter["fifth_column_condition"] = $request->enabled_flag_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP File Sources";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="FileThemesList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP File Themes";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->theme;
                $ParaMeter["first_column_condition"] = $request->theme_condition;
                $ParaMeter["second_column_value"] = $request->enabled_flag;
                $ParaMeter["second_column_condition"] = $request->enabled_flag_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP File Themes";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="FileLanguagesList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP File Languages";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->language;
                $ParaMeter["first_column_condition"] = $request->language_condition;
                $ParaMeter["second_column_value"] = $request->enabled_flag;
                $ParaMeter["second_column_condition"] = $request->enabled_flag_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP File Languages";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="SecuritySystemLogList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP Security System Log";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->login;
                $ParaMeter["first_column_condition"] = $request->login_condition;
                $ParaMeter["second_column_value"] = $request->date;
                $ParaMeter["second_column_condition"] = $request->date_condition;
                $ParaMeter["third_column_value"] = $request->session;
                $ParaMeter["third_column_condition"] = $request->session_condition;
                $ParaMeter["fourth_column_value"] = $request->ip;
                $ParaMeter["fourth_column_condition"] = $request->ip_condition;
                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP Security System Log";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="SecurityGroupUsersList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP Security Group Users";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = "";
                $ParaMeter["first_column_condition"] = $request->group_id;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP Security Group Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="SetupUsersList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP Setup Users";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->user_id;
                $ParaMeter["first_column_condition"] = $request->user_id_condition;
                $ParaMeter["second_column_value"] = $request->login;
                $ParaMeter["second_column_condition"] = $request->login_condition;
                $ParaMeter["third_column_value"] = $request->full_name;
                $ParaMeter["third_column_condition"] = $request->full_name_condition;
                $ParaMeter["fourth_column_value"] = $request->e_mail;
                $ParaMeter["fourth_column_condition"] = $request->e_mail_condition;
                $ParaMeter["fifth_column_value"] = $request->admin_priv;
                $ParaMeter["fifth_column_condition"] = $request->admin_priv_condition;
                $ParaMeter["six_column_value"] = $request->customer_id;
                $ParaMeter["six_column_condition"] = $request->customer_id_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP Setup Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            else if($request->ERPSearchName=="ReportsReportOfCustomerUsersList")
            {
                $ERPSearchName = $ParaMeter["page_name"] = "ERP Report Of Customer Users";

                $ParaMeter["filter_name"] = $request->FilterName;

                $ParaMeter["first_column_value"] = $request->customer_id;
                $ParaMeter["first_column_condition"] = $request->customer_id_condition;
                $ParaMeter["second_column_value"] = $request->login;
                $ParaMeter["second_column_condition"] = $request->login_condition;
                $ParaMeter["third_column_value"] = $request->full_name;
                $ParaMeter["third_column_condition"] = $request->full_name_condition;
                $ParaMeter["fourth_column_value"] = $request->e_mail;
                $ParaMeter["fourth_column_condition"] = $request->e_mail_condition;
                $ParaMeter["fifth_column_value"] = $request->admin_priv;
                $ParaMeter["fifth_column_condition"] = $request->admin_priv_condition;
                $ParaMeter["six_column_value"] = $request->enabled_flag;
                $ParaMeter["six_column_condition"] = $request->enabled_flag_condition;

                ErpSaveFilter::AddErpSaveFilter($ParaMeter);
                
                $ParaMeter1["PageName"] = "ERP Report Of Customer Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);
            }
            $SaveFilterLists = view('ERP.Filters.FilterList',compact("GetAllErpSaveFilters"))->render(); 
            $Message = "$ERPSearchName Search Save Successfully!";
            $Status = "success";
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPSearchName Search Save " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"SaveFilterLists"=>$SaveFilterLists]);
    }
}