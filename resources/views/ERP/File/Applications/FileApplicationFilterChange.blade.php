<div class="close-btn HideFilterChangeModal" ModalClass="ERPFileApplicationFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileApplicationAppName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('App name') }}</label>
	  <div class="col-sm-3 p-0">
	    <select id="app_name_condition_changee" name="app_name_condition_change" class="form-control ERPSearchEmptyContent" FieldId="app_name_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	@foreach($GetAllErpSearchTextOptions as $TextOption)
              <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	@endforeach
          @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="app_name_change" type="text" class="form-control" name="app_name_change"  autofocus placeholder="App Name" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileApplicationAppName" FieldNameCondition="app_name_condition_changee" FieldName="app_name_change" FormName="FileApplicationList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileApplicationAppURL")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('App URL') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="app_url_condition_change" name="app_url_condition_change" class="form-control ERPSearchEmptyContent" FieldId="app_url_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="app_url_change" type="text" class="form-control" name="app_url_change"  autofocus placeholder="App URL" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileApplicationAppURL" FieldNameCondition="app_url_condition_change" FieldName="app_url_change" FormName="FileApplicationList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileApplicationDescription")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Description') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="description_condition_change" name="description_condition_change" class="form-control ERPSearchEmptyContent" FieldId="description_change">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="description_change" type="text" class="form-control" name="description_change"  autofocus placeholder="Description" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileApplicationDescription" FieldNameCondition="description_condition_change" FieldName="description_change" FormName="FileApplicationList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileApplicationAppDeployed")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('App deployed?') }}</label>
    <div class="col-sm-6 p-0">
	    <select id="app_deployed_condition_change" name="app_deployed_condition_change" class="form-control">
	    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-3 check-f text-right">
        <span class="mr-1">
          <input id="app_deployed_change" type="radio" class="" name="app_deployed_change" value="1" @if($ParaMeter["FieldNameValue"]==NULL || $ParaMeter["FieldNameValue"]==1 ) {{ 'checked' }} @endif>Yes</span>
        <span class="mr-1">
          <input id="app_deployed_change" type="radio" class="" name="app_deployed_change" value="0" @if($ParaMeter["FieldNameValue"]==0 && $ParaMeter["FieldNameValue"]!=NULL) {{ 'checked' }} @endif>No</span>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileApplicationAppDeployed" FieldNameCondition="app_deployed_condition_change" FieldName="app_deployed_change" FormName="FileApplicationList"> Apply </a></div>
</div>
@endif