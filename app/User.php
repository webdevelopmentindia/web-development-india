<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Auth;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    static public function AddUser($request)
    {
        if($request->erp_table_id>0)
        {
            $User = User::find($request->erp_table_id);
        }
        else
        {
            $User = new User();  
        }

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }
        if($request->login!="")
        {
           $User->login = $request->login;
        }
        if($request->customer_id>=1)
        {
           $User->customer_id = $request->customer_id; 
        }
        if($request->language_id>=1)
        {
           $User->user_language = $request->language_id; 
        }
        if($request->theme_id>=1)
        {
           $User->user_theme = $request->theme_id; 
        }
        if($request->admin_priv>=0)
        {
           $User->admin_priv = $request->admin_priv; 
        }
        if($request->enabled_flag!="" && $request->enabled_flag>=0)
        {
           $User->enabled_flag  = $request->enabled_flag; 
        }
        $User->created_by = Auth::user()->id;
        if($request->name!="")
        {
           $User->name = $request->name; 
        }
        if($request->email!="")
        {
            $User->email = $request->email;
        }
        if($request->password!="")
        {
           $User->password  = Hash::make($request->password); 
        }
        $User->ip_address = getUserIpAddr();
        $User->save();
        return $User->id;
    }
    Static Public function GetAllUsers($ParaMeter)
    {
        $GetAllUsers = User::select("users.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllUsers =$GetAllUsers->leftjoin("erp_customers","erp_customers.id","users.customer_id")
                    ->addselect("users.*","erp_customers.full_name"); 
        }
        else if(isset($ParaMeter["CustomerAllUsers"]) && $ParaMeter["CustomerAllUsers"]=="CustomerAllUsers")
        {
            $GetAllUsers =$GetAllUsers->join("erp_customers","erp_customers.id","users.customer_id")
                    ->addselect("users.*","erp_customers.full_name","erp_customers.id as customer_id")
                    ->orderBy("erp_customers.id","DESC"); 
            if(isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]>=1)
            {
              $GetAllUsers =$GetAllUsers->where("erp_customers.id",$ParaMeter["CustomerId"]); 
            }
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllUsers =$GetAllUsers->where('users.id',$ParaMeter["ERPTableId"])
                            ->addselect("users.*"); 
        }
        else if(isset($ParaMeter["SecurityGroupId"]) && $ParaMeter["SecurityGroupId"]>=0)
        {
            $GetAllUsers = $GetAllUsers->join("user_groups","user_groups.user_id","users.id")
            ->where("user_groups.group_id",$ParaMeter["SecurityGroupId"])
            ->addselect("users.*");
        }
        else if((isset($ParaMeter["SetupUsersSearch"]) && $ParaMeter["SetupUsersSearch"]!="") || (isset($ParaMeter["ReportsReportOfCustomerUsersSearch"]) && $ParaMeter["ReportsReportOfCustomerUsersSearch"]!=""))
        {
            $GetAllUsers =$GetAllUsers->leftjoin("erp_customers","erp_customers.id","users.customer_id");

            $SetupUsersSearch = (isset($ParaMeter["SetupUsersSearch"]) && $ParaMeter["SetupUsersSearch"]!="") ? $ParaMeter["SetupUsersSearchArray"] : $ParaMeter["ReportsReportOfCustomerUsersSearchArray"] ;
            if(count($SetupUsersSearch)>0)
            {
              foreach ($SetupUsersSearch as $Search) 
              {
                //die("fgdfgsdf sdfgs");
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];

                if($Search["ColumnName"]=="id" && $ColumnCondition==1) // 1 for Greater Than
                {
                  $GetAllUsers = $GetAllUsers->where("$TableName.$ColumnName",">",$ColumnValue);
                }
                else if($Search["ColumnName"]=="id" && $ColumnCondition==2) // 2 for Less Than
                {
                  $GetAllUsers = $GetAllUsers->where("$TableName.$ColumnName","<",$ColumnValue);
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllUsers = $GetAllUsers->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllUsers = $GetAllUsers->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllUsers = $GetAllUsers->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllUsers = $GetAllUsers->whereNull("$TableName.$ColumnName");
                }
              }
            }
            $GetAllUsers = $GetAllUsers->addSelect("users.*","erp_customers.full_name");
        }
        $GetAllUsers = $GetAllUsers->orderBy("users.id","DESC")
                                            ->get();
        return $GetAllUsers;
    }
    Static Public function DeleteUsers($ParaMeter)
    {
        $User = User::find($ParaMeter["DeleteId"]);
        $User->delete();
        return $User->id;
    }
}
