<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpSearchRadioOption;
use App\ErpSearchTextOption;
use App\ErpCustomer;
use App\Services\PayUService\Exception;
class ERPAddFilterShowController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPAddFilterShowAjax(Request $request)
    {
        $Message = "Something Went Wrong Please Try Again!";
        $Status = "error";
        $ERPAddFilterShowHtml = "";
        $ERPFilterName = "";
        try {
            $ParaMeter = array();
            $ParaMeter["All"] = "All";
            $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter);
            $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter);
            if($request->PageName=="ERPSetupUsersSearchList" || $request->PageName=="ERPReportsReportOfCustomerUsersSearchList")
            {
                $GetAllERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);  
            }
            unset($ParaMeter["All"]);
            $ParaMeter["FilterName"] = $request->FilterName;
            if($request->PageName=="ERPFileApplicationSearchList")
            {
               $ERPFilterName = "ERP File Applications";
               $ERPAddFilterShowHtml = view('ERP.File.Applications.FileApplicationAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPFileCustomerSearchList")
            {
               $ERPFilterName = "ERP File Customers";
               $ERPAddFilterShowHtml = view('ERP.File.Customers.FileCustomerAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPFileCountriesSearchList")
            {
               $ERPFilterName = "ERP File Countries";
               $ERPAddFilterShowHtml = view('ERP.File.Countries.FileCountriesAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPFileSourcesSearchList")
            {
               $ERPFilterName = "ERP File Sources";
               $ERPAddFilterShowHtml = view('ERP.File.Sources.FileSourcesAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPFileThemesSearchList")
            {
               $ERPFilterName = "ERP File Themes";
               $ERPAddFilterShowHtml = view('ERP.File.Themes.FileThemesAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPFileLanguagesSearchList")
            {
               $ERPFilterName = "ERP File Languages";
               $ERPAddFilterShowHtml = view('ERP.File.Languages.FileLanguagesAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPSetupUsersSearchList")
            {
               $ERPFilterName = "ERP Setup Users";
               $ERPAddFilterShowHtml = view('ERP.Setup.Users.SetupUsersAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","GetAllERPCustomers"))->render();
            }
            else if($request->PageName=="ERPSecuritySystemLogSearchList")
            {
               $ERPFilterName = "ERP Security System Log";
               $ERPAddFilterShowHtml = view('ERP.Security.SystemLog.SecuritySystemLogAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->PageName=="ERPReportsReportOfCustomerUsersSearchList")
            {
               $ERPFilterName = "ERP Report Of Customer Users";
               $ERPAddFilterShowHtml = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerAddFilterShow',compact("ParaMeter","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","GetAllERPCustomers"))->render();
            }
            $Message = "$ERPFilterName Add Filter Successfully!";
            $Status = "success";
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPFilterName Add Filter " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"ERPAddFilterShowHtml"=>$ERPAddFilterShowHtml]);
    }
}