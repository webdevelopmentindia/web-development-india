<div class="close-btn HideFilterChangeModal" ModalClass="ERPFileSourcesFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileSourcesSourceId")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Source ID') }}</label>
	  <div class="col-sm-3">
	    <select id="source_id_condition_change" name="source_id_condition_change" class="form-control ERPSearchEmptyContent" FieldId="source_id_change">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	@foreach($GetAllErpSearchRadioOptions as $TextOption)
              <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	@endforeach
          @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="source_id_change" type="number" class="form-control" name="source_id_change"  autofocus placeholder="Source ID" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif'> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileSourcesSourceId" FieldNameCondition="source_id_condition_change" FieldName="source_id_change" FormName="FileSourcesList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesCode")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Code') }}</label>
    <div class="col-sm-3">
	    <select id="code_condition_change" name="code_condition_change" class="form-control ERPSearchEmptyContent" FieldId="code_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="code_change" type="text" class="form-control" name="code_change"  autofocus placeholder="Code" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileSourcesCode" FieldNameCondition="code_condition_change" FieldName="code_change" FormName="FileSourcesList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesDescription")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Description') }}</label>
    <div class="col-sm-3">
	    <select id="description_condition_change" name="description_condition_change" class="form-control ERPSearchEmptyContent" FieldId="description_change">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="description_change" type="text" class="form-control" name="description_change"  autofocus placeholder="Description" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileSourcesDescription" FieldNameCondition="description_condition_change" FieldName="description_change" FormName="FileSourcesList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesAutoPost")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Auto pos JE?') }}</label>
    <div class="col-sm-3">
      <select id="auto_post_condition_change" name="auto_post_condition_change" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 check-f">
        <span class="mr-1"><input id="auto_post_change" type="radio" class="" name="auto_post_change" value="1" @if($ParaMeter["FieldNameValue"]==NULL || $ParaMeter["FieldNameValue"]==1 ) {{ 'checked' }} @endif>Yes</span>
        <span class="mr-1"><input id="auto_post_change" type="radio" class="" name="auto_post_change" value="0" @if($ParaMeter["FieldNameValue"]==0 && $ParaMeter["FieldNameValue"]!=NULL) {{ 'checked' }} @endif>No</span>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileSourcesAutoPost" FieldNameCondition="auto_post_condition_change" FieldName="auto_post_change" FormName="FileSourcesList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesEnabledFlag")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
    <div class="col-sm-3">
      <select id="enabled_flag_condition_change" name="enabled_flag_condition_change" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 check-f">
        <span class="mr-1"><input id="enabled_flag_change" type="radio" class="" name="enabled_flag_change" value="1" @if($ParaMeter["FieldNameValue"]==NULL || $ParaMeter["FieldNameValue"]==1 ) {{ 'checked' }} @endif>Yes</span>
        <span class="mr-1"><input id="enabled_flag_change" type="radio" class="" name="enabled_flag_change" value="0" @if($ParaMeter["FieldNameValue"]==0 && $ParaMeter["FieldNameValue"]!=NULL) {{ 'checked' }} @endif>No</span>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileSourcesEnabledFlag" FieldNameCondition="enabled_flag_condition_change" FieldName="enabled_flag_change" FormName="FileSourcesList"> Apply </a></div>
</div>
@endif