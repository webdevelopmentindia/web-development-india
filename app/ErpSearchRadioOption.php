<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpSearchRadioOption extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    Static Public function GetAllErpSearchRadioOptions($ParaMeter)
    {
        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::select("erp_search_radio_options.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpSearchRadioOptions =$GetAllErpSearchRadioOptions->addselect("erp_search_radio_options.*"); 
        }
        else if(isset($ParaMeter["TableId"]) && $ParaMeter["TableId"]>0)
        {
           $GetAllErpSearchRadioOptions =$GetAllErpSearchRadioOptions->where("erp_search_radio_options.id",$ParaMeter["TableId"])->addselect("erp_search_radio_options.*"); 
        }
        $GetAllErpSearchRadioOptions = $GetAllErpSearchRadioOptions->orderBy("erp_search_radio_options.id","ASC")
            ->get();
        return $GetAllErpSearchRadioOptions;
    }
}