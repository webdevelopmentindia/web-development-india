<div class="close-btn HideAddFilterModal" ModalId="FileSourcesAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileSourcesSourceId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Source ID') }}</label>
	<div class="col-sm-3">
	    <select id="source_id_condition" name="source_id_condition" class="form-control" FieldId="source_id">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="source_id" type="number" class="form-control" name="source_id"  autofocus placeholder="Source ID" value=""> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="source_id_condition" FormName="FileSourcesList" FieldName="source_id"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesCode")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Code') }}</label>
    <div class="col-sm-3">
	    <select id="code_condition" name="code_condition" class="form-control ERPSearchEmptyContent" FieldId="code">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="code" type="text" class="form-control" name="code"  autofocus placeholder="Code" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileSourcesList" FieldNameCondition="code_condition" FieldName="code"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesDescription")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Description') }}</label>
    <div class="col-sm-3">
	    <select id="description_condition" name="description_condition" class="form-control ERPSearchEmptyContent" FieldId="description">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="description" type="text" class="form-control" name="description"  autofocus placeholder="Description" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileSourcesList" FieldNameCondition="description_condition" FieldName="description"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesAutoPost")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Auto pos JE?') }}</label>
    <div class="col-sm-3">
      <select id="auto_post_condition" name="auto_post_condition" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 check-f">
        <span class="mr-1"><input id="auto_post" type="radio" class="" name="auto_post" value="1" checked>Yes</span>
        <span class="mr-1"><input id="auto_post" type="radio" class="" name="auto_post" value="0">No</span>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileSourcesList" FieldNameCondition="auto_post_condition"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileSourcesEnabledFlag")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
    <div class="col-sm-3">
      <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 check-f">
        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" checked>Yes</span>
        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0">No</span>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileSourcesList" FieldNameCondition="enabled_flag_condition"> Apply </a></div>
</div>
@endif