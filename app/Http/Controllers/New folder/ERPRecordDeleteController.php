<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\Services\PayUService\Exception;
class ERPRecordDeleteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPRecordDeleteAjax(Request $request)
    {
        $Status = "error";
        $Message = "Error in Record Deletion!";
        try {
            $ParaMeter = array();
            if($request->FormName=="FileApplicationAdd")
            {
                if($request->DeleteId>0)
                {
                    $ParaMeter["ERPApplicationId"] = $request->DeleteId;
                    $DeleteERPApplications = ErpApplication::DeleteERPApplications($ParaMeter);
                    $Status = "success";
                }
            }
            else if($request->FormName=="FileCustomerAdd")
            {
                if($request->DeleteId>0)
                {
                    $ParaMeter["ERPApplicationId"] = $request->DeleteId;
                    $DeleteERPApplications = ErpApplication::DeleteERPApplications($ParaMeter);
                    $Status = "success";
                }
            }
            else if($request->ManuName=="Countries")
            {
                return view('ERP.File.Countries');
            }
            else if($request->ManuName=="Sources")
            {
                return view('ERP.File.Sources');
            }
            else if($request->ManuName=="ERP Themes")
            {
                return view('ERP.File.Themes');
            }
            else if($request->ManuName=="ERP Languages")
            {
                return view('ERP.File.Languages');
            }
        }
        catch (\Exception $e) {
          $Message = "Error in ERP Record Deletion " . $e->getMessage();
        }
        return json_encode(["Status"=>$Status,"Message"=>$Message]);
    }
}