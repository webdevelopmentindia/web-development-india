@if(!isset($ParaMeter["ERPTableId"]))
<div class="card-header header-elements-inline">
            <div class="page-title">
                <h3>
                	<?php 
					    echo strtoupper("Search of Logged users");
					?>
                </h3>
            </div>
            <div class="header-elements">
                <span>
                	<?php 
					    echo " ".date("m/d/Y");
					?>
                </span>
            </div>
        </div> <!-- emloy-hd p-0 -->
<div class="card-body">
	<div class="row">
	    <div class="col-md-12 employee-form">
	    	<form method="POST" aria-label="{{ __('ERPSearchSubmit') }}" enctype="multipart/form-data" id="ERPSearchSubmit" name="ERPSearchSubmit" FormParentDivId="SecuritySystemLogSearch">
		    @csrf
			<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecuritySystemLogList" SectionName="SecuritySystemLogSection" TabName="ERPSecuritySystemLog">
            <div id="ERPChangeFilter">
@endif

			<?php 
			    $LoginCondition = $ParaMeter["LoginCondition"];
			    $Login = $ParaMeter["Login"];
			    $DateCondition = $ParaMeter["DateCondition"];
			    $Date = $ParaMeter["Date"];
			    $SessionCondition = $ParaMeter["SessionCondition"];
			    $Session = $ParaMeter["Session"];
			    $IpCondition = $ParaMeter["IpCondition"];
			    $Ip = $ParaMeter["Ip"];
			    $ERPTableId = "";
			    if(isset($ParaMeter["ERPTableId"]))
			    {
			       $ERPTableId = $ParaMeter["ERPTableId"];
			    }
			?>
	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('User Name') }}</label>
	            <div class="col-sm-3">
				    <select id="login_condition" name="login_condition" class="form-control ERPSearchEmptyContent" FieldId="login">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LoginCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }} SearchFieldShow" name="login"  autofocus placeholder="User Name" value="<?php echo $Login; ?>" style='@if($LoginCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('Date') }}</label>
	            <div class="col-sm-3">
				    <select id="date_condition" name="date_condition" class="form-control ERPSearchEmptyContent" FieldId="date">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($DateCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="date" type="text" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} SearchFieldShow" name="date"  autofocus placeholder="Date" value="<?php echo $Date; ?>" style='@if($DateCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('Session') }}</label>
	            <div class="col-sm-3">
				    <select id="session_condition" name="session_condition" class="form-control ERPSearchEmptyContent" FieldId="session">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($SessionCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="session" type="text" class="form-control{{ $errors->has('session') ? ' is-invalid' : '' }} SearchFieldShow" name="session"  autofocus placeholder="Session" value="<?php echo $Session; ?>" style='@if($SessionCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('IP') }}</label>
	            <div class="col-sm-3">
				    <select id="ip_condition" name="ip_condition" class="form-control ERPSearchEmptyContent" FieldId="ip">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($IpCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="ip" type="text" class="form-control{{ $errors->has('ip') ? ' is-invalid' : '' }} SearchFieldShow" name="ip"  autofocus placeholder="IP" value="<?php echo $Ip; ?>" style='@if($IpCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>

	        <div class="clearfix down-filtr">
		        <div class="erp-src-app justifi-content-center form-flex">
	            	<div class="col-flex link-src">
		               <button type="submit" class="btn save-btn">Search</button>
		               <a type="button" onclick="ERPFormReset(this); return false;" class="btn cancel-btn save-btn" >Clear</a>
		           	</div>
		           	<div class="col-flex flex-2 link-src" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
		                <select id="erp_filter_list" name="erp_filter_list" class="form-control" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" FormName="SecuritySystemLogSearch">
			               	@if(!$GetAllErpSaveFilters->isEmpty())
			               	   <option value=""></option>
			               	   <option value="">Public</option>
			               	   @foreach($GetAllErpSaveFilters as $Filter)
		                            <option value="{{ $Filter->id }}" @if($ERPTableId==$Filter->id) {{ 'selected' }} @endif >{{ $Filter->filter_name }}</option>
			               	   @endforeach
			               	@endif
				        </select>
			       	</div>
			       	<div class="col-flex link-src">
			       		<a href="javascript:" type="button" class="btn save-btn SaveFilterModalShow" FormName="SecuritySystemLogSearch" ModalId="SearchSaveFilter">Save Filter</a>
		               <button type="button" class="btn btn-danger" ShowSection="SecuritySystemLogList" SectionName="SecuritySystemLogSection" id="ERPBackCancelButton">Back</button>
		           	</div>
		        </div>
		    </div>













@if(!isset($ParaMeter["ERPTableId"]))
            </div>
	        </form>
	    </div>

	       <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter </h3>
			            </div>
			            <div class="header-elements">
			                <button id="modelhide" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
			               	<div class="col-sm-9">
			               		<input id="filter_name" type="text" class="form-control{{ $errors->has('filter_name') ? ' is-invalid' : '' }}" name="filter_name"  autofocus placeholder="Filter Name" value="">
			               	</div>
			               	<div class="col-sm-3">
			               		<button type="button" class="btn cancel-btn ERPFilterSave" FormName="SecuritySystemLogSearch" FormSubmitId="ERPSearchSubmit">Save</button>
			               	</div>
				        </div>
				        <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				                    @if(!$GetAllErpSaveFilters->isEmpty())
					               	   <option value=""></option>
					               	   <option value="">Public</option>
					               	   @foreach($GetAllErpSaveFilters as $Filter)
				                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
					               	   @endforeach
					               	@endif
				               	</select>
				            </div>
				            <div class="col-sm-3"><button type="button" class="btn cancel-btn ERPFilterDelete" FormName="SecuritySystemLogSearch">Delete</button></div>
				        </div>
			       </div>
			    </div>
			</div>
		</div>
	</div>
@endif