@if(!(isset($OnlyData) && $OnlyData=="System Log"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPSecuritySystemLog">
	<div id="SecuritySystemLogList" class="SecuritySystemLogSection">
@endif

<div class="card">
<div class="card-header header-elements-inline">
	<div class="page-title">
		<h3>Logged users</h3>
	</div>
	<div class="header-elements">
		<span>
			<?php
		        echo " ".date("m/d/Y");
	        ?></span>
	</div>
</div>
<div class="row row-col">
	<div class="col-xs-12">
		<!-- <span class="link-src">
			<a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecuritySystemLogForm" SectionName="SecuritySystemLogSection"> Add New </a>
		</span> -->
		<span class="link-src">
			<a href="javascript:" class="btn save-btn ERPSearchShow" FormName="SecuritySystemLogSearch" SectionName="SecuritySystemLogSection" FilterChangeDivEmpty="ERPSecuritySystemLogFilterChange" FormClassName="ERPSecuritySystemLogRemoveSearchSubmit"> Search </a>
		</span>
	</div>
</div>
		

		<?php 
$CheckParameter = 0;
?>
<div id="SecuritySystemLogListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPSecuritySystemLogRemoveSearchSubmit add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecuritySystemLogList" SectionName="SecuritySystemLogSection" TabName="ERPSecuritySystemLog">	
@if(isset($ParaMeter))
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    <?php 
    //echo "<pre>";
    //print_r($ParaMeter);
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if(((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="") || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4)))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchLogin" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPSecuritySystemLog" FilterName="SecuritySystemLogLogin" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['LoginCondition'] }}" ShowDivClass="ERPSecuritySystemLogFilterChange"
		        FieldNameValue='{{$ParaMeter["Login"]}}' FormName="SecuritySystemLogList" AddFilterPopup="SecuritySystemLogAddFilterShow">
			        Login: {{ $ParaMeter["LoginConditionValue"] }} 
			            {{ $ParaMeter["Login"] }} 
	            </div>
	            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SecuritySystemLogList" RemoveSearch="SearchLogin" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSecuritySystemLogFilterChange"> <i class="fa fa-times" aria-hidden="true"></i></a>
	        </span>

	        <input type="hidden" id="login" name="login" value='{{ $ParaMeter["Login"] }}'>
	        <input type="hidden" id="login_condition" name="login_condition" value='{{ $ParaMeter["LoginCondition"] }}'>
	        <div style="display: none" class="ERPSecuritySystemLogFilterChange add-drop-down">
	        </div>
	        
        </div>
   @else
        <?php 
			$AddFilterOptions.='<li value="SecuritySystemLogLogin" id="ERPSecuritySystemLogSearchList" name="ERPSecuritySystemLogSearchList" class="ERPAddFilterShow" ShowDivId="SecuritySystemLogAddFilterShow" FilterChangeModal="ERPSecuritySystemLogFilterChange" FormName="SecuritySystemLogList">Login</li>';
		?>
   @endif
   @if(((isset($ParaMeter["Date"]) && $ParaMeter["Date"]!="") && (isset($ParaMeter["DateCondition"]) && $ParaMeter["DateCondition"]>=1) || (isset($ParaMeter["DateCondition"]) && $ParaMeter["DateCondition"]==4)))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchDate" class="SearchSection addfield-col">
		    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPSecuritySystemLog" FilterName="SecuritySystemLogDate" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['DateCondition'] }}" ShowDivClass="ERPSecuritySystemLogFilterChange"
		        FieldNameValue='{{$ParaMeter["Date"]}}' FormName="SecuritySystemLogList" AddFilterPopup="SecuritySystemLogAddFilterShow">
		            Date : {{ $ParaMeter["DateConditionValue"] }} 
		                {{ $ParaMeter["Date"] }}
		        </div>
		        <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SecuritySystemLogList" RemoveSearch="SearchDate" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSecuritySystemLogFilterChange"> <i class="fa fa-times" aria-hidden="true"></i></a>
		    </span>

	        <input type="hidden" id="date" name="date" value='{{ $ParaMeter["Date"] }}'>
	        <input type="hidden" id="date_condition" name="date_condition" value='{{ $ParaMeter["DateCondition"] }}'>
	        <div style="display: none" class="ERPSecuritySystemLogFilterChange add-drop-down">
	        </div>
	        
	    </div>
	@else
	    <?php 
	        $AddFilterOptions.='<li value="SecuritySystemLogDate" id="ERPSecuritySystemLogSearchList" name="ERPSecuritySystemLogSearchList" class="ERPAddFilterShow" ShowDivId="SecuritySystemLogAddFilterShow" FilterChangeModal="ERPSecuritySystemLogFilterChange" FormName="SecuritySystemLogList">Date</li>';
	    ?>
	@endif
	@if(((isset($ParaMeter["Session"]) && $ParaMeter["Session"]!="") && (isset($ParaMeter["SessionCondition"]) && $ParaMeter["SessionCondition"]>=1) || (isset($ParaMeter["SessionCondition"]) && $ParaMeter["SessionCondition"]==4)))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchSession" class="SearchSection addfield-col">
		    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPSecuritySystemLog" FilterName="SecuritySystemLogSession" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['SessionCondition'] }}" ShowDivClass="ERPSecuritySystemLogFilterChange"
		        FieldNameValue='{{$ParaMeter["Session"]}}' FormName="SecuritySystemLogList" AddFilterPopup="SecuritySystemLogAddFilterShow">
		            Session : {{ $ParaMeter["SessionConditionValue"] }} 
		                {{ $ParaMeter["Session"] }}
		        </div>
		        <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SecuritySystemLogList" RemoveSearch="SearchSession" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSecuritySystemLogFilterChange"> <i class="fa fa-times" aria-hidden="true"></i> </a>
		    </span>

	        <input type="hidden" id="session" name="session" value='{{ $ParaMeter["Session"] }}'>
	        <input type="hidden" id="session_condition" name="session_condition" value='{{ $ParaMeter["SessionCondition"] }}'>
	        <div style="display: none" class="ERPSecuritySystemLogFilterChange add-drop-down">
	        </div>
	        
	    </div>
	@else
	    <?php 
	        $AddFilterOptions.='<li value="SecuritySystemLogSession" id="ERPSecuritySystemLogSearchList" name="ERPSecuritySystemLogSearchList" class="ERPAddFilterShow" ShowDivId="SecuritySystemLogAddFilterShow" FilterChangeModal="ERPSecuritySystemLogFilterChange" FormName="SecuritySystemLogList">Session</li>';
	    ?>
	@endif
	@if(((isset($ParaMeter["Ip"]) && $ParaMeter["Ip"]!="") && (isset($ParaMeter["IpCondition"]) && $ParaMeter["IpCondition"]>=1) || (isset($ParaMeter["IpCondition"]) && $ParaMeter["IpCondition"]==4)))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchIp" class="SearchSection addfield-col">
		    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPSecuritySystemLog" FilterName="SecuritySystemLogIp" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['IpCondition'] }}" ShowDivClass="ERPSecuritySystemLogFilterChange"
		        FieldNameValue='{{$ParaMeter["Ip"]}}' FormName="SecuritySystemLogList" AddFilterPopup="SecuritySystemLogAddFilterShow">
		            IP : {{ $ParaMeter["IpConditionValue"] }} 
		                {{ $ParaMeter["Ip"] }}
		        </div>
		         <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="SecuritySystemLogList" RemoveSearch="SearchIp" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPSecuritySystemLogFilterChange"> <i class="fa fa-times" aria-hidden="true"></i> </a>
		    </span>

	        <input type="hidden" id="ip" name="ip" value='{{ $ParaMeter["Ip"] }}'>
	        <input type="hidden" id="ip_condition" name="ip_condition" value='{{ $ParaMeter["IpCondition"] }}'>
	        <div style="display: none" class="ERPSecuritySystemLogFilterChange add-drop-down">
	        </div>
	       
	    </div>
	@else
	    <?php 
	        $AddFilterOptions.='<li value="SecuritySystemLogIp" id="ERPSecuritySystemLogSearchList" name="ERPSecuritySystemLogSearchList" class="ERPAddFilterShow" ShowDivId="SecuritySystemLogAddFilterShow" FilterChangeModal="ERPSecuritySystemLogFilterChange" FormName="SecuritySystemLogList">IP</li>';
	    ?>
	@endif
   @if($CheckParameter)
   	<div class="addfield-col">
	   <!-- <select id="ERPSecuritySystemLogSearchList" name="ERPSecuritySystemLogSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="SecuritySystemLogAddFilterShow">
	   	    <option value=''>+ Add Filter</option>
	   	    <?php echo $AddFilterOptions; ?>
	   </select> -->
	   @if($AddFilterOptions!="")
		    <div class="dropdown">
			  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
			  <span class="caret"></span></button>
			  <ul class="dropdown-menu">
			    <?php echo $AddFilterOptions; ?>
			  </ul>
			</div>
		@endif
		<!-- <div id="SecuritySystemLogFilterShow" class="add-drop-down popclose" style="display: none">
		   	
	   	</div> -->
	   	<div id="SecuritySystemLogAddFilterShow" class="add-drop-down" style="display: none">
   		</div>
	</div>
	<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
	        <select id="erp_filter_list" name="erp_filter_list" class="form-control ERPSearchBySaveFilter" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" SectionName="SecuritySystemLogList" ManuName="ERPSecuritySystemLog" FormName="">
	   @if(!$GetAllErpSaveFilters->isEmpty())
	       	  <option value=""></option>
	          <option value="">Public</option>
		      @foreach($GetAllErpSaveFilters as $Filter)
		   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
		   	  @endforeach
	   @endif 
	        </select>
	</div>
	  
   
   <!-- GetAllErpSaveFilters -->
	   	<div class="addfield-col"><button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="SecuritySystemLogList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPSecuritySystemLogFilterChange" HideAddFilterPopup="SecuritySystemLogAddFilterShow">Save Filter</button></div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="SecuritySystemLogList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="SecuritySystemLogList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
   				<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="SecuritySystemLogList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
   			</div>
	</div>
</div>
   @endif
@endif
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPSecuritySystemLogtable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>User Name</th>
			        <th>Date</th>
			        <th>IP</th>
			        <th>Session</th>
			    </tr>
			</thead>
			<tbody>
				@if(!$GetAllUserLoggeds->isEmpty())
				    @foreach($GetAllUserLoggeds as $UserLogged)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $UserLogged->login }}</td>
			                <td>{{ $UserLogged->date_login }}</td>
			                <td>{{ $UserLogged->ip }}</td>
			                <td>{{ $UserLogged->sc_session }}</td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>User Name</th>
			        <th>Date</th>
			        <th>IP</th>
			        <th>Session</th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="System Log"))
	</div>
	<div id="SecuritySystemLogForm" style="display: none" class="SecuritySystemLogSection card">
	</div>
	<div id="SecuritySystemLogSearch" style="display: none" class="SecuritySystemLogSection card">
	</div>
</div>
@endif