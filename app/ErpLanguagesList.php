<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpLanguagesList extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    Static Public function GetAllErpLanguagesLists($ParaMeter)
    {
        $GetAllErpLanguagesLists = ErpLanguagesList::select("erp_languages_lists.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpLanguagesLists = $GetAllErpLanguagesLists->addselect("erp_languages_lists.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpLanguagesLists = $GetAllErpLanguagesLists->where('erp_languages_lists.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_languages_lists.*"); 
        }
        $GetAllErpLanguagesLists = $GetAllErpLanguagesLists->orderBy("erp_languages_lists.id","DESC")
            ->get();
        return $GetAllErpLanguagesLists;
    }
}