@if(!(isset($OnlyData) && $OnlyData=="Report of Customer / Users"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPReportsReportOfCustomerUsers">
	<div id="ReportsReportOfCustomerUsersList" class="ReportsReportOfCustomerUsersSection">
@endif
<div class="card">
	<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>REPORT OF ERP_USERS</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
	<div class="row row-col">
		<div class="col-xs-12">
				<span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="ReportsReportOfCustomerUsersSearch" SectionName="ReportsReportOfCustomerUsersSection" FilterChangeDivEmpty="ERPReportsReportOfCustomerUsersFilterChange"> Search </a></span>
				<span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="ReportsReportOfCustomerUsersForm" SectionName="ReportsReportOfCustomerUsersSection" > Summary </a></span>
		</div>
	</div>
<?php 
$CheckParameter = 0;
?>
<div id="ReportsReportOfCustomerUsersListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="ReportsReportOfCustomerUsersList" SectionName="ReportsReportOfCustomerUsersSection" TabName="ERPReportsReportOfCustomerUsers">	
@if(isset($ParaMeter))
    @if(
    ((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]!="") && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="")) || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4))
    ||
    (((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
    ||
    (((isset($ParaMeter["EMail"]) && $ParaMeter["EMail"]!="") && (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!="")) || (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]==4))
    ||
    (((isset($ParaMeter["AdminPriv"]) && $ParaMeter["AdminPriv"]!="") && (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!="")) || (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]==4))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]!=""))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    
    <?php 
    // echo "<pre>";
    // print_r($ParaMeter);
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
	@if((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]!="") && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCustomerId" class="SearchSection addfield-col">
            
            <input type="hidden" name="customer_id" value='{{ $ParaMeter["CustomerId"] }}'>
            <input type="hidden" name="customer_id_condition" value='{{ $ParaMeter["CustomerIdCondition"] }}'>
            <div style="display: none" class="ERPReportsReportOfCustomerUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPReportsReportOfCustomerUsers" FilterName="ReportsReportOfCustomerUsersCustomerId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CustomerIdCondition'] }}" ShowDivClass="ERPReportsReportOfCustomerUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["CustomerIdCondition"]!=4){{$ParaMeter["CustomerId"]}} @endif' FormName="ReportsReportOfCustomerUsersList" AddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">
	        	Customer ID: {{ $ParaMeter["CustomerIdConditionValue"] }} 
	        	{{ $ParaMeter["CustomerIdValue"] }} 
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="SearchCustomerId" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>
        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="ReportsReportOfCustomerUsersCustomerId" id="ERPReportsReportOfCustomerUsersSearchList" name="ERPReportsReportOfCustomerUsersSearchList" class="ERPAddFilterShow" ShowDivId="ReportsReportOfCustomerUsersAddFilterShow" FilterChangeModal="ERPReportsReportOfCustomerUsersFilterChange" FormName="ReportsReportOfCustomerUsersList">Customer ID</li>';
		?>
    @endif <!-- for CustomerId -->
   @if(((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="")) || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchLogin" class="SearchSection addfield-col"> 
        	
	        <input type="hidden" name="login" value='{{ $ParaMeter["Login"] }}'>
	        <input type="hidden" name="login_condition" value='{{ $ParaMeter["LoginCondition"] }}'>
	        <div style="display: none" class="ERPReportsReportOfCustomerUsersFilterChange add-drop-down">
	        </div>
	         <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPReportsReportOfCustomerUsers" FilterName="ReportsReportOfCustomerUsersLogin" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['LoginCondition'] }}" ShowDivClass="ERPReportsReportOfCustomerUsersFilterChange"
		        FieldNameValue='@if($ParaMeter["LoginCondition"]!=4){{$ParaMeter["Login"]}} @endif' FormName="ReportsReportOfCustomerUsersList" AddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">
			        Login: {{ $ParaMeter["LoginConditionValue"] }} 
			        @if(isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!=4)
			            {{ $ParaMeter["Login"] }} 
			        @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="SearchLogin" RemoveSearchType="ColumnSearchRemove">
		        	<i class="fa fa-times" aria-hidden="true"></i> 
		        </a>
		    </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="ReportsReportOfCustomerUsersLogin" id="ERPReportsReportOfCustomerUsersSearchList" name="ERPReportsReportOfCustomerUsersSearchList" class="ERPAddFilterShow" ShowDivId="ReportsReportOfCustomerUsersAddFilterShow" FilterChangeModal="ERPReportsReportOfCustomerUsersFilterChange" FormName="ReportsReportOfCustomerUsersList">Login</li>';
		?>
   @endif <!-- for Login -->
   @if(((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchFullName" class="SearchSection addfield-col">
            
            <input type="hidden" name="full_name" value='{{ $ParaMeter["FullName"] }}'>
            <input type="hidden" name="full_name_condition" value='{{ $ParaMeter["FullNameCondition"] }}'>
            <div style="display: none" class="ERPReportsReportOfCustomerUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPReportsReportOfCustomerUsers" FilterName="ReportsReportOfCustomerUsersFullName" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['FullNameCondition'] }}" ShowDivClass="ERPReportsReportOfCustomerUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["FullNameCondition"]!=4){{$ParaMeter["FullName"]}} @endif' FormName="ReportsReportOfCustomerUsersList" AddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">
	        	FullName: {{ $ParaMeter["FullNameConditionValue"] }} 
	        	@if(isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!=4)
		            {{ $ParaMeter["FullName"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="SearchFullName" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="ReportsReportOfCustomerUsersFullName" id="ERPReportsReportOfCustomerUsersSearchList" name="ERPReportsReportOfCustomerUsersSearchList" class="ERPAddFilterShow" ShowDivId="ReportsReportOfCustomerUsersAddFilterShow" FilterChangeModal="ERPReportsReportOfCustomerUsersFilterChange" FormName="ReportsReportOfCustomerUsersList">FullName</li>';
		?>
   @endif <!-- for FullName -->
   @if(((isset($ParaMeter["EMail"]) && $ParaMeter["EMail"]!="") && (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!="")) || (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchEMail" class="SearchSection addfield-col">
            
            <input type="hidden" name="e_mail" value='{{ $ParaMeter["EMail"] }}'>
            <input type="hidden" name="e_mail_condition" value='{{ $ParaMeter["EMailCondition"] }}'>
            <div style="display: none" class="ERPReportsReportOfCustomerUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPReportsReportOfCustomerUsers" FilterName="ReportsReportOfCustomerUsersEMail" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EMailCondition'] }}" ShowDivClass="ERPReportsReportOfCustomerUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["EMailCondition"]!=4){{$ParaMeter["EMail"]}} @endif' FormName="ReportsReportOfCustomerUsersList" AddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">
	        	E-Mail: {{ $ParaMeter["EMailConditionValue"] }} 
	        	@if(isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!=4)
		            {{ $ParaMeter["EMail"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="SearchEMail" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="ReportsReportOfCustomerUsersEMail" id="ERPReportsReportOfCustomerUsersSearchList" name="ERPReportsReportOfCustomerUsersSearchList" class="ERPAddFilterShow" ShowDivId="ReportsReportOfCustomerUsersAddFilterShow" FilterChangeModal="ERPReportsReportOfCustomerUsersFilterChange" FormName="ReportsReportOfCustomerUsersList">E-Mail</li>';
		?>
   @endif <!-- for E-Mail -->
   @if(((isset($ParaMeter["AdminPriv"]) && $ParaMeter["AdminPriv"]!="") && (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!="")) || (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchAdminPriv" class="SearchSection addfield-col">
            
            <input type="hidden" name="admin_priv" value='{{ $ParaMeter["AdminPriv"] }}'>
            <input type="hidden" name="admin_priv_condition" value='{{ $ParaMeter["AdminPrivCondition"] }}'>
            <div style="display: none" class="ERPReportsReportOfCustomerUsersFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPReportsReportOfCustomerUsers" FilterName="ReportsReportOfCustomerUsersAdminPriv" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AdminPrivCondition'] }}" ShowDivClass="ERPReportsReportOfCustomerUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["AdminPrivCondition"]!=4){{$ParaMeter["AdminPriv"]}} @endif' FormName="ReportsReportOfCustomerUsersList" AddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">
	        	Admin Role: {{ $ParaMeter["AdminPrivConditionValue"] }} 
	        	@if(isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!=4)
		            {{ $ParaMeter["AdminPriv"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="SearchAdminPriv" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="ReportsReportOfCustomerUsersAdminPriv" id="ERPReportsReportOfCustomerUsersSearchList" name="ERPReportsReportOfCustomerUsersSearchList" class="ERPAddFilterShow" ShowDivId="ReportsReportOfCustomerUsersAddFilterShow" FilterChangeModal="ERPReportsReportOfCustomerUsersFilterChange" FormName="ReportsReportOfCustomerUsersList">Admin Role</li>';
		?>
    @endif <!-- for AdminPriv -->
    @if((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]!=""))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchEnabledFlag" class="SearchSection addfield-col">

	        <input type="hidden" id="enabled_flag" name="enabled_flag" value='{{ $ParaMeter["EnabledFlag"] }}'>
	        <input type="hidden" id="enabled_flag_condition" name="enabled_flag_condition" value='{{ $ParaMeter["EnabledFlagCondition"] }}'>
	        <div style="display: none" class="ERPReportsReportOfCustomerUsersFilterChange add-drop-down">
	        </div>

	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        	<div PageName="ERPReportsReportOfCustomerUsers" FilterName="ReportsReportOfCustomerUsersEnabledFlag" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EnabledFlagCondition'] }}" ShowDivClass="ERPReportsReportOfCustomerUsersFilterChange"
	        FieldNameValue='@if($ParaMeter["EnabledFlagCondition"]!=4){{$ParaMeter["EnabledFlag"]}} @endif' FormName="ReportsReportOfCustomerUsersList" AddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">
		        Active : {{ $ParaMeter["EnabledFlagConditionValue"] }} 
		        {{ $ParaMeter["EnabledFlag"] }} 
            	</div>
	        	<a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="SearchEnabledFlag" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPReportsReportOfCustomerUsersFilterChange">
	        		<i class="fa fa-times" aria-hidden="true"></i> 
	        	</a>
	        </span>
        </div>   
    @else
        <?php 
			$AddFilterOptions.='<li value="ReportsReportOfCustomerUsersEnabledFlag" id="ERPReportsReportOfCustomerUsersSearchList" name="ERPReportsReportOfCustomerUsersSearchList" class="ERPAddFilterShow" ShowDivId="ReportsReportOfCustomerUsersAddFilterShow" FilterChangeModal="ERPReportsReportOfCustomerUsersFilterChange" FormName="ReportsReportOfCustomerUsersList">Active</li>';
		?>
    @endif <!-- for EnabledFlag -->
   @if($CheckParameter)
	   	<div class="addfield-col">
		    @if($AddFilterOptions!="")
			    <div class="dropdown">
				  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				    <?php echo $AddFilterOptions; ?>
				  </ul>
				</div>
			@endif
		   <div id="ReportsReportOfCustomerUsersAddFilterShow" class="add-drop-down popclose" style="display: none">
		   	
	   		</div>
		</div>
		<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
	        <select id="erp_filter_list" name="erp_filter_list" class="form-control ERPSearchBySaveFilter" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" SectionName="ReportsReportOfCustomerUsersList" ManuName="ERPReportsReportOfCustomerUsers" FormName="">
	   @if(!$GetAllErpSaveFilters->isEmpty())
	       	  <option value=""></option>
	          <option value="">Public</option>
		      @foreach($GetAllErpSaveFilters as $Filter)
		   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
		   	  @endforeach
	   @endif 
	        </select>
	   </div>
	   <!-- GetAllErpSaveFilters -->
	   	<div class="addfield-col"><button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="ReportsReportOfCustomerUsersList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPReportsReportOfCustomerUsersFilterChange" HideAddFilterPopup="ReportsReportOfCustomerUsersAddFilterShow">Save Filter</button></div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter">
			                	<i class="fa fa-times" aria-hidden="true"></i>
			                </button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="ReportsReportOfCustomerUsersList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="ReportsReportOfCustomerUsersList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
		   		<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="ReportsReportOfCustomerUsersList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
		   	</div>
    @endif <!-- CheckParameter -->
    @if(
    ((isset($ParaMeter["CustomerId"]) && $ParaMeter["CustomerId"]!="") && (isset($ParaMeter["CustomerIdCondition"]) && $ParaMeter["CustomerIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") && (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]!="")) || (isset($ParaMeter["LoginCondition"]) && $ParaMeter["LoginCondition"]==4))
    ||
    (((isset($ParaMeter["FullName"]) && $ParaMeter["FullName"]!="") && (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]!="")) || (isset($ParaMeter["FullNameCondition"]) && $ParaMeter["FullNameCondition"]==4))
    ||
    (((isset($ParaMeter["EMail"]) && $ParaMeter["EMail"]!="") && (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]!="")) || (isset($ParaMeter["EMailCondition"]) && $ParaMeter["EMailCondition"]==4))
    ||
    (((isset($ParaMeter["AdminPriv"]) && $ParaMeter["AdminPriv"]!="") && (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]!="")) || (isset($ParaMeter["AdminPrivCondition"]) && $ParaMeter["AdminPrivCondition"]==4))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]!=""))
    )
    </div>
</div>
    @endif
@endif <!-- ParaMeter -->
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPReportsReportOfCustomerUserstable" class="display table table-bordered table-striped" style="width:100%">
			<thead>
			    <tr style="background-color:#f5f7fb; border-top: 1px solid #ddd;">
			        <th>User ID</th>
			        <th>Login</th>
			        <th>Customer ID</th>
			        <!-- <th>User Password</th> -->
			        <th>Full Name</th>
			        <th>E-Mail</th>
			        <th>Admin Role?</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
@if(!$GetAllUsers->isEmpty())
    <?php 
    $i = 0;
    $OldCustomer = "";
    $CustomerUserCount = 0;
    ?>
    @foreach($GetAllUsers as $User)
	    <?php 
	    $NewCustomer = $User->customer_id;
	    if($OldCustomer!=$NewCustomer)
	    {
	    	if($i!=0)
	        {
	        	?>
	            <tr>
		        	<td style="display: none"></td>
		        	<!-- <td style="display: none"></td> -->
		        	<td style="display: none"></td>
		        	<td style="display: none"></td>
		        	<td style="display: none"></td>
		        	<td style="display: none"></td>
		        	<td style="display: none"></td>
		        	<td style="display: none"></td>
		        	<td colspan="8" align="right">{{ $CustomerUserCount }}</td>
		        </tr>
		        <?php
	        }
	        $CustomerUserCount = 0;
	    	?>
	    	<tr>
	        	<td colspan="8" >Customer ID	=> {{ $User->full_name }}</td>
	        	<td style="display: none"></td>
	        	<!-- <td style="display: none"></td> -->
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        </tr>
	        <?php
	    }
	    $CustomerUserCount++;
	    ?>
	        
	        <tr>
	            <td>{{ $User->erp_table_id }}</td>
	            <td>{{ $User->login }}</td>
	            <td>{{ $User->full_name }}</td>
	            <!-- <td>{{ $User->password }}</td> -->
	            <td>{{ $User->name }}</td>
	            <td>{{ $User->email }}</td>
	            <td>
	            	@if($User->admin_priv==1)
	            	   {{ 'Yes' }}
	            	@elseif($User->admin_priv==0)
	            	   {{ 'No' }}
	            	@endif
	            </td>
	            <td>
	            	@if($User->enabled_flag==1)
	            	   {{ 'Yes' }}
	            	@elseif($User->enabled_flag==0)
	            	   {{ 'No' }}
	            	@endif
	            </td>
	             <td class="not-export-col">
	             	<?php 
				        $ParaMeter["GroupId"] = Auth::user()->GroupId;
				        $ParaMeter["AppId"] = 17; //1 for ERP Applications
				        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
				   /*
				    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
	            	<a href="javascript:" class="btn ERPFormShow text-primary" FormName="ReportsReportOfCustomerUsersForm" SectionName="ReportsReportOfCustomerUsersSection" EditId="{{ $User->erp_table_id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
	            	@endif
			        @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
	            	<a href="javascript:" class="btn ERPRecordDelete text-danger" FormName="ReportsReportOfCustomerUsersAdd" DeleteId="{{ $User->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
	            	@endif */
	            	 ?>
	            </td>
	        </tr>
	        <?php
	        if($i!= 0 && $i+1==count($GetAllUsers))
	        { ?>
              <tr>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<!-- <td style="display: none"></td> -->
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td colspan="8" align="right">{{ $CustomerUserCount }}</td>
	        </tr>
	        <?php
	        } 
	        else if($i+1==count($GetAllUsers))
	        { ?>
              <tr>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<!-- <td style="display: none"></td> -->
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td style="display: none"></td>
	        	<td colspan="8" align="right">{{ $CustomerUserCount }}</td>
	        </tr>
	        <?php
	        }
            $OldCustomer=$NewCustomer;
	        $i++;
	        ?>
    @endforeach
    <tr>
    	<td colspan="4" align="left">Grand Summary({{ $CountGetAllUsers }})</td>
    	<td style="display: none"></td>
    	<td style="display: none"></td>
    	<!-- <td style="display: none"></td> -->
    	<td style="display: none"></td>
    	<td style="display: none"></td>
    	<td style="display: none"></td>
    	<td style="display: none"></td>
    	<td colspan="4" align="right">{{ $CountGetAllUsers }}</td>
    </tr>	
@endif
			</tbody>
		    <tfoot>
		        <tr>
		            <th>User ID</th>
			        <th>Login</th>
			        <th>Customer ID</th>
			        <!-- <th>User Password</th> -->
			        <th>Full Name</th>
			        <th>E-Mail</th>
			        <th>Admin Role?</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Report of Customer / Users"))
	</div>
	<div id="ReportsReportOfCustomerUsersForm" style="display: none" class="ReportsReportOfCustomerUsersSection card">
	</div>
	<div id="ReportsReportOfCustomerUsersSearch" style="display: none" class="ReportsReportOfCustomerUsersSection card">
	</div>
</div>
@endif