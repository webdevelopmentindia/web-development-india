<?php
$erp_table_id = $language_id = "";
$enabled_flag = -1;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpLanguageDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpLanguageDetails[0]->erp_table_id;
    $language_id = $ErpLanguageDetails[0]->language_id;
    $enabled_flag = $ErpLanguageDetails[0]->enabled_flag;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="FileLanguagesForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="FileLanguagesList" SectionName="FileLanguagesSection" TabName="ERPFileLanguages">
<div class="row">
<div class="card-header header-elements-inline">
        <div class="page-title">
            <h3>
                <?php 
            if($erp_table_id>0)
            {
                echo strtoupper("Update of erp_Languages");
            }
            else
            {
                echo strtoupper("New record of erp_Languages"); 
            } 
            ?>
            </h3>
        </div>
        <div class="header-elements">
            <span><?php 
                echo " ".date("m/d/Y");
            ?></span>
        <span class="pull-right"></span>
        </div>
    </div> <!-- emloy-hd p-0 -->







  <div class="row row-col">
    <div class="col-md-12">
         @if($erp_table_id>0)
         <a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileLanguagesForm" SectionName="FileLanguagesSection"> Add New </a>
         <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
         @endif
         <button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button>
         <button type="button" class="btn cancel-btn save-btn" ShowSection="FileLanguagesList" SectionName="FileLanguagesSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button>
    </div>
  </div>
    <div class="card-body employee-form">
      <div class="row">
          <div class="col-xs-12">
              <span class="req-field">* Required field(s)</span>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12 p-0">
            <div class="form-group form-flex">
                <label class="control-label col-sm-3">{{ __('Language *') }}</label>
                <div class="col-sm-9">
                    <select id="language_id" class="form-control{{ $errors->has('language_id') ? ' is-invalid' : '' }}" name="language_id">
                          @if(!$ErpLanguagesList->isEmpty())
                             @foreach($ErpLanguagesList as $Language)
                                 <option value="{{ $Language->erp_table_id }}" @if($Language->erp_table_id==$language_id) {{ 'selected' }} @endif>{{ $Language->language_name }}</option>
                             @endforeach
                          @endif
                    </select>
              </div>
            </div>
            <div class="form-group form-flex">
                <label class="control-label col-sm-3">{{ __('Active *') }}</label>
                <div class="col-sm-9 check-f">
                  <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==1 || $enabled_flag==-1){{ "checked" }} @endif>Yes</span>
                  <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0){{ "checked" }} @endif>No</span>
                    @if($errors->has('enabled_flag'))
    	                <span class="invalid-feedback" role="alert">
    	                    <strong>{{ $errors->first('enabled_flag') }}</strong>
    	                </span>
                    @endif
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
* Required field(s)
</form>