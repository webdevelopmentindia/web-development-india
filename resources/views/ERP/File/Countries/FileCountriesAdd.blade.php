<?php
$erp_table_id = $code = $description = "";
$enabled_flag = -1;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpCountryDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpCountryDetails[0]->erp_table_id;
    $code = $ErpCountryDetails[0]->country_code;
    $description = $ErpCountryDetails[0]->description;
    $enabled_flag = $ErpCountryDetails[0]->enabled_flag;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="FileCountriesForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="FileCountriesList" SectionName="FileCountriesSection" TabName="ERPFileCountries">
<div class="row">
<div class="card-header header-elements-inline">
    <div class="page-title">
        <h3>
             <?php 
            if($erp_table_id>0)
            {
                echo strtoupper("Update of erp_countries");
            }
            else
            {
                echo strtoupper("New record of erp_countries"); 
            } 
            ?>
        </h3>
    </div>
    <div class="header-elements">
        <span><?php 
            echo " ".date("m/d/Y");
        ?></span>
    <span class="pull-right"></span>
    </div>
</div> <!-- emloy-hd p-0 -->












    <div class="row row-col">
        <div class="col-md-12">
             @if($erp_table_id>0)
             <a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileCountriesForm" SectionName="FileCountriesSection"> Add New </a>
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button>
             <button type="button" class="btn cancel-btn save-btn" ShowSection="FileCountriesList" SectionName="FileCountriesSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button>
        </div>
    </div>
    <div class="card-body employee-form">
        <div class="row">
            <div class="col-xs-12">
                <span class="req-field">* Required field(s)</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Code *') }}</label>
                    <div class="col-sm-9">
                        <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code"  autofocus placeholder="Code" value="<?php if(old('code')) { echo old('code'); } else { echo $code; }?>" maxlength="5">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Description *') }}</label>
                    <div class="col-sm-9">
                        <input id="description " type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description"  autofocus placeholder="Description" value="<?php if(old('description')) { echo old('description'); } else { echo $description; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Active *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==1||$enabled_flag==-1){{ "checked" }} @endif>Yes</span>
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0){{ "checked" }} @endif>No</span>
                        @if($errors->has('enabled_flag'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('enabled_flag') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>