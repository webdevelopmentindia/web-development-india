@if(!(isset($OnlyData) && $OnlyData=="Change Password"))
<div class="ERPTabManuData card tab-pane fade in active" id="ERPSecurityChangePassword">
	<div id="SecurityChangePasswordList" class="SecurityChangePasswordSection">
@endif
<div class="card-header header-elements-inline">
    <div class="page-title">
      <h3>Change Password</h3>
    </div>
    <div class="header-elements">
      <span><?php
              echo " ".date("m/d/Y");
            ?></span>
    </div>
  </div>
<div id="SecurityChangePasswordListSearch">
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="SecurityChangePasswordList">
    @csrf
<div class="card-body">
	<input id="ERPFormName" type="hidden" name="ERPFormName" value="SecurityChangePassword" SectionName="SecurityChangePasswordSection" TabName="ERPSecurityChangePassword">
  <input id="erp_table_id" type="hidden" name="erp_table_id" value="{{ Auth::user()->id }}">
	<div class="form-group form-flex">
    <label class="control-label col-sm-3">{{ __('Old Password') }}</label>
    <div class="col-sm-9">
      <input id="old_password" type="password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" name="old_password"  autofocus placeholder="Old Password" value="">
    </div>
  </div>
  <div class="form-group form-flex">
    <label class="control-label col-sm-3">{{ __('New Password') }}</label>
    <div class="col-sm-9">
      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"  autofocus placeholder="Old Password" value="">
    </div>
  </div>
  <div class="form-group form-flex">
    <label class="control-label col-sm-3">{{ __('Confirm Password') }}</label>
    <div class="col-sm-9">
      <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation"  autofocus placeholder="Old Password" value="">
    </div>
  </div>
  <?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 15; //15 for ERP Change Password
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
  ?> 
  @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
  <div class="col-sm-9 col-sm-offset-3">
    <input type="submit" class="btn save-btn" name="OK" value="OK">
  </div>
  @endif
</div>
</form>
</div>
<div class="card-body">
	<div class="table-responsive">
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Change Password"))
	</div>
	<div id="SecurityChangePasswordForm" style="display: none" class="SecurityChangePasswordSection">
	</div>
	<div id="SecurityChangePasswordSearch" style="display: none" class="SecurityChangePasswordSection">
	</div>
</div>
@endif