<div class="close-btn HideAddFilterModal" ModalId="FileLanguagesAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>

@if($ParaMeter["FilterName"]=="FileLanguagesLanguage")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Language') }}</label>
    <div class="col-sm-3">
	    <select id="language_condition" name="language_condition" class="form-control ERPSearchEmptyContent" FieldId="language">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="language" type="text" class="form-control" name="language"  autofocus placeholder="Language" value="">
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileLanguagesList" FieldNameCondition="language_condition" FieldName="language"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileLanguagesEnabledFlag")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
    <div class="col-sm-3">
      <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 check-f">
        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" checked>Yes</span>
        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0">No</span>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileLanguagesList" FieldNameCondition="enabled_flag_condition"> Apply </a></div>
</div>
@endif