<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpThemesList extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    Static Public function GetAllErpThemesLists($ParaMeter)
    {
        $GetAllErpThemesLists = ErpThemesList::select("erp_themes_lists.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpThemesLists = $GetAllErpThemesLists->addselect("erp_themes_lists.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpThemesLists = $GetAllErpThemesLists->where('erp_themes_lists.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_themes_lists.*"); 
        }
        $GetAllErpThemesLists = $GetAllErpThemesLists->orderBy("erp_themes_lists.id","DESC")
            ->get();
        return $GetAllErpThemesLists;
    }
}