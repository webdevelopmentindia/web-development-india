<?php
$erp_table_id = $country_id = $full_name = $tax_id = $total_licenses = $address1 = $address2 = $office_phone = $mobile_phone = "";
$service_status = $enabled_flag = -2;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ERPCustomerDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ERPCustomerDetails[0]->erp_table_id;
    $country_id = $ERPCustomerDetails[0]->country_id;
    $full_name = $ERPCustomerDetails[0]->full_name;
    $tax_id = $ERPCustomerDetails[0]->tax_id;
    $total_licenses = $ERPCustomerDetails[0]->total_licenses;
    $address1 = $ERPCustomerDetails[0]->address1;
    $address2 = $ERPCustomerDetails[0]->address2;
    $office_phone = $ERPCustomerDetails[0]->office_phone;
    $mobile_phone = $ERPCustomerDetails[0]->mobile_phone;
    $service_status = $ERPCustomerDetails[0]->service_status;
    $enabled_flag = $ERPCustomerDetails[0]->enabled_flag;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="FileCustomerForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="FileCustomerList" SectionName="FileCustomersSection" TabName="ERPFileCustomers">
<div class="row">
    <div class="card-header header-elements-inline">
        <div class="page-title">
            <h3>
                <?php 
                if($erp_table_id>0)
                {
                    echo strtoupper("Update of erp_customers");
                }
                else
                {
                    echo strtoupper("New record of erp_customers"); 
                } 
                ?>
            </h3>
        </div>
        <div class="header-elements">
            <span><?php 
                echo " ".date("m/d/Y");
            ?></span>
        <span class="pull-right"></span>
        </div>
    </div> <!-- emloy-hd p-0 -->







    <div class="row row-col">
        <div class="col-md-12">
             @if($erp_table_id>0)
             <span class="link-src"><a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileCustomerForm" SectionName="FileCustomersSection"> Add New </a>
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <span class="link-src"><button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button></span>
             <span class="link-src"><button type="button" class="btn cancel-btn save-btn" ShowSection="FileCustomerList" SectionName="FileCustomersSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button></span>
        </div>
    </div>
    <div class="card-body employee-form">
        <div class="row">
            <div class="col-xs-12">
                <span class="req-field">* Required field(s)</span>
            </div>
        </div>
         <div class="row">
            <div class="col-md-12 p-0">
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Country ID *') }}</label>
                    <div class="col-sm-9">

                        <!-- <input id="country_id" type="text" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" name="country_id"  autofocus placeholder="Country ID" value="<?php //if(old('country_id')) { echo old('country_id'); } else { echo $country_id; }?>"> -->

                        <select id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" name="country_id">
                              @if(!$ERPCountries->isEmpty())
                                 @foreach($ERPCountries as $Country)
                                     <option value="{{ $Country->erp_table_id }}" @if($Country->erp_table_id==$country_id) {{ 'selected' }} @endif>{{ $Country->country_code."-".$Country->description }}</option>
                                 @endforeach
                              @endif
                        </select>
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Full Name  *') }}</label>
                    <div class="col-sm-9">
                        <input id="full_name " type="text" class="form-control{{ $errors->has('full_name') ? ' is-invalid' : '' }}" name="full_name"  autofocus placeholder="Full Name" value="<?php if(old('full_name')) { echo old('full_name'); } else { echo $full_name; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Tax ID *') }}</label>
                    <div class="col-sm-9">
                        <input id="tax_id" type="text" class="form-control{{ $errors->has('tax_id') ? ' is-invalid' : '' }}" name="tax_id"  autofocus placeholder="Tax ID" value="<?php if(old('tax_id')) { echo old('tax_id'); } else { echo $tax_id; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Total Licenses *') }}</label>
                    <div class="col-sm-9">
                        <input id="total_licenses" type="number" class="form-control{{ $errors->has('total_licenses') ? ' is-invalid' : '' }}" name="total_licenses"  autofocus placeholder="Total Licenses" value="<?php if(old('total_licenses')) { echo old('total_licenses'); } else { echo $total_licenses; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Address1 *') }}</label>
                    <div class="col-sm-9">
                        <input id="address1" type="text" class="form-control{{ $errors->has('address1') ? ' is-invalid' : '' }}" name="address1"  autofocus placeholder="Address1" value="<?php if(old('address1')) { echo old('address1'); } else { echo $address1; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Address2') }}</label>
                    <div class="col-sm-9">
                        <input id="address2" type="text" class="form-control{{ $errors->has('address2') ? ' is-invalid' : '' }}" name="taddress2ax_id"  autofocus placeholder="Address2" value="<?php if(old('address2')) { echo old('address2'); } else { echo $address2; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Office phone') }}</label>
                    <div class="col-sm-9">
                        <input id="office_phone" type="text" class="form-control" name="office_phone"  autofocus placeholder="Office phone" value="<?php if(old('office_phone')) { echo old('office_phone'); } else { echo $office_phone; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Mobile phone') }}</label>
                    <div class="col-sm-9">
                        <input id="mobile_phone" type="text" class="form-control{{ $errors->has('mobile_phone') ? ' is-invalid' : '' }}" name="mobile_phone"  autofocus placeholder="Mobile phone" value="<?php if(old('mobile_phone')) { echo old('mobile_phone'); } else { echo $mobile_phone; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Service status *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="service_status" type="radio" class="" name="service_status" value="1" @if($service_status==-2 || $service_status==1) {{ 'checked' }} @endif>Active</span>
                        <span class="mr-1"><input id="service_status" type="radio" class="" name="service_status" value="0" @if($service_status==0) {{ 'checked' }} @endif>Suspended</span>
                        <span class="mr-1"><input id="service_status" type="radio" class="" name="service_status" value="-1" @if($service_status==-1) {{ 'checked' }} @endif>Terminated</span>
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Active *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==-2 || $enabled_flag==1) {{ 'checked' }} @endif>Yes</span>
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0) {{ 'checked' }} @endif>No</span>
                        @if($errors->has('enabled_flag'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('enabled_flag') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>