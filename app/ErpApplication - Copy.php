<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ErpApplication extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpApplication($request)
    {
        if($request->erp_table_id>0)
        {
            $ErpApplication = ErpApplication::find($request->erp_table_id);
        }
        else
        {
            $ErpApplication = new ErpApplication();  
        } 

        if($request->hasFile('app_icon'))
        {
            if($request->erp_table_id>0)
            {
                if($ErpApplication->app_icon!=null && file_exists(public_path().'/images/ERP/ApplicationIcon/'.$ErpApplication->app_icon))
                {
                   unlink(public_path().'/images/ERP/ApplicationIcon/'.$ErpApplication->app_icon);
                }
            }
            $files = $request->file('app_icon');
            $filename = "erp_file_app_icon".time()."_".$files->getClientOriginalName();
            if($files->move(public_path().'/images/ERP/ApplicationIcon/', $filename))
            {
               $ErpApplication->app_icon = $filename;
            }
        }

        if($request->hasFile('app_picture'))
        {
            if($request->erp_table_id>0)
            {
                if($ErpApplication->app_picture!=null && file_exists(public_path().'/images/ERP/ApplicationImage/'.$ErpApplication->app_picture))
                {
                   unlink(public_path().'/images/ERP/ApplicationImage/'.$ErpApplication->app_picture);
                }
            }
            $files = $request->file('app_picture');
            $filename = "erp_file_app_image".time()."_".$files->getClientOriginalName();
            if($files->move(public_path().'/images/ERP/ApplicationImage/', $filename))
            {
               $ErpApplication->app_picture = $filename;
            }
        }

        $ErpApplication->app_name = $request->app_name;
        $ErpApplication->description = $request->description;
        $ErpApplication->app_url = $request->app_url;
        $ErpApplication->app_deployed = $request->app_deployed;
        $ErpApplication->enabled_flag = $request->enabled_flag;
        $ErpApplication->save();
        return $ErpApplication->id;
    }
    Static Public function GetAllERPApplications($ParaMeter)
    {
        $GetAllERPApplications = ErpApplication::select("erp_applications.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllERPApplications =$GetAllERPApplications->addselect("erp_applications.*"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllERPApplications =$GetAllERPApplications->where('erp_applications.id',$ParaMeter["ERPTableId"])
                                               ->addselect("erp_applications.*"); 
        }
        else if(isset($ParaMeter["FileApplicationSearch"]) && $ParaMeter["FileApplicationSearch"]!="")
        {
            //$FileApplicationSearch = array();
            $FileApplicationSearch = $ParaMeter["FileApplicationSearchArray"];

            // if((isset($ParaMeter["AppDeployed"]) && $ParaMeter["AppDeployed"]>=0) && (isset($ParaMeter["AppDeployedCondition"]) && $ParaMeter["AppDeployedCondition"]!=""))
            // {
            //   $FileApplicationSearch[] = array("TableName" => "erp_applications","ColumnName" => "app_deployed","ColumnValue" => $ParaMeter["AppDeployed"],"ColumnCondition" => $ParaMeter["AppDeployedCondition"]);
            // }

            // if((isset($ParaMeter["AppName"]) && $ParaMeter["AppName"]!="") && (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]!=""))
            // {
            //   $FileApplicationSearch[] = array("TableName" => "erp_applications","ColumnName" => "app_name","ColumnValue" => $ParaMeter["AppName"],"ColumnCondition" => $ParaMeter["AppNameCondition"]);
            // }

            // if((isset($ParaMeter["AppURL"]) && $ParaMeter["AppURL"]!="") && (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]!=""))
            // {
            //   $FileApplicationSearch[] = array("TableName" => "erp_applications","ColumnName" => "app_url","ColumnValue" => $ParaMeter["AppURL"],"ColumnCondition" => $ParaMeter["AppURLCondition"]);
            // }

            // if((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!=""))
            // {
            //   $FileApplicationSearch[] = array("TableName" => "erp_applications","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);
            // }

            if(count($FileApplicationSearch)>0)
            {
              foreach ($FileApplicationSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];
                if($ColumnCondition=="Greater than")
                {
                  $GetAllERPApplications = $GetAllERPApplications->where("$TableName.$ColumnName",">",$ColumnValue);
                }
                else if($ColumnCondition=="Less than")
                {
                  $GetAllERPApplications = $GetAllERPApplications->where("$TableName.$ColumnName","<",$ColumnValue);
                }
                else if($ColumnCondition=="Equal")
                {
                  $GetAllERPApplications = $GetAllERPApplications->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition=="Contains")
                {
                  $GetAllERPApplications = $GetAllERPApplications->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition=="Not Contains")
                {
                  $GetAllERPApplications = $GetAllERPApplications->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition=="Empty Content")
                {
                  $GetAllERPApplications = $GetAllERPApplications->whereNull("$TableName.$ColumnName");
                }
              }
            }
            $GetAllERPApplications = $GetAllERPApplications->addSelect("erp_applications.*");
        }
        $GetAllERPApplications = $GetAllERPApplications->orderBy("erp_applications.id","DESC")
                                            ->get();
        return $GetAllERPApplications;
    }
    Static Public function DeleteERPApplications($ParaMeter)
    {
        $ErpApplication = ErpApplication::find($ParaMeter["DeleteId"]);
        if($ErpApplication->app_picture!=null && file_exists(public_path().'/images/ERP/ApplicationImage/'.$ErpApplication->app_picture))
        {
           unlink(public_path().'/images/ERP/ApplicationImage/'.$ErpApplication->app_picture);
        }
        if($ErpApplication->app_icon!=null && file_exists(public_path().'/images/ERP/ApplicationIcon/'.$ErpApplication->app_icon))
        {
           unlink(public_path().'/images/ERP/ApplicationIcon/'.$ErpApplication->app_icon);
        }
        $ErpApplication->delete();
        return $ErpApplication->id;
    }
}