<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class Group extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];

    static public function AddGroup($request)
    {
        if($request->erp_table_id>0)
        {
            $Group = Group::find($request->erp_table_id);
        }
        else
        {
            $Group = new Group();  
        }

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }
        $Group->description  = $request->description;
        $Group->save();
        return $Group->id;
    }
    Static Public function GetAllGroups($ParaMeter)
    {
        $GetAllGroups = Group::select("groups.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllGroups = $GetAllGroups->addselect("groups.*");
           if(isset($ParaMeter["Login"]) && $ParaMeter["Login"]!="") 
           {
              $UserLogin = $ParaMeter["Login"];
              $GetAllGroups = $GetAllGroups->leftjoin('user_groups', function ($join) use ($UserLogin) {
                          $join->on('groups.id', '=', 'user_groups.group_id')
                               ->where('user_groups.user_id',$UserLogin);
                        })
                        ->addselect("user_groups.group_id");
           }
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllGroups =$GetAllGroups->where('groups.id',$ParaMeter["ERPTableId"])
                            ->addselect("groups.*"); 
        }
        $GetAllGroups = $GetAllGroups->orderBy("groups.id","DESC")
                                            ->get();
        return $GetAllGroups;
    }
    Static Public function DeleteGroups($ParaMeter)
    {
        $Group = Group::find($ParaMeter["DeleteId"]);
        $Group->delete();
        return $Group->id;
    }
}
