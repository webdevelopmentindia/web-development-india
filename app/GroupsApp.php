<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class GroupsApp extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];

    static public function AddGroupsApp($request)
    {
        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }
        // echo "<pre>";
        // print_r($request->all());
        // die();
        if(count($request->permissions)>0)
        {
          foreach ($request->permissions as $key => $value) 
          {
            $Access = "N";
            if($value[0]=="access")
            {
              $Access = "Y";
            }

            $Insert = "N";
            $Delete = "N";
            $Update = "N";
            $Export = "N";
            $Print = "N";
            
            $GroupsApp = new GroupsApp(); 
            if($key!="" && $request->erp_table_id)
            {
              $GroupsAppDetails = GroupsApp::select("groups_apps.id")->where("app_id",$key)->where("group_id",$request->erp_table_id)->get();
              if(!$GroupsAppDetails->isEmpty())
              {
                $GroupsApp = GroupsApp::find($GroupsAppDetails[0]->id);
              }
            }

            if($Access=="Y")
            {
              for ($i=1; $i < count($value); $i++) {
                if($value[$i]=="insert")
                {
                  $Insert = "Y";
                }
                else if($value[$i]=="delete")
                {
                  $Delete = "Y";
                }
                else if($value[$i]=="update")
                {
                  $Update = "Y";
                }
                else if($value[$i]=="export")
                {
                  $Export = "Y";
                }
                else if($value[$i]=="print")
                {
                  $Print = "Y";
                } 
              }//for
              
              $GroupsApp->group_id = $request->erp_table_id;
              $GroupsApp->app_id = $key;
              $GroupsApp->priv_access = $Access;
              $GroupsApp->priv_insert = $Insert;
              $GroupsApp->priv_delete = $Delete;
              $GroupsApp->priv_update = $Update;
              $GroupsApp->priv_export = $Export;
              $GroupsApp->priv_print = $Print;
              $GroupsApp->save();

            }//if
            else if(!$GroupsAppDetails->isEmpty() && $GroupsApp->priv_access=="Y")
            {
              $GroupsApp->group_id = $request->erp_table_id;
              $GroupsApp->app_id = $key;
              $GroupsApp->priv_access = $Access;
              $GroupsApp->priv_insert = $Insert;
              $GroupsApp->priv_delete = $Delete;
              $GroupsApp->priv_update = $Update;
              $GroupsApp->priv_export = $Export;
              $GroupsApp->priv_print = $Print;
              $GroupsApp->save();
            }
          }//foreach
      }//if
      return 1;
    }
    Static Public function GetAllGroupsApps($ParaMeter)
    {
        $GetAllGroupsApps = GroupsApp::select("groups_apps.id as erp_table_id");
        if((isset($ParaMeter["GroupId"]) && $ParaMeter["GroupId"]>0) && (isset($ParaMeter["AppId"]) && $ParaMeter["AppId"]>0))
        {
          $GetAllGroupsApps = $GetAllGroupsApps->addselect("groups_apps.*")
                                                ->where("groups_apps.group_id",$ParaMeter["GroupId"])
                                                ->where("groups_apps.app_id",$ParaMeter["AppId"]);
        }
        $GetAllGroupsApps = $GetAllGroupsApps->get();
        return $GetAllGroupsApps;
    }
    Static public function ExportPermissionCheck($ParaMeter)
    {
       $ParaMeter1["GroupId"] = Auth::user()->GroupId;
       $ParaMeter1["AppId"] = $ParaMeter["AppId"];
       $GetAllGroupsApps = GroupsApp::GetAllGroupsApps($ParaMeter1);
       if(!$GetAllGroupsApps->isEmpty())
       {
          return $GetAllGroupsApps[0]->priv_export;
       }
       return "N";
    }
}