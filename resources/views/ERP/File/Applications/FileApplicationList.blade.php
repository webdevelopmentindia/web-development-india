@if(!(isset($OnlyData) && $OnlyData=="ERP Applications"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPFileApplications">
	<div id="FileApplicationList" class="FileApplicationsSection">
@endif
<div class="card">
	<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>Applications</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
	<?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 1; //1 for ERP Applications
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?>
        
	<div class="row row-col">
		<div class="col-xs-12">
				@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
				<span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileApplicationForm" SectionName="FileApplicationsSection" > Add New </a>
				</span>
				@endif
				<span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="FileApplicationSearch" SectionName="FileApplicationsSection" FilterChangeDivEmpty="ERPFileApplicationFilterChange"> Search </a></span>
		</div>
	</div>
<?php 
$CheckParameter = 0;
?>
<div id="FileApplicationListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileApplicationList" SectionName="FileApplicationsSection" TabName="ERPFileApplications">	
@if(isset($ParaMeter))
    @if(
    (((isset($ParaMeter["AppName"]) && $ParaMeter["AppName"]!="") && (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]!="")) || (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]==4))
    ||
    (((isset($ParaMeter["AppURL"]) && $ParaMeter["AppURL"]!="") && (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]!="")) || (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]==4))
    ||
    (((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
    ||
    ((isset($ParaMeter["AppDeployed"]) && $ParaMeter["AppDeployed"]>=0) && (isset($ParaMeter["AppDeployedCondition"]) && $ParaMeter["AppDeployedCondition"]!=""))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if(((isset($ParaMeter["AppName"]) && $ParaMeter["AppName"]!="") && (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]!="")) || (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]==4))
		<?php 
		$CheckParameter = 1;
		?>
        <!-- <div id="SearchAppName" class="SearchSection">
	        <div PageName="ERPFileApplication" FilterName="FileApplicationAppName" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AppNameCondition'] }}" ShowDivClass="ERPFileApplicationFilterChange"
	        FieldNameValue='@if($ParaMeter["AppNameCondition"]!=4){{$ParaMeter["AppName"]}} @endif'>
		        App Name: {{ $ParaMeter["AppNameConditionValue"] }} 
		        @if($ParaMeter["AppNameCondition"]!=4)
		            {{ $ParaMeter["AppName"] }} 
		        @endif
            </div>

	        <input type="hidden" id="app_name" name="app_name" value='{{ $ParaMeter["AppName"] }}'>
	        <input type="hidden" id="app_name_condition" name="app_name_condition" value='{{ $ParaMeter["AppNameCondition"] }}'>
	        <div style="display: none" class="ERPFileApplicationFilterChange">
	        </div>
	        <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileApplicationList" RemoveSearch="SearchAppName" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileApplicationFilterChange"> Remove </a>
        </div> -->

        <div id="SearchAppName" class="SearchSection addfield-col">

	        <input type="hidden" id="app_name" name="app_name" value='{{ $ParaMeter["AppName"] }}'>
	        <input type="hidden" id="app_name_condition" name="app_name_condition" value='{{ $ParaMeter["AppNameCondition"] }}'>
	        <div style="display: none" class="ERPFileApplicationFilterChange add-drop-down">
	        </div>

	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        	<div PageName="ERPFileApplication" FilterName="FileApplicationAppName" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AppNameCondition'] }}" ShowDivClass="ERPFileApplicationFilterChange"
	        FieldNameValue='@if($ParaMeter["AppNameCondition"]!=4){{$ParaMeter["AppName"]}} @endif' FormName="FileApplicationList" AddFilterPopup="FileApplicationAddFilterShow">
		        App Name: {{ $ParaMeter["AppNameConditionValue"] }} 
		        @if($ParaMeter["AppNameCondition"]!=4)
		            {{ $ParaMeter["AppName"] }} 
		        @endif
            	</div>
	        	<a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileApplicationList" RemoveSearch="SearchAppName" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileApplicationFilterChange">
	        		<i class="fa fa-times" aria-hidden="true"></i> 
	        	</a>
	        </span>
        </div>   
   @else
        <?php 
			$AddFilterOptions.='<li value="FileApplicationAppName" id="ERPFileApplicationSearchList" name="ERPFileApplicationSearchList" class="ERPAddFilterShow" ShowDivId="FileApplicationAddFilterShow" FilterChangeModal="ERPFileApplicationFilterChange" FormName="FileApplicationList">App name</li>';
		?>
   @endif <!-- for AppName -->
   @if(((isset($ParaMeter["AppURL"]) && $ParaMeter["AppURL"]!="") && (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]!="")) || (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchAppURL" class="SearchSection addfield-col"> 
        	
	        <input type="hidden" name="app_url" value='{{ $ParaMeter["AppURL"] }}'>
	        <input type="hidden" name="app_url_condition" value='{{ $ParaMeter["AppURLCondition"] }}'>
	        <div style="display: none" class="ERPFileApplicationFilterChange add-drop-down">
	        </div>
	         <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileApplication" FilterName="FileApplicationAppURL" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AppURLCondition'] }}" ShowDivClass="ERPFileApplicationFilterChange"
		        FieldNameValue='@if($ParaMeter["AppURLCondition"]!=4){{$ParaMeter["AppURL"]}} @endif' FormName="FileApplicationList" AddFilterPopup="FileApplicationAddFilterShow">
			        App URL: {{ $ParaMeter["AppURLConditionValue"] }} 
			        @if(isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]!=4)
			            {{ $ParaMeter["AppURL"] }} 
			        @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileApplicationList" RemoveSearch="SearchAppURL" RemoveSearchType="ColumnSearchRemove">
		        	<i class="fa fa-times" aria-hidden="true"></i> 
		        </a>
		    </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="FileApplicationAppURL" id="ERPFileApplicationSearchList" name="ERPFileApplicationSearchList" class="ERPAddFilterShow" ShowDivId="FileApplicationAddFilterShow" FilterChangeModal="ERPFileApplicationFilterChange" FormName="FileApplicationList">App URL</li>';
		?>
   @endif <!-- for AppURL -->
   @if(((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchDescription" class="SearchSection addfield-col">
            
            <input type="hidden" name="description" value='{{ $ParaMeter["Description"] }}'>
            <input type="hidden" name="description_condition" value='{{ $ParaMeter["DescriptionCondition"] }}'>
            <div style="display: none" class="ERPFileApplicationFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPFileApplication" FilterName="FileApplicationDescription" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['DescriptionCondition'] }}" ShowDivClass="ERPFileApplicationFilterChange"
	        FieldNameValue='@if($ParaMeter["DescriptionCondition"]!=4){{$ParaMeter["Description"]}} @endif' FormName="FileApplicationList" AddFilterPopup="FileApplicationAddFilterShow">
	        	Description: {{ $ParaMeter["DescriptionConditionValue"] }} 
	        	@if(isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!=4)
		            {{ $ParaMeter["Description"] }} 
		        @endif
		    </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileApplicationList" RemoveSearch="SearchDescription" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i></a>


        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="FileApplicationDescription" id="ERPFileApplicationSearchList" name="ERPFileApplicationSearchList" class="ERPAddFilterShow" ShowDivId="FileApplicationAddFilterShow" FilterChangeModal="ERPFileApplicationFilterChange" FormName="FileApplicationList">Description</li>';
		?>
   @endif <!-- for Description -->
   @if((isset($ParaMeter["AppDeployed"]) && $ParaMeter["AppDeployed"]>=0) && (isset($ParaMeter["AppDeployedCondition"]) && $ParaMeter["AppDeployedCondition"]!=""))
        <?php 
		$CheckParameter = 1;
		?>
	
        <div id="SearchAppDeployed" class="SearchSection addfield-col">
        	
            <input type="hidden" name="app_deployed" value='{{ $ParaMeter["AppDeployed"] }}'>
            <input type="hidden" name="app_deployed_condition" value='{{ $ParaMeter["AppDeployedCondition"] }}'>
            <div style="display: none" class="ERPFileApplicationFilterChange add-drop-down">
	        </div>
	    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	    	<div PageName="ERPFileApplication" FilterName="FileApplicationAppDeployed" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AppDeployedCondition'] }}" ShowDivClass="ERPFileApplicationFilterChange"
	        FieldNameValue='{{$ParaMeter["AppDeployed"]}}' FormName="FileApplicationList" AddFilterPopup="FileApplicationAddFilterShow">
	        	App deployed?: {{ $ParaMeter["AppDeployedConditionValue"] }} 
	            @if($ParaMeter["AppDeployed"]==1) {{ 'Yes' }} @else {{ 'No' }} @endif
	        </div>	
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileApplicationList" RemoveSearch="SearchAppDeployed" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i> </a>
        </span>
        </div>
    @else
	    <?php 
			$AddFilterOptions.='<li value="FileApplicationAppDeployed" id="ERPFileApplicationSearchList" name="ERPFileApplicationSearchList" class="ERPAddFilterShow" ShowDivId="FileApplicationAddFilterShow" FilterChangeModal="ERPFileApplicationFilterChange" FormName="FileApplicationList">App deployed?</li>';
		?>

   @endif <!-- for Appdeployed -->
   @if($CheckParameter)
	   	<div class="addfield-col">
		   <!-- <select id="ERPFileApplicationSearchList" name="ERPFileApplicationSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="FileApplicationAddFilterShow" FilterChangeModal="ERPFileApplicationFilterChange" FormName="FileApplicationList">
		   	    <option value=''>+ Add Filter</option>
		   	    <?php echo $AddFilterOptions; ?>
		   </select> -->
		    @if($AddFilterOptions!="")
			    <div class="dropdown">
				  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				    <?php echo $AddFilterOptions; ?>
				  </ul>
				</div>
			@endif
		   <div id="FileApplicationAddFilterShow" class="add-drop-down popclose" style="display: none">
		   	
	   		</div>
		</div>
		<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
	        <select id="erp_filter_list" name="erp_filter_list" class="form-control ERPSearchBySaveFilter" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" SectionName="FileApplicationList" ManuName="ERPFileApplications" FormName="">
	   @if(!$GetAllErpSaveFilters->isEmpty())
	       	  <option value=""></option>
	          <option value="">Public</option>
		      @foreach($GetAllErpSaveFilters as $Filter)
		   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
		   	  @endforeach
	   @endif 
	        </select>
	   </div>
	   <!-- GetAllErpSaveFilters -->
	   	<div class="addfield-col"><button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="FileApplicationList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPFileApplicationFilterChange" HideAddFilterPopup="FileApplicationAddFilterShow">Save Filter</button></div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileApplicationList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="FileApplicationList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
	   		<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="FileApplicationList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
	   	</div>
    @endif <!-- CheckParameter -->
    @if(
    (((isset($ParaMeter["AppName"]) && $ParaMeter["AppName"]!="") && (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]!="")) || (isset($ParaMeter["AppNameCondition"]) && $ParaMeter["AppNameCondition"]==4))
    ||
    (((isset($ParaMeter["AppURL"]) && $ParaMeter["AppURL"]!="") && (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]!="")) || (isset($ParaMeter["AppURLCondition"]) && $ParaMeter["AppURLCondition"]==4))
    ||
    (((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
    ||
    ((isset($ParaMeter["AppDeployed"]) && $ParaMeter["AppDeployed"]>=0) && (isset($ParaMeter["AppDeployedCondition"]) && $ParaMeter["AppDeployedCondition"]!=""))
    )
    </div>
</div>
    @endif
@endif <!-- ParaMeter -->
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPFileApplicationstable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>App Id</th>
			        <th>App Name</th>
			        <th>Description</th>
			        <th>App URL</th>
			        <th>App Icon</th>
			        <th>App Image</th>
			        <th>App deployed</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$ERPApplications->isEmpty())
				    @foreach($ERPApplications as $Application)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Application->erp_table_id }}</td>
			                <td>{{ $Application->app_name }}</td>
			                <td>{{ $Application->description }}</td>
			                <td>{{ $Application->app_url }}</td>
			                <td>
				                @if($Application->app_icon!=null && file_exists(public_path().'/images/ERP/ApplicationIcon/'.$Application->app_icon))
				                <img src="{{ url('public/images/ERP/ApplicationIcon/'.$Application->app_icon) }}" width="100px" height="100px" />
				                @endif
			                </td>
			                <td>
			                	@if($Application->app_picture!=null && file_exists(public_path().'/images/ERP/ApplicationImage/'.$Application->app_picture))
			                	<img src="{{ url('public/images/ERP/ApplicationImage/'.$Application->app_picture) }}" width="100px" height="100px" />
			                	@endif
			                </td>
			                <td>
			                    @if($Application->app_deployed==1)
			                	   {{ 'Yes' }}
			                	@elseif($Application->app_deployed==0)
			                	   {{ 'No' }}
			                	@endif</td>
			                <td>
			                	@if($Application->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Application->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                 <td class="not-export-col">
			                 	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn ERPFormShow text-primary" FormName="FileApplicationForm" SectionName="FileApplicationsSection" EditId="{{ $Application->erp_table_id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                	<a href="javascript:" class="btn ERPRecordDelete text-danger" FormName="FileApplicationAdd" DeleteId="{{ $Application->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
			                	@endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>App Id</th>
			        <th>App Name</th>
			        <th>Description</th>
			        <th>App URL</th>
			        <th>App Icon</th>
			        <th>App Image</th>
			        <th>App deployed</th>
			        <th>Active</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="ERP Applications"))
	</div>
	<div id="FileApplicationForm" style="display: none" class="FileApplicationsSection card">
	</div>
	<div id="FileApplicationSearch" style="display: none" class="FileApplicationsSection card">
	</div>
</div>
@endif