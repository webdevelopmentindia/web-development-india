@extends('layouts.Backend')
@section('content')
@include('Include.navBarLeft')
<?php
//die("admin");
?>
    <div class="container-fluid content">
      <section>
      </section>
      <section>
        <div class="dash-tab-show clearfix">
          <ul class="nav nav-tabs" id="ERPManuTabList"  style="display: none">
            <!-- <li class="active rightselect1 dropdown">
              <a data-toggle="tab" href="#menu1">ERP Applications 2</a>
              <span class="close-tab"><i class="fa fa-times" aria-hidden="true"></i></span>
              <div class="dropdown-menu dropdown-menu-sm" id="context-menu1">
                <ul>
                  <li><a href="#">HTML</a></li>
                  <li><a href="#">CSS</a></li>
                  <li><a href="#">JavaScript</a></li>
                </ul>
              </div>
            </li> -->
            <!--<li><a data-toggle="tab" href="#menu2">ERP Customers</a></li>
            <li><a data-toggle="tab" href="#menu3">Countries</a></li>
            <li><a data-toggle="tab" href="#menu4">Sources</a></li>
            <li><a data-toggle="tab" href="#menu5">ERP Themes</a></li>
            <li><a data-toggle="tab" href="#menu6">ERP Languages</a></li> -->
          </ul>

          <!-- <div class="tab-content">
            <div id="menu1" class="tab-pane fade in active">
              jk
            </div>
            <div id="menu2" class="tab-pane fade">
             lk
            </div>
            <div id="menu3" class="tab-pane fade">
              
            </div>
            <div id="menu4" class="tab-pane fade">
              
            </div>
            <div id="menu5" class="tab-pane fade">
             
            </div>
            <div id="menu6" class="tab-pane fade">
              
            </div>
          </div> -->
          <!-- 
          <ul id="ERPManuTabList">
          </ul>
          -->
        </div>
        <div class="dashboarpage clearfix tab-content" id="Dashboard">
          <div id="ERPDefaultPage" class="ERPTabManuData">
            <div class="row">
          <div class="col-xs-12">
            <div class="jumbotron">
              <h1>DELTA ERP</h1>      
              <p>This is some text if you want to extend to the screen edges</p>
              <div class="lear-more">
                <a href="#">Learn more</a>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-2">
          <div class="flex-column-dash">
            <div class="col-md-4">
              <div class="card card-body card-height">
                <div class="d-flex">
                   <h4 class="text-top-h">Total User</h4>
                  <span>
                    <select class="form-control">
                      <option>30 Days</option>
                      <option>60 Days</option>
                    </select>
                  </span>
                </div>
                <h3 class="title-head-dash">85</h3>
                <div class="progress-point">
                  <span><img src="{{ asset('public/images/ERP/ApplicationIcon/decline.png') }}"> 3.41% Decrease</span>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-height">
                <div class="card-body">
                  <div class="d-flex">
                    <h4 class="text-top-h">New User</h4>
                    <span>
                      <select class="form-control">
                        <option>30 Days</option>
                        <option>60 Days</option>
                      </select>
                    </span>
                  </div>
                  <h3 class="title-head-dash">10</h3>
                </div>
                <div class="line-">
                 <canvas class="mt-n4 chartjs-render-monitor" height="60" id="total-revenue" width="508"></canvas>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-body card-height">
                <div class="d-flex">
                  <h4 class="text-top-h">Active User</h3>
                  <span>(506 Total)</span>
                </div>
                <div class="d-flex">
                  <div class="active-person">
                    <span><b><i class="fa fa-circle" aria-hidden="true" style="color: #ff9800;"></i></b> Inactive (269 - 53.16%)</span>
                    <span><b><i class="fa fa-circle" aria-hidden="true" style="color: #ffc107"></i></b>Active (237 - 46.84%)</span>
                  </div>
                  <div class="pie-chart">
                    <figure class="chart__figure">
                      <canvas class="chart__canvas" id="chartCanvas" width="120" height="120" aria-label="Example doughnut chart showing data as a percentage" role="img"></canvas>
                    </figure>
                    <div class="chart__value"><p>84%</p></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
        <div class="row mt-2">
          <div class="flex-column">
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa blue-bg"><i class="fa fa-file-text" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">General Ledger</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa green-light-bg"><i class="fa fa-list" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Project Management</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa red-bg"><i class="fa fa-money" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Expense</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa green-bg"><i class="fa fa-usd" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Finance</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa indigo-bg"><i class="fa fa-truck" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Supply Chain</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa orange-bg"><i class="fa fa-line-chart" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Sales</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa cyan-bg"><i class="fa fa-car" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Rental Business</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa purple-bg"><i class="fa fa-users" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Human Resources</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card-body-box">
                <a href="#" class="dash-vapor">
                  <div class="icon-box-dash">
                    <span class="icon-fa pink-bg"><i class="fa fa-list" aria-hidden="true"></i></span>
                  </div>
                  <div class="text-dash-box">
                    <h3 class="title-head">Management</h3>
                    <p class="text-disc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                  </div>
                </a>
              </div>
            </div>
        </div>
      </div>
          </div>
        </div>
      </section>
    </div>
    </div>
  </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>
<script type="text/javascript" src="{{ asset('public/js/line-chart.js') }}"></script>


<!---line chart---->

<!---Pie chart--->
  
<script type="text/javascript">
  // This demo uses the Chartjs javascript library
// Simple yet flexible JavaScript charting for designers & developers
// Webite: https://www.chartjs.org
// CDN: https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js

let percentValue = 84; // Set The Single Percentage Value Here

const colorGreen = "#f98f37", // Set color of chart
animationTime = "1400"; // Set speed/duration of the animation

const chartCanvas = document.getElementById("chartCanvas"), // Canvas element ID
chartContainer = document.getElementById("chartContainer"), // Char container element ID
divElement = document.createElement("div"), // Div element to hold and show percentage value in the center on the chart
domString = '<div class="chart__value"><p>' + percentValue + "%</p></div>"; // String holding markup for above div

// Create a new Chart object
const doughnutChart = new Chart(chartCanvas, {
  type: "doughnut", // Set the chart to be a doughnut chart
  data: {
    datasets: [
    {
      data: [percentValue, 100 - percentValue], // Set the value shown in the chart as a percentage (out of 100)
      backgroundColor: [colorGreen], // The background color of the filled chart
      borderWidth: 0 // Width of border around the chart
    }] },


  options: {
    cutoutPercentage: 84, // The percentage of the middle cut out of the chart
    responsive: false, // Set the chart to not be responsive
    tooltips: {
      enabled: false // Hide tooltips
    } } });



Chart.defaults.global.animation.duration = animationTime; // Apply the set animation duration

divElement.innerHTML = domString; // Parse the HTML set in the domString to the innerHTML of the divElement
chartContainer.appendChild(divElement.firstChild); // Append the divElement within the chartContainer element as it's child
</script>

@endsection