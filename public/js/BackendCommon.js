function ERPLogOut()
{
  $("#LogOutErp").trigger("click");
}
function GetManuName(ManuName)
{
	var ReturnManuName = "";
	if(ManuName=="ERPFileApplications")
	{
		ReturnManuName = "ERP Applications";
	}
	else if(ManuName=="ERPFileCustomers")
	{
		ReturnManuName = "ERP Customers";
	}
	return ReturnManuName;
}
$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, id = this.id, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (type == 'number')
      this.value = false;
    else if (tag == 'select')
    {
      this.selectedIndex = 0;
      if(id=="customer_id")
      {
        this.value=''
      }
    }
  });
};
function ERPFormReset($this)
{
  $($this).closest('#ERPSearchSubmit').clearForm();
  $($this).closest('#ERPSearchSubmit').find(".SearchFieldShow").show();
  //$('#ERPSearchSubmit').clearForm();
}
function ERPSecurityGroupUsersFormReset(FormName)
{
  $('#'+FormName).find("#group_id").val('');
  //$('#ERPSearchSubmit').clearForm();
}
$(document).ready(function(){
  // ERP Manu Data Start
  $(document).on("click",".ERPManuData",function(){
    var $this = $(this);
    $this.attr("disabled","disabled");
    var ManuName = $(this).attr("ManuName");
    var SectionName = $(this).attr("SectionName");
    var FormName = $(this).attr("FormName");
    var ReportsSummaryCustomerId = $(this).attr("ReportsSummaryCustomerId");// use only Reports 
    if(ManuName!="" && SectionName!="" && FormName!="")
    {      
      $("#ERPManuTabList").removeClass('close-tabs');
    	$(".ERPTabManuData").css("display","none");
	    var TabAlreadyExists = 0;
      //if($("#ERPTabManu").html()!="")
	    if($("#ERPManuTabList").html()!="")
	    {
	        if($('#'+ManuName).length)
	        {
	          /* it exists */
	          var TabAlreadyExists = 1;
            //$("li.ERPManuTabList").removeClass("active");
            //$('li.ERPManuTabList[ManuDataListId='+ManuName+']').addClass('active');
            $("li.rightselect").removeClass("active");
            $('li.rightselect[id=MenuTabId'+ManuName+']').addClass('active');
	          $('#'+ManuName).show();
            $("div.ERPTabManuData").removeClass("active");
            $('div.ERPTabManuData[id='+ManuName+']').addClass('active');
            $('div.ERPTabManuData[id='+ManuName+']').addClass('in');
	          $("."+SectionName).hide();
	          $('#'+FormName).show();
	        }
	    }
        $.ajax({
          url : ERPManuDataAjax,
          method : 'POST',
          data : { "ManuName" : ManuName , "ReportsSummaryCustomerId" : ReportsSummaryCustomerId , "TabAlreadyExists" : TabAlreadyExists , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
          	$this.removeAttr("disabled","disabled");
            if(data.Status=="success")
            {
    		  if(!TabAlreadyExists)
    		  {
                $("#Dashboard").find("div").removeClass("active");
  	            $("#Dashboard").append(data.Html);
  	            //var TabeName = GetManuName(ManuName);
                $("#ERPManuTabList").show();
                $("#ERPManuTabList").find("li").removeClass("active");
  	            $("#ERPManuTabList").append("<li class='rightselect active dropdown' id='MenuTabId"+ManuName+"'><a data-toggle='tab' class='ERPManuTabList' ManuDataListId='"+ManuName+"' href='javascript:"+ManuName+"' id='Test"+ManuName+"'>"+data.TabeName+"</a><span class='close-tab RemoveTabName' RemoveTabName='"+ManuName+"'><i class='fa fa-times' aria-hidden='true'></i></span><div class='dropdown-menu dropdown-menu-sm ContextMenuDiv' id='Tab"+ManuName+"'><ul><li class='MenuCloseTab' ManuDataListId='"+ManuName+"' TypeTab='Close tab'><a href='javascript:'>Close tab</a></li><li class='MenuCloseTab' ManuDataListId='"+ManuName+"' TypeTab='Close all'><a href='javascript:'>Close all</a></li><li class='MenuCloseTab' ManuDataListId='"+ManuName+"' TypeTab='Close others'><a href='javascript:'>Close others</a></li><li class='MenuCloseTab' ManuDataListId='"+ManuName+"' TypeTab='Close all from the right'><a href='javascript:'>Close all from the right</a></li><li class='MenuCloseTab' ManuDataListId='"+ManuName+"' TypeTab='Close all from the left'><a href='javascript:'>Close all from the left</a></li></ul></div></li>");
              }
              if(TabAlreadyExists)
              {
              	$('#'+FormName).html("");
              	$('#'+FormName).html(data.Html);
              }
              if(FormName=="SecurityGroupUsersList")
              {
                // Security Group Users List on click back
                GroupId = $this.attr("GroupId");
                if(GroupId)
                {
                  $('#'+FormName).find("#group_id").val(GroupId);
                }
                
              }
              if(FormName!="SecurityGroupsApplicationsList")
              {
                DataTableDraw(ManuName,data.GetExportPermission);
              }
            }
            else
            {
              alert(data.Message);
            }
          }
        });
    }
    else if(ManuName=="ERPDashboard")
    {
      $("#ERPManuTabList").find("li").removeClass("active");
      $(".ERPTabManuData").css("display","none");
      $("#Dashboard").find("div").removeClass("active");
      $("#ERPDefaultPage").addClass("active");
      $("#ERPDefaultPage").show();
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });
  // ERP Manu Data End
  // Change Manu Start
  $(document).on("click",".ERPManuTabList",function(){
    var $this = $(this);
    var ManuDataListId = $this.attr("ManuDataListId");
    var ContextMenuId = "Tab"+ManuDataListId;
    $("#"+ContextMenuId).removeClass("show").hide();
    if(ManuDataListId!="")
    {
      $(".ERPManuTabList active").removeClass("active");
      $this.addClass("active");
      //$(".ERPTabManuData").css("display","none");
      //$("#"+ManuDataListId).css("display","block");
      $(".ERPTabManuData").hide();
      //alert(ManuDataListId);
      $("#"+ManuDataListId).show();
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });
  // Change Manu End
  // Context Menu Show Start
  var ContextMenuId = "";
  $(document).on("contextmenu",".rightselect",function(){
    var $this = $(this);
    var ManuDataListId = $this.find("a").attr("ManuDataListId");
    var ContextMenuId = "Tab"+ManuDataListId;
    $this.parent().find(".ContextMenuDiv").removeClass("show").hide();
    $("#"+ContextMenuId).css({
      display: "block",
    }).addClass("show");
    return false; //blocks default Webbrowser right click menu
  });
  // $("#"+ContextMenuId+" a").on("click", function(){
  //   $(this).parent().removeClass("show").hide();
  // });
  // Context Menu Show End
  // Remove Tab Manu Start
  $(document).on("click",".RemoveTabName",function(){
    var $this = $(this);
    var RemoveTabName = $this.attr("RemoveTabName");
    if(RemoveTabName!="")
    {
      ManuDataListIdPrev = $this.parent().prev().find('a').attr("ManuDataListId");
      CheckPrev = 1;
      //alert(ManuDataListIdPrev);
      if(ManuDataListIdPrev!="" && typeof ManuDataListIdPrev!="undefined")
      {
        CheckPrev = 0;
        $('li a#Test'+ManuDataListIdPrev).trigger("click");
      }
      ManuDataListIdNext = $this.parent().next().find('a').attr("ManuDataListId");
      //alert(ManuDataListIdNext);
      //alert(CheckPrev);
      if(ManuDataListIdNext!="" && typeof ManuDataListIdNext!="undefined" && CheckPrev==1)
      {
        $('li a#Test'+ManuDataListIdNext).trigger("click");
      }

      if(typeof ManuDataListIdPrev=="undefined" && typeof ManuDataListIdNext=="undefined")
      {
      	$("#ERPManuTabList").addClass('close-tabs');
        $("#ERPDefaultPage").addClass("active");
        $("#ERPDefaultPage").show();
      }
      $("#ERPManuTabList").find(`li#MenuTabId`+RemoveTabName).remove();
      $("#Dashboard").find(`[id='`+RemoveTabName+`']`).remove();
    }
  });
  // Menu Remove Tab Manu End
  // Menu Close Tab Start
  $(document).on("click",".MenuCloseTab",function(){
    var $this = $(this);
    var ManuDataListId = $this.attr("ManuDataListId");
    var TypeTab = $this.attr("TypeTab");
    if(ManuDataListId!="" && TypeTab=="Close tab")
    {
      ManuDataListIdPrev = $this.closest("#MenuTabId"+ManuDataListId).prev().find('a').attr("ManuDataListId");
      CheckPrev = 1;
      //alert(ManuDataListIdPrev);
      if(ManuDataListIdPrev!="" && typeof ManuDataListIdPrev!="undefined")
      {
        CheckPrev = 0;
        $('li a#Test'+ManuDataListIdPrev).trigger("click");
      }
      ManuDataListIdNext = $this.closest("#MenuTabId"+ManuDataListId).next().find('a').attr("ManuDataListId");
      //alert(ManuDataListIdNext);
      //alert(CheckPrev);
      if(ManuDataListIdNext!="" && typeof ManuDataListIdNext!="undefined" && CheckPrev==1)
      {
        $('li a#Test'+ManuDataListIdNext).trigger("click");
      }
      if(typeof ManuDataListIdPrev=="undefined" && typeof ManuDataListIdNext=="undefined")
      {
      	 $("#ERPManuTabList").addClass('close-tabs');
         $("#ERPDefaultPage").addClass("active");
         $("#ERPDefaultPage").show();
      }
      $this.closest("#MenuTabId"+ManuDataListId).remove();
      $("#"+ManuDataListId).remove()
    }
    else if(ManuDataListId!="" && TypeTab=="Close all")
    {
    	$("#ERPManuTabList").html("");
    	$("#ERPManuTabList").addClass('close-tabs');
    	$(".ERPTabManuData").not(':first').remove();
    	//$("#ERPManuTabList").html("");
      $("#ERPDefaultPage").addClass("active");
      $("#ERPDefaultPage").show();
    }
    else if(ManuDataListId!="" && TypeTab=="Close others")
    {
    	$('li a#Test'+ManuDataListId).trigger("click");
    	$('.rightselect').each(function(i)
		{
		   MenuLiId = $(this).attr('id');
		   DivListId = $(this).find("a").attr('ManuDataListId');
		   if(MenuLiId!='MenuTabId'+ManuDataListId)
		   {
		   	  $("#"+MenuLiId).remove();
		   	  $("#"+DivListId).remove();
		   }
		});
    }
    else if(ManuDataListId!="" && TypeTab=="Close all from the right")
    {
    	$('li a#Test'+ManuDataListId).trigger("click");
        $this.closest("#MenuTabId"+ManuDataListId).nextAll().remove();
    	$("#"+ManuDataListId).nextAll().remove();
    }
    else if(ManuDataListId!="" && TypeTab=="Close all from the left")
    {
    	$('li a#Test'+ManuDataListId).trigger("click");
        $this.closest("#MenuTabId"+ManuDataListId).prevAll().remove();
        $("#"+ManuDataListId).prevAll().remove();
    }
  });
  // Menu Close Tab End
  //ERP Form Show Start
  $(document).on("click",".ERPFormShow",function(){
    var FormName = $(this).attr("FormName");
    var SectionName = $(this).attr("SectionName");
    var EditId = $(this).attr("EditId");
    var GroupId = $(this).attr("GroupId");
    if(FormName!="" && SectionName!="")
    {
       $.ajax({
        url : ERPFormShowAjax,
        method : 'POST',
        data : { "FormName" : FormName ,"EditId" : EditId ,"GroupId" : GroupId , _token : Csrf },
        dataType : "json",
        success : function(data)
        {
          if(data.Status=="success")
          {
            $("."+SectionName).hide();
            $("#"+FormName).html(data.Html);
            $("#"+FormName).show();
            if(FormName=="SecurityGroupsApplicationsForm")
            {
              DataTableDraw("ERPSecurityGroupsApplications");
            }
            if(FormName=="ReportsReportOfCustomerUsersForm")
            {
              DataTableDraw("ERPReportsReportOfCustomerUsersSummary");
            }
          }
          else
          {
            alert(data.Message);
          }
        }
      });
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });  
  //ERP Form Show End
  //ERP Search Show Start
  $(document).on("click",".ERPSearchShow",function(){
    var FormName = $(this).attr("FormName");
    var SectionName = $(this).attr("SectionName");
    var FilterChangeDivEmpty = $(this).attr("FilterChangeDivEmpty");
    if(FormName!="" && SectionName!="" && FilterChangeDivEmpty!="")
    {
      $("."+FilterChangeDivEmpty).html("");
      //var FormDataArray = new FormData(document.getElementById('ERPRemoveSearchSubmit')[0]);//ERPRemoveSearchSubmit id use
      //FormDataArray.append("ERPSearchName",FormName);//over write ERPSearchName
      var data = {
        'ERPSearchName' : FormName
      };
      var FormDataArray = $(this).parent().parent().parent().parent().find('form').serialize()+ '&' + $.param(data);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : ERPSearchShowAjax,
        method : 'POST',
        data : FormDataArray,
        dataType : "json",
        //processData: false,
        //contentType: false,
        success : function(data)
        {
          if(data.Status=="success")
          {
            //alert(FormName);
            $("."+SectionName).hide();
            $("#"+FormName).show();
            $("#"+FormName).html(data.ERPSearchShow);
            //alert(ManuName);
            //DataTableDraw(ManuName);
          }
          else
          {
            alert(data.Message);
          }
        }
      });
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });  
  //ERP Search Show End
  //ERP Back/Cancel Button Start
  $(document).on("click","#ERPBackCancelButton",function(){
    var SectionName = $(this).attr("SectionName");
    var ShowSection = $(this).attr("ShowSection");
    if(SectionName!="" && ShowSection!="")
    {
      $("."+SectionName).hide();
      $("#"+ShowSection).show();
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });
  //ERP Back/Cancel Button End
  //ERP Form Submit Start 
  $(document).on("submit","#ERPFormSubmit",function(){
     var $this = $(this);
     var FormParentDivId = $this.attr("FormParentDivId");
     var SelectElement = $this.closest("#"+FormParentDivId);
     if(SelectElement!="")
     {
        $.ajax({
          url : ERPFormSubmitAjax,
          method : 'POST',
          data : new FormData(this),
          processData: false,
          contentType: false,
          dataType : "json",
          success : function(data)
          {
            SelectElement.find('.alert-danger').html("");
            SelectElement.find('.alert-danger').hide();
            SelectElement.find('.alert-info').html("");
            SelectElement.find('.alert-info').hide();
            if(data.Status=="errors")
            {
              jQuery.each(data.Message, function(key, value){
                SelectElement.find('.alert-danger').show();
                SelectElement.find('.alert-danger').append('<p>'+value+'</p>');
              });
            }
            else if(data.Status=="error")
            {
              SelectElement.find('.alert-danger').show();
              SelectElement.find('.alert-danger').append('<p>'+data.Message+'</p>');
            }
            else if(data.Status=="success")
            {
              SelectElement.find('.alert-info').show();
              SelectElement.find('.alert-info').append('<p>'+data.Message+'</p>');
              
              if(FormParentDivId=="SecurityChangePasswordList")
              {
                ERPLogOut();
              }

              //or message alert alert(data.Message);

              //after add or update show list
              //$("."+$("#ERPFormName").attr("SectionName")).hide();
              //$("#"+$("#ERPFormName").val()).show();
              //after add or update show list

              var ERPFormName = SelectElement.find("#ERPFormName").val();//ERPFormName means section name
              if(ERPFormName!="SecurityGroupsApplicationsList")
              {
                $("#"+ERPFormName).html("");
                $("#"+ERPFormName).html(data.ERPManuDataRecord);
                TableId = SelectElement.find("#ERPFormName").attr("TabName");
                DataTableDraw(TableId,data.GetExportPermission);
              }
            }
          }
        });
        return false;
     }
  });
  //ERP Form Submit End
  // ERP Remove Search Submit Start
  $(document).on("click",".ERPRemoveSearchSubmit",function(){
    var $this = $(this);
    var FormName = $this.attr("FormName");
    var RemoveSearch = $this.attr("RemoveSearch");
    var RemoveSearchType = $this.attr("RemoveSearchType");
    if(FormName!="" && RemoveSearch!="" && RemoveSearchType!="")
    {
      if(confirm("Are You Sure You Want To Remove This ?"))
      {
        if(RemoveSearchType=="AllSearchRemove")
        {
          $("."+RemoveSearchType).parent().parent().remove();
        }
        else if(RemoveSearchType=="ColumnSearchRemove")
        {
          $("#"+RemoveSearch).remove();
        }
        var FormDataArray = $("#"+FormName).find('#ERPRemoveSearchSubmit').serialize();
        $.ajax({
          url : ERPSearchSubmitAjax,
          method : 'POST',
          data : FormDataArray,
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="error")
            {
              alert(data.Message);
            }
            else if(data.Status=="success")
            {
              SectionName = $("#"+FormName).find("#ERPSearchName").val();
              $("#"+SectionName).html("");
              $("#"+SectionName).html(data.ERPManuDataRecord);
              DataTableId = $("#"+FormName).find("#ERPSearchName").attr("TabName");
              DataTableDraw(DataTableId,data.GetExportPermission);
            }
          }
        });
      }
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });
  // ERP Remove Search Submit End 
  // ERP Add More Filter Apply Start
  $(document).on("click",".ERPAddFilterApply1",function(){
      var $this = $(this);
  	  FieldNameConditionId = $this.attr("FieldNameCondition");
  	  if(FieldNameConditionId=="group_id")//security group users search
      {
        FieldNameConditionValue = $this.parent().parent().parent().parent().find("#"+FieldNameConditionId).val();
      }
      else
      {
        FieldNameConditionValue = $this.parent().parent().find("#"+FieldNameConditionId).val();
      }
      if(FieldNameConditionValue>=1)
  	  {
  	  	FieldNameId = $this.attr("FieldName");
  	    FieldNameValue = $this.parent().parent().find("#"+FieldNameId).val();
  	  	if(FieldNameConditionValue!=4 && FieldNameValue=="")//4 for Empty Content
  	  	{
  	  	   alert("You need to enter some value to search");
           return false;
  	  	}
  	  	$.ajax({
          url : ERPSearchSubmitAjax,
          method : 'POST',
          data : $this.closest("#ERPRemoveSearchSubmit").serialize(),//$("#ERPRemoveSearchSubmit").serialize()
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="error")
            {
              alert(data.Message);
            }
            else if(data.Status=="success")
            {
              ListDivId = $("#ERPSearchName").val();
              TabName = $("#ERPSearchName").attr("TabName");
              $("#"+ListDivId).html("");
              $("#"+ListDivId).html(data.ERPManuDataRecord);
              DataTableDraw(TabName);
            }
          }
        });
  	  }
  });
  // ERP Add More Filter Apply End
  // ERP Add More Filter Apply Start
  $(document).on("click",".ERPAddFilterApply",function(){
      var $this = $(this);
      var FormName = $this.attr("FormName");
      FieldNameConditionId = $this.attr("FieldNameCondition");
      if(FieldNameConditionId=="group_id")//security group users search
      {
        FieldNameConditionValue = $("#"+FormName).find("#"+FieldNameConditionId).val();
      }
      else
      {
        FieldNameConditionValue = $("#"+FormName).find("#"+FieldNameConditionId).val();
      }
      if(FieldNameConditionValue>=1)
      {
        FieldNameId = $this.attr("FieldName");
        FieldNameValue = $("#"+FormName).find("#"+FieldNameId).val();
        if(FieldNameConditionValue!=4 && FieldNameValue=="")//4 for Empty Content
        {
           alert("You need to enter some value to search");
           return false;
        }
        $.ajax({
          url : ERPSearchSubmitAjax,
          method : 'POST',
          data : $this.closest("#ERPRemoveSearchSubmit").serialize(),//$("#ERPRemoveSearchSubmit").serialize()
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="error")
            {
              alert(data.Message);
            }
            else if(data.Status=="success")
            {
              ListDivId = $("#"+FormName).find("#ERPSearchName").val();
              TabName = $("#"+FormName).find("#ERPSearchName").attr("TabName");
              $("#"+ListDivId).html("");
              $("#"+ListDivId).html(data.ERPManuDataRecord);
              DataTableDraw(TabName,data.GetExportPermission);
            }
          }
        });
      }
  });
  // ERP Add More Filter Apply End
  // ERP Filter Change Apply Start
  $(document).on("click",".ERPFilterChangeApply",function(){
      var $this = $(this);
      var FilterName = $(this).attr("FilterName");
      var FieldNameConditionId = $this.attr("FieldNameCondition");
      //alert(FieldNameConditionId);
      var FieldNameConditionValue = $this.parent().parent().find("#"+FieldNameConditionId).val();
      //alert(FieldNameConditionValue);
      if(FilterName!="" && FieldNameConditionValue>=1)
      {
        FieldNameId = $this.attr("FieldName");
        FieldNameValue = $this.parent().parent().find("#"+FieldNameId).val();
        if(FieldNameConditionValue!=4 && FieldNameValue=="")//4 for Empty Content
        {
           alert("You need to enter some value to search");
           return false;
        }
        $.ajax({
          url : ERPSearchSubmitAjax,
          method : 'POST',
          data : $this.closest("#ERPRemoveSearchSubmit").serialize()+ "&FilterName="+FilterName,
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="error")
            {
              alert(data.Message);
            }
            else if(data.Status=="success")
            {
              var SectionName = $this.closest("#ERPRemoveSearchSubmit").find("#ERPSearchName").val();
              $("#"+SectionName).html("");
              $("#"+SectionName).html(data.ERPManuDataRecord);
              var DataTableId = $this.closest("#ERPRemoveSearchSubmit").find("#ERPSearchName").attr("TabName");
              DataTableDraw(DataTableId,data.GetExportPermission);
            }
          }
        });
      }
  });
  //ERP Filter Change Apply End
  // ERP Change Search Condition And Value Start
  $(document).on("click",".ERPFilterChange",function(){
      var $this = $(this);
      var SelectElement = $this.closest(".SearchSection");
  	  var FieldNameConditionValue = $this.attr("FieldNameConditionValue");
  	  var FieldNameValue = $this.attr("FieldNameValue");
      var FilterName = $this.attr("FilterName");
      var PageName = $this.attr("PageName");
      var ShowDivClass = $this.attr("ShowDivClass");
      var FormName = $this.attr("FormName");
      var AddFilterPopup = $this.attr("AddFilterPopup");
  	  if((FieldNameConditionValue>=1 && AddFilterPopup!="" && FieldNameValue!="" && FilterName!="" && PageName!="" && ShowDivClass!="")||(FieldNameConditionValue==4 && FilterName!="" && PageName!="" && ShowDivClass!=""))
  	  {
        $.ajax({
          url : ERPFilterChangeAjax,
          method : 'POST',
          data : { "FilterName" : FilterName , "PageName" : PageName ,"FieldNameConditionValue" : FieldNameConditionValue , "FieldNameValue" : FieldNameValue , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="success")
            {
                $("#"+FormName).find("."+ShowDivClass).hide();//hide any open filter change Popup
                $("#"+FormName).find("#SearchSaveFilter").hide();//hide Save Filter Popup
                $("#"+FormName).find("#"+AddFilterPopup).hide();//hide Add Filter Popup
                SelectElement.find("."+ShowDivClass).show();
                SelectElement.find("."+ShowDivClass).html("");
                SelectElement.find("."+ShowDivClass).html(data.FilterChangeHtml);
            }
            else
            {
                alert(data.Message);
            }
          }
        });
    	}
  });
  // ERP Change Search Condition And Value End
  //ERP Search Submit Start 
  $(document).on("submit","#ERPSearchSubmit",function(){
     var $this = $(this);
     var FormParentDivId = $this.attr("FormParentDivId");
     var SelectElement = $this.closest("#"+FormParentDivId);
     if(SelectElement!="")
     {
       $.ajax({
        url : ERPSearchSubmitAjax,
        method : 'POST',
        data : new FormData(this),
        processData: false,
        contentType: false,
        dataType : "json",
        success : function(data)
        {
          if(data.Status=="error")
          {
            alert(data.Message);
          }
          else if(data.Status=="success")
          {
            //after add or update show list
            SectionName = SelectElement.find("#ERPSearchName").attr("SectionName");
            SectionShow = SelectElement.find("#ERPSearchName").val();
            $("."+SectionName).hide();
            $("#"+SectionShow).show();
            //after add or update show list

            $("#"+SectionShow).html("");
            $("#"+SectionShow).html(data.ERPManuDataRecord);
            DataTableId = SelectElement.find("#ERPSearchName").attr("TabName");
            DataTableDraw(DataTableId,data.GetExportPermission);
          }
        }
      });
      return false;
    }
  });
  //ERP Search Submit End
  // ERP Common Delete Start
  $(document).on("click",".ERPRecordDelete",function(){
    var $this = $(this);
    var FormName = $this.attr("FormName");
    var DeleteId = $this.attr("DeleteId");
    if(FormName!="" && DeleteId!="")
    {
       $.ajax({
        url : ERPRecordDeleteAjax,
        method : 'POST',
        data : { "FormName" : FormName ,"DeleteId" : DeleteId , _token : Csrf },
        dataType : "json",
        success : function(data)
        {
          if(data.Status=="success")
          {
            $this.parent().parent().remove();
          }
          else
          {
            alert(data.Message);
          }
        }
      });
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });
  // ERP Common Delete End
  // ERP Search Empty Content Start
  $(document).on("change",".ERPSearchEmptyContent",function(){
      var $this = $(this);
      var Id = $this.attr("id");
      var FieldId = $this.attr("FieldId");
      var SelectElement = $this.closest('.SearchSection');
      var text = SelectElement.find("#"+Id+" option:selected").text();
      if(text=="Empty Content")
      {
        SelectElement.find("#"+FieldId).val("");
        SelectElement.find("#"+FieldId).hide();
      }
      else
      {
        SelectElement.find("#"+FieldId).show();
      }
  });
  // ERP Search Empty Content End
  // ERP Filter Save Start
  $(document).on("click",".ERPFilterSave",function(){
  	var FormName = $(this).attr("FormName");
    var FormSubmitId = $(this).attr("FormSubmitId");
    var FilterName = $("#"+FormName).find("#filter_name").val();
    if(FilterName!="" && FormName!="" && FormSubmitId!="")
    {
      //var FormDataArray = new FormData(document.getElementById('ERPSearchSubmit'));
      //FormDataArray.append("FilterName",FilterName);//over write ERPSearchName
      var data = {
        'FilterName' : FilterName
      };
      //ERPSearchSubmit
      var FormDataArray = $("#"+FormName).find('#'+FormSubmitId).serialize()+ '&' + $.param(data);
      $.ajax({
        url : ERPFilterSaveAjax,
        method : 'POST',
        data : FormDataArray ,
        dataType : "json",
        //processData: false,
        //contentType: false,
        success : function(data)
        {
          if(data.Status=="success")
          {
          	$("#"+FormName).find("#filter_name").val("");
            $("#"+FormName).find("#erp_filter_list_div").show();
            $("#"+FormName).find("#erp_filter_list").show();
            $("#"+FormName).find("#ERPFilterDeleteSection").show();
            $("#"+FormName).find("#erp_filter_list").html(data.SaveFilterLists);
            $("#"+FormName).find("#erp_filter_delete").html(data.SaveFilterLists);
          }
          else
          {
            alert(data.Message);
          }
        }
      });
    }
    else
    {
      alert("Something Went Wrong Please Try Again!");
    }
  });
  // ERP Filter Save End
  // ERP Old Filter Get Start
  $(document).on("change","#erp_filter_list",function(){
  	  var $this = $(this);
      var ERPTableId = $(this).val();
      var FormName = $(this).attr("FormName");
      if(ERPTableId>=1 && FormName!="" && typeof(FormName)!="undefined")
      {
        $.ajax({
          url : ERPFilterGetAjax,
          method : 'POST',
          data : { "ERPTableId" : ERPTableId , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="success")
            {
              if(FormName=="SecurityGroupUsersList")
              {            
                $("#"+FormName).find("#group_id").val(data.ERPFilterGetHtml);
              }
              else
              {
                $("#"+FormName).find("#ERPChangeFilter").html("");
                $("#"+FormName).find("#ERPChangeFilter").html(data.ERPFilterGetHtml);
              }
            }
            else
            {
              alert(data.Message);
            }
          }
        });
      }
  });
  // ERP Old Filter Get End
  // ERP Old Filter Save Get List Data Start
  $(document).on("change",".ERPSearchBySaveFilter",function(){
      var $this = $(this);
      var ERPTableId = $(this).val();
      var SectionName = $(this).attr("SectionName");
      var ManuName = $(this).attr("ManuName");
      if(ERPTableId>=1 && SectionName!="" && typeof(SectionName)!="undefined")
      {
        $.ajax({
          url : ERPFilterSaveGetListDataAjax,
          method : 'POST',
          data : { "ERPTableId" : ERPTableId , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="success")
            {
              if(SectionName=="SecurityGroupUsersList")
              {            
                $("#"+SectionName).find("#group_id").val(data.ERPFilterGetHtml);
              }
              else
              {
                $("#"+SectionName).html("");
                $("#"+SectionName).html(data.ERPFilterGetHtml);
                DataTableDraw(ManuName,data.GetExportPermission);
              }
            }
            else
            {
              alert(data.Message);
            }
          }
        });
      }
  });
  // ERP Old Filter Save Get List Data End
  // ERP Old Filter Get  Start
  $(document).on("click",".ERPFilterDelete",function(){
  	  var FormName = $(this).attr("FormName"); 
      var ERPTableId = $("#"+FormName).find("#erp_filter_delete").val();
      if(ERPTableId>=1 && confirm("Are You Sure You Want To Delete This ?"))
      {
        $.ajax({
          url : ERPFilterDeleteAjax,
          method : 'POST',
          data : { "ERPTableId" : ERPTableId , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="success")
            {
	            $("#"+FormName).find("#erp_filter_list").html(data.SaveFilterLists);
	            $("#"+FormName).find("#erp_filter_delete").html(data.SaveFilterLists);
            	if(data.SaveFilterLists=="")
            	{
                $("#"+FormName).find("#erp_filter_list_div").hide();
                $("#"+FormName).find("#erp_filter_list").hide();
                $("#"+FormName).find("#ERPFilterDeleteSection").hide();
            	}
            }
            else
            {
              alert(data.Message);
            }
          }
        });
      }
  });
  // ERP Old Filter Get End
  // ERP Add More Filter Show  Start
  $(document).on("click",".ERPAddFilterShow",function(){

      var FilterName = $(this).attr("value");
      var PageName = $(this).attr("name");
      var ShowDivId = $(this).attr("ShowDivId");
      var FormName = $(this).attr("FormName");
      var FilterChangeModal = $(this).attr("FilterChangeModal");
      if(FormName!="" &&  FilterName!="" && PageName!="" && ShowDivId!="" && FilterChangeModal!="")
      {
        $.ajax({
          url : ERPAddFilterShowAjax,
          method : 'POST',
          data : { "FilterName" : FilterName , "PageName" : PageName , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="success")
            {
	           $("#"+ShowDivId).show();
	           $("#"+ShowDivId).html("");
	           $("#"+ShowDivId).html(data.ERPAddFilterShowHtml);
             $("#"+FormName).find("."+FilterChangeModal).hide();//hide any open filter change Popup
             $("#"+FormName).find("#SearchSaveFilter").hide();//hide Save Filter Popup
            }
            else
            {
                alert(data.Message);
            }
          }
        });
      }
  });
  // ERP Add More Filter Show End
  // ERP Old Filter Get  Start
  $(document).on("change","#ERPGetOldFilterData",function(){
      var ERPTableId = $(this).val();
      var PageName = $(this).attr("PageName");
      if(ERPTableId>=1 && PageName!="")
      {
        $.ajax({
          url : ERPGetOldFilterDataAjax,
          method : 'POST',
          data : { "ERPTableId" : ERPTableId ,"PageName" : PageName , _token : Csrf },
          dataType : "json",
          success : function(data)
          {
            if(data.Status=="success")
            {
              $("#ERPChangeFilter").html(data.ERPFilterGetHtml);
            }
            else
            {
              alert(data.Message);
            }
          }
        });
      }
  });
  // ERP Old Filter Get End
  // ERP Save Filter Modal Show Start
  $(document).on("click",".SaveFilterModalShow",function(){
      var $this = $(this);
      var FormName = $this.attr("FormName");
      var ModalId = $this.attr("ModalId");
      var HideChangeFilterPopup = $this.attr("HideChangeFilterPopup");
      var HideAddFilterPopup = $this.attr("HideAddFilterPopup");
      if(FormName!="" && ModalId!="")
      {
        $this.closest("#"+FormName).find("#"+ModalId).show();
        $this.closest("#"+FormName).find("."+HideChangeFilterPopup).hide();//Hide Change filter popup
        $this.closest("#"+FormName).find("#"+HideAddFilterPopup).hide();//Hide add filter popup
      }
  });
  // ERP Save Filter Modal Show End
  // ERP Save Filter Modal Hide Start
  $(document).on("click",".SaveFilterModalHide",function(){
      var $this = $(this);
      var ModalId = $this.attr("ModalId");
      if(ModalId!="")
      {
        $this.closest("#"+ModalId).hide();
      }
  });
  // ERP Save Filter Modal Hide End
  // ERP Hide Filter Change Modal Start
  $(document).on("click",".HideFilterChangeModal",function(){
      var $this = $(this);
      var ModalClass = $this.attr("ModalClass");
      if(ModalClass!="")
      {
        $this.closest("."+ModalClass).hide();
      }
  });
  // ERP Hide Filter Change Modal End
  // ERP Hide Add Filter Modal Start
  $(document).on("click",".HideAddFilterModal",function(){
      var $this = $(this);
      var ModalId = $this.attr("ModalId");
      if(ModalId!="")
      {
        $this.closest("#"+ModalId).hide();
      }
  });
  // ERP Hide Add Filter Modal End
  // ERP Check Permission Access Start
  $(document).on("change",".CheckPermissionAccess",function(){
      var $this = $(this);
      var AccessId = $this.attr("AccessId");
      if(AccessId>0)
      {
        if($this.closest("tr").find("#permissions_access_"+AccessId).prop("checked") == false)
        {
           alert("Please Check Access To Give Permission To "+$this.closest("tr").find("td:eq(0)").text());
           $this.prop("checked",false);
        }
      }
  });
  // ERP Check Permission Access End
  // ERP Check Permission Access Start
  $(document).on("change",".SecurityGroupsApplicationsPermissionSet",function(){
      var $this = $(this);
      var GroupId = $this.val();
      if(GroupId>0)
      {
        $this.closest("#ERPRemoveSearchSubmit").find(".ERPFormShow").attr("editid",GroupId)
      }
  });
  // ERP Check Permission Access End
});