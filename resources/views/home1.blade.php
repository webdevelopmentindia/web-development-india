@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" id="Dashboard">
                <div class="card-header">Dashboard</div>
                <img src="{{ asset('public/images/ERP/ApplicationIcon/sc_app_icon_2820ub86gv0sepfpkmimhs00kquht.jpg') }}" width="100px" height="100px" />
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
