<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\ErpCountrie;
use App\ErpSource;
use App\ErpTheme;
use App\ErpLanguage;
use App\User;
use App\Application;
use App\UserLogged;
use App\Group;
use App\ErpSaveFilter;
use App\GroupsApp;
use App\Services\PayUService\Exception;
use Auth;
class ERPManuDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPManuDataAjax(Request $request)
    {
        //die("fndfgh dghsfgsdf");
        $ParaMeter = array();
        $ParaMeter["All"] = "All";
        $Html = "";
        $Status = "success";
        $Message = "";
        $TabeName = "";
        $GetExportPermission = "";
        try {
            $OnlyData = "";
            if($request->ManuName=="ERPFileApplications")
            {
                $TabeName = "ERP Applications";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $ERPApplications = ErpApplication::GetAllERPApplications($ParaMeter);
                $Html = view('ERP.File.Applications.FileApplicationList',compact("ERPApplications","OnlyData"))->render();
                $Permission["AppId"] = 1;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPFileCustomers")
            {
                $TabeName = "ERP Customers";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);
                $Html = view('ERP.File.Customers.FileCustomerList',compact("ERPCustomers","OnlyData"))->render();
                $Permission["AppId"] = 2;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPFileCountries")
            {
                $TabeName = "Countries";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $ERPCountries = ErpCountrie::GetAllErpCountries($ParaMeter);
                $Html = view('ERP.File.Countries.FileCountriesList',compact("ERPCountries","OnlyData"))->render();
                $Permission["AppId"] = 3;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPFileSources")
            {
                $TabeName = "Sources";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $ERPSources = ErpSource::GetAllErpSources($ParaMeter);
                $Html = view('ERP.File.Sources.FileSourcesList',compact("ERPSources","OnlyData"))->render();
                $Permission["AppId"] = 4;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPFileThemes")
            {
                $TabeName = "ERP Themes";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $ERPThemes = ErpTheme::GetAllErpThemes($ParaMeter);
                $Html = view('ERP.File.Themes.FileThemesList',compact("ERPThemes","OnlyData"))->render();
                $Permission["AppId"] = 5;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPFileLanguages")
            {
                $TabeName = "ERP Languages";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $ERPLanguages = ErpLanguage::GetAllErpLanguages($ParaMeter);
                $Html = view('ERP.File.Languages.FileLanguagesList',compact("ERPLanguages","OnlyData"))->render();
                $Permission["AppId"] = 6;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPSetupUsers")
            {
                $TabeName = "ERP Users";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $GetAllUsers = User::GetAllUsers($ParaMeter);
                $Html = view('ERP.Setup.Users.SetupUsersList',compact("GetAllUsers","OnlyData"))->render();
                $Permission["AppId"] = 7;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPSetupUsersGroups")
            {
                $TabeName = "Users/Groups";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $GetAllUsers = User::GetAllUsers($ParaMeter);
                $Html = view('ERP.Setup.UsersGroups.SetupUsersGroupsList',compact("GetAllUsers","OnlyData"))->render();
                $Permission["AppId"] = 8;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPSecurityApplications")
            {
                $TabeName = "Applications";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $GetAllApplications = Application::GetAllApplications($ParaMeter);
                $Html = view('ERP.Security.Applications.SecurityApplicationsList',compact("GetAllApplications","OnlyData"))->render();
                $Permission["AppId"] = 9;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPSecuritySystemLog")
            {
                $TabeName = "System Log";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $GetAllUserLoggeds = UserLogged::GetAllUserLoggeds($ParaMeter);
                $Html = view('ERP.Security.SystemLog.SecuritySystemLogList',compact("GetAllUserLoggeds","OnlyData"))->render();
                $Permission["AppId"] = 14;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPSecurityGroups")
            {
                $TabeName = "Groups";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $GetAllGroups = Group::GetAllGroups($ParaMeter);
                $Html = view('ERP.Security.Groups.SecurityGroupsList',compact("GetAllGroups","OnlyData"))->render();
                $Permission["AppId"] = 10;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ManuName=="ERPSecurityGroupUsers")
            {
                $TabeName = "Group / users";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = "Group users";
                }
                $GetAllGroups = Group::GetAllGroups($ParaMeter);
                $ParaMeter1 = array();
                $ParaMeter1["PageName"] = "ERP Security Group Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter1);  
                $SearchShow = "Show";
                $Html = view('ERP.Security.GroupUsers.SecurityGroupUsersList',compact("GetAllGroups","OnlyData","SearchShow","GetAllErpSaveFilters"))->render();
            }
            else if($request->ManuName=="ERPSecurityGroupsApplications")
            {
                $TabeName = "Groups / Applications";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = "Groups / Applications";
                }
                $GetAllGroups = Group::GetAllGroups($ParaMeter); 
                $Html = view('ERP.Security.GroupsApplications.SecurityGroupsApplicationsList',compact("GetAllGroups","OnlyData"))->render();
            }
            else if($request->ManuName=="ERPSecuritySyncApplications")
            {
                $TabeName = "Sync Applications";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $Html = view('ERP.Security.SyncApplications.SecuritySyncApplicationsList',compact("OnlyData"))->render();
            }
            else if($request->ManuName=="ERPSecurityChangePassword")
            {
                $TabeName = "Change Password";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $Html = view('ERP.Security.ChangePassword.SecurityChangePasswordList',compact("OnlyData"))->render();
            }
            else if($request->ManuName=="ERPSecurityChangePassword")
            {
                $TabeName = "Change Password";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                $Html = view('ERP.Security.ChangePassword.SecurityChangePasswordList',compact("OnlyData"))->render();
            }
            else if($request->ManuName=="ERPReportsReportOfCustomerUsers")
            {
                $TabeName = "Report of Customer / Users";
                if($request->TabAlreadyExists==1)
                {
                   $OnlyData = $TabeName;
                }
                unset($ParaMeter["All"]);

                $ParaMeter["CustomerAllUsers"] = "CustomerAllUsers";
                $GetAllUsers = User::GetAllUsers($ParaMeter);
                $CountGetAllUsers = count($GetAllUsers);

                if($request->ReportsSummaryCustomerId>0)
                {
                    $ParaMeter["CustomerAllUsers"] = "CustomerAllUsers";
                    $ParaMeter["CustomerId"] = $request->ReportsSummaryCustomerId;
                    $GetAllUsers = User::GetAllUsers($ParaMeter);
                }
                
                $Html = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersList',compact("OnlyData","GetAllUsers","CountGetAllUsers"))->render();
                $Permission["AppId"] = 17;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
        }
        catch (\Exception $e) {
          $Message = "Error in $TabeName Manu Data " . $e->getMessage();
          $Status = "error";
        }
        return json_encode(["Status"=>$Status,"Html"=>$Html,"TabeName"=>$TabeName,"Message"=>$Message,"GetExportPermission"=>$GetExportPermission]);
    }
}