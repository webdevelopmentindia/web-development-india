<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpSaveFilter;
use App\Services\PayUService\Exception;
class ERPFilterDeleteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFilterDeleteAjax(Request $request)
    {
        $Message = "Something Went Wrong Please Try Again!";
        $Status = "error";
        $SaveFilterLists = "";
        $ERPSearchName = "";
        try {
            $ParaMeter = array();
            $ParaMeter["ERPTableId"] =$request->ERPTableId;
            $DeleteErpSaveFilters = ErpSaveFilter::DeleteErpSaveFilters($ParaMeter);
            unset($ParaMeter["ERPTableId"]);
            if($DeleteErpSaveFilters=="ERP File Applications")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP File Applications";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP File Customers")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP File Customers";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP File Countries")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP File Countries";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP File Sources")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP File Sources";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP File Themes")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP File Themes";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP File Languages")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP File Languages";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP Security System Log")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP Security System Log";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP Setup Users")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP Setup Users";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }
            else if($DeleteErpSaveFilters=="ERP Security Group Users")
            {
               $ERPSearchName = $ParaMeter["PageName"] = "ERP Security Group Users";
               $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            }       
            $SaveFilterLists = view('ERP.Filters.FilterList',compact("GetAllErpSaveFilters"))->render(); 
            $Status = "success";
            $Message = "$ERPSearchName Delete Filter Successfully!";
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPSearchName Delete Filter " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"SaveFilterLists"=>$SaveFilterLists]);
    }
}