<div class="close-btn HideAddFilterModal" ModalId="ReportsReportOfCustomerUsersAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="ReportsReportOfCustomerUsersEnabledFlag")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
	<div class="col-sm-3 p-0">
	    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
              @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
              @endforeach
          @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="enabled_flag" type="text" class="form-control" name="enabled_flag"  autofocus placeholder="User ID" value=""> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="enabled_flag_condition" FieldName="enabled_flag" FormName="ReportsReportOfCustomerUsersList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="ReportsReportOfCustomerUsersLogin")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Login') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="login_condition" name="login_condition" class="form-control ERPSearchEmptyContent" FieldId="login">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="login" type="text" class="form-control" name="login"  autofocus placeholder="Login" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="ReportsReportOfCustomerUsersList" FieldNameCondition="login_condition" FieldName="login"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="ReportsReportOfCustomerUsersFullName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="full_name_condition" name="full_name_condition" class="form-control ERPSearchEmptyContent" FieldId="full_name">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="full_name" type="text" class="form-control" name="full_name"  autofocus placeholder="Full Name" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="full_name_condition" FieldName="full_name" FormName="ReportsReportOfCustomerUsersList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="ReportsReportOfCustomerUsersEMail")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('E-Mail') }}</label>
    <div class="col-sm-3 p-0">
      <select id="e_mail_condition" name="e_mail_condition" class="form-control ERPSearchEmptyContent" FieldId="e_mail">
          @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="e_mail" type="text" class="form-control" name="e_mail"  autofocus placeholder="E-Mail" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="ReportsReportOfCustomerUsersList" FieldNameCondition="e_mail_condition" FieldName="e_mail"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="ReportsReportOfCustomerUsersAdminPriv")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Admin Priv.') }}</label>
    <div class="col-sm-3 p-0">
      <select id="admin_priv_condition" name="admin_priv_condition" class="form-control ERPSearchEmptyContent" FieldId="admin_priv">
          @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="admin_priv" type="text" class="form-control" name="admin_priv"  autofocus placeholder="Admin Priv." value="" maxlength="1">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="ReportsReportOfCustomerUsersList" FieldNameCondition="admin_priv_condition" FieldName="admin_priv"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="ReportsReportOfCustomerUsersCustomerId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
  <div class="col-sm-3 p-0">
      <select id="customer_id_condition" name="customer_id_condition" class="form-control">
          @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <select id="customer_id" name="customer_id" class="form-control">
          @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="customer_id_condition" FieldName="customer_id" FormName="ReportsReportOfCustomerUsersList"> Apply  </a></div>
</div>
@endif