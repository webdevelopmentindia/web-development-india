<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class ErpLanguage extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    static public function AddErpLanguage($request)
    {
        if($request->erp_table_id>0)
        {
            $ErpLanguage = ErpLanguage::find($request->erp_table_id);
        }
        else
        {
            $ErpLanguage = new ErpLanguage();  
        } 

        function getUserIpAddr(){
            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }

        $ErpLanguage->language_id  = $request->language_id;
        $ErpLanguage->created_by = Auth::user()->id;
        $ErpLanguage->enabled_flag = $request->enabled_flag;
        $ErpLanguage->ip_address = getUserIpAddr();
        $ErpLanguage->save();
        return $ErpLanguage->id;
    }
    Static Public function GetAllErpLanguages($ParaMeter)
    {
        $GetAllErpLanguages = ErpLanguage::select("erp_languages.id as erp_table_id");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllErpLanguages =$GetAllErpLanguages->join("erp_languages_lists","erp_languages_lists.id","erp_languages.language_id")
               ->addselect("erp_languages.*","erp_languages_lists.language_name"); 
        }
        else if(isset($ParaMeter["ERPTableId"]) && $ParaMeter["ERPTableId"]>=0)
        {
            $GetAllErpLanguages =$GetAllErpLanguages->join("erp_languages_lists","erp_languages_lists.id","erp_languages.Language_id")
                            ->where('erp_languages.id',$ParaMeter["ERPTableId"])
                            ->addselect("erp_languages.*","erp_languages_lists.language_name"); 
        }
        else if((isset($ParaMeter["FileLanguagesSearch"]) && $ParaMeter["FileLanguagesSearch"]!="") && (isset($ParaMeter["FileLanguagesSearchArray"]) && count($ParaMeter["FileLanguagesSearchArray"])>0))
        {
            $GetAllErpLanguages = $GetAllErpLanguages->join("erp_languages_lists","erp_languages_lists.id","erp_languages.language_id")->addselect("erp_languages.*","erp_languages_lists.language_name");
            $FileLanguagesSearch = $ParaMeter["FileLanguagesSearchArray"];
            if(count($FileLanguagesSearch)>0)
            {
              foreach ($FileLanguagesSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];

                if($ColumnCondition==3 && $Search["ColumnName"]=="language_name") //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpLanguages = $GetAllErpLanguages->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllErpLanguages = $GetAllErpLanguages->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllErpLanguages = $GetAllErpLanguages->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllErpLanguages = $GetAllErpLanguages->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllErpLanguages = $GetAllErpLanguages->whereNull("$TableName.$ColumnName");
                }
              }
            }
        }
        $GetAllErpLanguages = $GetAllErpLanguages->orderBy("erp_languages.id","DESC")
                                            ->get();
        return $GetAllErpLanguages;
    }
    Static Public function DeleteErpLanguages($ParaMeter)
    {
        $ErpLanguage = ErpLanguage::find($ParaMeter["DeleteId"]);
        $ErpLanguage->delete();
        return $ErpLanguage->id;
    }
}