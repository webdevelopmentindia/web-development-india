<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
class UserGroup extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    static public function AddUserGroup($request)
    {

        $ParaMeter = array();
        $ParaMeter["DeleteId"] = $request->erp_table_id;
        UserGroup::DeleteUserGroups($ParaMeter);
        // if(count($request->groups))
        // {
        //    foreach ($request->groups as $Group) 
        //    {
                $UserGroup = new UserGroup(); 
                $UserGroup->user_id = $request->erp_table_id;
                //$UserGroup->group_id = $Group;
                $UserGroup->group_id = $request->groups;
                $UserGroup->save();
        //    } 
        // }
        return $request->id;
    }
    Static Public function GetAllUserGroups($ParaMeter)
    {
        $GetAllUserGroups = UserGroup::select("user_groups.id as erp_table_id");
        if(isset($ParaMeter["UserId"]) && $ParaMeter["UserId"]>0)
        {
           $GetAllUserGroups =$GetAllUserGroups->where("user_groups.user_id",$ParaMeter["UserId"])->addSelect("user_groups.*"); 
        }
        $GetAllUserGroups = $GetAllUserGroups->orderBy("user_groups.id","DESC")
                                            ->get();
        return $GetAllUserGroups;
    }
    Static Public function DeleteUserGroups($ParaMeter)
    {
        $UserGroup = UserGroup::where("user_id",$ParaMeter["DeleteId"]);
        $UserGroup->delete();
        return $UserGroup;
    }
}