<?php
$erp_table_id = $login = $name = $email = "";
$admin_priv = $enabled_flag = -1;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpUserDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpUserDetails[0]->erp_table_id;
    $login = $ErpUserDetails[0]->login;
    $name = $ErpUserDetails[0]->name;
    $email = $ErpUserDetails[0]->email;
    $enabled_flag = $ErpUserDetails[0]->enabled_flag;
    $admin_priv = $ErpUserDetails[0]->admin_priv;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="SecurityGroupUsersForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="SecurityGroupUsersList" SectionName="SecurityGroupUsersSection" TabName="ERPSecurityGroupUsers">
<div class="row">
    <div class="col-md-12 emloy-hd">
        <h4>
            <?php 
            if($erp_table_id>0)
            {
                echo strtoupper("Update of erp_users");
            }
            else
            {
                echo strtoupper("New record of erp_users"); 
            } 
            ?>
            <span class="pull-right">*Required</span></h4>
         @if($erp_table_id>0)
         <!-- <a href="javascript:" class="btn save-btn ERPFormShow" FormName="SecurityGroupUsersForm" SectionName="SecurityGroupUsersSection"> Add New </a> -->
         <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
         <input id="login" type="hidden" name="login" value="{{ $login }}">
         <input id="group_id" type="hidden" name="group_id" value="{{ $SecuritySearchGroupId }}">
         @endif
         <button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button>
         <button type="button" class="btn cancel-btn" ShowSection="SecurityGroupUsersList" SectionName="SecurityGroupUsersSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button>
    </div>
    <div class="col-md-12 employee-form">
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Login *') }}</label>
            <div class="col-sm-9">
                @if($erp_table_id>0)
                   {{ $login }}
                @else
                   <input id="login" type="text" class="form-control" name="login" value="{{ $login }}">
                @endif
          </div>
        </div>
    @if($erp_table_id=="")
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Password') }}</label>
            <div class="col-sm-9">
                <input id="password" type="password" class="form-control" name="password" value="">
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Confirm Password') }}</label>
            <div class="col-sm-9">
                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" value="">
            </div>
        </div>
    @endif
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
            <div class="col-sm-9">
                <input id="name" type="text" class="form-control" name="name" value="{{ $name }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('E-mail *') }}</label>
            <div class="col-sm-9">
                <input id="email" type="email" class="form-control" name="email" value="{{ $email }}">                
            </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Active') }}</label>
            <div class="col-sm-9">
                <input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==1){{ "checked" }} @endif>Yes 
                <input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0){{ "checked" }} @endif>No
                <input id="admin_priv" type="hidden" class="form-control" name="admin_priv" value="{{ $admin_priv }}">
          </div>
        </div>
        <div class="form-group row">
            <label class="control-label col-sm-3">{{ __('Groups') }}</label>
            <div class="col-sm-9">
                @if(!$GetAllGroups->isEmpty())
                    @foreach($GetAllGroups as $Group)
                        @if($Group->description!="")
                            <!-- <input id="groups" type="checkbox" class="" name="groups[]" value="{{ $Group->id }}" @if($Group->id==$Group->group_id) {{ 'checked' }} @endif>{{ $Group->description }} -->
                            <input id="groups" type="radio" class="" name="groups" value="{{ $Group->id }}" @if($Group->id==$Group->group_id) {{ 'checked' }} @endif>{{ $Group->description }}
                        @endif
                    @endforeach
                @endif
          </div>
        </div>
    </div>
</div>
* Required field(s)
</form>