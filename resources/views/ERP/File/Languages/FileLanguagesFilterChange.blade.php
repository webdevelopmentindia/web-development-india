<div class="close-btn HideFilterChangeModal" ModalClass="ERPFileLanguagesFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>

@if($ParaMeter["FilterName"]=="FileLanguagesLanguage")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Language') }}</label>
    <div class="col-sm-3">
	    <select id="language_condition_change" name="language_condition_change" class="form-control ERPSearchEmptyContent" FieldId="language_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="language_change" type="text" class="form-control" name="language_change"  autofocus placeholder="Language" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileLanguagesLanguage" FieldNameCondition="language_condition_change" FieldName="language_change" FormName="FileLanguagesList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileLanguagesEnabledFlag")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
    <div class="col-sm-3">
      <select id="enabled_flag_condition_change" name="enabled_flag_condition_change" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 check-f">
        <span class="mr-1"><input id="enabled_flag_change" type="radio" class="" name="enabled_flag_change" value="1" @if($ParaMeter["FieldNameValue"]==NULL || $ParaMeter["FieldNameValue"]==1 ) {{ 'checked' }} @endif>Yes</span>
       <span class="mr-1"><input id="enabled_flag_change" type="radio" class="" name="enabled_flag_change" value="0" @if($ParaMeter["FieldNameValue"]==0 && $ParaMeter["FieldNameValue"]!=NULL) {{ 'checked' }} @endif>No</span>
    </div>
    <div class="col-md-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileLanguagesEnabledFlag" FieldNameCondition="enabled_flag_condition_change" FieldName="enabled_flag_change" FormName="FileLanguagesList"> Apply </a></div>
</div>
@endif