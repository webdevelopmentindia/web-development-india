<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpCustomer;
use App\ErpSaveFilter;
use App\ErpSearchTextOption;
use App\ErpSearchRadioOption;
use App\Services\PayUService\Exception;
class ERPSearchShowController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPSearchShowAjax(Request $request)
    {
        // echo "<pre>";
        // print_r($request->all());
        // die();
        $ERPSearchName = "";
        $ERPSearchShow = "";
        try {
            $ParaMeter = array();
            $ParaMeter["All"] = "All";
            $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter);
            $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter);
            if($request->ERPSearchName=="SetupUsersSearch" || $request->ERPSearchName=="ReportsReportOfCustomerUsersSearch")
            {
                $GetAllERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);  
            }
            unset($ParaMeter["All"]);
            if($request->ERPSearchName=="FileApplicationSearch")
            {
                $ERPSearchName = $ParaMeter["PageName"] = "ERP File Applications";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["AppDeployed"] = $request->app_deployed;
                $ParaMeter["AppDeployedCondition"] = $request->app_deployed_condition;
                $ParaMeter["AppName"] = $request->app_name;
                $ParaMeter["AppNameCondition"] = $request->app_name_condition; 
                $ParaMeter["AppURL"] = $request->app_url;
                $ParaMeter["AppURLCondition"] = $request->app_url_condition;
                $ParaMeter["Description"] = $request->description;
                $ParaMeter["DescriptionCondition"] = $request->description_condition;                
                $ERPSearchShow = view('ERP.File.Applications.FileApplicationSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->ERPSearchName=="FileCustomerSearch")
            {
                $ERPSearchName = $ParaMeter["PageName"] = "ERP File Customers";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["TaxId"] = $request->tax_id;
                $ParaMeter["TaxIdCondition"] = $request->tax_id_condition;
                $ParaMeter["CustomerId"] = $request->customer_id;
                $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition; 
                $ParaMeter["CountryId"] = $request->country_id;
                $ParaMeter["CountryIdCondition"] = $request->country_id_condition;
                $ParaMeter["FullName"] = $request->full_name;
                $ParaMeter["FullNameCondition"] = $request->full_name_condition;                
                $ERPSearchShow = view('ERP.File.Customers.FileCustomerSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->ERPSearchName=="FileCountriesSearch")
            { 

                $ERPSearchName = $ParaMeter["PageName"] = "ERP File Countries";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["CountryId"] = $request->country_id;
                $ParaMeter["CountryIdCondition"] = $request->country_id_condition; 
                $ParaMeter["Code"] = $request->code;
                $ParaMeter["CodeCondition"] = $request->code_condition;
                $ParaMeter["Description"] = $request->description;
                $ParaMeter["DescriptionCondition"] = $request->description_condition;                
                $ERPSearchShow = view('ERP.File.Countries.FileCountriesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->ERPSearchName=="FileSourcesSearch")
            { 
                $ERPSearchName = $ParaMeter["PageName"] = "ERP File Sources";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["SourceId"] = $request->source_id;
                $ParaMeter["SourceIdCondition"] = $request->source_id_condition; 
                $ParaMeter["Code"] = $request->code;
                $ParaMeter["CodeCondition"] = $request->code_condition;
                $ParaMeter["Description"] = $request->description;
                $ParaMeter["DescriptionCondition"] = $request->description_condition;
                $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;
                $ParaMeter["AutoPost"] = $request->auto_post;
                $ParaMeter["AutoPostCondition"] = $request->auto_post_condition;                
                $ERPSearchShow = view('ERP.File.Sources.FileSourcesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->ERPSearchName=="FileThemesSearch")
            { 
                $ERPSearchName = $ParaMeter["PageName"] = "ERP File Themes";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["Theme"] = $request->theme;
                $ParaMeter["ThemeCondition"] = $request->theme_condition;
                $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;               
                $ERPSearchShow = view('ERP.File.Themes.FileThemesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->ERPSearchName=="FileLanguagesSearch")
            { 
                $ERPSearchName = $ParaMeter["PageName"] = "ERP File Languages";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["Language"] = $request->language;
                $ParaMeter["LanguageCondition"] = $request->language_condition;
                $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;               
                $ERPSearchShow = view('ERP.File.Languages.FileLanguagesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }            
            else if($request->ERPSearchName=="SetupUsersSearch")
            { 
                $ERPSearchName = $ParaMeter["PageName"] = "ERP Setup Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);

                $ParaMeter["UserId"] = $request->user_id;
                $ParaMeter["UserIdCondition"] = $request->user_id_condition;
                $ParaMeter["Login"] = $request->login;
                $ParaMeter["LoginCondition"] = $request->login_condition;
                $ParaMeter["FullName"] = $request->full_name;
                $ParaMeter["FullNameCondition"] = $request->full_name_condition;
                $ParaMeter["EMail"] = $request->e_mail;
                $ParaMeter["EMailCondition"] = $request->e_mail_condition;
                $ParaMeter["AdminPriv"] = $request->admin_priv;
                $ParaMeter["AdminPrivCondition"] = $request->admin_priv_condition;
                $ParaMeter["CustomerId"] = $request->customer_id;
                $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition; 
                 
                $ERPSearchShow = view('ERP.Setup.Users.SetupUsersSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","GetAllERPCustomers"))->render();
            }
            else if($request->ERPSearchName=="SecuritySystemLogSearch")
            { 
                $ERPSearchName = $ParaMeter["PageName"] = "ERP Security System Log";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ParaMeter["Login"] = $request->login;
                $ParaMeter["LoginCondition"] = $request->login_condition;
                $ParaMeter["Date"] = $request->date;
                $ParaMeter["DateCondition"] = $request->date_condition;
                $ParaMeter["Session"] = $request->session;
                $ParaMeter["SessionCondition"] = $request->session_condition;
                $ParaMeter["Ip"] = $request->ip;
                $ParaMeter["IpCondition"] = $request->ip_condition;  
                $ERPSearchShow = view('ERP.Security.SystemLog.SecuritySystemLogSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
            }
            else if($request->ERPSearchName=="ReportsReportOfCustomerUsersSearch")
            { 
                $ERPSearchName = $ParaMeter["PageName"] = "ERP Report Of Customer Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);

                $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;
                $ParaMeter["Login"] = $request->login;
                $ParaMeter["LoginCondition"] = $request->login_condition;
                $ParaMeter["FullName"] = $request->full_name;
                $ParaMeter["FullNameCondition"] = $request->full_name_condition;
                $ParaMeter["EMail"] = $request->e_mail;
                $ParaMeter["EMailCondition"] = $request->e_mail_condition;
                $ParaMeter["AdminPriv"] = $request->admin_priv;
                $ParaMeter["AdminPrivCondition"] = $request->admin_priv_condition;
                $ParaMeter["CustomerId"] = $request->customer_id;
                $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition;

                $ERPSearchShow = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","GetAllERPCustomers"))->render();
            }
            $Message = "$ERPSearchName Search Successfully!";
            $Status = "success";
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPSearchName Search " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"ERPSearchShow"=>$ERPSearchShow]);
    }
}