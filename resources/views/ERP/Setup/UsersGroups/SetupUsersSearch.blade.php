@if(!isset($ParaMeter["ERPTableId"]))
<?php 
    echo strtoupper("Search of erp_users");
    echo " ".date("m/d/Y");
?>
	<div class="row">
	    <div class="col-md-12 emloy-hd">
	    </div>
	    <div class="col-md-12 employee-form">
	    	<form method="POST" aria-label="{{ __('ERPSearchSubmit') }}" enctype="multipart/form-data" id="ERPSearchSubmit" name="ERPSearchSubmit" FormParentDivId="SetupUsersSearch">
		    @csrf
			<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SetupUsersList" SectionName="SetupUsersSection" TabName="ERPSetupUsers">
            <div id="ERPChangeFilter">
@endif	
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>		
			<?php 
			    $LanguageCondition = $ParaMeter["LanguageCondition"];
			    $Language = $ParaMeter["Language"];
			    $EnabledFlagCondition = $ParaMeter["EnabledFlagCondition"];
			    $EnabledFlag = $ParaMeter["EnabledFlag"];
			    $ERPTableId = "";
			    if(isset($ParaMeter["ERPTableId"]))
			    {
			       $ERPTableId = $ParaMeter["ERPTableId"];
			    }
			?>
	        <div class="form-group SearchSection">
	            <label class="control-label col-sm-3">{{ __('User ID') }}</label>
				<div class="col-sm-3">
				    <select id="language_condition" name="language_condition" class="form-control ERPSearchEmptyContent" FieldId="language">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
		                            <option value="{{ $RadioOption->id }}" @if($EnabledFlagCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="language" type="number" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }} SearchFieldShow" name="language"  autofocus placeholder="User ID" value="<?php echo $Language; ?>">
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('Login') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LanguageCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="language" type="text" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }} SearchFieldShow" name="language"  autofocus placeholder="Login" value="<?php echo $Language; ?>" style='@if($LanguageCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LanguageCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="language" type="text" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }} SearchFieldShow" name="language"  autofocus placeholder="Full Name" value="<?php echo $Language; ?>" style='@if($LanguageCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('E-mail') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LanguageCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="language" type="text" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }} SearchFieldShow" name="language"  autofocus placeholder="E-mail" value="<?php echo $Language; ?>" style='@if($LanguageCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('Admin Priv.') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($LanguageCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                 <input id="language" type="number" class="form-control{{ $errors->has('language') ? ' is-invalid' : '' }} SearchFieldShow" name="language"  autofocus placeholder="Admin Priv" value="<?php echo $Language; ?>" style='@if($LanguageCondition==4) {{ "display: none" }} @endif'>
	            </div>
	        </div>
            <div class="form-group">
	            <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
	            <div class="col-sm-3">
				    <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
		                            <option value="{{ $RadioOption->id }}" @if($EnabledFlagCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
				    	@if(!$GetAllERPCustomers->isEmpty())
		               	   @foreach($GetAllERPCustomers as $Customer)
		                            <option value="{{ $Customer->id }}" @if($EnabledFlagCondition==$Customer->id) {{ 'selected' }} @endif>{{ $Customer->full_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
	            </div>
	        </div>
	        <div class="form-group">
	            <label class="control-label col-sm-3"></label>
	            <div class="col-sm-3">
		        </div>
	            <div class="col-sm-6">
	               <button type="submit" class="btn cancel-btn">Search</button>
	               <a type="button" onclick="ERPFormReset(this); return false;" class="btn cancel-btn" >Clear</a>
	               <select id="erp_filter_list" name="erp_filter_list" class="form-control" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" FormName="SetupUsersSearch">
	               	@if(!$GetAllErpSaveFilters->isEmpty())
	               	   <option value=""></option>
	               	   <option value="">Public</option>
	               	   @foreach($GetAllErpSaveFilters as $Filter)
                            <option value="{{ $Filter->id }}" @if($ERPTableId==$Filter->id) {{ 'selected' }} @endif >{{ $Filter->filter_name }}</option>
	               	   @endforeach
	               	@endif
		           </select>
	               <a href="javascript:" type="button" class="btn cancel-btn">Save Filter</a>
	               <button type="button" class="btn cancel-btn" ShowSection="SetupUsersList" SectionName="SetupUsersSection" id="ERPBackCancelButton">Back</button>
	            </div>
	        </div>
@if(!isset($ParaMeter["ERPTableId"]))
            </div>
	        </form>
	        <div class="form-group">
	            <label class="control-label col-sm-3"></label>
	            <div class="col-sm-3">
		        </div>
		        <div class="col-sm-6">
		               Save Filter 
		               <input id="filter_name" type="text" class="form-control{{ $errors->has('filter_name') ? ' is-invalid' : '' }}" name="filter_name"  autofocus placeholder="Filter Name" value="">
		               <button type="button" class="btn cancel-btn ERPFilterSave" FormName="SetupUsersSearch">Save</button>
		        </div>
		        <div class="col-sm-6" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
		               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
		                    @if(!$GetAllErpSaveFilters->isEmpty())
			               	   <option value=""></option>
			               	   <option value="">Public</option>
			               	   @foreach($GetAllErpSaveFilters as $Filter)
		                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
			               	   @endforeach
			               	@endif
		               </select>
		               <button type="button" class="btn cancel-btn ERPFilterDelete" FormName="SetupUsersSearch">Delete</button>
		        </div>
	       </div>
	    </div>
	</div>
@endif