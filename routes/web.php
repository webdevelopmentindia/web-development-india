<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//die();
Route::get('/', function () {
    //return view('Welcome');
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware'=>['UserGroupMiddleware','auth']], function () 
{
	Route::get('/ERP_MENU', 'HomeController@index')->name('home');

	Route::post('/ERP_MENU', 'ERPManuDataController@ERPManuDataAjax')->name('ERPManuDataAjax');
	Route::post('/ERPRecordDeleteAjax', 'ERPRecordDeleteController@ERPRecordDeleteAjax')->name('ERPRecordDeleteAjax');
	Route::post('/ERPFormShowAjax', 'ERPFormShowController@ERPFormShowAjax')->name('ERPFormShowAjax');
	Route::post('/ERPFormSubmitAjax', 'ERPFormSubmitController@ERPFormSubmitAjax')->name('ERPFormSubmitAjax');
	Route::post('/ERPSearchShowAjax', 'ERPSearchShowController@ERPSearchShowAjax')->name('ERPSearchShowAjax');
	Route::post('/ERPSearchSubmitAjax', 'ERPSearchSubmitController@ERPSearchSubmitAjax')->name('ERPSearchSubmitAjax');
	Route::post('/ERPFilterSaveAjax', 'ERPFilterSaveController@ERPFilterSaveAjax')->name('ERPFilterSaveAjax');
	Route::post('/ERPFilterGetAjax', 'ERPFilterGetController@ERPFilterGetAjax')->name('ERPFilterGetAjax');
	Route::post('/ERPFilterDeleteAjax', 'ERPFilterDeleteController@ERPFilterDeleteAjax')->name('ERPFilterDeleteAjax');
	Route::post('/ERPAddFilterShowAjax', 'ERPAddFilterShowController@ERPAddFilterShowAjax')->name('ERPAddFilterShowAjax');
	Route::post('/ERPFilterChangeAjax', 'ERPFilterChangeController@ERPFilterChangeAjax')->name('ERPFilterChangeAjax');
	Route::post('/ERPGetOldFilterDataAjax', 'ERPGetOldFilterDataController@ERPGetOldFilterDataAjax')->name('ERPGetOldFilterDataAjax');
	Route::post('/ERPFilterSaveGetListDataAjax', 'ERPGetOldFilterDataController@ERPFilterSaveGetListDataAjax')->name('ERPFilterSaveGetListDataAjax');
});