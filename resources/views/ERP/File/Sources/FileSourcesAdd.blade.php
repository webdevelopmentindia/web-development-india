<?php
$erp_table_id = $code = $description = "";
$auto_post = $enabled_flag = -1;
$AddSaveButtonText = "Add";
$BackCancelButtonText = "Cancel";
if(!$ErpSourceDetails->isEmpty())
{
    $AddSaveButtonText = "Save";
    $BackCancelButtonText = "Back";
    $erp_table_id = $ErpSourceDetails[0]->erp_table_id;
    $code = $ErpSourceDetails[0]->source_code;
    $description = $ErpSourceDetails[0]->description;
    $enabled_flag = $ErpSourceDetails[0]->enabled_flag;
    $auto_post = $ErpSourceDetails[0]->auto_post;
}
?>
<div class="alert alert-danger" style="display:none"></div>
<div class="alert alert-info" style="display:none"></div>
<form method="POST" aria-label="{{ __('ERPFormSubmit') }}" enctype="multipart/form-data" id="ERPFormSubmit" FormParentDivId="FileSourcesForm">
@csrf
<input id="ERPFormName" type="hidden" name="ERPFormName" value="FileSourcesList" SectionName="FileSourcesSection" TabName="ERPFileSources">
<div class="row">
<div class="card-header header-elements-inline">
        <div class="page-title">
            <h3>
                <?php 
            if($erp_table_id>0)
            {
                echo strtoupper("Update of erp_sources");
            }
            else
            {
                echo strtoupper("New record of erp_sources"); 
            } 
            ?>
            </h3>
        </div>
        <div class="header-elements">
            <span><?php 
                echo " ".date("m/d/Y");
            ?></span>
        <span class="pull-right"></span>
        </div>
    </div> <!-- emloy-hd p-0 -->




    <div class="row row-col">
        <div class="col-md-12 emloy-hd">
             @if($erp_table_id>0)
             <a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileSourcesForm" SectionName="FileSourcesSection"> Add New </a>
             <input id="erp_table_id" type="hidden" name="erp_table_id" value="<?php if(old('erp_table_id')) { echo old('erp_table_id'); } else { echo $erp_table_id; }?>">
             @endif
             <button type="submit" class="btn save-btn">{{ $AddSaveButtonText  }}</button>
             <button type="button" class="btn cancel-btn save-btn" ShowSection="FileSourcesList" SectionName="FileSourcesSection" id="ERPBackCancelButton">{{ $BackCancelButtonText  }}</button>
        </div>
    </div>
    <div class="card-body employee-form">
        <div class="row">
            <div class="col-xs-12">
                <span class="req-field">* Required field(s)</span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 p-0">
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Code *') }}</label>
                    <div class="col-sm-9">
                        <input id="code" type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code"  autofocus placeholder="Code" value="<?php if(old('code')) { echo old('code'); } else { echo $code; }?>" maxlength="5">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Description *') }}</label>
                    <div class="col-sm-9">
                        <input id="description " type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description"  autofocus placeholder="Description" value="<?php if(old('description')) { echo old('description'); } else { echo $description; }?>">
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Auto pos JE? *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="auto_post" type="radio" class="" name="auto_post" value="1" @if($auto_post==-1 || $auto_post==1){{ "checked" }} @endif>Yes</span>
                        <span class="mr-1"><input id="auto_post" type="radio" class="" name="auto_post" value="0" @if($auto_post==0){{ "checked" }} @endif>No</span>
                        @if($errors->has('auto_post'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('auto_post') }}</strong>
                            </span>
                        @endif
                  </div>
                </div>
                <div class="form-group form-flex">
                    <label class="control-label col-sm-3">{{ __('Active *') }}</label>
                    <div class="col-sm-9 check-f">
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" @if($enabled_flag==1 || $enabled_flag==-1){{ "checked" }} @endif>Yes</span>
                        <span class="mr-1"><input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0" @if($enabled_flag==0){{ "checked" }} @endif>No</span>
                        @if($errors->has('enabled_flag'))
        	                <span class="invalid-feedback" role="alert">
        	                    <strong>{{ $errors->first('enabled_flag') }}</strong>
        	                </span>
                        @endif
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>