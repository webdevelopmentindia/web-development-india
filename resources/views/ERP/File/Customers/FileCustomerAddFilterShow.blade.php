<div class="close-btn HideAddFilterModal" ModalId="FileCustomerAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileCustomerTaxId")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Tax ID') }}</label>
	<div class="col-sm-3 p-0">
	    <select id="tax_id_condition" name="tax_id_condition" class="form-control ERPSearchEmptyContent" FieldId="tax_id">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="tax_id" type="text" class="form-control" name="tax_id"  autofocus placeholder="Tax ID" value=""> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="tax_id_condition" FieldName="tax_id" FormName="FileCustomerList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCustomerCustomerId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="customer_id_condition" name="customer_id_condition" class="form-control" FieldId="customer_id">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="customer_id" type="number" class="form-control" name="customer_id"  autofocus placeholder="Customer ID" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileCustomerList" FieldNameCondition="customer_id_condition" FieldName="customer_id"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCustomerCountryId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Country ID') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="country_id_condition" name="country_id_condition" class="form-control" FieldId="country_id">
	    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="country_id" type="number" class="form-control" name="country_id"  autofocus placeholder="Country Id" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="country_id_condition" FieldName="country_id" FormName="FileCustomerList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCustomerFullName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="full_name_condition" name="full_name_condition" class="form-control ERPSearchEmptyContent" FieldId="full_name">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="full_name" type="text" class="form-control" name="full_name"  autofocus placeholder="Full Name" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileCustomerList" FieldNameCondition="full_name_condition" FieldName="full_name"> Apply </a></div>
</div>
@endif