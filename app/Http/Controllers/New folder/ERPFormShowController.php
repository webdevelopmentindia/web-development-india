<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\Services\PayUService\Exception;
class ERPFormShowController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFormShowAjax(Request $request)
    {
        $ParaMeter = array();
        $ERPTableId = 0;
        $AddUpdateMessage = "Add";
        if($request->EditId>0)
        {
            $AddUpdateMessage = "Update";
            $ERPTableId = $request->EditId;
        }
        $ParaMeter["ERPTableId"] = $ERPTableId;

        try {
            if($request->FormName=="FileApplicationAdd")
            {
                $ERPApplicationDetails = ErpApplication::GetAllERPApplications($ParaMeter);
                $Html = view('ERP.File.Applications.Add',compact("ERPApplicationDetails"))->render();
                return json_encode(["Status"=>"success","Html"=>$Html]);
            }
            else if($request->FormName=="FileCustomerAdd")
            {
                $ERPCustomerDetails = ERPCustomer::GetAllERPCustomers($ParaMeter);
                $Html = view('ERP.File.Customers.Add',compact("ERPCustomerDetails"))->render();
                return json_encode(["Status"=>"success","Html"=>$Html]);
            }
            else if($request->FormName=="Countries")
            {
                return view('ERP.File.Countries');
            }
            else if($request->FormName=="Sources")
            {
                return view('ERP.File.Sources');
            }
            else if($request->FormName=="ERP Themes")
            {
                return view('ERP.File.Themes');
            }
            else if($request->FormName=="ERP Languages")
            {
                return view('ERP.File.Languages');
            }
        }
        catch (\Exception $e) {
          $Message = "Error in ERP $AddUpdateMessage Form Show " . $e->getMessage();
        }
        return json_encode(["Status"=>"error","Message"=>$Message]);
    }
    public function ERPFormAddAjax(Request $request)
    {
        $AddUpdateMessage = "Added";
        $ErpTableId = "";
        if($request->erp_table_id>0)
        {
            $ErpTableId = $request->erp_table_id;
            $AddUpdateMessage = "Updated";
        }
        if($request->ERPFormName=="FileApplicationsAdd")
        {
            $validator = \Validator::make($request->all(), [
                'app_name' => 'required|unique:erp_applications,app_name,'.$ErpTableId,
                'description' => 'required|unique:erp_applications,description,'.$ErpTableId,
                'app_url' => 'required|unique:erp_applications,app_url,'.$ErpTableId,
            ]);
            if ($validator->fails())
            {
                $Message = $validator->errors()->all();
                $Status = "errors";
            }
            else
            {
                try {
                    $ErpApplication = ErpApplication::AddErpApplication($request);
                    $Message = "File Application $AddUpdateMessage Successfully!";
                    $Status = "success";
                } 
                catch (\Exception $e) {
                  $Message = "Error in File Application $AddUpdateMessage " . $e->getMessage();
                  $Status = "error";
                }
            }
        }
        else if($request->ERPFormName=="FileCustomersAdd")
        {
            $validator = \Validator::make($request->all(), [
                'country_id' => 'required',
                'full_name' => 'required|unique:erp_customers,full_name,'.$ErpTableId,
                'tax_id' => 'required|unique:erp_customers,tax_id,'.$ErpTableId,
                'total_licenses' => 'required',
                'address1' => 'required',
            ]);
            if ($validator->fails())
            {
                $Message = $validator->errors()->all();
                $Status = "errors";
            }
            else
            {
                try {
                    $ErpCustomer = ErpCustomer::AddErpCustomer($request);
                    $Message = "File Customer $AddUpdateMessage Successfully!";
                    $Status = "success";
                } 
                catch (\Exception $e) {
                  $Message = "Error in File Customer $AddUpdateMessage " . $e->getMessage();
                  $Status = "error";
                }
            }
        }
        return response()->json(["Status"=>$Status, "Message"=>$Message]);
    }
    public function ERPRecordDeleteAjax(Request $request)
    {
        $Status = "error";
        $Message = "Error in Record Deletion!";
        try {
            $ParaMeter = array();
            if($request->FormName=="FileApplicationAdd")
            {
                if($request->DeleteId>0)
                {
                    $ParaMeter["ERPApplicationId"] = $request->DeleteId;
                    $DeleteERPApplications = ErpApplication::DeleteERPApplications($ParaMeter);
                    $Status = "success";
                }
            }
            else if($request->ManuName=="ERP Customers")
            {
                return view('ERP.File.Customers');
            }
            else if($request->ManuName=="Countries")
            {
                return view('ERP.File.Countries');
            }
            else if($request->ManuName=="Sources")
            {
                return view('ERP.File.Sources');
            }
            else if($request->ManuName=="ERP Themes")
            {
                return view('ERP.File.Themes');
            }
            else if($request->ManuName=="ERP Languages")
            {
                return view('ERP.File.Languages');
            }
        }
        catch (\Exception $e) {
          $Message = "Error in ERP Record Deletion " . $e->getMessage();
        }
        return json_encode(["Status"=>$Status,"Message"=>$Message]);
    }
}