<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpSaveFilter;
use App\ErpSearchRadioOption;
use App\ErpSearchTextOption;
use App\ErpApplication;
use App\ErpCustomer;
use App\ErpCountrie;
use App\ErpSource;
use App\ErpTheme;
use App\ErpLanguage;
use App\UserLogged;
use App\User;
use App\Group;
use App\GroupsApp;
use App\Services\PayUService\Exception;
class ERPGetOldFilterDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPGetOldFilterDataAjax(Request $request)
    {
        $Message = "Something Went Wrong Please Try Again!";
        $Status = "error";
        $FilterChangeHtml = "";
        $ERPFilterName = "";
        try {
            $ParaMeter = array();
            $ParaMeter["ERPTableId"] = $request->ERPTableId;
            $GetErpSaveFilterDetails = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            if(!$GetErpSaveFilterDetails->isEmpty())
            {
                foreach ($GetErpSaveFilterDetails as $Filter) 
                {
                    if($request->PageName=="ERPFileApplication")
                    {
                        $ParaMeter["AppNameCondition"] = $Filter->first_column_condition;
                        $ParaMeter["AppName"] = $Filter->first_column_value;
                        $ParaMeter["AppURLCondition"] = $Filter->second_column_condition;
                        $ParaMeter["AppURL"] = $Filter->second_column_value;
                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        $ParaMeter["AppDeployedCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["AppDeployed"] = $Filter->fourth_column_value;
                        
                        $ERPFilterGetHtml = view('ERP.File.Applications.FileApplicationSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                    }
                    $Status = "success";
                    $Message = "$ERPFilterName Filter Change Successfully!";
                }
            }
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPFilterName Filter Change " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"FilterChangeHtml"=>$FilterChangeHtml]);
    }
    public function ERPFilterSaveGetListDataAjax(Request $request)
    {
        $Message = "Something Went Wrong Please Try Again!";
        $Status = "error";
        $ERPFilterGetHtml = "";
        $ERPSearchName = "";
        $GetExportPermission = "";
        try {
            $ParaMeter = array();
            $ParaMeter["All"] = "All";

            $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter);
            $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter);
            unset($ParaMeter["All"]);

            $ParaMeter["ERPTableId"] = $request->ERPTableId;
            $ERPManuDataRecord = "";
            $GetErpSaveFilterDetails = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            if(!$GetErpSaveFilterDetails->isEmpty())
            {
                unset($ParaMeter["ERPTableId"]);

                if($GetErpSaveFilterDetails[0]->page_name=="ERP Setup Users" || $request->page_name=="ERP Setup Users")
                {
                    $ParaMeter["All"] = "All";
                    $GetAllERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);
                    unset($ParaMeter["All"]);  
                }
                
                foreach ($GetErpSaveFilterDetails as $Filter) 
                {
                   if($Filter->page_name=="ERP File Applications")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Applications";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                        unset($ParaMeter["PageName"]);

                        $OnlyData = "ERP Applications";

                        $ParaMeter["FileApplicationSearch"] = "FileApplicationSearch";

                        $FileApplicationSearchArray = array();
                       
                        $ParaMeter["AppNameCondition"] = $Filter->first_column_condition;
                        $ParaMeter["AppName"] = $Filter->first_column_value;
                        $ParaMeter["AppNameConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["AppNameCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                            $ParaMeter["AppNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "app_name","ColumnValue" => $ParaMeter["AppName"],"ColumnCondition" => $ParaMeter["AppNameCondition"]);

                        $ParaMeter["AppURLCondition"] = $Filter->second_column_condition;
                        $ParaMeter["AppURL"] = $Filter->second_column_value;
                        $ParaMeter["AppURLConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["AppURLCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["AppURLConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "app_url","ColumnValue" => $ParaMeter["AppURL"],"ColumnCondition" => $ParaMeter["AppURLCondition"]);

                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        $ParaMeter["DescriptionConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["DescriptionCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["DescriptionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);
                        
                        $ParaMeter["AppDeployedCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["AppDeployed"] = $Filter->fourth_column_value;
                        $ParaMeter["AppDeployedConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["AppDeployedCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["AppDeployedConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "app_deployed","ColumnValue" => $ParaMeter["AppDeployed"],"ColumnCondition" => $ParaMeter["AppDeployedCondition"]);

                        $ParaMeter["FileApplicationSearchArray"] = $FileApplicationSearchArray;
                        
                        $ERPApplications = ErpApplication::GetAllERPApplications($ParaMeter);
                        
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        unset($ParaMeter["FileApplicationSearchArray"]);

                        $ERPManuDataRecord = view('ERP.File.Applications.FileApplicationList',compact("ERPApplications","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 1;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);

                   }
                   else if($Filter->page_name=="ERP File Customers")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Customers";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                        unset($ParaMeter["PageName"]);
                        
                        $OnlyData = "ERP Customers";

                        $ParaMeter["FileCustomerSearch"] = "FileCustomerSearch";

                        $FileCustomerSearchArray = array();
                        
                        $ParaMeter["TaxIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["TaxId"] = $Filter->first_column_value;
                        $ParaMeter["TaxIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["TaxIdCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["TaxIdConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "tax_id","ColumnValue" => $ParaMeter["TaxId"],"ColumnCondition" => $ParaMeter["TaxIdCondition"]);


                        $ParaMeter["CustomerIdCondition"] = $Filter->second_column_condition;
                        $ParaMeter["CustomerId"] = $Filter->second_column_value;
                        $ParaMeter["CustomerIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["CustomerIdCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["CustomerIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }

                        $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "id","ColumnValue" => $ParaMeter["CustomerId"],"ColumnCondition" => $ParaMeter["CustomerIdCondition"]);

                        $ParaMeter["CountryIdCondition"] = $Filter->third_column_condition;
                        $ParaMeter["CountryId"] = $Filter->third_column_value;
                        $ParaMeter["CountryIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $request->country_id_condition;
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["CountryIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }

                        $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "country_id","ColumnValue" => $ParaMeter["CountryId"],"ColumnCondition" => $ParaMeter["CountryIdCondition"]);


                        $ParaMeter["FullNameCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["FullName"] = $Filter->fourth_column_value;
                        $ParaMeter["FullNameConditionValue"] = "";
                        $ParaMeter1["TableId"] = $request->full_name_condition;
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["FullNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "full_name","ColumnValue" => $ParaMeter["FullName"],"ColumnCondition" => $ParaMeter["FullNameCondition"]);

                        $ParaMeter["FileCustomerSearchArray"] = $FileCustomerSearchArray;
                        if(count($ParaMeter["FileCustomerSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }
                        
                        $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);

                        unset($ParaMeter["FileCustomerSearchArray"]);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ERPManuDataRecord = view('ERP.File.Customers.FileCustomerList',compact("ERPCustomers","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 2;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                   }
                   else if($Filter->page_name=="ERP File Countries")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Countries";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                        unset($ParaMeter["PageName"]);

                        $ERPSearchName = "ERP Countries";
                        $OnlyData = "Countries";

                        $ParaMeter["FileCountriesSearch"] = "FileCountriesSearch";

                        $FileCountriesSearchArray = array();
                        
                        $ParaMeter["CountryIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["CountryId"] = $Filter->first_column_value;
                        $ParaMeter["CountryIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["CountryIdCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["CountryIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        
                        $FileCountriesSearchArray[] = array("TableName" => "erp_countries","ColumnName" => "id","ColumnValue" => $ParaMeter["CountryId"],"ColumnCondition" => $ParaMeter["CountryIdCondition"]);

                        $ParaMeter["CodeCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Code"] = $Filter->second_column_value;
                        $ParaMeter["CodeConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["CodeCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                            $ParaMeter["CodeConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileCountriesSearchArray[] = array("TableName" => "erp_countries","ColumnName" => "country_code","ColumnValue" => $ParaMeter["Code"],"ColumnCondition" => $ParaMeter["CodeCondition"]);

                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        $ParaMeter["DescriptionConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["DescriptionCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                            $ParaMeter["DescriptionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileCountriesSearchArray[] = array("TableName" => "erp_countries","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);
                        
                        $ParaMeter["FileCountriesSearchArray"] = $FileCountriesSearchArray;
                        if(count($ParaMeter["FileCountriesSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }

                        $ERPCountries = ErpCountrie::GetAllErpCountries($ParaMeter);
                        unset($ParaMeter["FileCountriesSearchArray"]);
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ERPManuDataRecord = view('ERP.File.Countries.FileCountriesList',compact("ERPCountries","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 3;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);

                   }
                   else if($Filter->page_name=="ERP File Sources")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Sources";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                        unset($ParaMeter["PageName"]);

                        $ERPSearchName = "ERP Sources";

                        $OnlyData = "Sources";

                        $ParaMeter["FileSourcesSearch"] = "FileSourcesSearch";

                        $FileSourcesSearchArray = array();
                        
                        $ParaMeter["SourceIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["SourceId"] = $Filter->first_column_value;
                        $ParaMeter["SourceIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["SourceIdCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["SourceIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        
                        $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "id","ColumnValue" => $ParaMeter["SourceId"],"ColumnCondition" => $ParaMeter["SourceIdCondition"]);

                        $ParaMeter["CodeCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Code"] = $Filter->second_column_value;
                        $ParaMeter["CodeConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["CodeCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["CodeConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "source_code","ColumnValue" => $ParaMeter["Code"],"ColumnCondition" => $ParaMeter["CodeCondition"]);

                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        $ParaMeter["DescriptionConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["DescriptionCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["DescriptionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);

                        $ParaMeter["AutoPostCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["AutoPost"] = $Filter->fourth_column_value;
                        $ParaMeter["AutoPostConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["AutoPostCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["AutoPostConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        
                        $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "auto_post","ColumnValue" => $ParaMeter["AutoPost"],"ColumnCondition" => $ParaMeter["AutoPostCondition"]);


                        $ParaMeter["EnabledFlagCondition"] = $Filter->fifth_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->fifth_column_value;
                        $ParaMeter["EnabledFlagConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["EnabledFlagCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        
                        $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);

                        $ParaMeter["FileSourcesSearchArray"] = $FileSourcesSearchArray;
                        if(count($ParaMeter["FileSourcesSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }

                        $ERPSources = ErpSource::GetAllErpSources($ParaMeter);
                        unset($ParaMeter["FileSourcesSearchArray"]);
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ERPManuDataRecord = view('ERP.File.Sources.FileSourcesList',compact("ERPSources","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 4;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                        
                   } 
                   else if($Filter->page_name=="ERP File Themes")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Themes";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        unset($ParaMeter["PageName"]);

                        $ERPSearchName = $OnlyData = "ERP Themes";

                        $ParaMeter["FileThemesSearch"] = "FileThemesSearch";

                        $FileThemesSearchArray = array();

                        $ParaMeter["ThemeCondition"] = $Filter->first_column_condition;
                        $ParaMeter["Theme"] = $Filter->first_column_value;
                        $ParaMeter["ThemeConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["ThemeCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                           $ParaMeter["ThemeConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileThemesSearchArray[] = array("TableName" => "erp_themes_lists","ColumnName" => "theme_name","ColumnValue" => $ParaMeter["Theme"],"ColumnCondition" => $ParaMeter["ThemeCondition"]);

                        $ParaMeter["EnabledFlagCondition"] = $Filter->second_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->second_column_value;
                        $ParaMeter["EnabledFlagConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["EnabledFlagCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        
                        $FileThemesSearchArray[] = array("TableName" => "erp_themes","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);

                        $ParaMeter["FileThemesSearchArray"] = $FileThemesSearchArray;
                        if(count($ParaMeter["FileThemesSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }

                        $ERPThemes = ErpTheme::GetAllErpThemes($ParaMeter);
                        unset($ParaMeter["FileThemesSearchArray"]);
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ERPManuDataRecord = view('ERP.File.Themes.FileThemesList',compact("ERPThemes","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 5;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);

                   }
                   else if($Filter->page_name=="ERP File Languages")
                   {

                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Languages";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        unset($ParaMeter["PageName"]);

                        $ERPSearchName = $OnlyData = "ERP Languages";

                        $ParaMeter["FileLanguagesSearch"] = "FileLanguagesSearch";

                        $FileLanguagesSearchArray = array();

                        $ParaMeter["LanguageCondition"] = $Filter->first_column_condition;
                        $ParaMeter["Language"] = $Filter->first_column_value;
                        $ParaMeter["LanguageConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["LanguageCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                           $ParaMeter["LanguageConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $FileLanguagesSearchArray[] = array("TableName" => "erp_languages_lists","ColumnName" => "language_name","ColumnValue" => $ParaMeter["Language"],"ColumnCondition" => $ParaMeter["LanguageCondition"]);

                        $ParaMeter["EnabledFlagCondition"] = $Filter->second_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->second_column_value;
                        $ParaMeter["EnabledFlagConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["EnabledFlagCondition"];
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        
                        $FileLanguagesSearchArray[] = array("TableName" => "erp_languages","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);

                        $ParaMeter["FileLanguagesSearchArray"] = $FileLanguagesSearchArray;
                        if(count($ParaMeter["FileLanguagesSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }

                        $ERPLanguages = ErpLanguage::GetAllErpLanguages($ParaMeter);
                        unset($ParaMeter["FileLanguagesSearchArray"]);
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ERPManuDataRecord = view('ERP.File.Languages.FileLanguagesList',compact("ERPLanguages","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 6;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);

                   } 
                   else if($Filter->page_name=="ERP Setup Users")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP Setup Users";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        unset($ParaMeter["PageName"]);
               
                        $OnlyData = "ERP Users";

                        $ParaMeter["SetupUsersSearch"] = "SetupUsersSearch";

                        $SetupUsersSearchArray = array();

                        $ParaMeter["UserIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["UserId"] = $Filter->first_column_value;
                        $ParaMeter["UserIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["UserIdCondition"];
                       
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["UserIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }

                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "id","ColumnValue" => $ParaMeter["UserId"],"ColumnCondition" => $ParaMeter["UserIdCondition"]);

                        $ParaMeter["LoginCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Login"] = $Filter->second_column_value;
                        $ParaMeter["LoginConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["LoginCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["LoginConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "login","ColumnValue" => $ParaMeter["Login"],"ColumnCondition" => $ParaMeter["LoginCondition"]);


                        $ParaMeter["FullNameCondition"] = $Filter->third_column_condition;
                        $ParaMeter["FullName"] = $Filter->third_column_value;
                        $ParaMeter["FullNameConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["FullNameCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["FullNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "name","ColumnValue" => $ParaMeter["FullName"],"ColumnCondition" => $ParaMeter["FullNameCondition"]);


                        $ParaMeter["EMailCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["EMail"] = $Filter->fourth_column_value;
                        $ParaMeter["EMailConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["EMailCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["EMailConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "email","ColumnValue" => $ParaMeter["EMail"],"ColumnCondition" => $ParaMeter["EMailCondition"]);

                        $ParaMeter["AdminPrivCondition"] = $Filter->fifth_column_condition;
                        $ParaMeter["AdminPriv"] = $Filter->fifth_column_value;
                        $ParaMeter["AdminPrivConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["AdminPrivCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["AdminPrivConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "admin_priv","ColumnValue" => $ParaMeter["AdminPriv"],"ColumnCondition" => $ParaMeter["AdminPrivCondition"]);

                        $ParaMeter["CustomerIdCondition"] = $Filter->six_column_condition;
                        $ParaMeter["CustomerId"] = $Filter->six_column_value;
                        $ParaMeter["CustomerIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["CustomerIdCondition"];
                       
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["CustomerIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        unset($ParaMeter1["TableId"]);
                        $SetupUsersSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "id","ColumnValue" => $ParaMeter["CustomerId"],"ColumnCondition" => $ParaMeter["CustomerIdCondition"]);

                        $ParaMeter1["ERPTableId"] = $ParaMeter["CustomerId"];
                        $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter1);
                        $ParaMeter["CustomerIdValue"] = "";
                        if(!$ERPCustomers->isEmpty())
                        {
                            $ParaMeter["CustomerIdValue"] = $ERPCustomers[0]->full_name;
                        }
                        unset($ParaMeter1["ERPTableId"]);

                        $ParaMeter["SetupUsersSearchArray"] = $SetupUsersSearchArray;
                        if(count($ParaMeter["SetupUsersSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }
                        $GetAllUsers = User::GetAllUsers($ParaMeter);
                        unset($ParaMeter["SetupUsersSearchArray"]);
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ERPManuDataRecord = view('ERP.Setup.Users.SetupUsersList',compact("GetAllUsers","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render(); 
                        $Permission["AppId"] = 7;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                        
                   }
                   else if($Filter->page_name=="ERP Report Of Customer Users")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP Report Of Customer Users";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        unset($ParaMeter["PageName"]);
               
                        $OnlyData = "Report of Customer / Users";

                        $ParaMeter["ReportsReportOfCustomerUsersSearch"] = "ReportsReportOfCustomerUsersSearch";

                        $ReportsReportOfCustomerUsersSearchArray = array();

                        $ParaMeter["CustomerIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["CustomerId"] = $Filter->first_column_value;
                        $ParaMeter["CustomerIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["CustomerIdCondition"];
                       
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["CustomerIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }
                        unset($ParaMeter1["TableId"]);
                        $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "id","ColumnValue" => $ParaMeter["CustomerId"],"ColumnCondition" => $ParaMeter["CustomerIdCondition"]);

                        $ParaMeter1["ERPTableId"] = $ParaMeter["CustomerId"];
                        $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter1);
                        $ParaMeter["CustomerIdValue"] = "";
                        if(!$ERPCustomers->isEmpty())
                        {
                            $ParaMeter["CustomerIdValue"] = $ERPCustomers[0]->full_name;
                        }
                        unset($ParaMeter1["ERPTableId"]);

                        $ParaMeter["LoginCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Login"] = $Filter->second_column_value;
                        $ParaMeter["LoginConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["LoginCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["LoginConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "login","ColumnValue" => $ParaMeter["Login"],"ColumnCondition" => $ParaMeter["LoginCondition"]);


                        $ParaMeter["FullNameCondition"] = $Filter->third_column_condition;
                        $ParaMeter["FullName"] = $Filter->third_column_value;
                        $ParaMeter["FullNameConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["FullNameCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["FullNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "name","ColumnValue" => $ParaMeter["FullName"],"ColumnCondition" => $ParaMeter["FullNameCondition"]);


                        $ParaMeter["EMailCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["EMail"] = $Filter->fourth_column_value;
                        $ParaMeter["EMailConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["EMailCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["EMailConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "email","ColumnValue" => $ParaMeter["EMail"],"ColumnCondition" => $ParaMeter["EMailCondition"]);

                        $ParaMeter["AdminPrivCondition"] = $Filter->fifth_column_condition;
                        $ParaMeter["AdminPriv"] = $Filter->fifth_column_value;
                        $ParaMeter["AdminPrivConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["AdminPrivCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["AdminPrivConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "admin_priv","ColumnValue" => $ParaMeter["AdminPriv"],"ColumnCondition" => $ParaMeter["AdminPrivCondition"]);

                        $ParaMeter["EnabledFlagCondition"] = $Filter->six_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->six_column_value;
                        $ParaMeter["UserIdConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["EnabledFlagCondition"];
                       
                        $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                        if(!$GetAllErpSearchRadioOptions->isEmpty())
                        {
                          $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                        }

                        $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);

                        $ParaMeter["ReportsReportOfCustomerUsersSearchArray"] = $ReportsReportOfCustomerUsersSearchArray;
                        if(count($ParaMeter["ReportsReportOfCustomerUsersSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }
                        $GetAllUsers = User::GetAllUsers($ParaMeter);
                        unset($ParaMeter["ReportsReportOfCustomerUsersSearchArray"]);
                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ERPManuDataRecord = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersList',compact("GetAllUsers","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 17;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                   }
                   else if($Filter->page_name=="ERP Security System Log")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP Security System Log";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                        unset($ParaMeter["PageName"]);
               
                        $OnlyData = "System Log";

                        $ParaMeter["SecuritySystemLogSearch"] = "SecuritySystemLogSearch";

                        $SecuritySystemLogSearchArray = array();

                        $ParaMeter["LoginCondition"] = $Filter->first_column_condition;
                        $ParaMeter["Login"] = $Filter->first_column_value;
                        $ParaMeter["LoginConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["LoginCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["LoginConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "login","ColumnValue" => $ParaMeter["Login"],"ColumnCondition" => $ParaMeter["LoginCondition"]);


                        $ParaMeter["DateCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Date"] = $Filter->second_column_value;
                        $ParaMeter["DateConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["DateCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["DateConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "date_login","ColumnValue" => $ParaMeter["Date"],"ColumnCondition" => $ParaMeter["DateCondition"]);

                        $ParaMeter["SessionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Session"] = $Filter->third_column_value;
                        $ParaMeter["SessionConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["SessionCondition"];
                       
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                            $ParaMeter["SessionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }

                        $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "sc_session","ColumnValue" => $ParaMeter["Session"],"ColumnCondition" => $ParaMeter["SessionCondition"]);

                        $ParaMeter["IpCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["Ip"] = $Filter->fourth_column_value;
                        $ParaMeter["IpConditionValue"] = "";
                        $ParaMeter1["TableId"] = $ParaMeter["IpCondition"];
                        $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                        if(!$GetAllErpSearchTextOptions->isEmpty())
                        {
                          $ParaMeter["IpConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                        }
                        
                        $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "ip","ColumnValue" => $ParaMeter["Ip"],"ColumnCondition" => $ParaMeter["IpCondition"]);
                        $ParaMeter["SecuritySystemLogSearchArray"] = $SecuritySystemLogSearchArray;
                        if(count($ParaMeter["SecuritySystemLogSearchArray"])==0)
                        {
                          $ParaMeter["All"] = "All";
                        }
                        $GetAllUserLoggeds = UserLogged::GetAllUserLoggeds($ParaMeter);
                        unset($ParaMeter["SecuritySystemLogSearchArray"]);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ERPManuDataRecord = view('ERP.Security.SystemLog.SecuritySystemLogList',compact("GetAllUserLoggeds","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                        $Permission["AppId"] = 14;
                        $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
                   }
                   else if($Filter->page_name=="ERP Security Group Users")
                   {
                        $ERPSearchName = "ERP Security Group Users";
                        $ERPFilterGetHtml = $Filter->first_column_condition;
                   }
                   $Message = "$ERPSearchName Search Filter Successfully!";
                   $Status = "success";
                }
            }
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPSearchName Search Filter " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"ERPFilterGetHtml"=>$ERPManuDataRecord,"GetExportPermission"=>$GetExportPermission]);
    }
}