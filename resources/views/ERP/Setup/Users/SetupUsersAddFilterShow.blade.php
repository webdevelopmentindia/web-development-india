<div class="close-btn HideAddFilterModal" ModalId="SetupUsersAddFilterShow">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="SetupUsersUserId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('User ID') }}</label>
	<div class="col-sm-3 p-0">
	    <select id="user_id_condition" name="user_id_condition" class="form-control">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="user_id" type="text" class="form-control" name="user_id"  autofocus placeholder="User ID" value=""> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="user_id_condition" FieldName="user_id" FormName="SetupUsersList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersLogin")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Login') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="login_condition" name="login_condition" class="form-control ERPSearchEmptyContent" FieldId="login">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="login" type="text" class="form-control" name="login"  autofocus placeholder="Login" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SetupUsersList" FieldNameCondition="login_condition" FieldName="login"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersFullName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="full_name_condition" name="full_name_condition" class="form-control ERPSearchEmptyContent" FieldId="full_name">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="full_name" type="text" class="form-control" name="full_name"  autofocus placeholder="Full Name" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="full_name_condition" FieldName="full_name" FormName="SetupUsersList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersEMail")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('E-Mail') }}</label>
    <div class="col-sm-3 p-0">
      <select id="e_mail_condition" name="e_mail_condition" class="form-control ERPSearchEmptyContent" FieldId="e_mail">
          @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="e_mail" type="text" class="form-control" name="e_mail"  autofocus placeholder="E-Mail" value="">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SetupUsersList" FieldNameCondition="e_mail_condition" FieldName="e_mail"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersAdminPriv")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Admin Priv.') }}</label>
    <div class="col-sm-3 p-0">
      <select id="admin_priv_condition" name="admin_priv_condition" class="form-control ERPSearchEmptyContent" FieldId="admin_priv">
          @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="admin_priv" type="text" class="form-control" name="admin_priv"  autofocus placeholder="Admin Priv." value="" maxlength="1">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="SetupUsersList" FieldNameCondition="admin_priv_condition" FieldName="admin_priv"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersCustomerId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
  <div class="col-sm-3 p-0">
      <select id="customer_id_condition" name="customer_id_condition" class="form-control">
          @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <select id="customer_id" name="customer_id" class="form-control">
          @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPAddFilterApply" FieldNameCondition="customer_id_condition" FieldName="customer_id" FormName="SetupUsersList"> Apply  </a></div>
</div>
@endif