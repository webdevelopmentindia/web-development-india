@if(!(isset($OnlyData) && $OnlyData=="Countries"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPFileCountries">
	<div id="FileCountriesList" class="FileCountriesSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>Countries</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
    <?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 3; //3 for ERP Countries
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?> 
	<div class="row row-col">
		<div class="col-xs-12">
			    @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			    <span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileCountriesForm" SectionName="FileCountriesSection"> Add New </a></span>
				@endif
				<span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="FileCountriesSearch" SectionName="FileCountriesSection" FilterChangeDivEmpty="ERPFileCountriesFilterChange" FormClassName="ERPFileCountriesRemoveSearchSubmit"> Search </a></span>
		</div>
	</div>

		<?php 
$CheckParameter = 0;
?>
<div id="FileCountriesListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPFileCountriesRemoveSearchSubmit add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileCountriesList" SectionName="FileCountriesSection" TabName="ERPFileCountries">	
@if(isset($ParaMeter))
    @if(
    ((isset($ParaMeter["CountryId"]) && $ParaMeter["CountryId"]!="") && (isset($ParaMeter["CountryIdCondition"]) && $ParaMeter["CountryIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Code"]) && $ParaMeter["Code"]!="") && (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!="")) || (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]==4))
    ||
    (((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
    )
    <div class="row border-bottom mt-2">
        <div class="col-xs-12">
    @endif   	
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if((isset($ParaMeter["CountryId"]) && $ParaMeter["CountryId"]!="") && (isset($ParaMeter["CountryIdCondition"]) && $ParaMeter["CountryIdCondition"]!=""))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCountryId" class="SearchSection addfield-col">
        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        <div PageName="ERPFileCountries" FilterName="FileCountriesCountryId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CountryIdCondition'] }}" ShowDivClass="ERPFileCountriesFilterChange"
	        FieldNameValue='{{$ParaMeter["CountryId"]}}' FormName="FileCountriesList" AddFilterPopup="FileCountriesAddFilterShow">
		        Country Id: {{ $ParaMeter["CountryIdConditionValue"] }} 
		            {{ $ParaMeter["CountryId"] }} 
            </div>
            <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCountriesList" RemoveSearch="SearchCountryId" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileCountriesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i> </a>
        </span>
	        <input type="hidden" id="country_id" name="country_id" value='{{ $ParaMeter["CountryId"] }}'>
	        <input type="hidden" id="country_id_condition" name="country_id_condition" value='{{ $ParaMeter["CountryIdCondition"] }}'>
	        <div style="display: none" class="ERPFileCountriesFilterChange add-drop-down">
	        </div>
	        
        </div>
   @else
        <?php 
        	$AddFilterOptions.='<li value="FileCountriesCountryId" id="ERPFileCountriesSearchList" name="ERPFileCountriesSearchList" class="ERPAddFilterShow" ShowDivId="FileCountriesAddFilterShow" FilterChangeModal="ERPFileCountriesFilterChange" FormName="FileCountriesList">Country ID</li>';
		?>
   @endif
   @if(((isset($ParaMeter["Code"]) && $ParaMeter["Code"]!="") && (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!="")) || (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCode" class="SearchSection addfield-col">
        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled"> 
        	<div PageName="ERPFileCountries" FilterName="FileCountriesCode" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CodeCondition'] }}" ShowDivClass="ERPFileCountriesFilterChange"
	        FieldNameValue='@if($ParaMeter["CodeCondition"]!=4){{$ParaMeter["Code"]}} @endif' FormName="FileCountriesList" AddFilterPopup="FileCountriesAddFilterShow">
		        Code: {{ $ParaMeter["CodeConditionValue"] }} 
		        @if(isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!=4)
		            {{ $ParaMeter["Code"] }} 
		        @endif
	        </div>
	        <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCountriesList" RemoveSearch="SearchCode" RemoveSearchType="ColumnSearchRemove"><i class="fa fa-times" aria-hidden="true"></i> </a>
	    </span>
	        <input type="hidden" name="code" value='{{ $ParaMeter["Code"] }}'>
	        <input type="hidden" name="code_condition" value='{{ $ParaMeter["CodeCondition"] }}'>
	        <div style="display: none" class="ERPFileCountriesFilterChange add-drop-down">
	        </div>
	        
        </div>
    @else
	    <?php 
	    $AddFilterOptions.='<li value="FileCountriesCode" id="ERPFileCountriesSearchList" name="ERPFileCountriesSearchList" class="ERPAddFilterShow" ShowDivId="FileCountriesAddFilterShow" FilterChangeModal="ERPFileCountriesFilterChange" FormName="FileCountriesList">Code</li>';
		?>
   @endif
   @if(((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchDescription" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	            <div PageName="ERPFileCountries" FilterName="FileCountriesDescription" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['DescriptionCondition'] }}" ShowDivClass="ERPFileCountriesFilterChange"
		        FieldNameValue='@if($ParaMeter["DescriptionCondition"]!=4){{$ParaMeter["Description"]}} @endif' FormName="FileCountriesList" AddFilterPopup="FileCountriesAddFilterShow">
		        	Description: {{ $ParaMeter["DescriptionConditionValue"] }} 
		        	@if(isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!=4)
			            {{ $ParaMeter["Description"] }} 
			        @endif
			    </div>
			    <a href="javascript:" class="btn save-btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileCountriesList" RemoveSearch="SearchDescription" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i> </a>
			</span>
            <input type="hidden" name="description" value='{{ $ParaMeter["Description"] }}'>
            <input type="hidden" name="description_condition" value='{{ $ParaMeter["DescriptionCondition"] }}'>
            <div style="display: none" class="ERPFileCountriesFilterChange add-drop-down">
	        </div>
            
        </div>
    @else
	    <?php 
	    	$AddFilterOptions.='<li value="FileCountriesDescription" id="ERPFileCountriesSearchList" name="ERPFileCountriesSearchList" class="ERPAddFilterShow" ShowDivId="FileCountriesAddFilterShow" FilterChangeModal="ERPFileCountriesFilterChange" FormName="FileCountriesList">Description</li>';
		?>
   @endif
   @if($CheckParameter)

  <div class="addfield-col">
  <!--  <select id="ERPFileCountriesSearchList" name="ERPFileCountriesSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="FileCountriesAddFilterShow">
   	    <option value=''>+ Add Filter</option>
   	    <?php echo $AddFilterOptions; ?>
   </select> -->
   

   @if($AddFilterOptions!="")
	    <div class="dropdown">
		  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
		    <?php echo $AddFilterOptions; ?>
		  </ul>
		</div>
	@endif




   <div id="FileCountriesAddFilterShow" style="display: none" class="add-drop-down popclose">
   </div>
</div>
<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
	        <select id="erp_filter_list" name="erp_filter_list" class="form-control ERPSearchBySaveFilter" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" SectionName="FileCountriesList" ManuName="ERPFileCountries" FormName="">
	   @if(!$GetAllErpSaveFilters->isEmpty())
	       	  <option value=""></option>
	          <option value="">Public</option>
		      @foreach($GetAllErpSaveFilters as $Filter)
		   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
		   	  @endforeach
	   @endif 
	        </select>
	   </div>



	<div class="addfield-col">
   		<button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="FileCountriesList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPFileCountriesFilterChange" HideAddFilterPopup="FileCountriesAddFilterShow">Save Filter</button>
   	</div>
    <div class="col-xs-12 model-d" id="SearchSaveFilter">
    	<div class="src-modal employee-form card">
    		<div class="card-header header-elements-inline head-save">
	            <div class="page-title">
	                <h3>Save Filter</h3>
	            </div>
	            <div class="header-elements">
	                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
	            </div>
	        </div>
	        <div class="card-body">
		        <div class="form-flex form-group">
		               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
		               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileCountriesList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
		        </div>
		        
				  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
		            <div class="col-sm-9">
		               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
		               	@if(!$GetAllErpSaveFilters->isEmpty())
					       <select id="ERPGetOldFilterData" name="ERPGetOldFilterData" class="form-control ERPGetOldFilterData" PageName="ERPFileCountries">
					       	  <option value=""></option>
					          <option value="">Public</option>
						      @foreach($GetAllErpSaveFilters as $Filter)
						   	    <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
						   	  @endforeach
					      </select>
					   @endif
				      </select>
				  	</div>
				  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="FileCountriesList">Delete</button></div>
				  </div>
	       </div>
	    </div>
	</div>

<div class="addfield-col">
   <a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="FileCountriesList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" ><i class="fa fa-times" aria-hidden="true"></i> </a>
</div>
@endif
@if(
    ((isset($ParaMeter["CountryId"]) && $ParaMeter["CountryId"]!="") && (isset($ParaMeter["CountryIdCondition"]) && $ParaMeter["CountryIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Code"]) && $ParaMeter["Code"]!="") && (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!="")) || (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]==4))
    ||
    (((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
    )
    </div>
      </div>
    @endif 
@endif
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPFileCountriestable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>Country ID</th>
			        <th>Code</th>
			        <th>Description</th>
			        <th>Active</th>
			        <th>Created On </th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$ERPCountries->isEmpty())
				    @foreach($ERPCountries as $Country)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Country->erp_table_id }}</td>
			                <td>{{ $Country->country_code }}</td>
			                <td>{{ $Country->description }}</td>
			                <td>
			                	@if($Country->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Country->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td>{{ $Country->created_at }}</td>
			                <td>{{ $Country->created_by }}</td>
			                <td class="not-export-col">
                                @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="FileCountriesForm" SectionName="FileCountriesSection" EditId="{{ $Country->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                	<a href="javascript:" class="btn ERPRecordDelete text-danger" FormName="FileCountriesAdd" DeleteId="{{ $Country->erp_table_id }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
			                	@endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>	
		        	<!-- <th></th> -->	        	
		            <th>Country ID</th>
			        <th>Code</th>
			        <th>Description</th>
			        <th>Active</th>
			        <th>Created On </th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Countries"))
	</div>
	<div id="FileCountriesForm" style="display: none" class="FileCountriesSection card">
	</div>
	<div id="FileCountriesSearch" style="display: none" class="FileCountriesSection card">
	</div>
</div>
@endif