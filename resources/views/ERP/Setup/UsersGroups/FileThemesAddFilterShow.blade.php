@if($ParaMeter["FilterName"]=="FileThemesTheme")
<div class="form-group SearchSection">
    <label class="control-label col-sm-3">{{ __('Theme') }}</label>
    <div class="col-sm-3">
	    <select id="theme_condition" name="theme_condition" class="form-control ERPSearchEmptyContent" FieldId="theme">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}">{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="theme" type="text" class="form-control" name="theme"  autofocus placeholder="Theme" value="">
    </div>
    <a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileThemesList" FieldNameCondition="theme_condition" FieldName="theme"> Apply </a>
</div>
@elseif($ParaMeter["FilterName"]=="FileThemesEnabledFlag")
<div class="form-group">
    <label class="control-label col-sm-3">{{ __('Active') }}</label>
    <div class="col-sm-3">
      <select id="enabled_flag_condition" name="enabled_flag_condition" class="form-control">
        @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                @if($RadioOption->option_name=="Equal")
                    <option value="{{ $RadioOption->id }}">{{ $RadioOption->option_name }}</option>
                @endif
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="enabled_flag" type="radio" class="" name="enabled_flag" value="1" checked>Yes
        <input id="enabled_flag" type="radio" class="" name="enabled_flag" value="0">No
    </div>
    <a href="javascript:" class="btn save-btn ERPAddFilterApply" FormName="FileThemesList" FieldNameCondition="enabled_flag_condition"> Apply </a>
</div>
@endif