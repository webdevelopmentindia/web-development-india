<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpSaveFilter;
use App\ErpSearchRadioOption;
use App\ErpSearchTextOption;
use App\ErpCustomer;
use App\Services\PayUService\Exception;
class ERPFilterGetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPFilterGetAjax(Request $request)
    {
        $Message = "Something Went Wrong Please Try Again!";
        $Status = "error";
        $ERPFilterGetHtml = "";
        $ERPSearchName = "";
        try {
            $ParaMeter = array();
            $ParaMeter["All"] = "All";

            $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter);
            $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter);
            unset($ParaMeter["All"]);

            $ParaMeter["ERPTableId"] = $request->ERPTableId;
            $GetErpSaveFilterDetails = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
            if(!$GetErpSaveFilterDetails->isEmpty())
            { 

                unset($ParaMeter["ERPTableId"]);
                if($GetErpSaveFilterDetails[0]->page_name=="ERP Setup Users" || 
                    $GetErpSaveFilterDetails[0]->page_name=="ERP Report Of Customer Users")
                {
                    $ParaMeter["All"] = "All";
                    $GetAllERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);
                    unset($ParaMeter["All"]);  
                }
                foreach ($GetErpSaveFilterDetails as $Filter) 
                {
                   if($Filter->page_name=="ERP File Applications")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Applications";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["AppNameCondition"] = $Filter->first_column_condition;
                        $ParaMeter["AppName"] = $Filter->first_column_value;
                        $ParaMeter["AppURLCondition"] = $Filter->second_column_condition;
                        $ParaMeter["AppURL"] = $Filter->second_column_value;
                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        $ParaMeter["AppDeployedCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["AppDeployed"] = $Filter->fourth_column_value;
                        
                        $ERPFilterGetHtml = view('ERP.File.Applications.FileApplicationSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   }
                   else if($Filter->page_name=="ERP File Customers")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Customers";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["TaxIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["TaxId"] = $Filter->first_column_value;
                        $ParaMeter["CustomerIdCondition"] = $Filter->second_column_condition;
                        $ParaMeter["CustomerId"] = $Filter->second_column_value;
                        $ParaMeter["CountryIdCondition"] = $Filter->third_column_condition;
                        $ParaMeter["CountryId"] = $Filter->third_column_value;
                        $ParaMeter["FullNameCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["FullName"] = $Filter->fourth_column_value;
                        
                        $ERPFilterGetHtml = view('ERP.File.Customers.FileCustomerSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   }
                   else if($Filter->page_name=="ERP File Countries")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Countries";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["CountryIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["CountryId"] = $Filter->first_column_value;
                        $ParaMeter["CodeCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Code"] = $Filter->second_column_value;
                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        
                        $ERPFilterGetHtml = view('ERP.File.Countries.FileCountriesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   }
                   else if($Filter->page_name=="ERP File Sources")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Sources";
                        
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["SourceIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["SourceId"] = $Filter->first_column_value;
                        $ParaMeter["CodeCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Code"] = $Filter->second_column_value;
                        $ParaMeter["DescriptionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Description"] = $Filter->third_column_value;
                        $ParaMeter["AutoPostCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["AutoPost"] = $Filter->fourth_column_value;
                        $ParaMeter["EnabledFlagCondition"] = $Filter->fifth_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->fifth_column_value;
                        $ERPFilterGetHtml = view('ERP.File.Sources.FileSourcesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   } 
                   else if($Filter->page_name=="ERP File Themes")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Themes";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["ThemeCondition"] = $Filter->first_column_condition;
                        $ParaMeter["Theme"] = $Filter->first_column_value;
                        $ParaMeter["EnabledFlagCondition"] = $Filter->second_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->second_column_value;
                        $ERPFilterGetHtml = view('ERP.File.Themes.FileThemesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   } 
                   else if($Filter->page_name=="ERP File Languages")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP File Languages";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["LanguageCondition"] = $Filter->first_column_condition;
                        $ParaMeter["Language"] = $Filter->first_column_value;
                        $ParaMeter["EnabledFlagCondition"] = $Filter->second_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->second_column_value;
                        $ERPFilterGetHtml = view('ERP.File.Languages.FileLanguagesSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   } 
                   else if($Filter->page_name=="ERP Setup Users")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP Setup Users";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ParaMeter["UserIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["UserId"] = $Filter->first_column_value;

                        $ParaMeter["LoginCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Login"] = $Filter->second_column_value;
                        $ParaMeter["FullNameCondition"] = $Filter->third_column_condition;
                        $ParaMeter["FullName"] = $Filter->third_column_value;

                        $ParaMeter["EMailCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["EMail"] = $Filter->fourth_column_value;
                        $ParaMeter["AdminPrivCondition"] = $Filter->fifth_column_condition;
                        $ParaMeter["AdminPriv"] = $Filter->fifth_column_value;
                        $ParaMeter["CustomerIdCondition"] = $Filter->six_column_condition;
                        $ParaMeter["CustomerId"] = $Filter->six_column_value;
                        
                        $ERPFilterGetHtml = view('ERP.Setup.Users.SetupUsersSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","GetAllERPCustomers"))->render();
                   }
                   else if($Filter->page_name=="ERP Report Of Customer Users")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP Report Of Customer Users";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;

                        $ParaMeter["CustomerIdCondition"] = $Filter->first_column_condition;
                        $ParaMeter["CustomerId"] = $Filter->first_column_value;
                        $ParaMeter["LoginCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Login"] = $Filter->second_column_value;
                        $ParaMeter["FullNameCondition"] = $Filter->third_column_condition;
                        $ParaMeter["FullName"] = $Filter->third_column_value;
                        $ParaMeter["EMailCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["EMail"] = $Filter->fourth_column_value;
                        $ParaMeter["AdminPrivCondition"] = $Filter->fifth_column_condition;
                        $ParaMeter["AdminPriv"] = $Filter->fifth_column_value;
                        $ParaMeter["EnabledFlagCondition"] = $Filter->six_column_condition;
                        $ParaMeter["EnabledFlag"] = $Filter->six_column_value;

                        $ERPFilterGetHtml = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions","GetAllERPCustomers"))->render();
                   }
                   else if($Filter->page_name=="ERP Security System Log")
                   {
                        $ERPSearchName = $ParaMeter["PageName"] = "ERP Security System Log";
                        $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);

                        $ParaMeter["ERPTableId"] = $request->ERPTableId;
                        $ParaMeter["LoginCondition"] = $Filter->first_column_condition;
                        $ParaMeter["Login"] = $Filter->first_column_value;
                        $ParaMeter["DateCondition"] = $Filter->second_column_condition;
                        $ParaMeter["Date"] = $Filter->second_column_value;
                        $ParaMeter["SessionCondition"] = $Filter->third_column_condition;
                        $ParaMeter["Session"] = $Filter->third_column_value;
                        $ParaMeter["IpCondition"] = $Filter->fourth_column_condition;
                        $ParaMeter["Ip"] = $Filter->fourth_column_value;
                        
                        $ERPFilterGetHtml = view('ERP.Security.SystemLog.SecuritySystemLogSearch',compact("ParaMeter","GetAllErpSaveFilters","GetAllErpSearchTextOptions","GetAllErpSearchRadioOptions"))->render();
                   }
                   else if($Filter->page_name=="ERP Security Group Users")
                   {
                        $ERPSearchName = "ERP Security Group Users";
                        $ERPFilterGetHtml = $Filter->first_column_condition;
                   }
                   $Message = "$ERPSearchName Search Filter Successfully!";
                   $Status = "success";
                }
            }
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPSearchName Search Filter " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"ERPFilterGetHtml"=>$ERPFilterGetHtml]);
    }
}