<?php
namespace App\Http\Middleware;
use Closure;
use Auth;
use App\UserGroup;
class UserGroupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ParaMeter["UserId"] = $request->user()->id;
        $GetAllUserGroups = UserGroup::GetAllUserGroups($ParaMeter);
        if($GetAllUserGroups->isEmpty())
        {
            Auth::logout();
            return Redirect()->back()->with('message','You have not been given any permission!');
        }
        else
        {
            Auth::user()->setAttribute('GroupId',$GetAllUserGroups[0]->group_id);
        }
        return $next($request);
    }
}
