@if(!(isset($OnlyData) && $OnlyData=="Sources"))
<div class="ERPTabManuData tab-pane fade in active" id="ERPFileSources">
	<div id="FileSourcesList" class="FileSourcesSection">
@endif
<div class="card">
<div class="card-header header-elements-inline">
		<div class="page-title">
			<h3>Sources</h3>
		</div>
		<div class="header-elements">
			<span><?php
			        echo " ".date("m/d/Y");
		        ?></span>
		</div>
	</div>
	<?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 4; //4 for ERP Sources
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
    ?>
	<div class="row row-col">
		<div class="col-xs-12">
			@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
			<span class="link-src">
				<a href="javascript:" class="btn save-btn ERPFormShow" FormName="FileSourcesForm" SectionName="FileSourcesSection"> Add New </a>
			</span>
			@endif
				<span class="link-src"><a href="javascript:" class="btn save-btn ERPSearchShow" FormName="FileSourcesSearch" SectionName="FileSourcesSection" FilterChangeDivEmpty="ERPFileSourcesFilterChange" FormClassName="ERPFileSourcesRemoveSearchSubmit"> Search </a></span>
		</div>
	</div>


		<?php 
$CheckParameter = 0;
?>
<div id="FileSourcesListSearch">
<form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="ERPFileSourcesRemoveSearchSubmit add-field-listp">
    @csrf
	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileSourcesList" SectionName="FileSourcesSection" TabName="ERPFileSources">	
@if(isset($ParaMeter))
    
    		@if(
    ((isset($ParaMeter["SourceId"]) && $ParaMeter["SourceId"]!="") && (isset($ParaMeter["SourceIdCondition"]) && $ParaMeter["SourceIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Code"]) && $ParaMeter["Code"]!="") && (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!="")) || (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]==4))
    ||
    (((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
    ||
    ((isset($ParaMeter["AutoPost"]) && $ParaMeter["AutoPost"]!="") && (isset($ParaMeter["AutoPostCondition"]) && $ParaMeter["AutoPostCondition"]!=""))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]!=""))
    )
    <div class="row border-bottom mt-2">
    	<div class="col-xs-12">
    @endif
    <?php 
	$AddFilterOptions = "";
	$AddFilterOptionBody ="";
	?>
    @if((isset($ParaMeter["SourceId"]) && $ParaMeter["SourceId"]!="") && (isset($ParaMeter["SourceIdCondition"]) && $ParaMeter["SourceIdCondition"]!=""))
		<?php 
		$CheckParameter = 1;
		?>
        <div id="SearchSourceId" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileSources" FilterName="FileSourcesSourceId" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['SourceIdCondition'] }}" ShowDivClass="ERPFileSourcesFilterChange"
		        FieldNameValue='{{$ParaMeter["SourceId"]}}' FormName="FileSourcesList" AddFilterPopup="FileSourcesAddFilterShow">
			        Source ID: {{ $ParaMeter["SourceIdConditionValue"] }} 
			            {{ $ParaMeter["SourceId"] }} 
	            </div>
	            <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileSourcesList" RemoveSearch="SearchSourceId" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileSourcesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i>  </a>
	        </span>

	        <input type="hidden" id="source_id" name="source_id" value='{{ $ParaMeter["SourceId"] }}'>
	        <input type="hidden" id="source_id_condition" name="source_id_condition" value='{{ $ParaMeter["SourceIdCondition"] }}'>
	        <div style="display: none" class="ERPFileSourcesFilterChange add-drop-down">
	        </div>
	        
        </div>
   @else
        <?php
        	$AddFilterOptions.='<li value="FileSourcesSourceId" id="ERPFileSourcesSearchList" name="ERPFileSourcesSearchList" class="ERPAddFilterShow" ShowDivId="FileSourcesAddFilterShow" FilterChangeModal="ERPFileSourcesFilterChange" FormName="FileSourcesList">Source ID</li>';
		?>
   @endif
   @if(((isset($ParaMeter["Code"]) && $ParaMeter["Code"]!="") && (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!="")) || (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchCode" class="SearchSection addfield-col"> 
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	        	<div PageName="ERPFileSources" FilterName="FileSourcesCode" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['CodeCondition'] }}" ShowDivClass="ERPFileSourcesFilterChange"
		        FieldNameValue='@if($ParaMeter["CodeCondition"]!=4){{$ParaMeter["Code"]}} @endif' FormName="FileSourcesList" AddFilterPopup="FileSourcesAddFilterShow">
			        Code: {{ $ParaMeter["CodeConditionValue"] }} 
			        @if(isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!=4)
			            {{ $ParaMeter["Code"] }} 
			        @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileSourcesList" RemoveSearch="SearchCode" RemoveSearchType="ColumnSearchRemove"> <i class="fa fa-times" aria-hidden="true"></i>  </a>
		    </span>
	        <input type="hidden" name="code" value='{{ $ParaMeter["Code"] }}'>
	        <input type="hidden" name="code_condition" value='{{ $ParaMeter["CodeCondition"] }}'>
	        <div style="display: none" class="ERPFileSourcesFilterChange add-drop-down">
	        </div>
	        
        </div>
    @else
	    <?php
	    	 $AddFilterOptions.='<li value="FileSourcesCode" id="ERPFileSourcesSearchList" name="ERPFileSourcesSearchList" class="ERPAddFilterShow" ShowDivId="FileSourcesAddFilterShow" FilterChangeModal="ERPFileSourcesFilterChange" FormName="FileSourcesList">Code</li>';
		?>
   @endif
   @if(((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
        <?php 
		$CheckParameter = 1;
		?>
        <div id="SearchDescription" class="SearchSection addfield-col">
	        <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
	            <div PageName="ERPFileSources" FilterName="FileSourcesDescription" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['DescriptionCondition'] }}" ShowDivClass="ERPFileSourcesFilterChange"
		        FieldNameValue='@if($ParaMeter["DescriptionCondition"]!=4){{$ParaMeter["Description"]}} @endif' FormName="FileSourcesList" AddFilterPopup="FileSourcesAddFilterShow">
		        	Description: {{ $ParaMeter["DescriptionConditionValue"] }} 
		        	@if(isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!=4)
			            {{ $ParaMeter["Description"] }} 
			        @endif
			    </div>
			    <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileSourcesList" RemoveSearch="SearchDescription" RemoveSearchType="ColumnSearchRemove"><i class="fa fa-times" aria-hidden="true"></i>  </a>
			</span>
            <input type="hidden" name="description" value='{{ $ParaMeter["Description"] }}'>
            <input type="hidden" name="description_condition" value='{{ $ParaMeter["DescriptionCondition"] }}'>
            <div style="display: none" class="ERPFileSourcesFilterChange add-drop-down">
	        </div>
            
        </div>
    @else
	    <?php 
	    $AddFilterOptions.='<li value="FileSourcesDescription" id="ERPFileSourcesSearchList" name="ERPFileSourcesSearchList" class="ERPAddFilterShow" ShowDivId="FileSourcesAddFilterShow" FilterChangeModal="ERPFileSourcesFilterChange" FormName="FileSourcesList">Description</li>';
		?>
   @endif
   @if((isset($ParaMeter["AutoPost"]) && $ParaMeter["AutoPost"]!="") && (isset($ParaMeter["AutoPostCondition"]) && $ParaMeter["AutoPostCondition"]!=""))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchAutoPost" class="SearchSection addfield-col">
		    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileSources" FilterName="FileSourcesAutoPost" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['AutoPostCondition'] }}" ShowDivClass="ERPFileSourcesFilterChange"
		        FieldNameValue='{{$ParaMeter["AutoPost"]}}' FormName="FileSourcesList" AddFilterPopup="FileSourcesAddFilterShow">
		            Auto pos JE? : {{ $ParaMeter["AutoPostConditionValue"] }} 
		                @if($ParaMeter["AutoPost"]==1) {{ 'Yes' }} @else {{ 'No' }} @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileSourcesList" RemoveSearch="SearchAutoPost" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileSourcesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i>  </a>
		    </span>

	        <input type="hidden" id="auto_post" name="auto_post" value='{{ $ParaMeter["AutoPost"] }}'>
	        <input type="hidden" id="auto_post_condition" name="auto_post_condition" value='{{ $ParaMeter["AutoPostCondition"] }}'>
	        <div style="display: none" class="ERPFileSourcesFilterChange add-drop-down">
	        </div>
	        
	    </div>
	@else
	    <?php 
	    	$AddFilterOptions.='<li value="FileSourcesAutoPost" id="ERPFileSourcesSearchList" name="ERPFileSourcesSearchList" class="ERPAddFilterShow" ShowDivId="FileSourcesAddFilterShow" FilterChangeModal="ERPFileSourcesFilterChange" FormName="FileSourcesList">Auto pos JE?</li>';
	    ?>
	@endif
   @if((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]!=""))
	    <?php 
	    $CheckParameter = 1;
	    ?>
	    <div id="SearchEnabledFlag" class="SearchSection addfield-col">
		    <span class="btn save-btn legitRipple rounded-round btn-labeled-right btn-labeled">
		        <div PageName="ERPFileSources" FilterName="FileSourcesEnabledFlag" class="ERPFilterChange" FieldNameConditionValue="{{ $ParaMeter['EnabledFlagCondition'] }}" ShowDivClass="ERPFileSourcesFilterChange"
		        FieldNameValue='{{$ParaMeter["EnabledFlag"]}}' FormName="FileSourcesList" AddFilterPopup="FileSourcesAddFilterShow">
		            Active : {{ $ParaMeter["EnabledFlagConditionValue"] }}
		                @if($ParaMeter["EnabledFlag"]==1) {{ 'Yes' }} @else {{ 'No' }} @endif
		        </div>
		        <a href="javascript:" class="btn ERPRemoveSearchSubmit AllSearchRemove" FormName="FileSourcesList" RemoveSearch="SearchEnabledFlag" RemoveSearchType="ColumnSearchRemove" FilterChangeDivEmpty="ERPFileSourcesFilterChange"> <i class="fa fa-times" aria-hidden="true"></i>  </a>
		    </span>

	        <input type="hidden" id="enabled_flag" name="enabled_flag" value='{{ $ParaMeter["EnabledFlag"] }}'>
	        <input type="hidden" id="enabled_flag_condition" name="enabled_flag_condition" value='{{ $ParaMeter["EnabledFlagCondition"] }}'>
	        <div style="display: none" class="ERPFileSourcesFilterChange add-drop-down">
	        </div>
	        
	    </div>
	@else
	    <?php
	    	$AddFilterOptions.='<li value="FileSourcesEnabledFlag" id="ERPFileSourcesSearchList" name="ERPFileSourcesSearchList" class="ERPAddFilterShow" ShowDivId="FileSourcesAddFilterShow" FilterChangeModal="ERPFileSourcesFilterChange" FormName="FileSourcesList">Active</li>';
	    ?>
	@endif
   @if($CheckParameter)
   	<div class="addfield-col">
	   <!-- <select id="ERPFileSourcesSearchList" name="ERPFileSourcesSearchList" class="form-control ERPAddFilterShow" style="@if($AddFilterOptions=='') {{ 'display:none' }} @endif" ShowDivId="FileSourcesAddFilterShow">
	   	    <option value=''>+ Add Filter</option>
	   	    <?php echo $AddFilterOptions; ?>
	   </select> -->
	   @if($AddFilterOptions!="")
			    <div class="dropdown">
				  <button class="btn save-btn legitRipple rounded-round btn-labeled dropdown-toggle" type="button" data-toggle="dropdown">+ Add Filter
				  <span class="caret"></span></button>
				  <ul class="dropdown-menu">
				    <?php echo $AddFilterOptions; ?>
				  </ul>
				</div>
			@endif
			<div id="FileSourcesAddFilterShow" class="add-drop-down popclose" style="display: none">
		   	
	   		</div>
	</div>


<div class="addfield-col" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
   @if(!$GetAllErpSaveFilters->isEmpty())
       <select id="ERPGetOldFilterData" name="ERPGetOldFilterData" class="form-control ERPSearchBySaveFilter" PageName="ERPFileSources" SectionName="FileSourcesList" ManuName="ERPFileSources" FormName="">
       	  <option value=""></option>
          <option value="">Public</option>
	      @foreach($GetAllErpSaveFilters as $Filter)
	   	    <option value='{{ $Filter->id }}' @if(isset($ParaMeter["ERPTableId"]) && $Filter->id==$ParaMeter["ERPTableId"]) {{ 'selected' }} @endif>{{ $Filter->filter_name }}</option>
	   	  @endforeach
      </select>
   @endif
   <div id="FileSourcesAddFilterShow" style="display: none">
   </div>
</div>





<div class="addfield-col">
	   		<button type="button" class="btn save-btn legitRipple rounded-round btn-labeled SaveFilterModalShow" FormName="FileSourcesList" ModalId="SearchSaveFilter" HideChangeFilterPopup="ERPFileSourcesFilterChange" HideAddFilterPopup="FileSourcesAddFilterShow">Save Filter</button>
	   	</div>
            <div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter</h3>
			            </div>
			            <div class="header-elements">
			                <button type="button" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control" name="filter_name" autofocus="" placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileSourcesList" FormSubmitId="ERPRemoveSearchSubmit">Save</button></div>
				        </div>
				        
						  <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control">
				               	@if(!$GetAllErpSaveFilters->isEmpty())
				                    <option value=""></option>
					               	<option value="">Public</option>
							        @foreach($GetAllErpSaveFilters as $Filter)
							   	        <option value='{{ $Filter->id }}'>{{ $Filter->filter_name }}</option>
							   	    @endforeach
							   	@endif
						      </select>
						  	</div>
						  	<div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" formname="FileSourcesList">Delete</button></div>
						  </div>
			       </div>
			    </div>
			</div>
			<div class="addfield-col">
	   		<a href="javascript:" class="btn ERPRemoveSearchSubmit" FormName="FileSourcesList" RemoveSearch="AllSearchRemove" RemoveSearchType="AllSearchRemove" > <i class="fa fa-times" aria-hidden="true"></i></a>
	   	</div>
   @endif
   @if(
    ((isset($ParaMeter["SourceId"]) && $ParaMeter["SourceId"]!="") && (isset($ParaMeter["SourceIdCondition"]) && $ParaMeter["SourceIdCondition"]!=""))
    ||
    (((isset($ParaMeter["Code"]) && $ParaMeter["Code"]!="") && (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]!="")) || (isset($ParaMeter["CodeCondition"]) && $ParaMeter["CodeCondition"]==4))
    ||
    (((isset($ParaMeter["Description"]) && $ParaMeter["Description"]!="") && (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]!="")) || (isset($ParaMeter["DescriptionCondition"]) && $ParaMeter["DescriptionCondition"]==4))
    ||
    ((isset($ParaMeter["AutoPost"]) && $ParaMeter["AutoPost"]!="") && (isset($ParaMeter["AutoPostCondition"]) && $ParaMeter["AutoPostCondition"]!=""))
    ||
    ((isset($ParaMeter["EnabledFlag"]) && $ParaMeter["EnabledFlag"]!="") && (isset($ParaMeter["EnabledFlagCondition"]) && $ParaMeter["EnabledFlagCondition"]!=""))
    )
    </div>
</div>
    @endif
@endif
</form>
</div>
</div>
<div class="card">
	<div class="table-responsive">
		<table id="ERPFileSourcestable" class="display table border-bottom table--form table-striped" style="width:100%">
			<thead>
				<!-- <tr>
					<th colspan="2">
						<label class="checkbox-t">
							<input type="checkbox" class="checkbox-input">
							<span class="checkmark"></span>
						</label>
					</th>
					<th colspan="8"></th> 
				</tr> -->
			    <tr>
			    	<!-- <th></th> -->
			        <th>Source ID</th>
			        <th>Code</th>
			        <th>Description</th>
			        <th>Auto pos JE?</th>
			        <th>Active</th>
			        <th>Created On </th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
			    </tr>
			</thead>
			<tbody>
				@if(!$ERPSources->isEmpty())
				    @foreach($ERPSources as $Source)
		                <tr>
		                	<!-- <td>
		                		<label class="checkbox-t">
									<input type="checkbox" class="checkbox-input">
									<span class="checkmark"></span>
								</label>
							</td> -->
			                <td>{{ $Source->erp_table_id }}</td>
			                <td>{{ $Source->source_code }}</td>
			                <td>{{ $Source->description }}</td>
			                <td>
			                	@if($Source->auto_post==1)
			                	   {{ 'Yes' }}
			                	@elseif($Source->auto_post==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td>
			                	@if($Source->enabled_flag==1)
			                	   {{ 'Yes' }}
			                	@elseif($Source->enabled_flag==0)
			                	   {{ 'No' }}
			                	@endif
			                </td>
			                <td>{{ $Source->created_at }}</td>
			                <td>{{ $Source->created_by }}</td>
			                <td class="not-export-col">
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_update=="Y")
			                	<a href="javascript:" class="btn text-primary ERPFormShow" FormName="FileSourcesForm" SectionName="FileSourcesSection" EditId="{{ $Source->erp_table_id }}"> <i class="fa fa-pencil" aria-hidden="true"></i> </a>
			                	@endif
			                	@if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_delete=="Y")
			                    <a href="javascript:" class="btn text-danger ERPRecordDelete" FormName="FileSourcesAdd" DeleteId="{{ $Source->erp_table_id }}"> <i class="fa fa-trash" aria-hidden="true"></i>  </a>
			                    @endif
			                </td>
		                </tr>
				    @endforeach
				@endif
			</tbody>
		    <tfoot>
		        <tr>
		        	<!-- <th></th> -->
		            <th>Country ID</th>
			        <th>Code</th>
			        <th>Description</th>
			        <th>Auto pos JE?</th>
			        <th>Active</th>
			        <th>Created On </th>
			        <th>Created By</th>
			        <th class="not-export-col"></th>
		        </tr>
		    </tfoot>
		</table>
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Sources"))
	</div>
	<div id="FileSourcesForm" style="display: none" class="FileSourcesSection card">
	</div>
	<div id="FileSourcesSearch" style="display: none" class="FileSourcesSection card">
	</div>
</div>
@endif