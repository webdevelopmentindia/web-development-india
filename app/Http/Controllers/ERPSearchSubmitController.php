<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ErpApplication;
use App\ErpCustomer;
use App\ErpCountrie;
use App\ErpSource;
use App\ErpTheme;
use App\ErpLanguage;
use App\UserLogged;
use App\ErpSearchTextOption;
use App\ErpSearchRadioOption;
use App\ErpSaveFilter;
use App\User;
use App\Group;
use App\GroupsApp;
use App\Services\PayUService\Exception;
class ERPSearchSubmitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function ERPSearchSubmitAjax(Request $request)
    {
        //die("dfgssdg sdf sdfg");
        $ERPSearchName = "";
        $ERPManuDataRecord = "";
        $GetExportPermission = "";
        try {
            $ParaMeter = array();
            if($request->ERPSearchName=="FileApplicationList")
            {
                $ParaMeter["PageName"] = "ERP File Applications";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $ERPSearchName = $OnlyData = "ERP Applications";

                $ParaMeter["FileApplicationSearch"] = "FileApplicationSearch";

                $FileApplicationSearchArray = array();
                
                if($request->app_deployed!="" && $request->app_deployed_condition>=1)
                {
                    $ParaMeter["AppDeployed"] = $request->app_deployed;
                    $ParaMeter["AppDeployedCondition"] = $request->app_deployed_condition;
                    $ParaMeter["AppDeployedConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->app_deployed_condition;
                    if($request->FilterName=="FileApplicationAppDeployed")
                    {
                      if(($request->app_deployed_change!="" && $request->app_deployed_condition_change>=1))
                      {
                        $ParaMeter["AppDeployed"] = $request->app_deployed_change;
                        $ParaMeter["AppDeployedCondition"] = $request->app_deployed_condition_change;
                        $ParaMeter1["TableId"] = $request->app_deployed_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["AppDeployedConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "app_deployed","ColumnValue" => $ParaMeter["AppDeployed"],"ColumnCondition" => $ParaMeter["AppDeployedCondition"]);
                }
                
                if(($request->app_name!="" && $request->app_name_condition>=1) || ($request->app_name_condition==4))
                {
                   $ParaMeter["AppName"] = $request->app_name;
                   $ParaMeter["AppNameCondition"] = $request->app_name_condition;
                   $ParaMeter["AppNameConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->app_name_condition;
                   if($request->FilterName=="FileApplicationAppName")
                   {
                      if(($request->app_name_change!="" && $request->app_name_condition_change>=1) || ($request->app_name_condition_change==4))
                      {
                         $ParaMeter["AppName"] = $request->app_name_change;
                         $ParaMeter["AppNameCondition"] = $request->app_name_condition_change;
                         $ParaMeter1["TableId"] = $request->app_name_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["AppNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "app_name","ColumnValue" => $ParaMeter["AppName"],"ColumnCondition" => $ParaMeter["AppNameCondition"]);
                }
                
                if(($request->app_url!="" && $request->app_url_condition>=1) || ($request->app_url_condition==4))
                {
                   $ParaMeter["AppURL"] = $request->app_url;
                   $ParaMeter["AppURLCondition"] = $request->app_url_condition;
                   $ParaMeter["AppURLConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->app_url_condition;
                   if($request->FilterName=="FileApplicationAppURL")
                   {
                      if(($request->app_url_change!="" && $request->app_url_condition_change>=1) || ($request->app_url_condition_change==4))
                      {
                         $ParaMeter["AppURL"] = $request->app_url_change;
                         $ParaMeter["AppURLCondition"] = $request->app_url_condition_change;
                         $ParaMeter1["TableId"] = $request->app_url_condition_change;
                      }
                   }
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["AppURLConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "app_url","ColumnValue" => $ParaMeter["AppURL"],"ColumnCondition" => $ParaMeter["AppURLCondition"]);
                }

                if(($request->description!="" && $request->description_condition>=1) || ($request->description_condition==4))
                {
                   $ParaMeter["Description"] = $request->description;
                   $ParaMeter["DescriptionCondition"] = $request->description_condition;
                   $ParaMeter["DescriptionConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->description_condition;
                   if($request->FilterName=="FileApplicationDescription")
                   {
                      if(($request->description_change!="" && $request->description_condition_change>=1) || ($request->description_condition_change==4))
                      {
                        $ParaMeter["Description"] = $request->description_change;
                        $ParaMeter["DescriptionCondition"] = $request->description_condition_change;
                        $ParaMeter1["TableId"] = $request->description_condition_change;
                      }
                   }
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["DescriptionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileApplicationSearchArray[] = array("TableName" => "erp_applications","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);
                }

                $ParaMeter["FileApplicationSearchArray"] = $FileApplicationSearchArray;
                $ERPApplications = ErpApplication::GetAllERPApplications($ParaMeter);
                unset($ParaMeter["FileApplicationSearchArray"]);

                $ERPManuDataRecord = view('ERP.File.Applications.FileApplicationList',compact("ERPApplications","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();

                $Permission["AppId"] = 1;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);

            }
            else if($request->ERPSearchName=="FileCustomerList")
            {
                //die();
                $ParaMeter["PageName"] = "ERP File Customers";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                //echo "<pre>";
                //print_r($GetAllErpSaveFilters);
                //die();
                unset($ParaMeter["PageName"]);
                $ERPSearchName = $OnlyData = "ERP Customers";

                $ParaMeter["FileCustomerSearch"] = "FileCustomerSearch";

                $FileCustomerSearchArray = array();
                
                if(($request->tax_id!="" && $request->tax_id_condition>=1) || ($request->tax_id_condition==4))
                {
                    $ParaMeter["TaxId"] = $request->tax_id;
                    $ParaMeter["TaxIdCondition"] = $request->tax_id_condition;
                    $ParaMeter["TaxIdConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->tax_id_condition;
                    if($request->FilterName=="FileCustomerTaxId")
                    {
                      if(($request->tax_id_change!="" && $request->tax_id_condition_change>=1) || ($request->tax_id_condition_change==4))
                      {
                        $ParaMeter["TaxId"] = $request->tax_id_change;
                        $ParaMeter["TaxIdCondition"] = $request->tax_id_condition_change;
                        $ParaMeter1["TableId"] = $request->tax_id_condition_change;
                      }
                    }
                    $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                    if(!$GetAllErpSearchTextOptions->isEmpty())
                    {
                      $ParaMeter["TaxIdConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                    }
                    
                    $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "tax_id","ColumnValue" => $ParaMeter["TaxId"],"ColumnCondition" => $ParaMeter["TaxIdCondition"]);
                }
                
                if(($request->customer_id!="" && $request->customer_id_condition>=1))
                {
                   $ParaMeter["CustomerId"] = $request->customer_id;
                   $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition;
                   $ParaMeter["CustomerIdConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->customer_id_condition;
                   if($request->FilterName=="FileCustomerCustomerId")
                   {
                      if(($request->customer_id_change!="" && $request->customer_id_condition_change>=1))
                      {
                         $ParaMeter["CustomerId"] = $request->customer_id_change;
                         $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition_change;
                         $ParaMeter1["TableId"] = $request->customer_id_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                   if(!$GetAllErpSearchRadioOptions->isEmpty())
                   {
                      $ParaMeter["CustomerIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                   }

                   $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "id","ColumnValue" => $ParaMeter["CustomerId"],"ColumnCondition" => $ParaMeter["CustomerIdCondition"]);
                }

                if(($request->country_id!="" && $request->country_id_condition>=1))
                {
                   $ParaMeter["CountryId"] = $request->country_id;
                   $ParaMeter["CountryIdCondition"] = $request->country_id_condition;
                   $ParaMeter["CountryIdConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->country_id_condition;
                   if($request->FilterName=="FileCustomerCountryId")
                   {
                      if(($request->country_id_change!="" && $request->country_id_condition_change>=1))
                      {
                         $ParaMeter["CountryId"] = $request->country_id_change;
                         $ParaMeter["CountryIdCondition"] = $request->country_id_condition_change;
                         $ParaMeter1["TableId"] = $request->country_id_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                   if(!$GetAllErpSearchRadioOptions->isEmpty())
                   {
                      $ParaMeter["CountryIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                   }

                   $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "country_id","ColumnValue" => $ParaMeter["CountryId"],"ColumnCondition" => $ParaMeter["CountryIdCondition"]);
                }

                if(($request->full_name!="" && $request->full_name_condition>=1) || ($request->full_name_condition==4))
                {
                   $ParaMeter["FullName"] = $request->full_name;
                   $ParaMeter["FullNameCondition"] = $request->full_name_condition;
                   $ParaMeter["FullNameConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->full_name_condition;
                   if($request->FilterName=="FileCustomerFullName")
                   {
                      if(($request->full_name_change!="" && $request->full_name_condition_change>=1) || ($request->full_name_condition_change==4))
                      {
                        $ParaMeter["FullName"] = $request->full_name_change;
                        $ParaMeter["FullNameCondition"] = $request->full_name_condition_change;
                        $ParaMeter1["TableId"] = $request->full_name_condition_change;
                      }
                   }
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["FullNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileCustomerSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "full_name","ColumnValue" => $ParaMeter["FullName"],"ColumnCondition" => $ParaMeter["FullNameCondition"]);
                }

                $ParaMeter["FileCustomerSearchArray"] = $FileCustomerSearchArray;
                if(count($ParaMeter["FileCustomerSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }
                
                $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter);

                unset($ParaMeter["FileCustomerSearchArray"]);

                $ERPManuDataRecord = view('ERP.File.Customers.FileCustomerList',compact("ERPCustomers","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 2;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="FileCountriesList")
            {
                $ParaMeter["PageName"] = "ERP File Countries";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);

                $ERPSearchName = "ERP Countries";

                $OnlyData = "Countries";

                $ParaMeter["FileCountriesSearch"] = "FileCountriesSearch";

                $FileCountriesSearchArray = array();
                
                if($request->country_id!="" && $request->country_id_condition>=1)
                {
                    $ParaMeter["CountryId"] = $request->country_id;
                    $ParaMeter["CountryIdCondition"] = $request->country_id_condition;
                    $ParaMeter["CountryIdConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->country_id_condition;
                    if($request->FilterName=="FileCountriesCountryId")
                    {
                      if(($request->country_id_change!="" && $request->country_id_condition_change>=1))
                      {
                        //die("hjfgjghf fghdfghfg");
                        $ParaMeter["CountryId"] = $request->country_id_change;
                        $ParaMeter["CountryIdCondition"] = $request->country_id_condition_change;
                        $ParaMeter1["TableId"] = $request->country_id_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["CountryIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileCountriesSearchArray[] = array("TableName" => "erp_countries","ColumnName" => "id","ColumnValue" => $ParaMeter["CountryId"],"ColumnCondition" => $ParaMeter["CountryIdCondition"]);
                }
                
                if(($request->code!="" && $request->code_condition>=1) || ($request->code_condition==4))
                {
                   $ParaMeter["Code"] = $request->code;
                   $ParaMeter["CodeCondition"] = $request->code_condition;
                   $ParaMeter["CodeConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->code_condition;
                   if($request->FilterName=="FileCountriesCode")
                   {
                      if(($request->code_change!="" && $request->code_condition_change>=1) || ($request->code_condition_change==4))
                      {
                         $ParaMeter["Code"] = $request->code_change;
                         $ParaMeter["CodeCondition"] = $request->code_condition_change;
                         $ParaMeter1["TableId"] = $request->code_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["CodeConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileCountriesSearchArray[] = array("TableName" => "erp_countries","ColumnName" => "country_code","ColumnValue" => $ParaMeter["Code"],"ColumnCondition" => $ParaMeter["CodeCondition"]);
                }

                if(($request->description!="" && $request->description_condition>=1) || ($request->description_condition==4))
                {
                   $ParaMeter["Description"] = $request->description;
                   $ParaMeter["DescriptionCondition"] = $request->description_condition;
                   $ParaMeter["DescriptionConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->description_condition;
                   if($request->FilterName=="FileCountriesDescription")
                   {
                      if(($request->description_change!="" && $request->description_condition_change>=1) || ($request->description_condition_change==4))
                      {
                        $ParaMeter["Description"] = $request->description_change;
                        $ParaMeter["DescriptionCondition"] = $request->description_condition_change;
                        $ParaMeter1["TableId"] = $request->description_condition_change;
                      }
                   }
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["DescriptionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileCountriesSearchArray[] = array("TableName" => "erp_countries","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);
                }

                $ParaMeter["FileCountriesSearchArray"] = $FileCountriesSearchArray;
                if(count($ParaMeter["FileCountriesSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }

                $ERPCountries = ErpCountrie::GetAllErpCountries($ParaMeter);
                unset($ParaMeter["FileCountriesSearchArray"]);
                $ERPManuDataRecord = view('ERP.File.Countries.FileCountriesList',compact("ERPCountries","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 3;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="FileSourcesList")
            {
                $ParaMeter["PageName"] = "ERP File Sources";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);

                $ERPSearchName = "ERP Sources";

                $OnlyData = "Sources";

                $ParaMeter["FileSourcesSearch"] = "FileSourcesSearch";

                $FileSourcesSearchArray = array();
                
                if($request->source_id!="" && $request->source_id_condition>=1)
                {
                    $ParaMeter["SourceId"] = $request->source_id;
                    $ParaMeter["SourceIdCondition"] = $request->source_id_condition;
                    $ParaMeter["SourceIdConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->source_id_condition;
                    if($request->FilterName=="FileSourcesSourceId")
                    {
                      if(($request->source_id_change!="" && $request->source_id_condition_change>=1))
                      {
                        //die("hjfgjghf fghdfghfg");
                        $ParaMeter["SourceId"] = $request->source_id_change;
                        $ParaMeter["SourceIdCondition"] = $request->source_id_condition_change;
                        $ParaMeter1["TableId"] = $request->source_id_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["SourceIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "id","ColumnValue" => $ParaMeter["SourceId"],"ColumnCondition" => $ParaMeter["SourceIdCondition"]);
                }
                
                if(($request->code!="" && $request->code_condition>=1) || ($request->code_condition==4))
                {
                   $ParaMeter["Code"] = $request->code;
                   $ParaMeter["CodeCondition"] = $request->code_condition;
                   $ParaMeter["CodeConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->code_condition;
                   if($request->FilterName=="FileSourcesCode")
                   {
                      if(($request->code_change!="" && $request->code_condition_change>=1) || ($request->code_condition_change==4))
                      {
                         $ParaMeter["Code"] = $request->code_change;
                         $ParaMeter["CodeCondition"] = $request->code_condition_change;
                         $ParaMeter1["TableId"] = $request->code_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["CodeConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "source_code","ColumnValue" => $ParaMeter["Code"],"ColumnCondition" => $ParaMeter["CodeCondition"]);
                }

                if(($request->description!="" && $request->description_condition>=1) || ($request->description_condition==4))
                {
                   $ParaMeter["Description"] = $request->description;
                   $ParaMeter["DescriptionCondition"] = $request->description_condition;
                   $ParaMeter["DescriptionConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->description_condition;
                   if($request->FilterName=="FileSourcesDescription")
                   {
                      if(($request->description_change!="" && $request->description_condition_change>=1) || ($request->description_condition_change==4))
                      {
                        $ParaMeter["Description"] = $request->description_change;
                        $ParaMeter["DescriptionCondition"] = $request->description_condition_change;
                        $ParaMeter1["TableId"] = $request->description_condition_change;
                      }
                   }
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["DescriptionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "description","ColumnValue" => $ParaMeter["Description"],"ColumnCondition" => $ParaMeter["DescriptionCondition"]);
                }

                if($request->auto_post!="" && $request->auto_post_condition>=1)
                {
                    $ParaMeter["AutoPost"] = $request->auto_post;
                    $ParaMeter["AutoPostCondition"] = $request->auto_post_condition;
                    $ParaMeter["AutoPostConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->auto_post_condition;
                    if($request->FilterName=="FileSourcesAutoPost")
                    {
                      if(($request->auto_post_change!="" && $request->auto_post_condition_change>=1))
                      {
                        $ParaMeter["AutoPost"] = $request->auto_post_change;
                        $ParaMeter["AutoPostCondition"] = $request->auto_post_condition_change;
                        $ParaMeter1["TableId"] = $request->auto_post_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["AutoPostConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "auto_post","ColumnValue" => $ParaMeter["AutoPost"],"ColumnCondition" => $ParaMeter["AutoPostCondition"]);
                }

                if(count($request->auto_post1)>0 && $request->auto_post_condition>=1)
                {
                    echo "<pre>";
                    print_r($request->auto_post1);
                    echo $ParaMeter["AutoPost"] = array(0,1);
                    die();
                    $ParaMeter["AutoPostCondition"] = $request->auto_post_condition;
                    $ParaMeter["AutoPostConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->auto_post_condition;
                    if($request->FilterName=="FileSourcesAutoPost")
                    {
                      if(($request->auto_post_change!="" && $request->auto_post_condition_change>=1))
                      {
                        $ParaMeter["AutoPost"] = $request->auto_post_change;
                        $ParaMeter["AutoPostCondition"] = $request->auto_post_condition_change;
                        $ParaMeter1["TableId"] = $request->auto_post_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["AutoPostConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "auto_post","ColumnValue" => $ParaMeter["AutoPost"],"ColumnCondition" => $ParaMeter["AutoPostCondition"]);
                }

                if($request->enabled_flag!="" && $request->enabled_flag_condition>=1)
                {
                    $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                    $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;
                    $ParaMeter["EnabledFlagConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->enabled_flag_condition;
                    if($request->FilterName=="FileSourcesEnabledFlag")
                    {
                      if(($request->enabled_flag_change!="" && $request->enabled_flag_condition_change>=1))
                      {
                        $ParaMeter["EnabledFlag"] = $request->enabled_flag_change;
                        $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition_change;
                        $ParaMeter1["TableId"] = $request->enabled_flag_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileSourcesSearchArray[] = array("TableName" => "erp_sources","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);
                }
                $ParaMeter["FileSourcesSearchArray"] = $FileSourcesSearchArray;
                if(count($ParaMeter["FileSourcesSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }

                $ERPSources = ErpSource::GetAllErpSources($ParaMeter);
                unset($ParaMeter["FileSourcesSearchArray"]);
                $ERPManuDataRecord = view('ERP.File.Sources.FileSourcesList',compact("ERPSources","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 4;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="FileThemesList")
            {
                $ParaMeter["PageName"] = "ERP File Themes";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);

                $ERPSearchName = $OnlyData = "ERP Themes";

                $ParaMeter["FileThemesSearch"] = "FileThemesSearch";

                $FileThemesSearchArray = array();
                
                if(($request->theme!="" && $request->theme_condition>=1) || ($request->theme_condition==4))
                {
                   $ParaMeter["Theme"] = $request->theme;
                   $ParaMeter["ThemeCondition"] = $request->theme_condition;
                   $ParaMeter["ThemeConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->theme_condition;
                   if($request->FilterName=="FileThemesTheme")
                   {
                      if(($request->theme_change!="" && $request->theme_condition_change>=1) || ($request->theme_condition_change==4))
                      {
                         $ParaMeter["Theme"] = $request->theme_change;
                         $ParaMeter["ThemeCondition"] = $request->theme_condition_change;
                         $ParaMeter1["TableId"] = $request->theme_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["ThemeConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileThemesSearchArray[] = array("TableName" => "erp_themes_lists","ColumnName" => "theme_name","ColumnValue" => $ParaMeter["Theme"],"ColumnCondition" => $ParaMeter["ThemeCondition"]);
                }

                if($request->enabled_flag!="" && $request->enabled_flag_condition>=1)
                {
                    $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                    $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;
                    $ParaMeter["EnabledFlagConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->enabled_flag_condition;
                    if($request->FilterName=="FileThemesEnabledFlag")
                    {
                      if(($request->enabled_flag_change!="" && $request->enabled_flag_condition_change>=1))
                      {
                        $ParaMeter["EnabledFlag"] = $request->enabled_flag_change;
                        $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition_change;
                        $ParaMeter1["TableId"] = $request->enabled_flag_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileThemesSearchArray[] = array("TableName" => "erp_themes","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);
                }
                $ParaMeter["FileThemesSearchArray"] = $FileThemesSearchArray;
                if(count($ParaMeter["FileThemesSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }

                $ERPThemes = ErpTheme::GetAllErpThemes($ParaMeter);
                unset($ParaMeter["FileThemesSearchArray"]);
                $ERPManuDataRecord = view('ERP.File.Themes.FileThemesList',compact("ERPThemes","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 5;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="FileLanguagesList")
            {
                $ParaMeter["PageName"] = "ERP File Languages";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);

                $ERPSearchName = $OnlyData = "ERP Languages";

                $ParaMeter["FileLanguagesSearch"] = "FileLanguagesSearch";

                $FileLanguagesSearchArray = array();
                
                if(($request->language!="" && $request->language_condition>=1) || ($request->language_condition==4))
                {
                   $ParaMeter["Language"] = $request->language;
                   $ParaMeter["LanguageCondition"] = $request->language_condition;
                   $ParaMeter["LanguageConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->language_condition;
                   if($request->FilterName=="FileLanguagesLanguage")
                   {
                      if(($request->language_change!="" && $request->language_condition_change>=1) || ($request->language_condition_change==4))
                      {
                         $ParaMeter["Language"] = $request->language_change;
                         $ParaMeter["LanguageCondition"] = $request->language_condition_change;
                         $ParaMeter1["TableId"] = $request->language_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["LanguageConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $FileLanguagesSearchArray[] = array("TableName" => "erp_languages_lists","ColumnName" => "language_name","ColumnValue" => $ParaMeter["Language"],"ColumnCondition" => $ParaMeter["LanguageCondition"]);
                }

                if($request->enabled_flag!="" && $request->enabled_flag_condition>=1)
                {
                    $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                    $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;
                    $ParaMeter["EnabledFlagConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->enabled_flag_condition;
                    if($request->FilterName=="FileLanguagesEnabledFlag")
                    {
                      if(($request->enabled_flag_change!="" && $request->enabled_flag_condition_change>=1))
                      {
                        $ParaMeter["EnabledFlag"] = $request->enabled_flag_change;
                        $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition_change;
                        $ParaMeter1["TableId"] = $request->enabled_flag_condition_change;
                      }
                    }
                    $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                    if(!$GetAllErpSearchRadioOptions->isEmpty())
                    {
                      $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                    }
                    
                    $FileLanguagesSearchArray[] = array("TableName" => "erp_languages","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);
                }
                $ParaMeter["FileLanguagesSearchArray"] = $FileLanguagesSearchArray;
                if(count($ParaMeter["FileLanguagesSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }

                $ERPLanguages = ErpLanguage::GetAllErpLanguages($ParaMeter);
                unset($ParaMeter["FileLanguagesSearchArray"]);
                $ERPManuDataRecord = view('ERP.File.Languages.FileLanguagesList',compact("ERPLanguages","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 6;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="SetupUsersList")
            {
                $ParaMeter["PageName"] = $ERPSearchName = "ERP Setup Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
               
                $OnlyData = "ERP Users";

                $ParaMeter["SetupUsersSearch"] = "SetupUsersSearch";

                $SetupUsersSearchArray = array();

                if($request->user_id!="" && $request->user_id_condition>=1)
                { 
                   $ParaMeter["UserId"] = $request->user_id;
                   $ParaMeter["UserIdCondition"] = $request->user_id_condition;
                   $ParaMeter["UserIdConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->user_id_condition;
                   if($request->FilterName=="SetupUsersUserId")
                   {
                      if($request->user_id_change!="" && $request->user_id_condition_change>=1)
                      {
                         $ParaMeter["UserId"] = $request->user_id_change;
                         $ParaMeter["UserIdCondition"] = $request->user_id_condition_change;
                         $ParaMeter1["TableId"] = $request->user_id_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                   if(!$GetAllErpSearchRadioOptions->isEmpty())
                   {
                      $ParaMeter["UserIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                   }

                   $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "id","ColumnValue" => $ParaMeter["UserId"],"ColumnCondition" => $ParaMeter["UserIdCondition"]);
                }
               
                if(($request->login!="" && $request->login_condition>=1) || ($request->login_condition==4))
                { 
                   $ParaMeter["Login"] = $request->login;
                   $ParaMeter["LoginCondition"] = $request->login_condition;
                   $ParaMeter["LoginConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->login_condition;
                   if($request->FilterName=="SetupUsersLogin")
                   {
                      if(($request->login_change!="" && $request->login_condition_change>=1) || ($request->login_condition_change==4))
                      {
                         $ParaMeter["Login"] = $request->login_change;
                         $ParaMeter["LoginCondition"] = $request->login_condition_change;
                         $ParaMeter1["TableId"] = $request->login_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["LoginConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "login","ColumnValue" => $ParaMeter["Login"],"ColumnCondition" => $ParaMeter["LoginCondition"]);
                }

                if(($request->full_name!="" && $request->full_name_condition>=1) || ($request->full_name_condition==4))
                {
                    $ParaMeter["FullName"] = $request->full_name;
                    $ParaMeter["FullNameCondition"] = $request->full_name_condition;
                    $ParaMeter["FullNameConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->full_name_condition;
                    if($request->FilterName=="SetupUsersFullName")
                    {
                      if(($request->full_name_change!="" && $request->full_name_condition_change>=1) || ($request->full_name_condition_change==4))
                      {
                        $ParaMeter["FullName"] = $request->full_name_change;
                        $ParaMeter["FullNameCondition"] = $request->full_name_condition_change;
                        $ParaMeter1["TableId"] = $request->full_name_condition_change;
                      }
                    }
                    $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                    if(!$GetAllErpSearchTextOptions->isEmpty())
                    {
                      $ParaMeter["FullNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                    }
                    
                    $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "name","ColumnValue" => $ParaMeter["FullName"],"ColumnCondition" => $ParaMeter["FullNameCondition"]);
                }
                
                if(($request->e_mail!="" && $request->e_mail_condition>=1) || ($request->e_mail_condition==4))
                {
                   $ParaMeter["EMail"] = $request->e_mail;
                   $ParaMeter["EMailCondition"] = $request->e_mail_condition;
                   $ParaMeter["EMailConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->e_mail_condition;
                   if($request->FilterName=="SetupUsersEMail")
                   {
                      if(($request->e_mail_change!="" && $request->e_mail_condition_change>=1) || ($request->e_mail_condition_change==4))
                      {
                         $ParaMeter["EMail"] = $request->e_mail_change;
                         $ParaMeter["EMailCondition"] = $request->e_mail_condition_change;
                         $ParaMeter1["TableId"] = $request->e_mail_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["EMailConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "email","ColumnValue" => $ParaMeter["EMail"],"ColumnCondition" => $ParaMeter["EMailCondition"]);
                }

                if(($request->admin_priv!="" && $request->admin_priv_condition>=1) || ($request->admin_priv_condition==4))
                {
                    $ParaMeter["AdminPriv"] = $request->admin_priv;
                    $ParaMeter["AdminPrivCondition"] = $request->admin_priv_condition;
                    $ParaMeter["AdminPrivConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->admin_priv_condition;
                    if($request->FilterName=="SetupUsersAdminPriv")
                    {
                      if(($request->admin_priv_change!="" && $request->admin_priv_condition_change>=1) || ($request->admin_priv_condition_change==4))
                      {
                        $ParaMeter["AdminPriv"] = $request->admin_priv_change;
                        $ParaMeter["AdminPrivCondition"] = $request->admin_priv_condition_change;
                        $ParaMeter1["TableId"] = $request->admin_priv_condition_change;
                      }
                    }
                    $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                    if(!$GetAllErpSearchTextOptions->isEmpty())
                    {
                      $ParaMeter["AdminPrivConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                    }
                    
                    $SetupUsersSearchArray[] = array("TableName" => "users","ColumnName" => "admin_priv","ColumnValue" => $ParaMeter["AdminPriv"],"ColumnCondition" => $ParaMeter["AdminPrivCondition"]);
                }
                
                if($request->customer_id!="" && $request->customer_id_condition>=1)
                { 
                   $ParaMeter["CustomerId"] = $request->customer_id;
                   $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition;
                   $ParaMeter["CustomerIdConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->customer_id_condition;
                   if($request->FilterName=="SetupUsersCustomerId")
                   {
                      if($request->customer_id_change!="" && $request->customer_id_condition_change>=1)
                      {
                         $ParaMeter["CustomerId"] = $request->customer_id_change;
                         $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition_change;
                         $ParaMeter1["TableId"] = $request->customer_id_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                   if(!$GetAllErpSearchRadioOptions->isEmpty())
                   {
                      $ParaMeter["CustomerIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                   }
                   $SetupUsersSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "id","ColumnValue" => $ParaMeter["CustomerId"],"ColumnCondition" => $ParaMeter["CustomerIdCondition"]);
                   
                   $ParaMeter1["ERPTableId"] = $ParaMeter["CustomerId"];
                   $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter1);
                   $ParaMeter["CustomerIdValue"] = "";
                   if(!$ERPCustomers->isEmpty())
                   {
                      $ParaMeter["CustomerIdValue"] = $ERPCustomers[0]->full_name;
                   }
                }

                $ParaMeter["SetupUsersSearchArray"] = $SetupUsersSearchArray;
                if(count($ParaMeter["SetupUsersSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }
                $GetAllUsers = User::GetAllUsers($ParaMeter);
                unset($ParaMeter["SetupUsersSearchArray"]);
                
                $ERPManuDataRecord = view('ERP.Setup.Users.SetupUsersList',compact("GetAllUsers","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 7;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="SecuritySystemLogList")
            {
                $ParaMeter["PageName"] = $ERPSearchName = "ERP Security System Log";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
               
                $OnlyData = "System Log";

                $ParaMeter["SecuritySystemLogSearch"] = "SecuritySystemLogSearch";

                $SecuritySystemLogSearchArray = array();
                
                if(($request->login!="" && $request->login_condition>=1) || ($request->login_condition==4))
                { 
                   $ParaMeter["Login"] = $request->login;
                   $ParaMeter["LoginCondition"] = $request->login_condition;
                   $ParaMeter["LoginConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->login_condition;
                   if($request->FilterName=="SecuritySystemLogLogin")
                   {
                      if(($request->login_change!="" && $request->login_condition_change>=1) || ($request->login_condition_change==4))
                      {
                         $ParaMeter["Login"] = $request->login_change;
                         $ParaMeter["LoginCondition"] = $request->login_condition_change;
                         $ParaMeter1["TableId"] = $request->login_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["LoginConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "login","ColumnValue" => $ParaMeter["Login"],"ColumnCondition" => $ParaMeter["LoginCondition"]);
                }

                if(($request->date!="" && $request->date_condition>=1) || ($request->date_condition==4))
                {
                    $ParaMeter["Date"] = $request->date;
                    $ParaMeter["DateCondition"] = $request->date_condition;
                    $ParaMeter["DateConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->date_condition;
                    if($request->FilterName=="SecuritySystemLogDate")
                    {
                      if(($request->date_change!="" && $request->date_condition_change>=1) || ($request->date_condition_change==4))
                      {
                        $ParaMeter["Date"] = $request->date_change;
                        $ParaMeter["DateCondition"] = $request->date_condition_change;
                        $ParaMeter1["TableId"] = $request->date_condition_change;
                      }
                    }
                    $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                    if(!$GetAllErpSearchTextOptions->isEmpty())
                    {
                      $ParaMeter["DateConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                    }
                    
                    $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "date_login","ColumnValue" => $ParaMeter["Date"],"ColumnCondition" => $ParaMeter["DateCondition"]);
                }
                
                if(($request->session!="" && $request->session_condition>=1) || ($request->session_condition==4))
                {
                   $ParaMeter["Session"] = $request->session;
                   $ParaMeter["SessionCondition"] = $request->session_condition;
                   $ParaMeter["SessionConditionValue"] = "";
                   $ParaMeter1["TableId"] = $request->session_condition;
                   if($request->FilterName=="SecuritySystemLogSession")
                   {
                      if(($request->session_change!="" && $request->session_condition_change>=1) || ($request->session_condition_change==4))
                      {
                         $ParaMeter["Session"] = $request->session_change;
                         $ParaMeter["SessionCondition"] = $request->session_condition_change;
                         $ParaMeter1["TableId"] = $request->session_condition_change;
                      }
                   }
                   
                   $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                   if(!$GetAllErpSearchTextOptions->isEmpty())
                   {
                      $ParaMeter["SessionConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                   }

                   $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "sc_session","ColumnValue" => $ParaMeter["Session"],"ColumnCondition" => $ParaMeter["SessionCondition"]);
                }

                if(($request->ip!="" && $request->ip_condition>=1) || ($request->ip_condition==4))
                {
                    $ParaMeter["Ip"] = $request->ip;
                    $ParaMeter["IpCondition"] = $request->ip_condition;
                    $ParaMeter["IpConditionValue"] = "";
                    $ParaMeter1["TableId"] = $request->ip_condition;
                    if($request->FilterName=="SecuritySystemLogIp")
                    {
                      if(($request->ip_change!="" && $request->ip_condition_change>=1) || ($request->ip_condition_change==4))
                      {
                        $ParaMeter["Ip"] = $request->ip_change;
                        $ParaMeter["IpCondition"] = $request->ip_condition_change;
                        $ParaMeter1["TableId"] = $request->ip_condition_change;
                      }
                    }
                    $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                    if(!$GetAllErpSearchTextOptions->isEmpty())
                    {
                      $ParaMeter["IpConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                    }
                    
                    $SecuritySystemLogSearchArray[] = array("TableName" => "user_loggeds","ColumnName" => "ip","ColumnValue" => $ParaMeter["Ip"],"ColumnCondition" => $ParaMeter["IpCondition"]);
                }

                $ParaMeter["SecuritySystemLogSearchArray"] = $SecuritySystemLogSearchArray;
                if(count($ParaMeter["SecuritySystemLogSearchArray"])==0)
                {
                  $ParaMeter["All"] = "All";
                }
                $GetAllUserLoggeds = UserLogged::GetAllUserLoggeds($ParaMeter);
                unset($ParaMeter["SecuritySystemLogSearchArray"]);
                
                $ERPManuDataRecord = view('ERP.Security.SystemLog.SecuritySystemLogList',compact("GetAllUserLoggeds","OnlyData","ParaMeter","GetAllErpSaveFilters"))->render();
                $Permission["AppId"] = 14;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="SecurityGroupUsersList")
            {
                //die("hsdfghsf gsdfgasd");
                $ParaMeter["PageName"] = $ERPSearchName = "ERP Security Group Users";
                $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
                unset($ParaMeter["PageName"]);
                $OnlyData = "Group users";
          
                $ParaMeter1 = array();
                $ParaMeter1["SecurityGroupId"] = $SecuritySearchGroupId = $request->group_id;
                $GetAllGroupUsers = User::GetAllUsers($ParaMeter1);
                
                $ParaMeter = array();
                $ParaMeter["All"] = "All";
                $GetAllGroups = Group::GetAllGroups($ParaMeter);

                $SearchShow = "Hide";
                $ERPManuDataRecord = view('ERP.Security.GroupUsers.SecurityGroupUsersList',compact("GetAllGroupUsers","OnlyData","GetAllErpSaveFilters","SearchShow","GetAllGroups","SecuritySearchGroupId"))->render();
                $Permission["AppId"] = 8;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            else if($request->ERPSearchName=="ReportsReportOfCustomerUsersList")
            {
              $ParaMeter["PageName"] = $ERPSearchName = "ERP Report Of Customer Users";
              $GetAllErpSaveFilters = ErpSaveFilter::GetAllErpSaveFilters($ParaMeter);
              unset($ParaMeter["PageName"]);

              $OnlyData = "Report of Customer / Users";

              $ParaMeter["ReportsReportOfCustomerUsersSearch"] = "ReportsReportOfCustomerUsersSearch";

              $ReportsReportOfCustomerUsersSearchArray = array();

              if($request->enabled_flag!="" && $request->enabled_flag_condition>=1)
              { 
                 $ParaMeter["EnabledFlag"] = $request->enabled_flag;
                 $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition;
                 $ParaMeter["EnabledFlagConditionValue"] = "";
                 $ParaMeter1["TableId"] = $request->enabled_flag_condition;
                 if($request->FilterName=="ReportsReportOfCustomerUsersEnabledFlag")
                 {
                    if($request->enabled_flag_change!="" && $request->enabled_flag_condition_change>=1)
                    {
                       $ParaMeter["EnabledFlag"] = $request->enabled_flag_change;
                       $ParaMeter["EnabledFlagCondition"] = $request->enabled_flag_condition_change;
                       $ParaMeter1["TableId"] = $request->enabled_flag_condition_change;
                    }
                 }
                 
                 $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                 if(!$GetAllErpSearchRadioOptions->isEmpty())
                 {
                    $ParaMeter["EnabledFlagConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                 }

                 $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "enabled_flag","ColumnValue" => $ParaMeter["EnabledFlag"],"ColumnCondition" => $ParaMeter["EnabledFlagCondition"]);
              }

              if(($request->login!="" && $request->login_condition>=1) || ($request->login_condition==4))
              { 
                 $ParaMeter["Login"] = $request->login;
                 $ParaMeter["LoginCondition"] = $request->login_condition;
                 $ParaMeter["LoginConditionValue"] = "";
                 $ParaMeter1["TableId"] = $request->login_condition;
                 if($request->FilterName=="ReportsReportOfCustomerUsersLogin")
                 {
                    if(($request->login_change!="" && $request->login_condition_change>=1) || ($request->login_condition_change==4))
                    {
                       $ParaMeter["Login"] = $request->login_change;
                       $ParaMeter["LoginCondition"] = $request->login_condition_change;
                       $ParaMeter1["TableId"] = $request->login_condition_change;
                    }
                 }
                 
                 $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                 if(!$GetAllErpSearchTextOptions->isEmpty())
                 {
                    $ParaMeter["LoginConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                 }

                 $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "login","ColumnValue" => $ParaMeter["Login"],"ColumnCondition" => $ParaMeter["LoginCondition"]);
              }

              if(($request->full_name!="" && $request->full_name_condition>=1) || ($request->full_name_condition==4))
              {
                  $ParaMeter["FullName"] = $request->full_name;
                  $ParaMeter["FullNameCondition"] = $request->full_name_condition;
                  $ParaMeter["FullNameConditionValue"] = "";
                  $ParaMeter1["TableId"] = $request->full_name_condition;
                  if($request->FilterName=="ReportsReportOfCustomerUsersFullName")
                  {
                    if(($request->full_name_change!="" && $request->full_name_condition_change>=1) || ($request->full_name_condition_change==4))
                    {
                      $ParaMeter["FullName"] = $request->full_name_change;
                      $ParaMeter["FullNameCondition"] = $request->full_name_condition_change;
                      $ParaMeter1["TableId"] = $request->full_name_condition_change;
                    }
                  }
                  $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                  if(!$GetAllErpSearchTextOptions->isEmpty())
                  {
                    $ParaMeter["FullNameConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                  }
                  
                  $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "name","ColumnValue" => $ParaMeter["FullName"],"ColumnCondition" => $ParaMeter["FullNameCondition"]);
              }

              if(($request->e_mail!="" && $request->e_mail_condition>=1) || ($request->e_mail_condition==4))
              {
                 $ParaMeter["EMail"] = $request->e_mail;
                 $ParaMeter["EMailCondition"] = $request->e_mail_condition;
                 $ParaMeter["EMailConditionValue"] = "";
                 $ParaMeter1["TableId"] = $request->e_mail_condition;
                 if($request->FilterName=="ReportsReportOfCustomerUsersEMail")
                 {
                    if(($request->e_mail_change!="" && $request->e_mail_condition_change>=1) || ($request->e_mail_condition_change==4))
                    {
                       $ParaMeter["EMail"] = $request->e_mail_change;
                       $ParaMeter["EMailCondition"] = $request->e_mail_condition_change;
                       $ParaMeter1["TableId"] = $request->e_mail_condition_change;
                    }
                 }
                 
                 $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                 if(!$GetAllErpSearchTextOptions->isEmpty())
                 {
                    $ParaMeter["EMailConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                 }

                 $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "email","ColumnValue" => $ParaMeter["EMail"],"ColumnCondition" => $ParaMeter["EMailCondition"]);
              }

              if(($request->admin_priv!="" && $request->admin_priv_condition>=1) || ($request->admin_priv_condition==4))
              {
                  $ParaMeter["AdminPriv"] = $request->admin_priv;
                  $ParaMeter["AdminPrivCondition"] = $request->admin_priv_condition;
                  $ParaMeter["AdminPrivConditionValue"] = "";
                  $ParaMeter1["TableId"] = $request->admin_priv_condition;
                  if($request->FilterName=="ReportsReportOfCustomerUsersAdminPriv")
                  {
                    if(($request->admin_priv_change!="" && $request->admin_priv_condition_change>=1) || ($request->admin_priv_condition_change==4))
                    {
                      $ParaMeter["AdminPriv"] = $request->admin_priv_change;
                      $ParaMeter["AdminPrivCondition"] = $request->admin_priv_condition_change;
                      $ParaMeter1["TableId"] = $request->admin_priv_condition_change;
                    }
                  }
                  $GetAllErpSearchTextOptions = ErpSearchTextOption::GetAllErpSearchTextOptions($ParaMeter1);
                  if(!$GetAllErpSearchTextOptions->isEmpty())
                  {
                    $ParaMeter["AdminPrivConditionValue"] = $GetAllErpSearchTextOptions[0]->option_name; 
                  }
                  
                  $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "users","ColumnName" => "admin_priv","ColumnValue" => $ParaMeter["AdminPriv"],"ColumnCondition" => $ParaMeter["AdminPrivCondition"]);
              }

              if($request->customer_id!="" && $request->customer_id_condition>=1)
              { 
                 $ParaMeter["CustomerId"] = $request->customer_id;
                 $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition;
                 $ParaMeter["CustomerIdConditionValue"] = "";
                 $ParaMeter1["TableId"] = $request->customer_id_condition;
                 if($request->FilterName=="ReportsReportOfCustomerUsersCustomerId")
                 {
                    if($request->customer_id_change!="" && $request->customer_id_condition_change>=1)
                    {
                       $ParaMeter["CustomerId"] = $request->customer_id_change;
                       $ParaMeter["CustomerIdCondition"] = $request->customer_id_condition_change;
                       $ParaMeter1["TableId"] = $request->customer_id_condition_change;
                    }
                 }
                 
                 $GetAllErpSearchRadioOptions = ErpSearchRadioOption::GetAllErpSearchRadioOptions($ParaMeter1);
                 if(!$GetAllErpSearchRadioOptions->isEmpty())
                 {
                    $ParaMeter["CustomerIdConditionValue"] = $GetAllErpSearchRadioOptions[0]->option_name; 
                 }
                 $ReportsReportOfCustomerUsersSearchArray[] = array("TableName" => "erp_customers","ColumnName" => "id","ColumnValue" => $ParaMeter["CustomerId"],"ColumnCondition" => $ParaMeter["CustomerIdCondition"]);
                 
                 $ParaMeter1["ERPTableId"] = $ParaMeter["CustomerId"];
                 $ERPCustomers = ErpCustomer::GetAllERPCustomers($ParaMeter1);
                 $ParaMeter["CustomerIdValue"] = "";
                 if(!$ERPCustomers->isEmpty())
                 {
                    $ParaMeter["CustomerIdValue"] = $ERPCustomers[0]->full_name;
                 }
              }

              $ParaMeter["ReportsReportOfCustomerUsersSearchArray"] = $ReportsReportOfCustomerUsersSearchArray;
              if(count($ParaMeter["ReportsReportOfCustomerUsersSearchArray"])==0)
              {
                $ParaMeter["All"] = "All";
              }
              $GetAllUsers = User::GetAllUsers($ParaMeter);
              $CountGetAllUsers = count($GetAllUsers);
              unset($ParaMeter["ReportsReportOfCustomerUsersSearchArray"]);

              $ERPManuDataRecord = view('ERP.Reports.ReportOfCustomer.ReportsReportOfCustomerUsersList',compact("GetAllUsers","OnlyData","ParaMeter","GetAllErpSaveFilters","CountGetAllUsers"))->render();
              $Permission["AppId"] = 17;
                $GetExportPermission = GroupsApp::ExportPermissionCheck($Permission);
            }
            $Message = "$ERPSearchName Search Successfully!";
            $Status = "success";
        }
        catch (\Exception $e) {
            $Message = "Error in $ERPSearchName Search " . $e->getMessage();
            $Status = "error";
        }
        return response()->json(["Status"=>$Status,"Message"=>$Message,"GetExportPermission"=>$GetExportPermission,"ERPManuDataRecord"=>$ERPManuDataRecord]);
    }
}