<div class="close-btn HideFilterChangeModal" ModalClass="ERPSetupUsersFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="SetupUsersUserId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('User ID') }}</label>
	  <div class="col-sm-3 p-0">
	    <select id="user_id_condition_changee" name="user_id_condition_change" class="form-control">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="user_id_change" type="number" class="form-control" name="user_id_change"  autofocus placeholder="User ID" value='{{$ParaMeter["FieldNameValue"]}}'> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SetupUsersUserId" FieldNameCondition="user_id_condition_changee" FieldName="user_id_change"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersLogin")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Login') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="login_condition_change" name="login_condition_change" class="form-control ERPSearchEmptyContent" FieldId="login_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="login_change" type="text" class="form-control" name="login_change"  autofocus placeholder="Login" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SetupUsersLogin" FieldNameCondition="login_condition_change" FieldName="login_change"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersFullName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
    <div class="col-sm-3 p-0">
	    <select id="full_name_condition_change" name="full_name_condition_change" class="form-control ERPSearchEmptyContent" FieldId="full_name_change">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="full_name_change" type="text" class="form-control" name="full_name_change"  autofocus placeholder="Full Name" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SetupUsersFullName" FieldNameCondition="full_name_condition_change" FieldName="full_name_change"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersEMail")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('E-Mail') }}</label>
    <div class="col-sm-3 p-0">
      <select id="e_mail_condition_change" name="e_mail_condition_change" class="form-control ERPSearchEmptyContent" FieldId="e_mail_change">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="e_mail_change" type="text" class="form-control" name="e_mail_change"  autofocus placeholder="E-Mail" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SetupUsersEMail" FieldNameCondition="e_mail_condition_change" FieldName="e_mail_change"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersAdminPriv")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Admin Priv.') }}</label>
    <div class="col-sm-3 p-0">
      <select id="admin_priv_condition_change" name="admin_priv_condition_change" class="form-control ERPSearchEmptyContent" FieldId="admin_priv_change">
        @if(!$GetAllErpSearchTextOptions->isEmpty())
               @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="admin_priv_change" type="text" class="form-control" name="admin_priv_change"  autofocus placeholder="Admin Priv." value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif' maxlength="1">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SetupUsersAdminPriv" FieldNameCondition="admin_priv_condition_change" FieldName="admin_priv_change"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="SetupUsersCustomerId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
    <div class="col-sm-3 p-0">
      <select id="customer_id_condition_changee" name="customer_id_condition_change" class="form-control">
          @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6">
        <select id="customer_id_change" name="customer_id_change" class="form-control ERPSearchEmptyContent">
          @if(!$GetAllERPCustomers->isEmpty())
               @foreach($GetAllERPCustomers as $Customer)
                    <option value="{{ $Customer->id }}" @if($Customer->id==$ParaMeter["FieldNameValue"]) {{ 'selected' }} @endif>{{ $Customer->full_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="SetupUsersCustomerId" FieldNameCondition="customer_id_condition_changee" FieldName="customer_id_change"> Apply  </a></div>
</div>
@endif