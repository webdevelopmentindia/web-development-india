@if(!isset($ParaMeter["ERPTableId"]))
		<div class="card-header header-elements-inline">
            <div class="page-title">
                <h3>
                	<?php 
					    echo strtoupper("Search of erp_applications");
					?>
                </h3>
            </div>
            <div class="header-elements">
                <span>
                	<?php 
					    echo " ".date("m/d/Y");
					?>
                </span>
            </div>
        </div> <!-- emloy-hd p-0 -->
<div class="card-body">
	<div class="row">
	    <div class="col-md-12 employee-form">
	    	<form method="POST" aria-label="{{ __('ERPSearchSubmit') }}" enctype="multipart/form-data" id="ERPSearchSubmit" name="ERPSearchSubmit" FormParentDivId="FileApplicationSearch" class="form-src-form">
		    @csrf
			<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="FileApplicationList" SectionName="FileApplicationsSection" TabName="ERPFileApplications">
            <div id="ERPChangeFilter">
@endif			
			<?php 
			    $AppNameCondition = $ParaMeter["AppNameCondition"];
			    $AppName = $ParaMeter["AppName"];
			    $AppURLCondition = $ParaMeter["AppURLCondition"];
			    $AppURL = $ParaMeter["AppURL"];
			    $DescriptionCondition = $ParaMeter["DescriptionCondition"];
			    $Description = $ParaMeter["Description"];
			    $AppDeployedCondition = $ParaMeter["AppDeployedCondition"];
			    $AppDeployed = $ParaMeter["AppDeployed"];
			    $ERPTableId = "";
			    if(isset($ParaMeter["ERPTableId"]))
			    {
			       $ERPTableId = $ParaMeter["ERPTableId"];
			    }
			?>
	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('App name') }}</label>
				<div class="col-sm-3">
				    <select id="app_name_condition" name="app_name_condition" class="form-control ERPSearchEmptyContent" FieldId="app_name">
			        	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($AppNameCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="app_name" type="text" class="form-control{{ $errors->has('app_name') ? ' is-invalid' : '' }} SearchFieldShow" name="app_name"  autofocus placeholder="App Name" value="<?php echo $AppName; ?>" style='@if($AppNameCondition==4) {{ "display: none" }} @endif'>
	                <!-- 4 for Empty Content --> 
	            </div>
	        </div>

	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('App URL') }}</label>
	            <div class="col-sm-3">
				    <select id="app_url_condition" name="app_url_condition" class="form-control ERPSearchEmptyContent" FieldId="app_url">
			        	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($AppURLCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="app_url" type="text" class="form-control{{ $errors->has('app_url') ? ' is-invalid' : '' }} SearchFieldShow" name="app_url"  autofocus placeholder="App URL" value="<?php echo $AppURL; ?>" style='@if($AppURLCondition==4) {{ "display: none" }} @endif'>
	                <!-- 4 for Empty Content --> 
	            </div>
	        </div>

	        <div class="form-group SearchSection form-flex">
	            <label class="control-label col-sm-3">{{ __('Description') }}</label>
	            <div class="col-sm-3">
				    <select id="description_condition" name="description_condition" class="form-control ERPSearchEmptyContent" FieldId="description">
				    	@if(!$GetAllErpSearchTextOptions->isEmpty())
		               	   @foreach($GetAllErpSearchTextOptions as $TextOption)
	                            <option value="{{ $TextOption->id }}" @if($DescriptionCondition==$TextOption->id) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6">
	                <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }} SearchFieldShow" name="description"  autofocus placeholder="Description" value="<?php echo $Description; ?>" style='@if($DescriptionCondition==4) {{ "display: none" }} @endif'>
	                <!-- 4 for Empty Content --> 
	            </div>
	        </div>

	        <div class="form-group form-flex">
	            <label class="control-label col-sm-3">{{ __('App deployed?') }}</label>
	            <div class="col-sm-3">
				    <select id="app_deployed_condition" name="app_deployed_condition" class="form-control">
				    	@if(!$GetAllErpSearchRadioOptions->isEmpty())
		               	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
	                            <option value="{{ $RadioOption->id }}" @if($AppDeployedCondition==$RadioOption->id) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
		               	   @endforeach
		               	@endif
		            </select>
		        </div>
	            <div class="col-sm-6 check-f">
	                <span class="mr-1"><input id="app_deployed" type="radio" class="" name="app_deployed" value="1" @if($AppDeployed==NULL || $AppDeployed==1 ) {{ 'checked' }} @endif>Yes</span>
	                <span class="mr-1"><input id="app_deployed" type="radio" class="" name="app_deployed" value="0" @if($AppDeployed==0 && $AppDeployed!=NULL) {{ 'checked' }} @endif>No</span>
	            </div>
	        </div>
	        <div class="clearfix down-filtr">
		        <div class="erp-src-app form-flex">
	            	<div class="col-flex link-src">
		               <button type="submit" class="btn save-btn">Search</button>
		               <a type="button" onclick="ERPFormReset(this); return false;" class="btn cancel-btn save-btn" >Clear</a>
		           	</div>
		           	<div class="col-flex flex-2 link-src" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="erp_filter_list_div">
		               <select id="erp_filter_list" name="erp_filter_list" class="form-control" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" FormName="FileApplicationSearch">
		               	@if(!$GetAllErpSaveFilters->isEmpty())
		               	   <option value=""></option>
		               	   <option value="">Public</option>
		               	   @foreach($GetAllErpSaveFilters as $Filter)
	                            <option value="{{ $Filter->id }}" @if($ERPTableId==$Filter->id) {{ 'selected' }} @endif >{{ $Filter->filter_name }}</option>
		               	   @endforeach
		               	@endif
			           </select>
			       	</div>
			       	<div class="col-flex link-src">
		               <a href="javascript:" type="button" class="btn save-btn SaveFilterModalShow" FormName="FileApplicationSearch" ModalId="SearchSaveFilter">Save Filter</a>
		               <button type="button" class="btn btn-danger" ShowSection="FileApplicationList" SectionName="FileApplicationsSection" id="ERPBackCancelButton">Back</button>
		           	</div>
		        </div>
		    </div>
@if(!isset($ParaMeter["ERPTableId"]))
            </div>
	        </form>
	    </div>
	    	<div class="col-xs-12 model-d" id="SearchSaveFilter">
	        	<div class="src-modal employee-form card">
	        		<div class="card-header header-elements-inline head-save">
			            <div class="page-title">
			                <h3>Save Filter </h3>
			            </div>
			            <div class="header-elements">
			                <button id="modelhide" class="icon-close SaveFilterModalHide" ModalId="SearchSaveFilter"><i class="fa fa-times" aria-hidden="true"></i></button>
			            </div>
			        </div>
			        <div class="card-body">
				        <div class="form-flex form-group">
				               <div class="col-sm-9"><input id="filter_name" type="text" class="form-control{{ $errors->has('filter_name') ? ' is-invalid' : '' }}" name="filter_name"  autofocus placeholder="Filter Name" value=""></div>
				               <div class="col-sm-3"><button type="button" class="btn save-btn ERPFilterSave" FormName="FileApplicationSearch" FormSubmitId="ERPSearchSubmit">Save</button></div>
				        </div>
				        <div class="form-flex form-group" style="@if($GetAllErpSaveFilters->isEmpty()) 	{{ 'display:none' }}@endif" id="ERPFilterDeleteSection">
				            <div class="col-sm-9">
				               <select id="erp_filter_delete" name="erp_filter_delete" class="form-control" FormName="FileCustomerSearch">
				                    @if(!$GetAllErpSaveFilters->isEmpty())
					               	   <option value=""></option>
					               	   <option value="">Public</option>
					               	   @foreach($GetAllErpSaveFilters as $Filter)
				                            <option value="{{ $Filter->id }}">{{ $Filter->filter_name }}</option>
					               	   @endforeach
					               	@endif
				               </select>
				            </div>
				            <div class="col-sm-3"><button type="button" class="btn btn-danger ERPFilterDelete" FormName="FileApplicationSearch">Delete</button></div>
				        </div>
			       </div>
			    </div>
			</div>
	    </div>
	</div>
</div>
@endif