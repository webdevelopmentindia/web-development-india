<?php
namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class UserLogged extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //'password', 'remember_token',
    ];
    Static Public function GetAllUserLoggeds($ParaMeter)
    {
        $GetAllUserLoggeds = UserLogged::select("user_loggeds.login");
        if(isset($ParaMeter["All"]) && $ParaMeter["All"]=="All")
        {
           $GetAllUserLoggeds =$GetAllUserLoggeds->addselect("user_loggeds.*"); 
        }
        else if((isset($ParaMeter["SecuritySystemLogSearch"]) && $ParaMeter["SecuritySystemLogSearch"]!="") && (isset($ParaMeter["SecuritySystemLogSearchArray"]) && count($ParaMeter["SecuritySystemLogSearchArray"])>0))
        {
            $GetAllUserLoggeds = $GetAllUserLoggeds->addselect("user_loggeds.*");
            $SecuritySystemLogSearch = $ParaMeter["SecuritySystemLogSearchArray"];
            if(count($SecuritySystemLogSearch)>0)
            {
              foreach ($SecuritySystemLogSearch as $Search) 
              {
                $TableName = $Search["TableName"];
                $ColumnName = $Search["ColumnName"];
                $ColumnValue = $Search["ColumnValue"];
                $ColumnCondition = $Search["ColumnCondition"];
                if($ColumnCondition==1) // 1 for Contains 
                {
                  $GetAllUserLoggeds = $GetAllUserLoggeds->where("$TableName.$ColumnName", "LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==2) // 2 for Not Contains
                {
                  $GetAllUserLoggeds = $GetAllUserLoggeds->where("$TableName.$ColumnName", "Not LIKE" ,"%".$ColumnValue."%");
                }
                else if($ColumnCondition==3) //3 for Equal
                {
                  //both table equal id 3
                  $GetAllUserLoggeds = $GetAllUserLoggeds->where("$TableName.$ColumnName",$ColumnValue);
                }
                else if($ColumnCondition==4) // 4 for Not Contains
                {
                  $GetAllUserLoggeds = $GetAllUserLoggeds->whereNull("$TableName.$ColumnName");
                }
              }
            }
        }
        $GetAllUserLoggeds = $GetAllUserLoggeds->orderBy("user_loggeds.date_login","DESC")
                                            ->get();
        return $GetAllUserLoggeds;
    }
}