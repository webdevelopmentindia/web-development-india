<div class="close-btn HideFilterChangeModal" ModalClass="ERPFileCustomerFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileCustomerTaxId")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Tax ID') }}</label>
	  <div class="col-sm-3">
	    <select id="tax_id_condition_changee" name="tax_id_condition_change" class="form-control ERPSearchEmptyContent" FieldId="tax_id_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	@foreach($GetAllErpSearchTextOptions as $TextOption)
              <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	@endforeach
          @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="tax_id_change" type="text" class="form-control" name="tax_id_change"  autofocus placeholder="Tax ID" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCustomerTaxId" FieldNameCondition="tax_id_condition_changee" FieldName="tax_id_change" FormName="FileCustomerList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCustomerCustomerId")
<div class="form-group row form-flex">
    <label class="control-label col-sm-3">{{ __('Customer ID') }}</label>
    <div class="col-sm-3">
	    <select id="customer_id_condition_change" name="customer_id_condition_change" class="form-control" FieldId="customer_id_change">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	   @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="customer_id_change" type="number" class="form-control" name="customer_id_change"  autofocus placeholder="Customer ID" value='{{$ParaMeter["FieldNameValue"]}}'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCustomerCustomerId" FieldNameCondition="customer_id_condition_change" FieldName="customer_id_change" FormName="FileCustomerList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCustomerCountryId")
<div class="form-group  row form-flex">
    <label class="control-label col-sm-3">{{ __('Country ID') }}</label>
    <div class="col-sm-3">
      <select id="country_id_condition_change" name="country_id_condition_change" class="form-control" FieldId="country_id_change">
          @if(!$GetAllErpSearchRadioOptions->isEmpty())
               @foreach($GetAllErpSearchRadioOptions as $RadioOption)
                    <option value="{{ $RadioOption->id }}" @if($RadioOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $RadioOption->option_name }}</option>
               @endforeach
            @endif
        </select>
    </div>
    <div class="col-sm-6 row form-flex">
        <input id="country_id_change" type="number" class="form-control" name="country_id_change"  autofocus placeholder="Country ID" value='{{ $ParaMeter["FieldNameValue"] }}'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCustomerCountryId" FieldNameCondition="country_id_condition_change" FieldName="country_id_change" FormName="FileCustomerList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCustomerFullName")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Full Name') }}</label>
    <div class="col-sm-3">
	    <select id="full_name_condition_change" name="full_name_condition_change" class="form-control ERPSearchEmptyContent" FieldId="full_name_change">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="full_name_change" type="text" class="form-control" name="full_name_change" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif' placeholder="Full Name">
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCustomerFullName" FieldNameCondition="full_name_condition_change" FieldName="full_name_change" FormName="FileCustomerList"> Apply </a></div>
</div>
@endif