<div class="close-btn HideFilterChangeModal" ModalClass="ERPFileCountriesFilterChange">
  <button class="close-addfl" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
</div>
@if($ParaMeter["FilterName"]=="FileCountriesCountryId")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Country ID') }}</label>
	  <div class="col-sm-3">
	    <select id="country_id_condition_change" name="country_id_condition_change" class="form-control ERPSearchEmptyContent" FieldId="country_id_change">
        	@if(!$GetAllErpSearchRadioOptions->isEmpty())
           	@foreach($GetAllErpSearchRadioOptions as $TextOption)
              <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	@endforeach
          @endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="country_id_change" type="number" class="form-control" name="country_id_change"  autofocus placeholder="Country ID" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif'> 
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCountriesCountryId" FieldNameCondition="country_id_condition_change" FieldName="country_id_change" FormName="FileCountriesList"> Apply  </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCountriesCode")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Code') }}</label>
    <div class="col-sm-3">
	    <select id="code_condition_change" name="code_condition_change" class="form-control ERPSearchEmptyContent" FieldId="code_change">
        	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="code_change" type="text" class="form-control" name="code_change"  autofocus placeholder="Code" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCountriesCode" FieldNameCondition="code_condition_change" FieldName="code_change" FormName="FileCountriesList"> Apply </a></div>
</div>
@elseif($ParaMeter["FilterName"]=="FileCountriesDescription")
<div class="form-group SearchSection row form-flex">
    <label class="control-label col-sm-3">{{ __('Description') }}</label>
    <div class="col-sm-3">
	    <select id="description_condition_change" name="description_condition_change" class="form-control ERPSearchEmptyContent" FieldId="description_change">
	    	@if(!$GetAllErpSearchTextOptions->isEmpty())
           	   @foreach($GetAllErpSearchTextOptions as $TextOption)
                    <option value="{{ $TextOption->id }}" @if($TextOption->id==$ParaMeter["FieldNameConditionValue"]) {{ 'selected' }} @endif>{{ $TextOption->option_name }}</option>
           	   @endforeach
           	@endif
        </select>
    </div>
    <div class="col-sm-6">
        <input id="description_change" type="text" class="form-control" name="description_change"  autofocus placeholder="Description" value='@if($ParaMeter["FieldNameConditionValue"]!=4){{$ParaMeter["FieldNameValue"]}}@endif' style='@if($ParaMeter["FieldNameConditionValue"]==4) {{ "display: none" }} @endif'>
    </div>
    <div class="col-xs-12 text-right mt-2"><a href="javascript:" class="btn save-btn ERPFilterChangeApply" FilterName="FileCountriesDescription" FieldNameCondition="description_condition_change" FieldName="description_change"> Apply </a></div>
</div>
@endif