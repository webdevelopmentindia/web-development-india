<style>
.switch {
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  text-align: left;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(20px);
  -ms-transform: translateX(20px);
  transform: translateX(20px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@if(!(isset($OnlyData) && $OnlyData=="Sync Applications"))
<div class="ERPTabManuData card tab-pane fade in active" id="ERPSecuritySyncApplications">
	<div id="SecuritySyncApplicationsList" class="SecuritySyncApplicationsSection">
@endif
<div id="SecuritySyncApplicationsListSearch" class="clearfix">
<div class="card sec-syn">
  <form id="ERPRemoveSearchSubmit" name="ERPRemoveSearchSubmit" class="add-field-listp">
      @csrf
  	<input id="ERPSearchName" type="hidden" name="ERPSearchName" value="SecuritySyncApplicationsList" SectionName="SecuritySyncApplicationsSection" TabName="ERPSecuritySyncApplications">
      <h3 class="syc-title border-bottom">Do you want to synchronize the applications?</h3>
      <div class="switch-box">
        <label class="switch">
    	  <input type="checkbox" value="1" name="auto_post1[]">
    	  <span class="slider round"><i class="fa fa-check" aria-hidden="true"></i></span>
    	 </label> Check if application is published.
      </div>
      <?php 
        $ParaMeter["GroupId"] = Auth::user()->GroupId;
        $ParaMeter["AppId"] = 13; //1 for ERP Sync Applications
        $AccessPermissionCheck = App\GroupsApp::GetAllGroupsApps($ParaMeter);
      ?> 
      @if(!$AccessPermissionCheck->isEmpty() && $AccessPermissionCheck[0]->priv_insert=="Y")
      <div class="switch-done">
        <input type="submit" class="btn save-btn" name="OK" value="OK">
      </div>
      @endif
  </form>
</div>
</div>
<div class="card-body">
	<div class="table-responsive">
	</div>
</div>
@if(!(isset($OnlyData) && $OnlyData=="Sync / Applications"))
	</div>
	<div id="SecuritySyncApplicationsForm" style="display: none" class="SecuritySyncApplicationsSection">
	</div>
	<div id="SecuritySyncApplicationsearch" style="display: none" class="SecuritySyncApplicationsSection">
	</div>
</div>
@endif